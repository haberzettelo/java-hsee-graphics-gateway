#version 430

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

struct GlyphsStruct {
	int offsetX;
	int width;
	int height;
};

layout (std430, binding = 5) buffer Glyphs {
	GlyphsStruct glyph[256];
};

uniform sampler2D sampler;
uniform mat4 mvp;
uniform vec4 textPosition;
uniform vec4 device;

in int character[1];
in int position[1];
out vec2 texCoord;
flat out int out_char;

void main () {
	int actualChar = character[0];
	GlyphsStruct gl = glyph[actualChar];
	ivec2 max = textureSize(sampler, 0);
	float x = gl_in[0].gl_Position.x;//float(position[0]);
	float y = gl_in[0].gl_Position.y;
	
	/*
	 * Space coordinates
	 */
	vec4 P = vec4(x, y, 0, 1);
	
	float screenXRelation = 1/device.z;
	float screenYRelation = 1/device.w;
	vec4 U = vec4(gl.width * screenXRelation/2, 0, 0, 0);
	vec4 V = vec4(0, gl.height * screenYRelation/2, 0, 0);
	
	
	/*
	 * Textures coordinates
	 */
	float X0 = gl.offsetX;
	float Y0 = 0;
	float X1 = X0 + gl.width;
	float Y1 = Y0 + gl.height;
	
	X0 = X0 / max.x;
	Y0 = Y0 / max.y;
	X1 = X1 / max.x;
	Y1 = Y1 / max.y;
	
	out_char = actualChar;
	
	
	texCoord = vec2(X0, Y1);
	gl_Position = P/*-U-V*/;
	EmitVertex();
	
	texCoord = vec2(X1, Y1);
	gl_Position = P+2*U/*-V*/;
	EmitVertex();
	
	texCoord = vec2(X0, Y0);
	gl_Position = P/*-U*/+2*V;
	EmitVertex();
	
	texCoord = vec2(X1, Y0);
	gl_Position = P+2*U+2*V;
	EmitVertex();
	
	EndPrimitive();
}