#version 430
layout(triangles) in;
layout (location = 1) uniform mat4 mvp = mat4(-1.28, 1.54, 0.304, 0.301, 0, 1.02, -0.91, -0.9, 1.28, 1.54, 0.3, 0.3, 0, 0, 2.34, 3.31);
uniform mat4 normalMatrix;
layout(triangle_strip, max_vertices=3) out;
smooth out vec3 N;
out vec3 frag_color;
out vec3 frag_position;
in vec3 N_out[];
in vec3 color[];
in vec4 P_out[];
in vec2 T_out[];
out vec4 P;
out vec2 T;

void main()
{	

  	/*vec3 v1 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz,
  		v2 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
  	normal = normalize(normalMatrix * vec4(cross(v2, v1), 0));*/
  for(int i=0; i<3; i++)
  {
  	frag_color = color[i];
    gl_Position = gl_in[i].gl_Position;
    frag_position = vec3(gl_in[i].gl_Position);
    N = N_out[i];
    P = P_out[i];
    T = T_out[i];
    EmitVertex();
  }
  EndPrimitive();
}  