#version 430
layout (location = 1) uniform mat4 mvp = mat4(-1.28, 1.54, 0.304, 0.301, 0, 1.02, -0.91, -0.9, 1.28, 1.54, 0.3, 0.3, 0, 0, 2.34, 3.31);
layout (location = 2) uniform mat4 normalMatrix = mat4(-1.28, 1.54, 0.304, 0.301, 0, 1.02, -0.91, -0.9, 1.28, 1.54, 0.3, 0.3, 0, 0, 2.34, 3.31);
layout (location = 3) uniform mat4 modelMatrix;
layout (location = 4) uniform mat4 modelViewMatrix;
uniform Hsee_DirectionalLight light;
layout (location = 8) in vec3 in_position;
layout (location = 7) in vec3 in_color;
layout (location = 12) in vec3 in_normal;
layout (location = 15) in vec3 in_texcoord;

out vec3 N;
out vec4 frag_color;
out vec4 P;
out vec2 T;

void main() 
{
	//normal_in = vec4(normalize(transpose(inverse(mat3(modelMatrix))) * in_normal),1);//normalize(normalMatrix * vec4(in_normal, 0));
	N = normalize(transpose(inverse(mat3(modelMatrix))) * in_normal);//mat3(modelMatrix) * in_normal;//normalize(normalMatrix * vec4(in_normal, 0));
	//normal_in = vec4(in_normal, 1);
	frag_color = vec4(in_color, 1) + vec4(0, 0.7, 1, 1);
	gl_Position = mvp * vec4(in_position, 1);
	P = modelMatrix * vec4(in_position, 1);
	T = in_texcoord.xy;
}