package com.hsee.example;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import com.hsee.gl.impl.jogl4.scene.SceneJogl4;
import com.hsee.gl.scene.Loop;
import com.hsee.gl.scene.impl.LoopFps;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

public class TestJogl4 {
	
	public static void main (String[] args) {
		Frame frame = new Frame();
		GLProfile glp = GLProfile.getDefault();
        GLCapabilities caps = new GLCapabilities(glp);
        
        GLCanvas c = new GLCanvas(caps);
        
        SceneJogl4 scene = new SceneJogl4(c);
        
        scene.addSceneListener(new Painter());
        
		frame.add(c);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				super.windowClosing(e);
				System.exit(0);
			}
		});
		
		frame.setSize(640, 480);
		frame.setVisible(true);
		Loop loopFps = new LoopFps();
		loopFps.addScene(scene).startLoop(120);
		
	}
}
