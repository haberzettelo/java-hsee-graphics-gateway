package com.hsee.example;

import java.nio.FloatBuffer;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;
import com.hsee.gl.camera.CameraSetter;
import com.hsee.gl.camera.impl.CameraImpl;
import com.hsee.gl.exception.HseeProgramShaderException;
import com.hsee.gl.exception.HseeShaderException;
import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.impl.jogl4.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.light.DirectionalLight;
import com.hsee.gl.light.PointLight;
import com.hsee.gl.light.SpotLight;
import com.hsee.gl.math.impl.ProjectionImpl;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.scene.SceneListener;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.impl.io.ObjImporter;
import com.hsee.gl.sprite.impl.io.SimpleCompiler;
import com.jogamp.common.nio.Buffers;

public class Painter implements SceneListener {
	private ShaderProgram	program;
	private ShaderVariable<FloatBuffer>	cameraPosition;
	private ModelViewProjection	mvp;
	private DirectionalLight dlight;
	private PointLight plight;
	private SpotLight slight;
	
	private CameraSetter camera;
	private FloatBuffer pos;
	
	private Sprite importedObject;
	private Sprite cube;
	private Sprite plan;
	
	private Vector3 temp = new Vector3(), temp1 = new Vector3(), up = new Vector3(0, 1, 0);
	float angle = 0, speed = 0.01f, x, z, twoPi = 2 * (float)Math.PI;
	
	@Override
	public void reshape(HseeAPI api, GraphicsGateway g, int x, int y, int newWidth, int newHeight) {
		mvp.setProjection(ProjectionImpl.getInstance().perspective(45, newWidth/(float)newHeight, 0.5f, 100));
		mvp.notifyUpdated();
	}
	
	@Override
	public void render(HseeAPI api, GraphicsGateway g, float relativeSpeed) {
		g.clearColor(0, 1, 0, 1);
		g.enable(GraphicsGateway.BLEND);
		g.clear(GraphicsGateway.COLOR_BUFFER_BIT | GraphicsGateway.DEPTH_BUFFER_BIT);
		angle += speed;
		if (angle > twoPi)
			angle -= twoPi;
		g.enable(GraphicsGateway.DEPTH_TEST);
		x = (float)Math.cos(angle) * 20/*temp.getX()*/;
		z = (float)Math.sin(angle) * 20/*temp.getZ()*/;
		temp1.set(x, temp.getY(), z);
		
		g.clear(GraphicsGateway.DEPTH_BUFFER_BIT);
		g.clear(GraphicsGateway.COLOR_BUFFER_BIT);
		cube.translate(0.5f,  0,  0.5f);
		cube.rotate(relativeSpeed*speed/2, 0, 1, 0);
		cube.translate(-0.5f,  0, -0.5f);
		
		plan.draw(program, mvp);
		importedObject.rotate(relativeSpeed*speed, 0, 1, 0);
		//System.out.println(relativeSpeed);
		//importedObject.getChildren().get(1).rotate(speed, 0, 1, 0);
		
		//plight.setPosition(temp1);
		//plight.notifyUpdated();
		//program.addShaderVariableToUpdate(plight);
		
		
		//_camera.setPosition(temp1);
		//pos.rewind();
		//pos.put(temp1.getX()).put(temp1.getY()).put(temp1.getZ());
		//cameraPosition.setValue(pos);
		//cameraPosition.notifyUpdated();
		//_camera.setPosition(temp1);
		//System.out.println(_camera.getPosition());
		//_camera.rotate(speed, 0, 1, 0);
		
		mvp.update(camera);
		importedObject.draw(program, mvp);
		
		cube.draw(program, mvp);
		g.flush();
	}
	
	@Override
	public void initialize(HseeAPI api, GraphicsGateway g) {
		//g = arg0.getGL().getGL4();
		//initialize(g);
		//System.out.println("initialize");
		Shader vertexShader = api.getShaderFactory().newVertexShader();
		Shader fragmentShader = api.getShaderFactory().newFragmentShader();
		Shader geometryShader = api.getShaderFactory().newGeometryShader();
		cameraPosition = api.getShaderVariableFactory().newFloatVec("cameraPosition", 3);
		program = api.getShaderProgramFactory().newProgram();
		
		MaterialSSB.ssb.initShader(fragmentShader);
		
		cube = api.createCube("myCube");
		plan = api.createPlan("myPlan", 20);
		
		Vector3 lightDirection = new Vector3(-1, 0, 1);
		
		dlight = api.createDirectionalLight("light");
		dlight.setAmbiantColor(new Vector3(0, 0, 0));
		dlight.setDiffuseColor(new Vector3(1, 1, 1));
		dlight.setSpecularColor(new Vector3(0.7f, 1, 1));
		dlight.setDirection(lightDirection);
		dlight.initShader(vertexShader);
		dlight.initShader(fragmentShader);
		
		plight = api.createPointLight("pointLight");
		plight.setDiffuseColor(new Vector3(0.8f, 0.8f, 1));
		plight.setSpecularColor(new Vector3(1, 0.7f, 1));
		plight.setPosition(new Vector3(6, 3, 3));
		plight.setAmbiantColor(new Vector3(0.1f, 0.1f, 0.1f));
		plight.setAttenuationConstant(1);
		plight.setAttenuationLinear(0);
		plight.setAttenuationExp(0.3f);
		plight.initShader(vertexShader);
		plight.initShader(fragmentShader);
		
		slight = api.createSpotLight("spotLight");
		slight.setDiffuseColor(new Vector3(1, 0.6f, 0.6f));
		slight.setSpecularColor(new Vector3(1, 1, 0.6f));
		slight.setPosition(new Vector3(0, 4, 7));
		slight.setAmbiantColor(new Vector3(0.1f, 0.1f, 0.1f));
		slight.setAttenuationConstant(1);
		slight.setAttenuationLinear(0.2f);
		slight.setAttenuationExp(0);
		slight.setSpotCutoff(0.2f);
		slight.setSpotExponent(3);
		slight.setConeDirection(new Vector3(0, -0.8f, -1));
		slight.initShader(vertexShader);
		slight.initShader(fragmentShader);
		
		
		//importedObject = new SpriteImporterJogl4(g, new ObjImporter("modeles3d/Mustang gt500kr/"), new SimpleCompiler(), "pyramide", "modeles3d/Mustang gt500kr/", "mustang_gt500kr.obj");
		//importedObject = api.createSprite(new ObjImporter("src/main/resources/modeles3d/Aventador/"), new SimpleCompiler(), "pyramide", "src/main/resources/modeles3d/Aventador/", "Avent.obj");
		//importedObject = new SpriteImporterJogl4(g, new ObjImporter("modeles3d/BMW_M3_GTR/"), new SimpleCompiler(), "pyramide", "modeles3d/BMW_M3_GTR/", "BMW_M3_GTR.obj");
		
		//importedObject = new SpriteImporterJogl4(g, new ObjImporter("modeles3d/Lamborghini/Formats/"), new SimpleCompiler(), "pyramide", "modeles3d/Lamborghini/Formats/", "Reventon.obj");
		importedObject = api.createSprite(new ObjImporter("src/main/resources/modeles3d/ec725/FullMeshLOD0/"), new SimpleCompiler(), "pyramide", "src/main/resources/modeles3d/ec725/FullMeshLOD0/", "ec725.obj");
		//importedObject = new SpriteImporterJogl4(g, new ObjImporter("modeles3d/Aston Martin DBR9/"), new SimpleCompiler(), "pyramide", "modeles3d/Aston Martin DBR9/", "DBR9.obj");
		//importedObject = new SpriteImporterJogl4(g, new ObjImporter("modeles3d/"), new SimpleCompiler(), "pyramide", "modeles3d/", "hyundai_scion_TC_2010_RS_6_(26-06-2013)_v14(triangles).obj");
		importedObject.translate(0, 2, 0);
		//importedObject.scale(0.04f);
		//importedObject.scale(0.001f);
		importedObject.scale(1.5f);
		//cube.
		try
		{
			vertexShader.addFileName("src/main/resources/shaders/vertex.glsl");
			fragmentShader.addFileName("src/main/resources/shaders/fragmentSimple.glsl");
			//geometryShader.addFileName("shaders/geometry.glsl");
			
			vertexShader.compileShader();
			fragmentShader.compileShader();
			//geometryShader.compileShader();
			
			
			program.attachShader(vertexShader);
			program.attachShader(fragmentShader);
			//program.attachShader(geometryShader);
			program.link();
			
			importedObject.initRenderer(program);
			cube.initRenderer(program);
			plan.initRenderer(program);
		}
		catch (HseeShaderException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (HseeProgramShaderException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//f.notifyUpdated();
		//f.setValue(13f);
		mvp = api.createModelViewProjection();
		
		mvp.setProjection(ProjectionImpl.getInstance().perspective(45, 4/3f, 0.5f, 100000));
		camera = new CameraImpl();
		Vector3 eye = new Vector3(), position = new Vector3(10, 7, 10), lookPoint = new Vector3(0, 0, 0);
		pos = Buffers.newDirectFloatBuffer(3);
		lookPoint.sub(position, eye);
		pos.rewind();
		pos.put(eye.getX()).put(eye.getY()).put(eye.getZ());
		pos.rewind();
		cameraPosition.setValue(pos);
		camera.setPosition(new Vector3(position));
		temp = camera.getPosition();
		camera.setLookPoint(new Vector3(lookPoint));
		camera.setUp(new Vector3(0, 1, 0));
		mvp.setView(camera);
		
		mvp.getValue();
		try
		{
			mvp.addShaderProgram(program);
			dlight.updateHalfVector(camera);
			
			program.addShaderVariableToUpdate(dlight);
			program.addShaderVariableToUpdate(plight);
			cameraPosition.addShaderProgram(program);
			program.addShaderVariableToUpdate(slight);
			//System.out.println(g.glGetUniformLocation(program.getProgramId(), "light.direction"));
			//program.useProgram();
		}
		catch (HseeShaderStructureException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//cube.scale(1.5f);
		//plan.translate(0, -4, 0);
		cube.translate(0, 3, 0);
		g.blendFunc(GraphicsGateway.SRC_ALPHA, GraphicsGateway.ONE_MINUS_SRC_ALPHA);
	}
}
