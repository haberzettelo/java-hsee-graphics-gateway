package com.hsee.gl.text;

import java.awt.Font;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Text compiler interface allow you to compile a Font to be used into 
 * the desired implementation of graphical context
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 27 avr. 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface TextCompiler {
	
	/**
	 * Compile the desired font into file and store the serialized object in the same directory.<br>
	 * No control on the existence will be done, all potentially existing files will be overwritten.
	 * @param font Font to be compile into files
	 * @param antiAliased True if anti aliasing must be activated, false otherwise
	 */
	public void compileFont(Font font, boolean antiAliased);
}
