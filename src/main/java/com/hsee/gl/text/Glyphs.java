package com.hsee.gl.text;

import java.io.Serializable;

public class Glyphs implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	public int width[], height[], offsetX[], ascii[];
	
	public Glyphs(int length)
	{
		this.ascii = new int[length];
		this.height = new int[length];
		this.offsetX = new int[length];
		this.width = new int[length];
	}
	
	public void addGlyph(int id, int width, int height, int offsetX, int ascii)
	{
		System.out.println("offsetX:" + offsetX + ", ascii:" + ascii);
		this.ascii[id] = ascii;
		this.height[id] = height;
		this.offsetX[id] = offsetX;
		this.width[id] = width;
	}
	
	public String toString () {
		String result = "";
		result += "length:" + ascii.length + "\n";
		for(int i = 0, max = ascii.length; i < max; i++) {
			result += "	char:	" + (char)ascii[i];
			result += "	height:	" + height[i];
			result += "	width:	" + width[i];
			result += "	offset:	" + offsetX[i] + "\n";
		}
		return result;
	}
}