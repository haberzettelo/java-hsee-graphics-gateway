package com.hsee.gl.text.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.font.LineMetrics;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.hsee.gl.text.TextCompiler;
/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Compiler for text, no monospaced font, compile the desired font
 * into file with one character by column
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Construct the text compiler with contructor with Font ant antialiased mode, 
 * then use simply compileFont to store the desired font into texture (default, png format used)
 * and a serialised object that contain offset and size info of each
 * character.
 * By default, the compiled files will bo stored in ressources/text/compiled, you can change
 * location and format by using the statics methods of TextIO, setPath() and setFormat().
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 27 avr. 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class TextCompilerImpl extends TextIO implements TextCompiler
{
	public static TextCompiler compiler = new TextCompilerImpl();
	private TextCompilerImpl() {
	}

	@Override
	/**
	 * Compile the desired font into file and store the serialized object in the same directory.<br>
	 * No control on the existence will be done, all potentially existing files will be overwritten.
	 */
	public void compileFont (Font font, boolean aa) {
		init(font, aa);
		FontRenderContext frc = new FontRenderContext(new AffineTransform(), _antialiased, true);
		
		Object mode = null;
		if (_antialiased)
			mode = RenderingHints.VALUE_ANTIALIAS_ON;
		else
			mode = RenderingHints.VALUE_ANTIALIAS_OFF;
		//GlyphVector v;// = _font.layoutGlyphVector(frc, text.toCharArray(), 0, text.length()-1, Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT);
		
		BufferedImage tex = new BufferedImage(1, 1, BufferedImage.TYPE_BYTE_GRAY);
		Graphics2D g = tex.createGraphics();
		g.setFont(_font);
		String allFont = "";
		for(int i = 0; i < _asciiLength; i++)
			allFont += (char)(i);
		
		GlyphVector gv = _font.createGlyphVector(g.getFontRenderContext(), allFont);
		int offset = 0, maxY = 0;;
		AffineTransform at;
		int widthWhitespace = (int)g.getFontMetrics().getStringBounds(" ", g).getWidth();
		for(int i = 0; i < _asciiLength; i++) {
			//Shape shape = gv.getGlyphOutline(i);
			_glyphFont.addGlyph(i, i == 32?widthWhitespace:(int)gv.getGlyphVisualBounds(i).getBounds().getWidth(), 
					maxY/*shape.getBounds().height*/, 
					offset + (i == 32?0:(int)gv.getGlyphMetrics(i).getLSB()), 
					i);
			at = AffineTransform.getTranslateInstance(offset, 0);
			//g.draw(at.createTransformedShape(shape));
			if (maxY < gv.getVisualBounds().getHeight())
				maxY = (int) gv.getVisualBounds().getHeight();
			offset  += gv.getGlyphLogicalBounds(i).getBounds().getWidth();
		}
		
		BufferedImage resized = new BufferedImage(_glyphFont.offsetX[_asciiLength-1] + _glyphFont.width[_asciiLength-1], maxY, BufferedImage.TYPE_4BYTE_ABGR);
		g = resized.createGraphics();
		g.setFont(_font);
		//g.drawGlyphVector(gv, 0, 0);
		LineMetrics lm = null;
		frc = g.getFontRenderContext();
		g.setColor(Color.BLACK);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, mode);
		for(int i = 0; i < _asciiLength; i++) {
			Shape shape = gv.getGlyphOutline(i);
			lm = _font.getLineMetrics(new String(new char[]{allFont.toCharArray()[i]}), frc);
			at = AffineTransform.getTranslateInstance(0, lm.getAscent()-1);
			g.fill(at.createTransformedShape(shape));
		}
		
		
		//File file = new File(getFileName());
		try {
			//boolean ok = ImageIO.write(resized, _compiledFormat, file);
			//Desktop.getDesktop().open(file);
			FileOutputStream fo = new FileOutputStream(getFileNameWithoutExtension() + "." + _serializedFormat);
			ObjectOutputStream out = new ObjectOutputStream(fo);
			out.writeObject(_glyphFont);
			out.close();
			fo.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
