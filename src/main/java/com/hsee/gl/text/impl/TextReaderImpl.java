package com.hsee.gl.text.impl;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import com.hsee.gl.text.Glyphs;
import com.hsee.gl.text.TextReader;
import com.jogamp.common.nio.Buffers;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Text reader allow you to read a compiled font from texture and read a Glyphs
 * serialized object to get the texture read in ByteBuffer and a Glyphs object.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Construct TextReader by using constructor and use storeFont() method to store 
 * the compiled font into this object (in RAM), then you can read the stored values 
 * by using the two methods, getTexture() and getGlyphs().
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 27 avr. 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class TextReaderImpl extends TextIO implements TextReader
{
	ByteBuffer _texture;
	Glyphs _glyphs;
	int _width, _height;
	public TextReaderImpl(Font font, boolean aa)
	{
		super(font, aa);
		_texture = null;
		_glyphs = null;
	}
	
	@Override
	/**
	 * Store the desired font in this object
	 */
	public void storeFont() {
		try {
			BufferedImage image = ImageIO.read(new File(getFileName()));
			
			_width = image.getWidth();
			_height = image.getHeight();
			_texture = Buffers.newDirectByteBuffer(_width * _height * 4);
			int[] pixels = new int[_width * _height];
			image.getRGB(0, 0, _width, _height, pixels, 0, _width);
			for (int y = 0; y < _height; y++)
				for(int x = 0; x < _width; x++) {
					int pixel = pixels[y * _width + x];
					_texture.put((byte) ((pixel >> 16) & 0xFF));
					_texture.put((byte) ((pixel >> 8) & 0xFF));
					_texture.put((byte) ((pixel) & 0xFF));
					_texture.put((byte) ((pixel >> 24) & 0xFF));
				}
			_texture.rewind();
			FileInputStream fi = new FileInputStream(getFileNameWithoutExtension() + "." + _serializedFormat);
			ObjectInputStream in = new ObjectInputStream(fi);
			_glyphs = (Glyphs)in.readObject();
			in.close();
			fi.close();
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	/**
	 * Return true if the font is already compiled
	 * @return 
	 */
	public boolean fontCompiled () {
		if (!new File(_compiledPath).exists()) {
			new File(_compiledPath).mkdirs();
			return false;
		}
		if (new File(getFileName()).exists())
			return new File(getFileNameWithoutExtension() + "." + _serializedFormat).exists();
		return false;
	}
	
	@Override
	public int getTextureWidth () {
		return _width;
	}
	
	@Override
	public int getTextureHeight () {
		return _height;
	}
	
	@Override
	/**
	 * Get the texture of the compiled font into ByteBuffer
	 * @return ByteBuffer contain the desired texture (font)
	 */
	public ByteBuffer getTexture () {
		return _texture;
	}
	
	@Override
	/**
	 * Get the Glyphs object that contain properties of each character of the font into texture.
	 * @return Glyphs object
	 */
	public Glyphs getGlyphs () {
		return _glyphs;
	}

	@Override
	public Font getFont () {
		return _font;
	}
}
