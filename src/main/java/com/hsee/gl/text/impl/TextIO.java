package com.hsee.gl.text.impl;

import java.awt.Font;

import com.hsee.gl.text.Glyphs;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Define all info to the children which want to use text
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 27 avr. 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public abstract class TextIO
{
	static String _path = "resources/fonts/", _compiledPath = _path + "compiled/", _compiledFormat = "png", _serializedFormat = "ser";
	static int _asciiLength = 256;
	
	Glyphs _glyphFont;
	
	/**
	 * Set the path of TextRenderer, default is "resources/text/"
	 * @param newPath
	 */
	public static void setPath (String newPath) {
		_path = newPath;
	}
	
	public static void setFormat(String format) {
		_compiledFormat = format;
	}
	
	protected Font _font;
	protected boolean _antialiased;
	
	/**
	 * Contruct TextIO without font
	 */
	public TextIO () {
	}
	
	protected void init(Font font, boolean antialiased) {
		_font = font;
		_antialiased = antialiased;
		
		_glyphFont = new Glyphs(_asciiLength);	
	}
	
	/**
	 * Construct TextIO from font and antialiased mode
	 * @param font
	 * @param antialiased
	 */
	public TextIO(Font font, boolean antialiased){
		init(font, antialiased);
	}
	
	/**
	 * Get the filename of the file without extension
	 * @param font
	 * @return
	 */
	protected String getFileNameWithoutExtension () {
		return _compiledPath + _font.getName() + "_s" + _font.getSize() + "_t" + _font.getStyle() + (_antialiased?"aa":"na");
	}
	
	/**
	 * Get the filename of the file with the compiled extension 
	 * @param font
	 * @return
	 */
	protected String getFileName () {
		return getFileNameWithoutExtension() + "." + _compiledFormat;
	}
}
