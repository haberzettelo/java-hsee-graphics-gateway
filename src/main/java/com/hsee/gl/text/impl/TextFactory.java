package com.hsee.gl.text.impl;

import java.awt.Font;

public class TextFactory
{
	/**
	 * Get the factory 
	 */
	public static TextFactory factory = new TextFactory();
	private TextFactory() {
	}
	
	/**
	 * Store font texture and all of this properties into TextRender to render your font as you want
	 * @param font
	 * @param antialiased
	 * @return
	 */
	public TextReaderImpl newTextRender (Font font, boolean antialiased) {
		TextReaderImpl reader = new TextReaderImpl(font, antialiased);
		if (!reader.fontCompiled())
			TextCompilerImpl.compiler.compileFont(font, antialiased);
		reader.storeFont();
			
		return reader;
	}
}
