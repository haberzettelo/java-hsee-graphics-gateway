package com.hsee.gl.text;

import java.awt.Font;
import java.nio.ByteBuffer;

public interface TextReader
{
	/**
	 * Return true if the specified font is already compiled, false else.
	 * @param font Font to test
	 * @return True if already compiled, False otherwise
	 */
	public boolean fontCompiled();
	
	/**
	 * Return the font
	 * @return Font
	 */
	public Font getFont();
	
	/**
	 * Store the desired font in this object.
	 */
	public void storeFont();
	
	/**
	 * Return the width of the read texture.
	 * @return
	 */
	public int getTextureWidth();
	
	/**
	 * Return the height of the read texture.
	 * @return
	 */
	public int getTextureHeight();
	
	/**
	 * Return the texture data into a flow of byte.
	 * @return The compiled texture read.
	 */
	public ByteBuffer getTexture ();
	
	/**
	 * Get the Glyphs object that contain properties of each character of the font into texture.
	 * @return Glyphs object
	 */
	public Glyphs getGlyphs();
}
