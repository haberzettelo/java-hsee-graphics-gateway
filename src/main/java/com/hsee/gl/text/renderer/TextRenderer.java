package com.hsee.gl.text.renderer;

import com.hsee.gl.sprite.Drawable;
import com.hsee.gl.sprite.Model;
import com.hsee.gl.text.TextReader;

public interface TextRenderer<GraphicalContext> extends Model, Drawable
{
	/**
	 * Load the specified font into the graphical context. Max character parameter
	 * allow you to define the maximum character that will be displayed. This value
	 * define the memory size allocated to render your text.
	 * @param g Graphical context
	 * @param reader Text reader that contain a read font
	 * @param maxCharacter Maximum character to display
	 */
	public void loadFont (GraphicalContext g, TextReader reader, int maxCharacter);
	
	/**
	 * Set the text to be displayed. Cannot be most large than parameter macCharacter specified
	 * with loadFont() method.
	 * @param text Text to be displayed
	 */
	public void setText(String text);
	
	
}
