package com.hsee.gl.animation;

import com.hsee.gl.math.MatrixTransform;
import com.hsee.gl.math.interpolation.Interpolation;
import com.hsee.gl.math.primitive.FloatValuable;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.math.quaternion.Quaternion;
import com.hsee.gl.scene.RenderListener;

public interface Animation
{
	public void setRenderListener(RenderListener listener);
	
	public void start(int delay);
	public void start();
	
	/**
	 * Set the number of loop, set to 0 imply no animation, set to < 0 (ex -1) imply infinite loop, other case it's the number of loop. <br>
	 * Be Caution ! : If infinite loop is setted, all following animation tailed will be discard
	 * @param loop
	 * @return Chaining
	 */
	public Animation setIterate(int loop);
	
	/**
	 * Add an animation to the end of all previous animations tailed
	 * @param followingAnimation Animation will be start after the end of all previous animation tailed and this animation.<br>
	 * At the end of the animation list, the specified delay will be respected and the specified animation will be stated.
	 * @param delay Delay before starting the specified animation
	 * @return Chaining
	 */
	public Animation addTail(long delay, Animation followingAnimation);
	
	public void translate(MatrixTransform mat, Interpolation<Vector3> displacments, long time, Interpolation<FloatValuable> inter);
	public void rotate(MatrixTransform mat, Interpolation<Quaternion> rotations, long time, Interpolation<FloatValuable> inter);
	public void scale(MatrixTransform mat, Interpolation<Vector3> displacment, long time, Interpolation<FloatValuable> inter);
}
