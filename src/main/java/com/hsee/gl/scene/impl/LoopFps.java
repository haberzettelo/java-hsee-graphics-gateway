package com.hsee.gl.scene.impl;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.scene.Loop;
import com.hsee.gl.scene.Scene;

public class LoopFps implements Loop {
	protected float fps = 60;
	protected List<Scene> scenes = new ArrayList<Scene>(1);
	
	@Override
	public Loop startLoop() {
		return startLoop(this.fps);
	}

	@Override
	public Loop startLoop(float fps) {
		for (Scene scene : scenes)
			startScene(scene, fps);
		return this;
	}
	
	protected void startScene(final Scene scene, final float fps) {
		new Thread(new Runnable() {
			double effectiveFps, effectivePeriod, desiredPeriod;
			@Override
			public void run() {
				GraphicsGateway g = scene.getGraphicsGateway();
				//initialize(graphicsGateway);
				// In second
				
				// In s-1
				effectiveFps = fps;
				
				// In s
				desiredPeriod = 1/fps;
				while (true) {
					effectivePeriod = System.nanoTime() / 1000000000f;
					//render(graphicsGateway, fpsSpeed / effectiveFps);
					scene.display((float)(fps / effectiveFps));
					scene.waitEndRender();
					effectivePeriod = System.nanoTime() / 1000000000f - effectivePeriod;
					if (effectivePeriod < desiredPeriod) {
						try {
							Thread.yield();
							Thread.sleep( (long) ((desiredPeriod - effectivePeriod)*1000f) );
							effectivePeriod = desiredPeriod;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					effectiveFps = 1/effectivePeriod;
				}
			}
		}).start();;
	}

	@Override
	public Loop addScene(Scene scene) {
		this.scenes.add(scene);
		return this;
	}

	@Override
	public Loop removeScene(Scene scene) {
		this.scenes.remove(scene);
		return this;
	}

	@Override
	public List<Scene> getSceneList() {
		return this.scenes;
	}

	@Override
	public Loop setFps(float fps) {
		this.fps = fps;
		return this;
	}

	@Override
	public float getFps() {
		return this.fps;
	}

}
