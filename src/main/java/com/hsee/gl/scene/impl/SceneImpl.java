package com.hsee.gl.scene.impl;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;
import com.hsee.gl.scene.RenderListener;
import com.hsee.gl.scene.Scene;
import com.hsee.gl.scene.SceneListener;

public abstract class SceneImpl implements Scene {
	protected class Reshaped {
		public int width, height;
		public boolean reshaped = false;
	}
	protected Reshaped reshape = new Reshaped();
	protected GraphicsGateway graphicsGateway;
	protected HseeAPI api;
	public SceneImpl(GraphicsGateway graphics) {
		graphicsGateway = graphics;
	}
	
	public SceneImpl(HseeAPI api) {
		this(api.getGraphicsGateway());
		this.api = api;
	}
	
	/*@Override
	public Runnable getRunnable(Render render) {
		return getRunnable(60, render);
	}*/
	
	protected List<SceneListener> listeners = new ArrayList<SceneListener>(1);
	
	@Override
	public void addSceneListener(SceneListener listener) {
		listeners.add(listener);
	}
	@Override
	public void removeSceneListener(SceneListener listener) {
		listeners.remove(listener);
	}
	
	@Override
	public void initialize() {
		for (SceneListener sceneListener : listeners) {
			sceneListener.initialize(this.api, this.graphicsGateway);
		}
		//this.initialize(this.graphicsGateway);
	}

	/*@Override
	public Runnable getRunnable(float fpsSpeed, Render render) {
		return new Runnable() {
			double effectiveFps, effectivePeriod, desiredPeriod;
			@Override
			public void run() {
				//initialize(graphicsGateway);
				// In second
				
				// In s-1
				effectiveFps = fpsSpeed;
				
				// In s
				desiredPeriod = 1/fpsSpeed;
				while (true) {
					effectivePeriod = System.nanoTime() / 1000000000f;
					//render(graphicsGateway, fpsSpeed / effectiveFps);
					render.render(graphicsGateway, (float)(fpsSpeed / effectiveFps));
					render.waitEndRender();
					effectivePeriod = System.nanoTime() / 1000000000f - effectivePeriod;
					if (effectivePeriod < desiredPeriod) {
						try {
							Thread.yield();
							Thread.sleep( (long) ((desiredPeriod - effectivePeriod)*1000f) );
							effectivePeriod = desiredPeriod;
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					effectiveFps = 1/effectivePeriod;
				}
			}
		};
	}*/

	@Override
	public void addRenderListener(RenderListener listener) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeRenderListener(RenderListener listener) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public HseeAPI getApi() {
		return api;
	}
	
	@Override
	public GraphicsGateway getGraphicsGateway() {
		return graphicsGateway;
	}
}
