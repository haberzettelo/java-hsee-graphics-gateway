package com.hsee.gl.scene;

import java.util.List;


public interface Loop {
	Loop startLoop();
	Loop startLoop(float fps);
	Loop addScene(Scene scene);
	Loop removeScene(Scene scene);
	List<Scene> getSceneList();
	Loop setFps(float fps);
	float getFps();
}
