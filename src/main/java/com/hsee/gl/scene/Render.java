package com.hsee.gl.scene;

import com.hsee.gl.GraphicsGateway;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Render interface represent an element that can be rendered.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 2 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Render {
	/**
	 * Render loop called at each spin of loop if loop is defined, the speed 
	 * was configured by start method.
	 * If no loop is defined, relative speed will be equals to 0 at display call
	 * 
	 * @param context
	 *            Graphical context depended of your implementation
	 * @param Speed
	 *            relative to the render time. If you set the refresh time at
	 *            60fps and if the device is able to draw 60 frames/second,
	 *            relative speed will be equal to 1.0f and this method will be
	 *            called 60 times/second. If the device isn't able to display 60
	 *            frames/second, speed will be equal to (60/realFPS). By
	 *            example, for 30 effectives frames/second against 60 asked,
	 *            relative speed will be 2.0f, to compensate the less frames. To
	 *            generalize, speed is equal to (WantedFPS / RealFPS).
	 */
	//public void render(GraphicsGateway graphics, float relativeSpeed);
	
	/**
	 * Call the render method with the previous context
	 */
	public void display();
	public void display(float relativeSpeed);
	
	/**
	 * Block the current thread until the end of render method
	 */
	public void waitEndRender();
	
	public void addSceneListener(SceneListener listener);
	public void removeSceneListener(SceneListener listener);
}
