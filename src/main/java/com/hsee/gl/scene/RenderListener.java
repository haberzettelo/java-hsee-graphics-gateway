package com.hsee.gl.scene;

public interface RenderListener
{
	public void notifyRender();
}
