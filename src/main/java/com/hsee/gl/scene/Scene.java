package com.hsee.gl.scene;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Scene interface represent your main loop with your main calls. initialize() method
 * was called one time before the render loop was launched. At the end of initialize() 
 * method, the main loop renderer begin and was slowed when the loop is too fast.
 * The speed of the loop is customizable in start() method.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Use start method to begin render loop. You can specify your render speed with this 
 * method, speed unit is the number of frames per second, 60 by default.
 * You must create your scene by creating class extended by an implementation provided
 * and override initialize() and render() method.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 2 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Scene extends Render {
	/**
	 * Start the render loop in new thread with speed by default setted at 60
	 * fps
	 */
	//public Runnable getRunnable(Render render);

	/**
	 * Start the render loop in new thread with your custom speed
	 * 
	 * @param fpsSpeed
	 *            Speed in frames per second
	 */
	//public Runnable getRunnable(float fpsSpeed, Render render);
	
	//public void start();


	/**
	 * Initialize the Api by the correct implementation, like make the new thread to main thread
	 */
	//public void initByApi();
	
	/**
	 * Call this method to execute the initialization manually.
	 */
	public void initialize();
	
	/**
	 * Method called only one time before the render.
	 */
	//public void initialize(GraphicsGateway graphics);



	/**
	 * Method called when windows is reshaping
	 * 
	 * @param context
	 * @param newWidth
	 * @param newHeight
	 */
	//public void reshape(int x, int y, int newWidth, int newHeight);

	/**
	 * Add a RenderListener
	 * 
	 * @param listener
	 */
	public void addRenderListener(RenderListener listener);

	/**
	 * Remove a RenderListener
	 * 
	 * @param listener
	 */
	public void removeRenderListener(RenderListener listener);
	
	/**
	 * Return the Hsee context used in this scene
	 * @return
	 */
	public HseeAPI getApi();
	
	/**
	 * Return the graphics gateway used in this scene
	 * @return
	 */
	public GraphicsGateway getGraphicsGateway();
}
