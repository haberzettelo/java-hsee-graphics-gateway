package com.hsee.gl.scene;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;

public interface SceneListener {
	/**
	 * Method called only one time before the render.
	 */
	public void initialize(HseeAPI api, GraphicsGateway graphics);
	
	/**
	 * Method called when windows is reshaping
	 * 
	 * @param context
	 * @param newWidth
	 * @param newHeight
	 */
	public void reshape(HseeAPI api, GraphicsGateway graphics, int x, int y, int newWidth, int newHeight);
	
	/**
	 * Render loop called at each spin of loop if loop is defined, the speed 
	 * was configured by start method.
	 * If no loop is defined, relative speed will be equals to 0 at display call
	 * 
	 * @param context
	 *            Graphical context depended of your implementation
	 * @param Speed
	 *            relative to the render time. If you set the refresh time at
	 *            60fps and if the device is able to draw 60 frames/second,
	 *            relative speed will be equal to 1.0f and this method will be
	 *            called 60 times/second. If the device isn't able to display 60
	 *            frames/second, speed will be equal to (60/realFPS). By
	 *            example, for 30 effectives frames/second against 60 asked,
	 *            relative speed will be 2.0f, to compensate the less frames. To
	 *            generalize, speed is equal to (WantedFPS / RealFPS).
	 */
	public void render(HseeAPI api, GraphicsGateway graphics, float relativeSpeed);
}
