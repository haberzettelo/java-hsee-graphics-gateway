package com.hsee.gl;

public interface GraphicsGateway {
	public static int COLOR_BUFFER_BIT = 0x4000, 
			DEPTH_BUFFER_BIT = 0x100, 
			DEPTH_TEST = 0xb71,
			BLEND = 0xbe2,
			ZERO = 0,
			SRC_COLOR = 0x300,
			ONE_MINUS_SRC_COLOR = 0x301,
			DST_COLOR = 0x306,
			ONE_MINUS_DST_COLOR = 0x307,
			SRC_ALPHA = 0x302,
			DST_ALPHA = 0x304,
			ONE_MINUS_DST_ALPHA = 0x305,
			CONSTANT_COLOR = 0x8001,
			ONE_MINUS_CONSTANT_COLOR = 0x8002,
			CONSTANT_ALPHA = 0x8003,
			ONE_MINUS_CONSTANT_ALPHA = 0x8004,
			ONE = 0x1,
			ONE_MINUS_SRC_ALPHA = 0x303;
	
	//TODO add all utilities statics numbers
	
	/**
	 * Clear the total part of screen delegate by the parent with the specified color.
	 * @param red Red value, from 0 to 1
	 * @param green Green value from 0 to 1
	 * @param blue Blue, from 0 to 1
	 * @param alpha Alpha, from 0 (fully transparent) to 1 (fully opaque)
	 * @return Chaining
	 */
	GraphicsGateway clearColor(float red, float green, float blue, float alpha);
	
	/**
	 * Clear buffers of the specified targets
	 * @param target One or more targets to clear
	 * @return Chaining
	 */
	GraphicsGateway clear(int target);
	
	/**
	 * Enable or disable server-side capabilities
	 * @param targets One or more targets
	 * @return Chaining
	 */
	GraphicsGateway enable(int targets);
	
	/**
	 * Force execution of GL commands in finite time
	 * @return Chaining
	 */
	GraphicsGateway flush();
	
	/**
	 * Pixels can be drawn using a function that blends the incoming (source) RGBA values with the RGBA 
	 * values that are already in the frame buffer (the destination values). Blending is initially disabled. 
	 * Use glEnable and glDisable with argument GL_BLEND to enable and disable blending. 
	 * @param sourceFactor Specifies how the red, green, blue, and alpha source blending factors are computed. The initial value is ONE. 
	 * @param destinationFactor Specifies how the red, green, blue, and alpha destination blending factors are computed. The following symbolic
	 *  constants are accepted: ZERO, ONE, SRC_COLOR, ONE_MINUS_SRC_COLOR, DST_COLOR, ONE_MINUS_DST_COLOR, SRC_ALPHA, 
	 *  ONE_MINUS_SRC_ALPHA, DST_ALPHA, ONE_MINUS_DST_ALPHA. CONSTANT_COLOR, ONE_MINUS_CONSTANT_COLOR, CONSTANT_ALPHA, 
	 *  and ONE_MINUS_CONSTANT_ALPHA. The initial value is ZERO.
	 */
	GraphicsGateway blendFunc(int sourceFactor, int destinationFactor);
}
