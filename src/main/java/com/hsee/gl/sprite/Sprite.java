package com.hsee.gl.sprite;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public interface Sprite<GraphicalContext> extends Model, Drawable {
	public List<Sprite<GraphicalContext>> getChildren();
	public Sprite<GraphicalContext> getParent();
	public String getName();
	public void updateModel(Matrix newModel);
	public void setRenderer (SpriteRenderer<GraphicalContext> renderer);
	public DrawWay getDrawWay();
	
	/**
	 * Initialize the renderer
	 * @param program Shader program used to draw
	 */
	public void initRenderer(ShaderProgram program);
	
	/**
	 * Tell if at each child, index of vertices continue or not. By example<br>
	 * Consider two children, each represent a quad with triangles drawing way :
	 * <pre>Sprite 1:
	 * 	Position [0,0,0,  1,0,0,  1,1,0,  0,1,0]
	 * 	Index [0,1,2,  1,2,3]
	 * 
	 * Sprite 2: (same as 1 but translated by 1 on z axis)
	 * 	Position [0,0,1,  1,0,1,  1,1,1,  0,1,1]
	 * 	If this method return false, Index will be :
	 * 		Index [0,1,2,  1,2,3] (same as 1 but duplicated)
	 * 	If this method return true, Index will be :
	 * 		Index [4,5,6,  5,6,7] (continuation of index)</pre>
	 * @return True if index don't reset at each Sprite, else false
	 */
	public boolean indexContinue();
	
	public class ObjectFaces {
		public IntBuffer faces;
		public Material material;
		public ObjectFaces (IntBuffer faces, Material material) {
			this.faces = faces; this.material = material;
		}
	}
	/**
	 * Get the indexes of the vertex. Each child of parent must have the index
	 * @return
	 */
	public List<ObjectFaces> 	getVertexIndexes();
	public FloatBuffer 	getVertexPosition();
	public FloatBuffer 	getVertexNormal();
	public FloatBuffer 	getVertexTextureCoordinates();
	public FloatBuffer 	getVertexColor();
}
