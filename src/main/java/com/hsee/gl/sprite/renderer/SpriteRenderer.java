package com.hsee.gl.sprite.renderer;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.sprite.Sprite;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * This interface allow you to control the rendering of the sprite or any object
 * that support this interface.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * You must add your sprites with addSprite() method to add your sprite in the 
 * drawing, when all object was added, call init() method to make the minimal
 * call as possible when you call draw() method.
 * If you update any data on one or more sprite, use updateSprite() method. You
 * don't need to call init() after this call.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 30 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface SpriteRenderer<GraphicalContext>
{
	/**
	 * Add the sprite to the compressed rendering
	 * @param sprite
	 */
	public void addSprite(Sprite<GraphicalContext> sprite);
	/**
	 * Add the sprite and all these children and the children of the childs ..., to the compressed rendering
	 * @param sprite
	 */
	public void addSpriteIncludingChildren(Sprite<GraphicalContext> sprite);
	
	/**
	 * Update the specified sprite previously added by addSprite() method
	 * @param sprite
	 */
	public void updateSprite(Sprite<GraphicalContext> sprite);
	
	/**
	 * Update the specified sprite and all these children previously added by addSprite() method
	 * @param sprite
	 */
	public void updateSpriteIncludingChildren(Sprite<GraphicalContext> sprite);
	
	/**
	 * Remove the specified sprite
	 * @param sprite
	 */
	public void removeSprite(Sprite<GraphicalContext> sprite);
	/**
	 * The the specified sprite and all its children
	 * @param sprite
	 */
	public void removeSpriteIncludingChildren(Sprite<GraphicalContext> sprite);
	
	/**
	 * Compress the rendering
	 * @param program Program shader with you want this sprite will be initialized
	 */
	public void init(GraphicalContext g, ShaderProgram program);
	
	/**
	 * Call init method with the saved ShaderProgram
	 */
	public void update();
	
	/**
	 * Draw all referenced sprite previously added by addSprite() method
	 * @param g Graphical context for rendering
	 */
	public void draw(GraphicalContext g, ShaderProgram program);
	
	/**
	 * Return the current restart index used with STRIP way to skip and restart the drawing
	 * @return
	 */
	public int getRestartIndex();
}