package com.hsee.gl.sprite;

import java.nio.ByteBuffer;

public interface Texture
{
	/**
	 * Return the width of texture
	 * @return
	 */
	int getWidth();
	
	/**
	 * Return the height of texture
	 * @return
	 */
	int getHeight();
	
	/**
	 * Type can be BufferedImage.TYPE_***
	 */
	int getType();
	
	/**
	 * Return the texture data in order specified by the type of texture
	 * @return
	 */
	ByteBuffer getData();
}
