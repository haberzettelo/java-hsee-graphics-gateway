package com.hsee.gl.sprite;

import java.nio.FloatBuffer;
import java.util.List;

public interface SpriteSetter<GraphicalContext> extends Sprite<GraphicalContext> {
	public void setVertexIndexes(List<ObjectFaces> datas);
	public void setVertexPosition(FloatBuffer datas);
	public void setVertexNormal(FloatBuffer datas);
	public void setVertexTextureCoordinates(FloatBuffer datas);
	public void setVertexColor(FloatBuffer datas);
	public void notifyDataUpdated();
}
