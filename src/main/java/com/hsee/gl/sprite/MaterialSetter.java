package com.hsee.gl.sprite;

import com.hsee.gl.math.primitive.Vector3;

public interface MaterialSetter extends Material {
	/**
	 * Set the id of the material in the Shader Storage Buffer
	 * @return The id of ssb, -1 if not setted.
	 */
	MaterialSetter setIdSsb(int id);
	
	/**
	 * Set the material's name
	 * @return
	 */
	MaterialSetter setName(String name);
	
	/**
	 * Set the Ka factor, ambient reflexivity, in RGB, values range from 0 to 1
	 * @return
	 */
	MaterialSetter setAmbientReflexivity(Vector3 values);
	
	/**
	 * Set the texture applied to the ambient reflexivity
	 * @return
	 */
	MaterialSetter setAmbientTexture(Texture text);
	/**
	 * Set the texture applied to the diffuse reflexivity
	 * @return
	 */
	MaterialSetter setDiffuseTexture(Texture text);
	/**
	 * Set the texture applied to the specular reflexivity
	 * @return
	 */
	MaterialSetter setSpecularTexture(Texture text);
	
	/**
	 * Set the texture applied to the specular exponent, his texture is non colored, scalar texture
	 * @return
	 */
	MaterialSetter setSpecularExponentTexture(Texture text);

	/**
	 * Set the bump texture, map_bump or bump
	 * @return
	 */
	MaterialSetter setBumpTexture(Texture text);
	
	/**
	 * Set the dissolve texture applied to dissolve factor, map_d
	 * @return
	 */
	MaterialSetter setDissolveTexture(Texture text);
	
	/**
	 * Set the Kd factor, diffuse reflexivity, in RGB, values range from 0 to 1
	 * @return
	 */
	MaterialSetter setDiffuseReflexivity(Vector3 values);
	
	/**
	 * Set the Ks factor, specular reflexivity, in RGB, values range from 0 to 1
	 * @return
	 */
	MaterialSetter setSpecularReflexivity(Vector3 values);
	
	/**
	 * Set the Ns factor. A high exponent results in a tight, concentrated highlight. Ns values normally range from 0 to 1000.
	 * @return
	 */
	MaterialSetter setSpecularExponent(float value);
	
	/**
	 * Set the sharpness factor of the reflections from the local reflection map.<br>
	 *  If a material does not have a local reflection map defined in its material definition, sharpness will apply to the global reflection map defined in PreView. <br>
	 * "value" can be a number from 0 to 1000. The default is 60. A high value results in a clear reflection of objects in the reflection map. 
	 * @return
	 */
	MaterialSetter setSharpness(float value);
	
	/**
	 * Set the Ni factor, optical density or index of refraction. <br>
	 *  The values can range from 0.001 to 10. A value of 1.0 means that light does not bend as it passes through an object. Increasing the optical_density increases the amount of bending. <br>
	 *  Glass has an index of refraction of about 1.5. Values of less than 1.0 produce bizarre results and are not recommended. 
	 * @return
	 */
	MaterialSetter setOpticalDensity(float value);
	
	/**
	 * Set the Tf factor, transmission filter, in RGB, values range from 0 to 1
	 * @return
	 */
	MaterialSetter setTransmissionFilter(Vector3 values);
	
	/**
	 * Set the illum factor, illumination model, value range from 0 to 10 <br><pre>
	 * 	0 Color on and Ambient off
	 *	1 Color on and Ambient on
	 *	2 Highlight on
	 *	3 Reflection on and Ray trace on
	 *	4 Transparency: Glass on
	 *	Reflection: Ray trace on
	 *	5 Reflection: Fresnel on and Ray trace on
	 *	6 Transparency: Refraction on
	 *	Reflection: Fresnel off and Ray trace on
	 *	7 Transparency: Refraction on
	 *	Reflection: Fresnel on and Ray trace on
	 *	8 Reflection on and Ray trace off
	 *	9 Transparency: Glass on
	 *	Reflection: Ray trace off
	 *	10 Casts shadows onto invisible surfaces </pre>
	 * @return
	 */
	MaterialSetter setIlluminationModel(int value);
	
	/**
	 * Set the dissolve factor. "factor" is the amount this material dissolves into the background. 
	 * A factor of 1.0 is fully opaque. This is the default when a new material is created. A factor of 0.0 is fully dissolved (completely transparent). 
	 * @return
	 */
	MaterialSetter setDissolve(float value);
}
