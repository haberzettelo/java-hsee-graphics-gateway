package com.hsee.gl.sprite;

import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderProgram;

public interface Drawable
{
	public void draw(ShaderProgram program, ModelViewProjection mvp);
}
