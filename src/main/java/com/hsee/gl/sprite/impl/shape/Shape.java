package com.hsee.gl.sprite.impl.shape;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.sprite.DrawWay;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.impl.SpriteImpl;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public class Shape<GraphicalContext> extends SpriteImpl<GraphicalContext>
{
	FloatBuffer _position, _normal, _color, _texture;
	List<Sprite.ObjectFaces> _index;
	public Shape (GraphicalContext g, Sprite<GraphicalContext> parent, SpriteRenderer<GraphicalContext> renderer, String name)
	{
		super(g, parent, renderer, name);
	}

	@Override
	public void setVertexIndexes (List<ObjectFaces> datas) {
		_index = datas;//new ArrayList<Sprite.ObjectFaces>();
		//_index.add(datas.get(0)/*new ObjectFaces(datas, new MaterialImpl())*/);
	}

	@Override
	public void setVertexPosition (FloatBuffer datas) {
		_position = datas;
	}

	@Override
	public void setVertexNormal (FloatBuffer datas) {
		_normal = datas;
	}

	@Override
	public void setVertexTextureCoordinates (FloatBuffer datas) {
		_texture = datas;
	}

	@Override
	public void setVertexColor (FloatBuffer datas) {
		_color = datas;
	}

	@Override
	public DrawWay getDrawWay () {
		return DrawWay.TrianglesStrip;
	}

	@Override
	public boolean indexContinue () {
		return true;
	}

	@Override
	public List<com.hsee.gl.sprite.Sprite.ObjectFaces> getVertexIndexes () {
		return _index;
	}

	@Override
	public FloatBuffer getVertexPosition () {
		return _position;
	}

	@Override
	public FloatBuffer getVertexNormal () {
		return _normal;
	}

	@Override
	public FloatBuffer getVertexTextureCoordinates () {
		return _texture;
	}

	@Override
	public FloatBuffer getVertexColor () {
		return _color;
	}

	@Override
	protected SpriteRenderer<GraphicalContext> newRendererByApi (
			GraphicalContext g) {
		return _renderer;
	}

}
