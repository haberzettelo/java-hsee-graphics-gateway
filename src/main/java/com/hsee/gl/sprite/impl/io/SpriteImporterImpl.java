package com.hsee.gl.sprite.impl.io;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.sprite.Material;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.SpriteSetter;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.impl.SpriteImpl;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;
import com.hsee.gl.sprite.io.Importer;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.jogamp.common.nio.Buffers;

public abstract class SpriteImporterImpl<GraphicalContext> extends SpriteImpl<GraphicalContext>
{

	protected SpriteImporterImpl (GraphicalContext g, ImporterSetter importer, SpriteCompiler compiler, String name, String path, String filename) {
		super(g, name, path, filename, importer, compiler);
		
		_position = Buffers.newDirectFloatBuffer(0);
		_indexes = new ArrayList<com.hsee.gl.sprite.Sprite.ObjectFaces>();
		_indexes.add(new ObjectFaces(Buffers.newDirectIntBuffer(0), new MaterialImpl()));
	}
	
	protected SpriteImporterImpl (GraphicalContext g, ImporterSetter importer, SpriteCompiler compiler, InputStreamGetter reader, String name, String path, String filename) {
		super(g, reader, name, path, filename, importer, compiler);
		
		_position = Buffers.newDirectFloatBuffer(0);
		_indexes = new ArrayList<com.hsee.gl.sprite.Sprite.ObjectFaces>();
		_indexes.add(new ObjectFaces(Buffers.newDirectIntBuffer(0), new MaterialImpl()));
	}

	@Override
	public void setVertexIndexes (List<ObjectFaces> datas) {
	}

	@Override
	public void setVertexPosition (FloatBuffer datas) {
	}

	@Override
	public void setVertexNormal (FloatBuffer datas) {
	}

	@Override
	public void setVertexTextureCoordinates (FloatBuffer datas) {
	}

	@Override
	public void setVertexColor (FloatBuffer datas) {
	}

	@Override
	public List<ObjectFaces> getVertexIndexes () {
		return _indexes;
	}

	@Override
	public FloatBuffer getVertexPosition () {
		return _position;
	}

	@Override
	public FloatBuffer getVertexNormal () {
		return _position;
	}

	@Override
	public FloatBuffer getVertexTextureCoordinates () {
		return _position;
	}

	@Override
	public FloatBuffer getVertexColor () {
		return _position;
	}

	FloatBuffer _position;
	List<ObjectFaces> _indexes;
	
	@Override
	protected List<Sprite<GraphicalContext>> getChildrenByApi (String filename, Importer importer) {
		int size = importer.getPositions().size();
		_children = new ArrayList<Sprite<GraphicalContext>>();
		List<FloatBuffer> pos = importer.getPositions(), norm = importer.getNormals(),
				tex = importer.getTextureCoordinates(), col = importer.getColors();
		List<List<ObjectFaces>> ids = importer.getIndexes();
		//importer.getMaterials();
		for (int i = 0; i < size; i++) {
			SpriteSetter<GraphicalContext> child = newChild(_name + i);
			
			child.setVertexPosition(pos.get(i));
			child.setVertexNormal(norm.get(i));
			child.setVertexColor(col.get(i));
			child.setVertexTextureCoordinates(tex.get(i));
			
			List<ObjectFaces> f = ids.get(i);
			child.setVertexIndexes(f);
			
			_children.add(child);
		}
		return _children;
	}
	
	/**
	 * Create new SpriteSetter
	 * @param name
	 * @return
	 */
	public abstract SpriteSetter<GraphicalContext> newChild(String name);
}
