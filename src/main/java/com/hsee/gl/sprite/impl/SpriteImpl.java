package com.hsee.gl.sprite.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.SpriteSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;
import com.hsee.gl.sprite.io.Importer;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public abstract class SpriteImpl<GraphicalContext> extends ModelImpl implements
		SpriteSetter<GraphicalContext> {
	private static Logger logger = Logger.getLogger(SpriteImpl.class);
	protected List<Sprite<GraphicalContext>>	_children;
	protected Sprite<GraphicalContext>			_parent;
	protected String _name;
	protected SpriteRenderer<GraphicalContext> _renderer;
	protected GraphicalContext g;

	protected SpriteImpl (GraphicalContext g, Sprite<GraphicalContext> parent, SpriteRenderer<GraphicalContext> renderer, String name) {
		_name = name;
		_parent = parent;
		_children = new ArrayList<Sprite<GraphicalContext>>();
		setRenderer(renderer);
		_renderer.addSprite(this);
		this.g = g;
	}
	
	protected SpriteImpl(GraphicalContext g, String name, String path, String filename, ImporterSetter importer, SpriteCompiler compiler) {
		_name = name;
		setRenderer(newRendererByApi(g));
		importer.setRestartIndex(_renderer.getRestartIndex());

		Importer imp;
		if (compiler.alreadyCompiled(path, filename))
			imp = compiler.importCompiledFile(importer, path, filename);
		else {
			importer.importFile(filename);
			imp = compiler.compile(importer, path, filename);
			//imp = NormalComputer.computeNormal(importer);
		}
		_children = getChildrenByApi(filename, imp);
		_renderer.addSprite(this);
		this.g = g;
	}
	
	protected SpriteImpl(GraphicalContext g, InputStreamGetter reader, String name, String path, String filename, ImporterSetter importer, SpriteCompiler compiler) {
		_name = name;
		setRenderer(newRendererByApi(g));
		importer.setRestartIndex(_renderer.getRestartIndex());
		
		Importer imp;
		if (compiler.alreadyCompiled(reader, path, filename))
			imp = compiler.importCompiledFile(importer, reader, path, filename);
		else {
			importer.importFile(reader, filename);
			imp = compiler.compile(importer, reader, path, filename);
			//imp = NormalComputer.computeNormal(importer);
		}
		_children = getChildrenByApi(filename, imp);
		_renderer.addSprite(this);
		this.g = g;
	}
	
	protected SpriteImpl(GraphicalContext g, List<Sprite<GraphicalContext>> children, Sprite<GraphicalContext> parent, String name) {
		_children = children;
		_parent = parent;
		_name = name;
		setRenderer(newRendererByApi(g));
		_renderer.addSprite(this);
		this.g = g;
	}
	
	@Override
	public List<Sprite<GraphicalContext>> getChildren () {
		return _children;
	}

	@Override
	public void initRenderer (ShaderProgram program) {
		_renderer.init(g, program);
	}
	
	@Override
	public Sprite<GraphicalContext> getParent () {
		return _parent;
	}

	@Override
	public String getName () {
		return _name;
	}

	@Override
	public void updateModel (Matrix newModel) {
		updateMatrix(newModel);
	}

	@Override
	public void setRenderer (SpriteRenderer<GraphicalContext> renderer) {
		_renderer = renderer;
	}

	protected abstract SpriteRenderer<GraphicalContext> newRendererByApi (GraphicalContext g);
	/**
	 * This method is called is you use the constructor without children argument.<br>
	 * if you use this constructor, override this method.<br>
	 * The importFile method from importer will be already called when this method was called.
	 * @return
	 */
	protected List<Sprite<GraphicalContext>> getChildrenByApi (String filename, Importer importer){
		return null;
	}
	
	@Override
	public void draw (ShaderProgram program, ModelViewProjection mvp) {
		mvp.update(this);
		mvp.notifyUpdated();
		program.useProgram();
		_renderer.draw(g, program);
	}

	//protected abstract void drawByApi (ShaderProgram program);

	@Override
	public void notifyDataUpdated () {
		/*_renderer.
		_renderer.update();*/
	}

}
