package com.hsee.gl.sprite.impl.io;


import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.io.Importer;
import com.jogamp.common.nio.Buffers;

public class ExportObject extends ImportExport
{
	public static void exportObject(String path, String fileName, Importer objects) throws IOException
	{
		int size = objects.getPositions().size();
		//TODO
		FloatBuffer[] vertex = new FloatBuffer[size],
					normal = new FloatBuffer[size],
					texture = new FloatBuffer[size],
					color = new FloatBuffer[size];
		for (int counter = 0; counter < size; counter++)
		{
			vertex[counter] = objects.getPositions().get(counter);
			normal[counter] = objects.getNormals().get(counter);
			texture[counter] = objects.getTextureCoordinates().get(counter);
			color[counter] = objects.getColors().get(counter);
			//index[counter] = objects.getIndexes().get(counter);
			//index[counter] = objects.getIndexes().get(counter);
		}
		
		DataOutputStream file = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path + fileName + extensionCompileFile)));
		
		byte dataInfo = (byte) (typeFloat | dataVertex);
		if (objects.getMtlFilename() != null) {
			dataInfo = (byte) (typeString | dataFileMTLName);
			writeString(file, objects.getMtlFilename(), dataInfo);
		}
			
		dataInfo = (byte) (typeFloat | dataVertex);
		writeTableBuffer(file, vertex, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataNormal);
		writeTableBuffer(file, normal, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataTexCoord);
		writeTableBuffer(file, texture, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataColor);
		writeTableBuffer(file, color, dataInfo);
		
		dataInfo = (byte) (typeInt | typeString | dataFace);
		//writeTableBuffer(file, index, dataInfo);
		writeTableOfTableObjectFaces(file, objects.getIndexes(), dataInfo);
		
		file.close();
		
	}
	
	//---
	//----------
	//	Privates Methods
	//--------------------------
	//------------------------------------
	
		//---------------------
		//	Functions writer
		//---------------------
	private static void writeBoolean (DataOutputStream file, boolean data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);

		file.writeBoolean(data);
	}
	
	/**
	 * Write a table of Buffer whatever the type
	 * @param file The file where the data will be written
	 * @param data The table of buffer to write in file
	 * @param infoTypeAndData 
	 * @throws IOException
	 */
	private static void writeTableBuffer (DataOutputStream file, Buffer[] data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);

		writeTableBufferUnlessInfo(file, data, infoTypeAndData);
	}
	
	private static void writeTableBufferUnlessInfo (DataOutputStream file, Buffer[] data, byte infoTypeAndData) throws IOException
	{
		file.writeInt(data.length);
		
		for (int i = 0, max = data.length; i < max; i++)
		{
			if (data[i] != null)
			{
				data[i].rewind();
				writeBufferUnlessInfo(file, data[i]);
			}
			else
				file.writeInt(0);
		}
	}
	
	private static void writeTableOfTableObjectFaces(DataOutputStream file, List<List<ObjectFaces>> data, byte infoTypeAndData) throws IOException {
		file.writeByte(infoTypeAndData);
		
		file.writeInt(data.size());
		for (List<ObjectFaces> list : data) {
			writeTableObjectFacesWithoutInfo(file, list);
		}
	}
	
	private static void writeTableObjectFacesWithoutInfo(DataOutputStream file, List<ObjectFaces> data) throws IOException {
		file.writeInt(data.size());
		for (ObjectFaces objectFaces : data) {
			writeObjectFacesWithoutInfo(file, objectFaces);
		}
	}
	
	private static void writeObjectFacesWithoutInfo(DataOutputStream file, ObjectFaces data) throws IOException {
		writeBufferUnlessInfo(file, data.faces);
		writeStringWithoutInfo(file, data.material.getName());
	}
	
	private static void writeVector (DataOutputStream file, Vector<Integer> data, byte infoTypeAndData) throws IOException
	{
		int length = data.size();
		
		file.write(infoTypeAndData);
		file.writeInt(length);
		
		for (int i = 0; i < length; i++)
		{
			file.writeInt(data.get(i));
		}
	}
	
	private static void writeInt (DataOutputStream file, int data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);
		file.writeInt(data);
	}
	
	private static void writeBuffer (DataOutputStream file, Buffer data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);
		
		writeBufferUnlessInfo(file, data);
	}
	private static void writeBufferUnlessInfo (DataOutputStream file, Buffer data) throws IOException
	{
		IntBuffer intBufDirect = Buffers.newDirectIntBuffer(0);
		FloatBuffer floatBufDirect = Buffers.newDirectFloatBuffer(0);
		ShortBuffer shortBufDirect = Buffers.newDirectShortBuffer(0);
		IntBuffer intBuf = IntBuffer.allocate(0);
		FloatBuffer floatBuf = FloatBuffer.allocate(0);
		ShortBuffer shortBuf = ShortBuffer.allocate(0);
		
		file.writeInt(data.capacity());
		
		if (data.getClass().equals(intBuf.getClass()) || data.getClass().equals(intBufDirect.getClass()))
		{
			IntBuffer dataTypeKnown = (IntBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeInt(dataTypeKnown.get(i));
		}
		else if (data.getClass().equals(floatBuf.getClass()) || data.getClass().equals(floatBufDirect.getClass()))
		{
			FloatBuffer dataTypeKnown = (FloatBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeFloat(dataTypeKnown.get(i));
		}
		else if (data.getClass().equals(shortBuf.getClass()) || data.getClass().equals(shortBufDirect.getClass()))
		{
			ShortBuffer dataTypeKnown = (ShortBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeShort(dataTypeKnown.get(i));
		}
		else
		{
			System.err.println("Problem when compilation file.");
		}		
	}

	private static void writeTableOfTableIntBuffer (DataOutputStream file, Buffer[][] data, byte infoTypeAndData) throws IOException
	{
		int length = data.length;
		
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		
		for (int i = 0; i < length; i++)
		{
			writeTableBufferUnlessInfo(file, data[i], infoTypeAndData);
		}
	}
	
	private static void writeTableInt (DataOutputStream file, int[] data, byte infoTypeAndData) throws IOException
	{
		int length = data.length;
		
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		
		for (int i = 0; i < length; i++)
		{
			file.writeInt(data[i]);
		}
	}
	
	private static void writeString (DataOutputStream file, String data, byte infoTypeAndData) throws IOException
	{
		file.write(infoTypeAndData);
		writeStringWithoutInfo(file, data);
	}
	
	private static void writeStringWithoutInfo (DataOutputStream file, String data) throws IOException
	{
		if (data == null)
			file.writeShort(0);
		else
		{
			file.writeShort(data.length());
			file.writeChars(data);
		}
	}
	
	private static void writeTableString (DataOutputStream file, String[] data, byte infoTypeAndData) throws IOException
	{
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		for (int i = 0, max = data.length; i < max; i++)
		{
			writeStringWithoutInfo(file, data[i]);
		}
	}

}
