package com.hsee.gl.sprite.impl.shape;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.sprite.DrawWay;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.impl.SpriteImpl;
import com.jogamp.common.nio.Buffers;

public abstract class AbstractCube<GraphicalContext> extends SpriteImpl<GraphicalContext>
{
	FloatBuffer _position, _normal, _color;
	List<ObjectFaces> _index;
	public AbstractCube (GraphicalContext g, String name)
	{
		super(g, new ArrayList<Sprite<GraphicalContext>>(), null, name);
		_position = Buffers.newDirectFloatBuffer(new float[]{
				0, 1, 0, 
				1, 1, 0, 
				0, 0, 0, 
				1, 0, 0, 

				1, 1, 0, 
				1, 1, 1, 
				1, 0, 0, 
				1, 0, 1, 
				
				1, 1, 1, 
				0, 1, 1, 
				1, 0, 1, 
				0, 0, 1,
				
				0, 0, 0, 
				0, 0, 1, 
				0, 1, 0, 
				0, 1, 1,
				
				0, 0, 0, 
				1, 0, 0,
				0, 0, 1, 
				1, 0, 1, 
				
				0, 1, 0,
				1, 1, 0,
				0, 1, 1,
				1, 1, 1,
		});
		_normal = Buffers.newDirectFloatBuffer(new float[]{
				0, 0, -1, 
				0, 0, -1, 
				0, 0, -1, 
				0, 0, -1, 
				
				1, 0, 0,
				1, 0, 0,
				1, 0, 0,
				1, 0, 0,

				0, 0, 1,
				0, 0, 1,
				0, 0, 1,
				0, 0, 1,

				-1, 0, 0,
				-1, 0, 0,
				-1, 0, 0,
				-1, 0, 0,

				0, -1, 0,
				0, -1, 0,
				0, -1, 0,
				0, -1, 0,

				0, 1, 0,
				0, 1, 0,
				0, 1, 0,
				0, 1, 0
		});
		
		_color = Buffers.newDirectFloatBuffer(new float[]{
				0, 0, 1, 
				0, 0, 1, 
				0, 0, 1, 
				0, 0, 1, 
				
				1, 0, 0,
				1, 0, 0,
				1, 0, 0,
				1, 0, 0,

				0, 0, 1,
				0, 0, 1,
				0, 0, 1,
				0, 0, 1,

				1, 1, 0,
				1, 1, 0,
				1, 1, 0,
				1, 1, 0,

				0, 1, 1,
				0, 1, 1,
				0, 1, 1,
				0, 1, 1,

				1, 1, 1,
				1, 1, 1,
				1, 1, 1,
				1, 1, 1
		});
		
		
		int restart = _renderer.getRestartIndex();
		
		
		_index = new ArrayList<Sprite.ObjectFaces>();
		_index.add(new ObjectFaces(Buffers.newDirectIntBuffer(new int[]{
				/*4, 1, 3, 2, 7, 6, 8, 5, 4, 1, Integer.MAX_VALUE, 
				1, 5, 2, 6, Integer.MAX_VALUE,
				7, 8, 3, 4*/
				0, 1, 2, 3, 3, 4, //restart,
				4, 5, 6, 7, 7, 8, //restart,
				8, 9, 10, 11, 11, 12, //restart,
				12, 13, 14, 15, 15, 16, //restart,
				16, 17, 18, 19, 19, 20, //restart,
				20, 21, 22, 23
		}), new MaterialImpl()));
		//_renderer.addSprite(this);
	}

	@Override
	public void setVertexIndexes (List<Sprite.ObjectFaces> datas) {
	}

	@Override
	public void setVertexPosition (FloatBuffer datas) {
	}

	@Override
	public void setVertexNormal (FloatBuffer datas) {
	}

	@Override
	public void setVertexTextureCoordinates (FloatBuffer datas) {
	}

	@Override
	public void setVertexColor (FloatBuffer datas) {
	}

	@Override
	public DrawWay getDrawWay () {
		return DrawWay.TrianglesStrip;
	}

	@Override
	public boolean indexContinue () {
		return true;
	}

	@Override
	public List<ObjectFaces> getVertexIndexes () {
		return _index;
	}

	@Override
	public FloatBuffer getVertexPosition () {
		return _position;
	}

	@Override
	public FloatBuffer getVertexNormal () {
		return _normal;
	}

	@Override
	public FloatBuffer getVertexTextureCoordinates () {
		return Buffers.newDirectFloatBuffer(0);
	}

	@Override
	public FloatBuffer getVertexColor () {
		return _color;
	}
	
	public String toString () {
		return "Cube " + _name;
	}
}
