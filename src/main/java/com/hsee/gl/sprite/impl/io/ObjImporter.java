package com.hsee.gl.sprite.impl.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.sprite.Material;
import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.jogamp.common.nio.Buffers;

import static com.hsee.gl.sprite.impl.io.ObjUtilParser.*;


public class ObjImporter implements ImporterSetter
{
	public class SubObject
	{
		public ArrayList<Vector3> position = new ArrayList<Vector3>();
		public ArrayList<Vector3> normal = new ArrayList<Vector3>();
		public ArrayList<Vector3> texture = new ArrayList<Vector3>();
		public ArrayList<Vector3> color = new ArrayList<Vector3>();
		
		public ArrayList<ArrayList<int[]>> index = new ArrayList<ArrayList<int[]>>();
		public ArrayList<String> materials = new ArrayList<String>();
	}
	
	
    private final static Logger logger = Logger.getLogger(ObjImporter.class);
    
    
    List<FloatBuffer> _position, _normal, _texutres, _color;
    List<List<ObjectFaces>> _ids;
    List<MaterialSetter> _materials;
    String _mtlFilename;
    int _resetId;
    
    protected String _path;
    
    private ArrayList<SubObject> _objects = new ArrayList<SubObject>();
    
	public ObjImporter (String path) {
		_path = path;
		_resetId = Integer.MAX_VALUE;
		
		_position = new ArrayList<FloatBuffer>();
		_normal = new ArrayList<FloatBuffer>();
		_texutres = new ArrayList<FloatBuffer>();
		_color = new ArrayList<FloatBuffer>();
		
		_ids = new ArrayList<List<ObjectFaces>>();
		
		_materials = new ArrayList<MaterialSetter>();
		
		_mtlFilename = null;
	}
	
	ArrayList<SubObject> getObjects() {
		return _objects;
	}

	@Override
	public void importFromInputStream (InputStream inputStream,
			String shaderNameToDebug) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		importFromBufferedReader(reader);
	}
	
	@Override
	public void importFile (String filename)
	{
		/**
         * Opening object file (.obj)
         */
		BufferedReader reader = getReader(_path + filename);
		importFromBufferedReader(reader);
	}
	@Override
	public void importFile (InputStreamGetter reader, String filenameIncludePath) {
		InputStream input = reader.open(filenameIncludePath);
		if (input == null) {
			logger.warn("The file : \"" + filenameIncludePath + "\" wax not found.");
			return ;
		}
		BufferedReader bufReader = new BufferedReader(new InputStreamReader(reader.open(filenameIncludePath)));
		importFromBufferedReader(bufReader);
	}
	
	private void importFromBufferedReader(BufferedReader reader) {
		String line = "";
		// Part lecture
		try
		{
			while ((line = reader.readLine()) != null)
			{
				treatLine(line);
			}
		}
		catch (IOException e)
		{
			logger.error(e.toString());
		}
		
		// Part conversion
		if (_mtlFilename != null)
			_materials = MtlImporter.importer.readMtlFile(_path, _mtlFilename);
		else
			_materials = new ArrayList<MaterialSetter>();
		storeSubObjects();
	}
	
	private void storeSubObjects ()
	{
		FloatBuffer buf;
		//IntBuffer iBuf;
		for (SubObject sub : _objects) {
			buf = Buffers.newDirectFloatBuffer(sub.position.size() * 3);
			fillBuffer(buf, sub.position);
			_position.add(buf);
			
			buf = Buffers.newDirectFloatBuffer(sub.normal.size() * 3);
			fillBuffer(buf, sub.normal);
			_normal.add(buf);
			
			buf = Buffers.newDirectFloatBuffer(sub.texture.size() * 3);
			fillBuffer(buf, sub.texture);
			_texutres.add(buf);
			
			buf = Buffers.newDirectFloatBuffer(sub.color.size() * 3);
			fillBuffer(buf, sub.color);
			_color.add(buf);
			
			List<ObjectFaces> iBuf = fillBuffer(_materials, sub.materials, sub.index, _resetId);//Buffers.newDirectIntBuffer(getIndexSize(sub.index));
			//fillBuffer(iBuf, sub.index, _resetId);
			_ids.add(iBuf);
		}
	}
	
	private static void fillBuffer (FloatBuffer buf, List<Vector3> data) {
		buf.rewind();
		for (Vector3 vector3 : data)
				buf.put(vector3.getCoordinates());
	}
	
	private static int getIndexSize (ArrayList<int[]> ids) {
		int result = 0;
		for (int[] face : ids)
			result += face.length;
		// Add this to add restart index
		//result += ids.size() - 1;
		// Add this to add swap
		result += (ids.size() - 1)*2;
		
		return result;
	}
	
	private static List<ObjectFaces> fillBuffer (List<MaterialSetter> allMaterials, ArrayList<String> mat, ArrayList<ArrayList<int[]>> data, int restartId) {
		List<ObjectFaces> result = new ArrayList<ObjectFaces>();
		Iterator<String> itMat = mat.iterator();
		int previousEnd = -1;
		for (ArrayList<int[]> list : data) {
			IntBuffer buf = Buffers.newDirectIntBuffer(getIndexSize(list));
			Iterator<int[]> it = list.iterator();
			buf.rewind();
			int [] face = null;
			if (it.hasNext()) {
				face = it.next();
				buf.put(face);
			}
			previousEnd = face[face.length-1];
			while(it.hasNext()){
				face = it.next();
				if (face.length == 4) {
					int temp = face[3];
					face[3] = face[2];
					face[2] = temp;
				}
				if (previousEnd != -1) {
					buf.put(previousEnd);
					buf.put(face[0]);
				}
				previousEnd = face[face.length-1];
				buf.put(face);
			}
			result.add(new ObjectFaces(buf, getMaterialByName(allMaterials, itMat.next())));
			
		}
		
		return result;
	}
	
	/*private static ObjectFaces fillObjectFace (ArrayList<ArrayList<int[]>> data, int restartId) {
		ObjectFaces result = new ObjectF
		IntBuffer buf;
		buf.rewind();
		Iterator<int[]> it = data.iterator();
		if (it.hasNext())
			buf.put(it.next());
		while(it.hasNext()){
			buf.put(restartId);
			int [] face = it.next();
			if (face.length == 4) {
				int temp = face[3];
				face[3] = face[2];
				face[2] = temp;
			}
			buf.put(face);
		}
	}*/
	
	private static Material getMaterialByName(List<MaterialSetter> list, String name){
		for (Material mat : list) {
			if (name.equals(mat.getName()))
				return mat;
		}
		return MaterialImpl.DEFAULT_MATERIAL;
	}
	
	
	/*private SubObject getTotalCapacity (ArrayList<SubObject> object)
	{
		SubObject regroupObject = new SubObject();
		if (object.size() < 1)
			return null;
		regroupObject.position = new ArrayList<>();
		regroupObject.normal = new ArrayList<>();
		regroupObject.texture = new ArrayList<>();
		regroupObject.color = new ArrayList<>();
		regroupObject.index = new ArrayList<>();
		for (SubObject objects : object)
		{
			regroupObject.position.addAll(objects.position);
			regroupObject.normal.addAll(objects.normal);
			regroupObject.texture.addAll(objects.texture);
			regroupObject.color.addAll(objects.color);
			regroupObject.index.addAll(objects.index);
		}
		return regroupObject;
	}*/
	
	private boolean _flagNewGroup = false, _materialChanged = false;
	private int _index = 0;
	private String _currentMaterial = "";
	private void treatLine (String line)
	{
		String[] splitedLine = line.split(" ");
		_materialChanged = false;
		if (splitedLine.length < 1 || (splitedLine.length == 1 && splitedLine[0] == ""))
			return;
		
		if (splitedLine.length > 1 && splitedLine[1].equals("")) {
			String[] temp = new String[splitedLine.length-1];
			temp[0] = splitedLine[0];
			for (int i = 2; i < splitedLine.length; i++)
				temp[i-1] = splitedLine[i];
			splitedLine = temp;
		}
		
		
		/**
		 * Change switch to if, else if to jre6
		 */
		if (splitedLine[0].equals("mtllib")) {
			_mtlFilename = splitedLine[1];
		} else if (splitedLine[0].equals("usemtl")) {
			_currentMaterial = splitedLine[1];
			_materialChanged = true;
			if (!_flagNewGroup)
			{
				_index++;
			}
		} else if (splitedLine[0].equals("v")) {
			if (!_flagNewGroup)
			{
				_materialChanged = false;
				_flagNewGroup = true;
				_objects.add(new SubObject());
				_storedTexture = null;
				_index = 0;
			}
			_objects.get(_objects.size()-1).position.add(parseThreeFloat(splitedLine));
		} else if (splitedLine[0].equals("vt")) {
			_objects.get(_objects.size()-1).texture.add(parseTwoFloat(splitedLine));
		} else if (splitedLine[0].equals("vn")) {
			_objects.get(_objects.size()-1).normal.add(parseThreeFloat(splitedLine));
		} else if (splitedLine[0].equals("vc")) {
			_objects.get(_objects.size()-1).color.add(parseThreeFloat(splitedLine));
		} else if (splitedLine[0].equals("f")) {
			if (!_flagNewGroup && _materialChanged) {
				_materialChanged = false;
			}
			if (_flagNewGroup) {
				_objects.get(_objects.size()-1).index = new ArrayList<ArrayList<int[]>>();
				_flagNewGroup = false;
			}
			if (_objects.get(_objects.size()-1).index.size() <= _index) {
				_objects.get(_objects.size()-1).index.add(new ArrayList<int[]>());
				_objects.get(_objects.size()-1).materials.add(_currentMaterial);
			}
			treatFace(splitedLine, _objects.get(_objects.size()-1).index.get(_index));
		}
		/*
		switch (splitedLine[0])	{
			case "mtllib":
				_mtlFilename = splitedLine[1];
				break;
			case "usemtl":
				_currentMaterial = splitedLine[1];
				_materialChanged = true;
				if (!_flagNewGroup)
				{
					_index++;
				}
				break;
			case "v":
				if (!_flagNewGroup)
				{
					_materialChanged = false;
					_flagNewGroup = true;
					_objects.add(new SubObject());
					_storedTexture = null;
					_index = 0;
				}
				_objects.get(_objects.size()-1).position.add(parseThreeFloat(splitedLine));
				break;
			case "vt":
				_objects.get(_objects.size()-1).texture.add(parseTwoFloat(splitedLine));
				
				break;
			case "vn":
				_objects.get(_objects.size()-1).normal.add(parseThreeFloat(splitedLine));
				
				break;
			case "vc":
				_objects.get(_objects.size()-1).color.add(parseThreeFloat(splitedLine));
				
				break;
			case "f":
				if (!_flagNewGroup && _materialChanged) {
					_materialChanged = false;
				}
				if (_flagNewGroup) {
					_objects.get(_objects.size()-1).index = new ArrayList<ArrayList<int[]>>();
					_flagNewGroup = false;
				}
				if (_objects.get(_objects.size()-1).index.size() <= _index) {
					_objects.get(_objects.size()-1).index.add(new ArrayList<Object>());
					_objects.get(_objects.size()-1).materials.add(_currentMaterial);
				}
				treatFace(splitedLine, _objects.get(_objects.size()-1).index.get(_index));
				break;

			default:
				break;
		}*/
	}
	
	private ArrayList<Vector3> _storedTexture = null;
	private int _offsetTexture = 0, _offsetTexturePrev = 1;
	
	private void treatFace(String[] splitedLine, List<int[]> faces)
	{
		int[] face = new int[(splitedLine.length - 1)/* * 4*/];
		
		SubObject currentObject = _objects.get(_objects.size()-1);
		
		int offsetId = 0;
		if (_objects.size()-2 > -1)
			for (int j = 0, maxJ = _objects.size() - 1; j < maxJ; j++)
				offsetId += _objects.get(j).position.size();
		for (int i = 1, max = splitedLine.length; i < max; i++)
		{
			//int offset = (i - 1) * 4;
			// Pour chaque point d'une face
			String[] splitedPoint = splitedLine[i].split("/");
			int id = new Integer(splitedPoint[0]);
			
			if (id > -1)
				face[i-1] = id - 1;
			else
				face[i-1] = offsetId + currentObject.position.size() + id;
			if (face[i-1] < 0) {
				logger.error("error while importing object");
			}
			
			// Texture adaptation
			if (_storedTexture == null) {
				_storedTexture = currentObject.texture;
				currentObject.texture = new ArrayList<Vector3>(currentObject.position.size());
				for (int j = 0, maxj = currentObject.position.size(); j < maxj; j++) {
					currentObject.texture.add(null);
				}
				_offsetTexture = _offsetTexturePrev;
				_offsetTexturePrev += _storedTexture.size();
			}
			if (!splitedPoint[1].equals(""))
				id = new Integer(splitedPoint[1]);
			/*if (id > -1)
				face[i-1] = id - 1;
			else
				face[i-1] = offsetId + currentObject.position.size() + id;
			*/
			Vector3 v = _storedTexture.get(id-_offsetTexture);
			currentObject.texture.set(face[i-1] - offsetId, v);
				
			/*for (int j = 0, max1 = splitedPoint.length; j < max1; j++)
				// Pour chaque donnee d'un point
				face[offset + j] = new Integer(splitedPoint[j]) - 1;
			for (int j = splitedPoint.length, max1 = 4; j < max1; j++)
				// Pour chaque donnee d'un point
				face[offset + j] = 0;
			
			// On inverse l'index 2 avec le trois car les textures sont specifies avant les normale ce qui n'est pas le cas pour nous
			int tmp = face[offset + 1];
			face[offset + 1] = face[offset + 2];
			face[offset + 2] = tmp;*/
		}
		faces.add(face);
		//_objects.get(_objects.size()-1).index.add(face);
	}
	
	private BufferedReader getReader (String fileName)
	{
		FileInputStream fichierObj = null;
        try {
            fichierObj = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
        	logger.error("The file \"" + fileName + "\" is not found for sprite \"" + "" + "\".");
        }

        return new BufferedReader(new InputStreamReader(fichierObj));
	}

	@Override
	public List<FloatBuffer> getPositions () {
		return _position;
	}

	@Override
	public List<FloatBuffer> getNormals () {
		return _normal;
	}

	@Override
	public List<FloatBuffer> getColors () {
		return _color;
	}

	@Override
	public List<FloatBuffer> getTextureCoordinates () {
		return _texutres;
	}

	@Override
	public List<List<ObjectFaces>> getIndexes () {
		return _ids;
	}

	@Override
	public void setPositions (List<FloatBuffer> pos) {
		_position = pos;
	}

	@Override
	public void setNormals (List<FloatBuffer> norm) {
		_normal = norm;
	}

	@Override
	public void setColors (List<FloatBuffer> col) {
		_color = col;
	}

	@Override
	public void setTextureCoordinates (List<FloatBuffer> tex) {
		_texutres = tex;
	}

	@Override
	public void setIndexes (List<List<ObjectFaces>> ids) {
		_ids = ids;
	}

	@Override
	public int getRestartIndex () {
		return _resetId;
	}

	@Override
	public void setRestartIndex (int restartId) {
		_resetId = restartId;
	}

	@Override
	public List<MaterialSetter> getMaterials () {
		return _materials;
	}

	@Override
	public void setMaterials (List<MaterialSetter> mat) {
		_materials = mat;
	}

	@Override
	public String getMtlFilename () {
		return _mtlFilename;
	}


}