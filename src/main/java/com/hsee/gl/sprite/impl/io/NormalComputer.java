package com.hsee.gl.sprite.impl.io;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.math.matrix.MathOperation;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.io.Importer;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.jogamp.common.nio.Buffers;

public class NormalComputer
{
	/**
	 * Compute normals of specified object if object hasn't normals
	 * @param object
	 * @return
	 */
	public static Importer computeNormal(ImporterSetter object) {
		/*if (object.getNormals().size() != 0 && object.getNormals().get(0).capacity() != 0)
			return object;
		*/int restartId = object.getRestartIndex(), offsetIndex = 0;
		ArrayList<FloatBuffer> normalsResult = new ArrayList<FloatBuffer>();
		for(int i = 0, max = object.getPositions().size(); i < max; i++) {
			FloatBuffer positions = object.getPositions().get(i);
			List<ObjectFaces> indexes = object.getIndexes().get(i);
			FloatBuffer normals = Buffers.newDirectFloatBuffer(positions.capacity());
			positions.rewind();
			
			//for (int j = 0, maxj = indexes.capacity(); j < max; j+= 1)
				//System.out.println("get : " + indexes.get(j));
			for (int j = 0, counter = 0, nbPoints = positions.capacity(); j < nbPoints; counter++, j+=3) {
				//System.out.println("new normal " + positions.capacity()/3);
				computeOneNormal(counter, offsetIndex, restartId, indexes, positions, normals);
			}
			offsetIndex += positions.capacity()/3;
			normalsResult.add(normals);
		}
		
		object.setNormals(normalsResult);
		return object;
	}
	
	private static void computeOneNormal(int idPosition, int offset, int restartIndex, List<ObjectFaces> indexes, FloatBuffer positions, FloatBuffer normals) {
		ArrayList<int[]> linkedPoints = getLinkedPoints(idPosition + offset, restartIndex, indexes, positions);
		ArrayList<Vector3> normalsFlat = new ArrayList<Vector3>();
		/*for (int[] v : linkedPoints) {
			System.out.println("offset:" + offset + " " + v[0] + ", " + v[1] + ", " + v[2]);
		}*/
		for (int[] is : linkedPoints) {
			Vector3 p0 = getVector(is[0] - offset, positions),
					p1 = getVector(is[1] - offset, positions),
					p2 = getVector(is[2] - offset, positions);
			Vector3 v10 = new Vector3(), v20 = new Vector3(), normalFlat = new Vector3();
			p1.sub(p0, v10);
			p2.sub(p0, v20);
			MathOperation.crossProductVec3(v10, v20, normalFlat);
			
			normalsFlat.add(new Vector3(MathOperation.normalizeVec3(normalFlat.getCoordinates())));
		}
		
		//revertAberant(normalsFlat);
		
		Vector3 vertexNormal = new Vector3();
		//vertexNormal.add(normalsFlat.get(0).opposite(), vertexNormal);
		Vector3.add(vertexNormal, normalsFlat);
		/*new Vector3(MathOperation.normalizeVec3(normalsFlat.get(0).getCoordinates())), vertexNormal);
		for (int i = 1, max = normalsFlat.size(); i < max; i++) {
			Vector3 vector3 = normalsFlat.get(i);
			Vector3 norm = new Vector3(MathOperation.normalizeVec3(vector3.getCoordinates()));
			vertexNormal.add(norm, vertexNormal);
		}*/
		normals.position(idPosition * 3);
		normals.put(vertexNormal.getCoordinates());
	}
	
	static class AAA {
		private ArrayList<Vector3> normals;
		public AAA (ArrayList<Vector3> normals)
		{	this.normals = normals;
		}
		public boolean near(int i, int j) {
			return Vector3.equals(normals.get(i), normals.get(j), 1.2f);
		}
	}
	
	private static void revertAberant(ArrayList<Vector3> normals) {
		AAA a = new AAA(normals);
		int nbAberant = 0;
		boolean firstAberant = true;
		if (normals.size() < 3) {
			if (normals.size() < 2)
				return ;
			if (!a.near(0, 1))
				normals.get(0).opposite();
			return;
		}
		
		if (a.near(0, 1)){
			if (!a.near(1, 2))
				normals.get(2).opposite();
		}
		else {
			if (a.near(1, 2)) {
				normals.get(0).opposite();
			}
			else {
				normals.get(1).opposite();
			}
		}
			
		for (int i = 3; i < normals.size(); i++) {
			if (!a.near(i, i-1)){
				normals.get(i).opposite();
				nbAberant++;
				firstAberant = false;
			}
		}
		
		if (nbAberant + 1 >= normals.size())
			for (Vector3 vector3 : normals) 
				vector3.opposite();
	}
	
	/**
	 * Get the points linked by faces
	 * @param idPosition
	 * @param indexes
	 * @param positions
	 * @return
	 */
	private static ArrayList<int[]> getLinkedPoints(int idPosition, int restartIndex, List<ObjectFaces> indexesGrouped, FloatBuffer positions) {
		ArrayList<int[]> result = new ArrayList<int[]>();
		
		for (ObjectFaces objectFaces : indexesGrouped) {
			
			IntBuffer indexes = objectFaces.faces;
			
			indexes.rewind();
			for (int i = 0, max = indexes.capacity(), faceVertex = 0; i < max; i++, faceVertex++) {
				int id = indexes.get();
				if (id == restartIndex) {
					faceVertex = -1;
					continue;
				}
				
				if (id == idPosition) {
					if (faceVertex == 0)
						result.add(new int[]{id, indexes.get(i+1), indexes.get(i+2)});
					else if (i + 1 == max || indexes.get(i+1) == restartIndex)
						if (faceVertex %2 == 1)
							result.add(new int[]{indexes.get(i-1), indexes.get(i-2), id});
						else
							result.add(new int[]{indexes.get(i-2), indexes.get(i-1), id});
					else
						if (faceVertex %2 == 1)
							result.add(new int[]{indexes.get(i-1), id, indexes.get(i+1)});
						else 
							result.add(new int[]{indexes.get(i+1), id, indexes.get(i-1)});
				}
			}
		}
		
		
		return result;
	}
	
	
	
	private static Vector3 getVector(int id, FloatBuffer position) {
		position.position(id * 3);
		return new Vector3(position.get(),position.get(),position.get());
	}
}
