package com.hsee.gl.sprite.impl.io;

import com.hsee.gl.math.primitive.Vector3;

public class ObjUtilParser {
	   /**
     * Parse three float contained in string ignoring the first element
     * Typically the form of String is [String, Float in String, Float in
     * String, Float in String]
     *
     * @param threeFloats Table of String to parse
     * @return
     */
    public static Vector3 parseThreeFloat(String[] threeFloats) {
    	if (threeFloats[1] != null && !threeFloats[1].equals(""))
    		return new Vector3(Float.parseFloat(threeFloats[1]), Float.parseFloat(threeFloats[2]), Float.parseFloat(threeFloats[3]));
    	else
    		return new Vector3(Float.parseFloat(threeFloats[2]), Float.parseFloat(threeFloats[3]), Float.parseFloat(threeFloats[4]));
    }
    /**
     * Parse three float contained in string ignoring the first element
     * Typically the form of String is [String, Float in String, Float in
     * String, Float in String]
     *
     * @param threeFloats Table of String to parse
     * @return
     */
    public static Vector3 parseTwoFloat(String[] threeFloats) {
    	if (threeFloats[1] != null && !threeFloats[1].equals(""))
    		return new Vector3(Float.parseFloat(threeFloats[1]), Float.parseFloat(threeFloats[2]), 0);
    	else
    		return new Vector3(Float.parseFloat(threeFloats[2]), Float.parseFloat(threeFloats[3]), 0);
    }
    
    /**
     * Parse three float contained in string ignoring the first element
     * Typically the form of String is [String, Float in String, Float in
     * String, Float in String]
     *
     * @param threeFloats Table of String to parse
     * @return
     */
    public static float parseFloat(String[] threeFloats) {
    	if (threeFloats[1] != null && !threeFloats[1].equals(""))
    		return Float.parseFloat(threeFloats[1]);
    	else
    		return Float.parseFloat(threeFloats[2]);
    }
}
