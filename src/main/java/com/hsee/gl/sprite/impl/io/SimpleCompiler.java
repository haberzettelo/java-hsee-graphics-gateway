package com.hsee.gl.sprite.impl.io;

import java.io.IOException;

import org.apache.log4j.Logger;

import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;
import com.hsee.gl.sprite.io.Importer;
import com.hsee.gl.sprite.io.ImporterSetter;

public class SimpleCompiler implements SpriteCompiler
{
	private static Logger logger = Logger.getLogger(SimpleCompiler.class);
	@Override
	public boolean alreadyCompiled (String path, String filename) {
		return ImportObject.isCompiled(path, filename);
	}

	@Override
	public boolean alreadyCompiled (InputStreamGetter reader, String path, String filename) {
		return ImportObject.isCompiled(reader, path, filename);
	}

	@Override
	public Importer compile (ImporterSetter importer, String path, String filename) {
		Importer imp = null;
		
		// TODO Here, add strategy to optimize geometry
		imp = NormalComputer.computeNormal(importer);
		
		try {
			ExportObject.exportObject(path, filename, importer);
		}
		catch (IOException e) {
			logger.error("Error while export file \"" + filename + "\" :" + e.getMessage());
		}//*/
		return imp;
	}
	
	@Override
	public Importer compile (ImporterSetter importer, InputStreamGetter reader, String path, String filename) {
		Importer imp = null;
		
		// TODO Here, add strategy to optimize geometry
		imp = NormalComputer.computeNormal(importer);

		logger.warn("The file : \"" + path + filename + "\" cannot be compiled with InputStreamGetter.");
		return imp;
	}

	@Override
	public Importer importCompiledFile (ImporterSetter importer, String path, String filename) {
		ImporterSetter imp = importer;
		try {
			imp = ImportObject.importObject(path, filename, imp);
			if (imp == null)
				throw new IOException();
		}
		catch (IOException e) {
			logger.error("Error while import file \"" + filename + "\" :" + e.getMessage());
		}
		return imp;
	}

	@Override
	public Importer importCompiledFile (ImporterSetter dest, InputStreamGetter reader, String path, String filename) {
		ImporterSetter imp = dest;
		try {
			imp = ImportObject.importObject(reader, path, filename, dest);
			if (imp == null)
				throw new IOException();
		}
		catch (IOException e) {
			logger.error("Error while import file \"" + filename + "\" :" + e.getMessage());
		}
		return imp;
	}

}
