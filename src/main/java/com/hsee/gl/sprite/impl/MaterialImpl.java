package com.hsee.gl.sprite.impl;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.Texture;

public class MaterialImpl implements MaterialSetter {
	public static MaterialImpl DEFAULT_MATERIAL = new MaterialImpl();
	private String _name = "HSEE_DEFAULT_MATERIAL";
	private Vector3 _aReflexivity = new Vector3(), _sReflexivity = new Vector3(0.8f, 0.8f, 0.8f), _dReflexivity = new Vector3(0.5f, 0.5f, 0.5f), _trFilter;
	private Texture _textAmbient, _textDiffuse, _textSpecular, _textSpecularExponent, _textBump, _textDissolve;
	private float _speclarExponent = 1, _sharpness = 1, _opticalDensity = 1, _dissolve = 1;
	private int _illuminationModel, _ssbId = 0;

	@Override
	public String getName () {
		return _name;
	}

	@Override
	public Vector3 getAmbientReflexivity () {
		return _aReflexivity;
	}

	@Override
	public Texture getAmbientTexture () {
		return _textAmbient;
	}

	@Override
	public Texture getDiffuseTexture () {
		return _textDiffuse;
	}

	@Override
	public Texture getSpecularTexture () {
		return _textSpecular;
	}

	@Override
	public Texture getSpecularExponentTexture () {
		return _textSpecularExponent;
	}

	@Override
	public Vector3 getDiffuseReflexivity () {
		return _dReflexivity;
	}

	@Override
	public Vector3 getSpecularReflexivity () {
		return _sReflexivity;
	}

	@Override
	public float getSpecularExponent () {
		return _speclarExponent;
	}

	@Override
	public float getSharpness () {
		return _sharpness;
	}

	@Override
	public float getOpticalDensity () {
		return _opticalDensity;
	}

	@Override
	public Vector3 getTransmissionFilter () {
		return _trFilter;
	}

	@Override
	public int getIlluminationModel () {
		return _illuminationModel;
	}

	@Override
	public float getDissolve () {
		return _dissolve;
	}

	@Override
	public MaterialSetter setName (String name) {
		_name = name;
		return this;
	}

	@Override
	public MaterialSetter setAmbientReflexivity (Vector3 values) {
		_aReflexivity = values;
		return this;
	}

	@Override
	public MaterialSetter setAmbientTexture (Texture text) {
		_textAmbient = text;
		return this;
	}

	@Override
	public MaterialSetter setDiffuseTexture (Texture text) {
		_textDiffuse = text;
		return this;
	}

	@Override
	public MaterialSetter setSpecularTexture (Texture text) {
		_textSpecular = text;
		return this;
	}

	@Override
	public MaterialSetter setSpecularExponentTexture (Texture text) {
		_textSpecularExponent = text;
		return this;
	}

	@Override
	public MaterialSetter setDiffuseReflexivity (Vector3 values) {
		_dReflexivity = values;
		return this;
	}

	@Override
	public MaterialSetter setSpecularReflexivity (Vector3 values) {
		_sReflexivity = values;
		return this;
	}

	@Override
	public MaterialSetter setSpecularExponent (float value) {
		_speclarExponent = value;
		return this;
	}

	@Override
	public MaterialSetter setSharpness (float value) {
		_sharpness = value;
		return this;
	}

	@Override
	public MaterialSetter setOpticalDensity (float value) {
		_opticalDensity = value;
		return this;
	}

	@Override
	public MaterialSetter setTransmissionFilter (Vector3 values) {
		_trFilter = values;
		return this;
	}

	@Override
	public MaterialSetter setIlluminationModel (int value) {
		_illuminationModel = value;
		return this;
	}

	@Override
	public MaterialSetter setDissolve (float value) {
		_dissolve = value;
		return this;
	}

	@Override
	public Texture getBumpTexture () {
		return _textBump;
	}

	@Override
	public Texture getDissolveTexture () {
		return _textDissolve;
	}

	@Override
	public MaterialSetter setBumpTexture (Texture text) {
		_textBump = text;
		return this;
	}

	@Override
	public MaterialSetter setDissolveTexture (Texture text) {
		_textDissolve = text;
		return this;
	}

	
	public String toString () {
		return "Material " + _name + "\n"
				+ "\tambient reflexivity (float rgb) : " + _aReflexivity + "\n"
				+ "\tdiffuse reflexivity (float rgb) : " + _dReflexivity + "\n"
				+ "\tspeculat reflexivity (float rgb) : " + _sReflexivity + "\n"
				+ "\ttransmission filter (float rgb) : " + _trFilter + "\n"
				+ "\tspecular exponent : " + _speclarExponent + "\n"
				+ "\tdissolve factor : " + _dissolve + "\n"
				+ "\toptical density : " + _opticalDensity + "\n"
				+ "\tsharpness factor : " + _sharpness;
	}

	@Override
	public int getIdSsb () {
		return _ssbId;
	}
	
	@Override
	public MaterialSetter setIdSsb(int newId) {
		_ssbId = newId;
		return this;
	}
}
