package com.hsee.gl.sprite.impl.renderer;

import java.util.ArrayList;

import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public abstract class SpriteRendererImpl<GraphicalContext> implements SpriteRenderer<GraphicalContext>
{
	protected int _restartIndex = Integer.MAX_VALUE;
	protected GraphicalContext g;
	protected ArrayList<Sprite<GraphicalContext>> _objects;
	protected ShaderProgram _program;
	
	protected SpriteRendererImpl ()
	{
		_objects = new ArrayList<Sprite<GraphicalContext>>();
	}
	
	@Override
	public void addSprite (Sprite<GraphicalContext> sprite)
	{
		if (!_objects.contains(sprite))
			_objects.add(sprite);
	}

	@Override
	public void addSpriteIncludingChildren (Sprite<GraphicalContext> sprite)
	{
		addSprite(sprite);
		if (sprite.getChildren() != null && sprite.getChildren().size() > 0)
			for (Sprite<GraphicalContext> child : sprite.getChildren())
				if (child != null)
					addSpriteIncludingChildren(child);
	}

	@Override
	public void updateSprite (Sprite<GraphicalContext> sprite)
	{
		if (_objects.contains(sprite)) {
			int i = 0;
			for (Sprite<GraphicalContext> object : _objects) {
				if (object == sprite) {
					_objects.set(i, sprite);
					break;
				}
				i++;
			}
		}
		//_objectsUpdates.push(sprite);
	}
	@Override
	public void updateSpriteIncludingChildren (Sprite<GraphicalContext> sprite)
	{
		updateSprite(sprite);
		if (sprite.getChildren() != null && sprite.getChildren().size() > 0)
			for (Sprite<GraphicalContext> child : sprite.getChildren())
				if (child != null)
					updateSpriteIncludingChildren(child);	
	}
	
	@Override
	public void removeSprite (Sprite<GraphicalContext> sprite)
	{
		_objects.remove(sprite);
		removeByApi(sprite);
	}
	@Override
	public void removeSpriteIncludingChildren (Sprite<GraphicalContext> sprite)
	{
		removeSprite(sprite);
		if (sprite.getChildren() != null && sprite.getChildren().size() > 0)
			for (Sprite<GraphicalContext> child : sprite.getChildren())
				if (child != null)
					removeSpriteIncludingChildren(child);
	}

	@Override
	public void init (GraphicalContext g, ShaderProgram program)
	{
		initByApi(g, program);
		//_objectsUpdates.clear();
		this.g = g;
		_program = program;
	}
	
	@Override
	public void update () {
		init(g, _program);
	}
	
	/**
	 * Compact all referenced objects to call minimum function during draw() method.<br>
	 * This method may be called more than once, it be called also to update the updated sprites.
	 */
	protected abstract void initByApi(GraphicalContext g, ShaderProgram program);
	
	/**
	 * Remove one sprite
	 * @param sprite Sprite to be removed
	 */
	protected abstract void removeByApi(Sprite<GraphicalContext> sprite);
	
	@Override
	public int getRestartIndex () {
		return _restartIndex;
	}
}
