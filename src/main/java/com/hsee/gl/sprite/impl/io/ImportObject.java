package com.hsee.gl.sprite.impl.io;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hsee.gl.sprite.Material;
import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.InputStreamGetterDefault;
import com.jogamp.common.nio.Buffers;

public class ImportObject extends ImportExport {
	private static Logger logger = Logger.getLogger(ImportObject.class);
	/**
	 * @throws IOException 
	 *Import object if exist, null otherwise
	 * @param fileName name of the file without extension.
	 * @param path path to access to file
	 * @param dest The object where the result will be stored
	 * @return null if the file is not found, the same ImporterSetter you have specified otherwise
	 */
	public static ImporterSetter importObject (String path, String fileName, ImporterSetter dest) throws IOException {
		if (!isCompiled(path, fileName)) {
			logger.error("The object \"" + path + fileName+ "\" was not found.");
			return null;
		}
		return importObject(InputStreamGetterDefault.instance, new FileInputStream(path + fileName + extensionCompileFile), path, fileName, dest);
	}
	
	public static ImporterSetter importObject(InputStreamGetter reader, String path, String fileName, ImporterSetter dest) throws IOException {
		if (!isCompiled(reader, path, fileName)) {
			logger.error("The object \"" + path + fileName + extensionCompileFile+ "\" was not found.");
			return null;
		}
		return importObject(reader, reader.open(path + fileName + extensionCompileFile), path, fileName, dest);
	}
	
	protected static ImporterSetter importObject(InputStreamGetter reader, InputStream input, String path, String fileName, ImporterSetter dest) throws IOException {
		DataInputStream file = new DataInputStream(input);
		//JoglObject object = new JoglObject();
		
		FloatBuffer[] vertex = null, normal = null, texture = null, color = null;
		//IntBuffer[] index = null;
		List<List<ObjectFaces>> indexes = null;
		List<MaterialSetter> materials = null;;
		String fileMtlName = null;
		
		//mask = 0b 0111 1000
		byte mask = (byte)0x78;//0b1111000;
		
		while (file.available() > 0)
		{
			byte typeOfData = file.readByte();
			
			switch (typeOfData & mask) 
			{
				case dataVertex:
					vertex = readTableFloatBuffer(file); break;
					
				case dataTexCoord:
					texture = readTableFloatBuffer(file); break;
					
				case dataNormal:
					normal = readTableFloatBuffer(file); break;
					
				case dataColor:
					color = readTableFloatBuffer(file); break;
					
				case dataFace:
					//index = readTableIntBuffer(file); 
					indexes = readTableOfTableObjectFaces(file, materials);
					break;
					
				case dataFileMTLName:
					/*TODO dataResult.fileMTLName = readString(file);*/
					fileMtlName = readString(file); 
					materials = MtlImporter.importer.readMtlFile(reader, path, fileMtlName);
					break;
					
				case dataFileMtlExist:
					/*TODO dataResult.fileMTLExist = readBoolean(file);*/ break;
					
				default:
					System.err.println("Nothing"); break;
				
			}
		}
		if (fileMtlName == null)
			materials = new ArrayList<MaterialSetter>();
		file.close();
		
		dest.setPositions(new ArrayList<FloatBuffer>(vertex.length));
		dest.setNormals(new ArrayList<FloatBuffer>(normal.length));
		dest.setTextureCoordinates(new ArrayList<FloatBuffer>(texture.length));
		dest.setColors(new ArrayList<FloatBuffer>(color.length));
		dest.setIndexes(indexes/*new ArrayList<IntBuffer>(index.length)*/);
		
		for (int counter = 0, max = vertex.length; counter < max; counter++)
		{
			dest.getPositions().add(vertex[counter]);
			dest.getNormals().add(normal[counter]);
			dest.getTextureCoordinates().add(texture[counter]);
			dest.getColors().add(color[counter]);
			//dest.getIndexes().add(index[counter]);
		}
		
		dest.setMaterials(materials);
		
		return dest;
	}
	
	public static boolean isCompiled (String path, String filename)
	{
		return new File(path + filename + extensionCompileFile).exists();
	}
	
	public static boolean isCompiled (InputStreamGetter reader, String path, String filename) {
		return reader.open(path + filename + extensionCompileFile) != null;
	}
	
	//---------------------
	//	Functions reader
	//---------------------	

	
	/*private static boolean readBoolean (DataInputStream file) throws IOException
	{
		return file.readBoolean();
	}
	
	private static int[] readTableInt (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		int[] dataResult = new int[max];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = file.readInt();
		}
		
		return dataResult;
	}*/
	
	/*private static IntBuffer[] readTableIntBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		IntBuffer[] dataResult = new IntBuffer[max];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = readIntBuffer(file);
		}
		return dataResult;
	}*/
	
	private static IntBuffer readIntBuffer (DataInputStream file) throws IOException
	{
		int length = file.readInt();
		IntBuffer dataResult = Buffers.newDirectIntBuffer(length);
		for (int j = 0; j < length; j++)
		{
			dataResult.put(file.readInt());
		}
		dataResult.rewind();
		return dataResult;
	}
	
	
	
	/*private static IntBuffer[][] readTableOfTableIntBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		IntBuffer[][] dataResult = new IntBuffer[max][];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = readTableIntBuffer(file);
		}
		
		return dataResult;
	}*/
	
	private static List<List<ObjectFaces>> readTableOfTableObjectFaces(DataInputStream file, List<MaterialSetter> materials) throws IOException {
		int size = file.readInt();
		List<List<ObjectFaces>> result = new ArrayList<List<ObjectFaces>> (size);
		for (int i = 0; i < size; i++) {
			result.add(readTableObjectFaces(file, materials));
		}
		return result;
	}
	
	private static List<ObjectFaces> readTableObjectFaces(DataInputStream file, List<MaterialSetter> materials) throws IOException {
		int size = file.readInt();
		List<ObjectFaces> result = new ArrayList<ObjectFaces> (size);
		for (int i = 0; i < size; i++) {
			result.add(readObjectFaces(file, materials));
		}
		return result;
	}
	
	private static ObjectFaces readObjectFaces(DataInputStream file, List<MaterialSetter> materials) throws IOException {
		Material mat = null;
		IntBuffer indexes = readIntBuffer(file);
		String matName = readString(file);
		
		if (materials != null && materials.size() > 0) {
			for (Material material : materials) {
				if (material.getName().equals(matName)) {
					mat = material;
					break;
				}
			}
		}
		if (mat == null)
			mat = MaterialImpl.DEFAULT_MATERIAL;
		
		ObjectFaces result = new ObjectFaces(
				indexes, 
				mat);
		return result;
	}	
	
	private static FloatBuffer[] readTableFloatBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		FloatBuffer[] dataResult = new FloatBuffer[max];
		
		for (int i = 0; i < max; i++)
		{
			int length = file.readInt();
			dataResult[i] = Buffers.newDirectFloatBuffer(length);
			for (int j = 0; j < length; j++)
			{
				dataResult[i].put(file.readFloat());
			}
			dataResult[i].rewind();
		}
		
		return dataResult;
	}
	/*private static FloatBuffer readFloatBuffer (DataInputStream file) throws IOException
	{
		FloatBuffer dataResult;
		
		int length = file.readInt();
		dataResult = Buffers.newDirectFloatBuffer(length);
		for (int j = 0; j < length; j++)
		{
			dataResult.put(file.readFloat());
		}
		dataResult.rewind();
		
		return dataResult;
	}
	
	private static Vector<Integer> readVector (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		Vector<Integer> dataResult = new Vector<Integer>(max);
		
		for (int i = 0; i < max; i++)
		{
			dataResult.add(file.readInt());
		}
		
		return dataResult;
	}
	*/
	private static String readString (DataInputStream file) throws IOException
	{
		short length = file.readShort();
		String dataResult = "";
		for (int i = 0; i < length; i++)
		{
			dataResult += file.readChar();
		}
		return dataResult;
	}
	
	/*private static String[] readTableString (DataInputStream file) throws IOException
	{
		String dataResult[] = new String[file.readInt()];
		for (int i = 0, max = dataResult.length; i < max; i++)
		{
			dataResult[i] = readString(file);
		}
		return dataResult;
	}*/
}
