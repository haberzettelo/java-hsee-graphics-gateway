package com.hsee.gl.sprite.impl.shape;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.sprite.DrawWay;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.impl.SpriteImpl;
import com.jogamp.common.nio.Buffers;

public abstract class AbstractPlan<GraphicalContext> extends SpriteImpl<GraphicalContext>
{
	FloatBuffer _position, _normal, _texture;
	List<Sprite.ObjectFaces> _index;
	public AbstractPlan (GraphicalContext g, String name, float size)
	{
		super(g, new ArrayList<Sprite<GraphicalContext>>(), null, name);
		float sizeOverTwo = size/2;
		_position = Buffers.newDirectFloatBuffer(new float[]{
				sizeOverTwo, 0, sizeOverTwo,
				-sizeOverTwo, 0, sizeOverTwo,
				sizeOverTwo, 0, -sizeOverTwo,
				-sizeOverTwo, 0, -sizeOverTwo
		});
		
		_texture = Buffers.newDirectFloatBuffer(new float[]{
				0, 0,
				0, 1, 
				1, 0, 
				1, 1
		});
		_normal = Buffers.newDirectFloatBuffer(new float[]{
				0, 1, 0,
				0, 1, 0,
				0, 1, 0,
				0, 1, 0
		});
		
		_index = new ArrayList<Sprite.ObjectFaces>();
		_index.add(new ObjectFaces(Buffers.newDirectIntBuffer(new int[]{
				0, 1, 2, 3
		}), new MaterialImpl()));
	}

	@Override
	public DrawWay getDrawWay () {
		return DrawWay.TrianglesStrip;
	}

	@Override
	public void setVertexIndexes (List<Sprite.ObjectFaces> datas) {
		_index = datas;/*new ArrayList<Sprite.ObjectFaces>();
		_index.add(new ObjectFaces(datas, new MaterialImpl()));*/
	}

	@Override
	public void setVertexPosition (FloatBuffer datas) {
		_position = datas;
	}

	@Override
	public void setVertexNormal (FloatBuffer datas) {
		_normal = datas;
	}

	@Override
	public void setVertexTextureCoordinates (FloatBuffer datas) {
		_texture = datas;
	}

	@Override
	public void setVertexColor (FloatBuffer datas) {
	}

	@Override
	public boolean indexContinue () {
		return true;
	}

	@Override
	public List<ObjectFaces> getVertexIndexes () {
		return _index;
	}

	@Override
	public FloatBuffer getVertexPosition () {
		return _position;
	}

	@Override
	public FloatBuffer getVertexNormal () {
		return _normal;
	}

	@Override
	public FloatBuffer getVertexTextureCoordinates () {
		return _texture;
	}

	@Override
	public FloatBuffer getVertexColor () {
		return Buffers.newDirectFloatBuffer(0);
	}	
}
