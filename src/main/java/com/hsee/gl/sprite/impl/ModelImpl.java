package com.hsee.gl.sprite.impl;

import com.hsee.gl.math.impl.MatrixTransformImpl;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.sprite.Model;

public class ModelImpl extends MatrixTransformImpl implements Model
{
	public ModelImpl ()
	{
		super(new Matrix());
	}
	@Override
	public Matrix getMatrix () {
		return _matrix;
	}

	public String toString () {
		return "Model : \n" + _matrix;
	}
}
