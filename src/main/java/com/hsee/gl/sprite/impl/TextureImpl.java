package com.hsee.gl.sprite.impl;

import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.DisplayMetrics;

import com.hsee.gl.sprite.Texture;
import com.hsee.gl.sprite.impl.texture.TargaTextureReader;
import com.hsee.gl.sprite.impl.texture.TargaTextureReader.ImageTarga;
import com.jogamp.common.nio.Buffers;

public class TextureImpl implements Texture {
	private static Logger logger = Logger.getLogger(TextureImpl.class);
	private int _width, _height;
	private ByteBuffer _data;

	/**
	 * Can be BufferedImage.TYPE_***
	 */
	private	int _type;
	
	public TextureImpl (String filename) {
		BufferedImage image = readImage(filename);
		if (image == null)
			return ;
		_type = image.getType();
		_data = makeRGBTexture(image);
		_height = image.getHeight();
		_width = image.getWidth();
		_type = image.getType();
	}
	
	public TextureImpl (InputStream input, String filename) {
		try {
			BufferedImage image;
			image = readImage(input, filename);
			if (image == null)
				return ;
			_type = image.getType();
			_data = makeRGBTexture(image);
			_height = image.getHeight();
			_width = image.getWidth();
		} catch (NoClassDefFoundError e) {
			// If Targa format
			if (filename.substring(filename.length()-3, filename.length()).equals("tga")) {
    			try {
					ImageTarga img = TargaTextureReader.readImageTarga(input);
					_data = img.data;
					_width = img.width;
					_height = img.height;
					_type = 0;
				}
				catch (IOException e1) {
		    		logger.error(new RuntimeException(e + " filename : " + filename));
				}
			} else {
				// Fill data without BufferredUmage
				Bitmap image = BitmapFactory.decodeStream(input);
				//image.
				_height = image.getHeight();
				_width = image.getWidth();
				
				Matrix m = new Matrix();
				m.preScale(1, -1);
				image = Bitmap.createBitmap(image, 0, 0, _width, _height, m, false);
				
				int[] result = new int[_width*_height];
				image.getPixels(result, 0, _width, 0, 0, _width, _height);
				ByteBuffer data;
				switch (image.getConfig()) {
				case ALPHA_8:
					data = Buffers.newDirectByteBuffer(result.length * Buffers.SIZEOF_INT);
					for(int i = 0; i < result.length; i++)
						data.put((byte)result[i]);
					break;
				case ARGB_4444:
					data = Buffers.newDirectByteBuffer(result.length * 2);
					for(int i = 0; i < result.length; i++) {
						data.put((byte) (result[i]  >> 8 & 0xFF)); // Red component
						data.put((byte) ((result[i] >> 4) & 0xFF)); // Green component
						data.put((byte) ((result[i] ) & 0xFF));     // Blue component
						data.put((byte) (result[i]  >> 12 & 0xFF)); // Red component
					}
					break;
				case ARGB_8888:
					data = Buffers.newDirectByteBuffer(result.length * 3/*Buffers.SIZEOF_INT*/);
					for(int i = 0; i < result.length; i++) {
						data.put((byte) (result[i]  >> 16 & 0xFF)); // Red component
						data.put((byte) ((result[i] >> 8) & 0xFF)); // Green component
						data.put((byte) ((result[i] ) & 0xFF));     // Blue component
						//data.put((byte) (result[i]  >> 24 & 0xFF)); // Red component
					}
					break;
				case RGB_565:
					data = Buffers.newDirectByteBuffer(result.length * 3/*Buffers.SIZEOF_INT*/);
					for(int i = 0; i < result.length; i++)
						data.putInt(result[i]);
					break;
	
				default:
					data = Buffers.newDirectByteBuffer(result.length * 3);
					for(int i = 0; i < result.length; i++) {
						data.put((byte) (result[i]  >> 16 & 0xFF)); // Red component
						data.put((byte) ((result[i] >> 8) & 0xFF)); // Green component
						data.put((byte) ((result[i] ) & 0xFF));     // Blue component
					}
					break;
				}
				data.rewind();
				_data = data;
			}
		}
	}

	public TextureImpl(ByteBuffer data, int width, int height, int type){
		_width = width;
		_height = height;
		_data = data;
		_type = type;
	}
	
	public TextureImpl(Texture text){
		_width = text.getWidth();
		_height = text.getHeight();
		_data = text.getData();
		_type = text.getType();
	}
	
	@Override
	public int getWidth () {
		return _width;
	}

	@Override
	public int getHeight () {
		return _height;
	}

	@Override
	public ByteBuffer getData () {
		return _data;
	}

    private static BufferedImage readImage(String filename)  throws NoClassDefFoundError
    {
    	try {
			return readImage(new FileInputStream(filename), filename);
		}
		catch (FileNotFoundException e) {
			logger.error(new RuntimeException(e + " filename : " + filename));
			return null;
		}
    }
    
    private static BufferedImage readImage(InputStream input, String filename) throws NoClassDefFoundError
    {
    	logger.debug("read texture file " + filename);
    	try {
    		
    		BufferedImage img;
    		if (filename.substring(filename.length()-3, filename.length()).equals("tga"))
    			img = TargaTextureReader.readBufferedImage(input);
    		else
    			img = ImageIO.read(input);
    		
    		if (img == null) {
    			logger.error(new RuntimeException("ImageIO.read method has returned null for this file : " + filename + ", 3 last characters : " + filename.substring(filename.length()-3, filename.length())));
    			return null;
    		}
    		/*
		    	Mirroring the Image
    		 */
    		java.awt.geom.AffineTransform tx = java.awt.geom.AffineTransform.getScaleInstance(1, -1);
    		tx.translate(0, -img.getHeight(null));
    		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
    		img = op.filter(img, null);//*/
    		return img;
    	} catch (IOException e) {
    		logger.error(new RuntimeException(e + " filename : " + filename));
    		return null;
    	} catch (NoClassDefFoundError e) {
    		throw e;
		}
    }
	
    private static ByteBuffer makeRGBTexture(BufferedImage img)
    {
	    ByteBuffer dest = null;
	    int[] pixels, data;
	    ByteBuffer buffer;
	    switch (img.getType())
	    {
		    case BufferedImage.TYPE_3BYTE_BGR:
		    	/*WritableRaster raster = img.getRaster();
		    	DataBuffer buf = raster.getDataBuffer();
		    	//DataBufferInt bufint = (DataBufferInt)buf;
		    	int[] data = new int[img.getWidth() * img.getHeight()];
		    	img.getRGB(0, 0, img.getWidth(), img.getHeight(), data, 0, img.getWidth());
			    dest = ByteBuffer.allocateDirect(data.length *3);
			    dest.order(ByteOrder.nativeOrder());
			    dest.asIntBuffer().put(data, 0, data.length);*/
		    	
			    pixels = new int[img.getWidth() * img.getHeight()];
		    	img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			    
			   
			    buffer = Buffers.newDirectByteBuffer(img.getWidth() * img.getHeight() * 3); //4 for RGBA, 3 for RGB
		        
		        for(int y = 0; y < img.getHeight(); y++){
		            for(int x = 0; x < img.getWidth(); x++){
		                int pixel = pixels[y * img.getWidth() + x];
		                //logger.debug(pixel);
		                buffer.put((byte) (pixel  >> 16 & 0xFF));               // Red component
		                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
		                buffer.put((byte) ((pixel ) & 0xFF));     // Blue component
		                //buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
		            }
		        }

		        buffer.rewind(); //FOR THE LOVE OF GOD DO NOT FORGET THIS
		        dest = buffer;
			    
			    break;
		    case BufferedImage.TYPE_CUSTOM:
		    {
		    	pixels = new int[img.getWidth() * img.getHeight()];
		    	img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			    
			   
			    buffer = Buffers.newDirectByteBuffer(img.getWidth() * img.getHeight() * 3); //4 for RGBA, 3 for RGB
		        
		        for(int y = 0; y < img.getHeight(); y++){
		            for(int x = 0; x < img.getWidth(); x++){
		                int pixel = pixels[y * img.getWidth() + x];
		                //logger.debug(pixel);
		                buffer.put((byte) (pixel & 0xFF));               // Blue component
		                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
		                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
		                //buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
		            }
		        }

		        buffer.rewind(); //FOR THE LOVE OF GOD DO NOT FORGET THIS
		        dest = buffer;

			    break;
		    }
		    case BufferedImage.TYPE_INT_RGB:
		    {
			    data = ((DataBufferInt) img.getRaster().getDataBuffer()).getData();
			    dest = ByteBuffer.allocateDirect(data.length * Buffers.SIZEOF_INT * 3);
			    dest.order(ByteOrder.nativeOrder());
			    dest.asIntBuffer().put(data, 0, data.length);
			    break;
		    }
		    default:
		    	byte[] datab = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
				
			    dest = ByteBuffer.allocateDirect(datab.length * Buffers.SIZEOF_INT); // If multiplied by 2 shows a dark cube
			    
			    dest.order(ByteOrder.nativeOrder());
			    dest.put(datab, 0, datab.length);
			    break;
		    	//throw new RuntimeException("Unsupported image type " + img.getType());
	    }
	    return dest;
    }

	@Override
	public int getType () {
		return _type;
	}
}
