package com.hsee.gl.sprite.impl.io;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.io.ObjImporter.SubObject;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;
import com.hsee.gl.sprite.io.Importer;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.jogamp.common.nio.Buffers;

public class StripCompiler implements SpriteCompiler
{
	public final static Logger logger = Logger.getLogger(StripCompiler.class);

	public class JoglObject
	{
		List<FloatBuffer> vertex, normal, texture, color;
		List<List<ObjectFaces>> index;
		List<IntBuffer> indexMerged;
		public JoglObject ()
		{
			vertex = new ArrayList<FloatBuffer>();
			normal = new ArrayList<FloatBuffer>();
			texture = new ArrayList<FloatBuffer>();
			color = new ArrayList<FloatBuffer>();
			index = new ArrayList<List<ObjectFaces>>();
			indexMerged = new ArrayList<IntBuffer>();
		}
		public List<FloatBuffer> getVertex ()
		{
			return vertex;
		}
		/*public void setVertex (FloatBuffer vertex)
		{
			this.vertex = vertex;
		}*/
		public List<FloatBuffer> getNormal ()
		{
			return normal;
		}
		/*public void setNormal (FloatBuffer normal)
		{
			this.normal = normal;
		}*/
		public List<FloatBuffer> getTexture ()
		{
			return texture;
		}
		/*public void setTexture (FloatBuffer texture)
		{
			this.texture = texture;
		}*/
		public List<FloatBuffer> getColor ()
		{
			return color;
		}
		/*public void setColor (FloatBuffer color)
		{
			this.color = color;
		}*/
		public List<List<ObjectFaces>> getIndex ()
		{
			return index;
		}
		/*public void setIndex (IntBuffer[] index)
		{
			this.index = index;
		}*/
		
		public List<IntBuffer> getIndexMerged ()
		{
			return indexMerged;
		}
		
	}
	
	@Override
	public boolean alreadyCompiled (String path, String filename) {
		return ImportObject.isCompiled(path, filename);
	}


	@Override
	public boolean alreadyCompiled (InputStreamGetter reader, String path,
			String filename) {
		return ImportObject.isCompiled(reader, path, filename);
	}
	
	@Override
	public Importer compile (ImporterSetter importer, String path, String filname) {
		/*int size = importer.getPositions().size();
		_importer = importer;
		logger.debug("Begining of the coversion");
		if (size < 1)
			return null;
		
		outObject = new JoglObject();
		_restartId = importer.getRestartIndex();
		
		logger.debug("Begining of importation, " + size + " subobject to convert");
		for (int i = 0, max = size; i < max; i++)
		{
			logger.debug("Achievement to convert object " + (float)(i*100)/max + "%");
			_allVertexIndex = new ArrayList<>();
			//this.object = importer.getObjects().get(i);
			_numberOfDatas = 0;
			
			_vertexPresent = importer.getPositions().size() > 0;
			_normalPresent = importer.getNormals().size() > 0;
			_texturePresent = importer.getTextureCoordinates().size() > 0;
			_colorPresent = importer.getColors().size() > 0;
			
			if (_vertexPresent)
				_numberOfDatas++;
			if (_normalPresent)
				_numberOfDatas++;
			if (_texturePresent)
				_numberOfDatas++;
			if (_colorPresent)
				_numberOfDatas++;
			
			
			logger.debug("Now convert all index");
			int numberOfPoint = convertIndex();
			
			if (i == 1)
				System.out.println();;
			
			logger.debug("All datas collected, now convert");
			JoglObject out = convertDatas(numberOfPoint);
			
			out = optimizeToTrianglesStrip(out);
			out = mergeIndex(out, i);
			
			//outObject = out;
		}
		
		importer.setColors(outObject.color);
		importer.setPositions(outObject.vertex);
		importer.setTextureCoordinates(outObject.texture);
		importer.setNormals(outObject.normal);
		importer.setIndexes(outObject.indexMerged);
		return _importer;*/
		return null;
	}

	@Override
	public Importer importCompiledFile (ImporterSetter importer, String path, String filname) {
		return null;
	}

	@Override
	public Importer importCompiledFile (ImporterSetter dest,
			InputStreamGetter reader, String path, String filename) {
		return null;
	}
	
	private JoglObject outObject;
	
	//private SubObject object;
	

	private boolean _vertexPresent,
					_normalPresent,
					_texturePresent,
					_colorPresent;
	private int _numberOfDatas = 0, _restartId;
	private ArrayList<NUplets> _allVertexIndex = new ArrayList<NUplets>();
	
	private Importer _importer;
	
	public StripCompiler (ObjImporter importer)
	{
		_importer = importer;
		
	}
	
	
	
	/**
	 * Permet de convertir plusieurs faces pouvant etre mises bout a bout en une seule
	 * @param object
	 * @return
	 */
	public JoglObject optimizeToTrianglesStrip (JoglObject object)
	{
		return object;
	}
	
	/**
	 * Permet de fusioner toutes les faces en une seule en les separant par le RESTART_INDEX
	 * @param object
	 * @return
	 */
	public JoglObject mergeIndex (JoglObject object, int id)
	{
		/*IntBuffer[] previousIndex = object.getIndex().get(id);
		IntBuffer resultIndex;
		
		int sizeOfBuffer = previousIndex.length - 1;
		for (IntBuffer intBuffer : previousIndex)
		{
			sizeOfBuffer += intBuffer.capacity();
		}
		
		resultIndex = Buffers.newDirectIntBuffer(sizeOfBuffer);
		for (int i = 0, max = previousIndex.length - 1; i < max; i++)
		{
			resultIndex.put(previousIndex[i]);
			resultIndex.put(_restartId);
		}
		resultIndex.put(previousIndex[previousIndex.length - 1]);
		resultIndex.rewind();
		object.getIndex().set(id, new IntBuffer[]{resultIndex});
		object.getIndexMerged().add(resultIndex);
		*/
		return object;
	}
	
	public JoglObject getJoglObject ()
	{
		/*JoglObject object = new JoglObject();
		
		object.setColor(getColor());
		object.setIndex(getIndexIntBuffer());
		object.setNormal(getNormal());
		object.setVertex(getVertex());
		object.setTexture(getTexture());
		*/
		return outObject;
	}
	
	int[][] index;
	
	/**
	 * Convertit les donnees en Buffers adaptes a OpenGL
	 * @param points
	 * @return
	 */
	private JoglObject convertDatas (int points)
	{
		JoglObject result = new JoglObject();
		result.color = _importer.getColors();
		result.normal = _importer.getNormals();
		result.texture = _importer.getTextureCoordinates();
		result.vertex = _importer.getPositions();
		result.index = _importer.getIndexes();
		//result.indexMerged = _importer.getIndexes();
		return result;
		/**
		 * Il faut creer autant de donnees qu'il y a de points differents
		 *//*
		if (_vertexPresent)
			result.vertex.add(Buffers.newDirectFloatBuffer(points * 3));// new Vector3[points];
		if (_normalPresent)
			result.normal.add(Buffers.newDirectFloatBuffer(points * 3));//  = new Vector3[points];
		if (_texturePresent)
			result.texture.add(Buffers.newDirectFloatBuffer(points * 3));//  = new Vector3[points];
		if (_colorPresent)
			result.color.add(Buffers.newDirectFloatBuffer(points * 3));//  = new Vector3[points];

		int size = _importer.getPositions().size();
		
		index = new int[size][];
		result.index.add(new IntBuffer[index.length]);
		int iterator = 0, id = result.index.size();
		int counterToDislayAdvancement = 0;
		for (int faceIterator = 0, maxFace = index.length; faceIterator < maxFace; faceIterator++, counterToDislayAdvancement++)
		{
			if (counterToDislayAdvancement > 10000)
			{
				counterToDislayAdvancement = 0;
				logger.debug(" . . . Achievement to convert datas " + (float)(faceIterator*100)/maxFace + "%");
			}
			/**
			 * Pour chaque face
			 *//*
			int numberOfPoint = _importer.getIndexes().get(faceIterator)index.get(faceIterator).length / _numberOfDatas;
			index[faceIterator] = new int[numberOfPoint];
			result.index.get(id)[faceIterator] = Buffers.newDirectIntBuffer(numberOfPoint);
			for (int point = 0, maxPoint = numberOfPoint; point < numberOfPoint; point++, iterator++)
			{
				/**
				 * Pour chaque point (chaque N-Uplet)
				 *//*
				// On ajoute l'indice au tableau d'indice
				index[faceIterator][point] = _allVertexIndex.get(iterator).getIndex();
				result.index.get(id)[faceIterator].put(index[faceIterator][point]);
				// On reference nos points
				if (_vertexPresent)
				{
					result.vertex.get(id).position(index[faceIterator][point]*3);
					NUplets nuplet = _allVertexIndex.get(iterator);
					int indexToGetVertex = nuplet.get();
					Vector3 vertexSearched = object.position.get(indexToGetVertex);
					float[] vertexConverted = vertexSearched.getCoordinates();
					result.vertex.get(id).put(vertexConverted);
					//result.vertex.put(object.vertex.get(_allVertexIndex.get(iterator).get()).getCoordinates());
				}
				if (_normalPresent)
				{
					result.normal.get(id).position(index[faceIterator][point]*3);
					result.normal.get(id).put(object.normal.get(_allVertexIndex.get(iterator).get()).getCoordinates());
				}
					//					normal[index[faceIterator][point]] = object.getNormal()[_allVertexIndex.get(iterator).get()];
				if (_texturePresent)
				{
					result.texture.get(id).position(index[faceIterator][point]*3);
					result.texture.get(id).put(object.texture.get(_allVertexIndex.get(iterator).get()).getCoordinates());
				}
//					texture[index[faceIterator][point]] = object.getTexture()[_allVertexIndex.get(iterator).get()];
				if (_colorPresent)
				{
					result.color.get(id).position(index[faceIterator][point]*3);
					int index = _allVertexIndex.get(iterator).get();
					if (index < 0)
						index = 0;
					result.color.get(id).put(object.color.get(index).getCoordinates());
				}
//					color[index[faceIterator][point]] = object.getColor()[_allVertexIndex.get(iterator).get()];
			}
		}
		result.vertex.get(id).rewind();
		result.normal.get(id).rewind();
		result.texture.get(id).rewind();
		result.color.get(id).rewind();
		for (IntBuffer buf : result.index.get(id))
			buf.rewind();
		
		return result;*/
	}
	
	
	private int[] offset = new int[]{0, 0, 0, 0};
	/**
	 * Convertit les indices et retourne le nombre de points differents repertories
	 * @return Nombre de points differents
	 */
	private int convertIndex()
	{
		/*int indexPoint = 0;
		int[] newOffset = new int[] {offset[0], offset[1], offset[2], offset[3]};
		
		logger.debug("Begining of iteration, " + object.index.size() + " index to convert");
		int counterToDislayAdvancement = 0;
		long timeInAlgo = 0, timeOutAlgo = 0;
		for (int faceIterator = 0, maxFace = object.index.size(); faceIterator < maxFace; faceIterator++, counterToDislayAdvancement++)
		{
			long p1 = System.currentTimeMillis();
			if (counterToDislayAdvancement > 1000)
			{
				counterToDislayAdvancement = 0;
				logger.debug("Achievement to convert " + (float)(faceIterator*100)/maxFace + "%");
				logger.debug("	Time out algo :  " + (timeOutAlgo - timeInAlgo)/1000 + "s");
				logger.debug("	Time in algo :  " + (timeInAlgo)/1000 + "s");
			}
			// 
			// Pour chaque face
			// 
			int[] face = object.index.get(faceIterator);
			for (int vertexData = 0, maxVertex = face.length; vertexData < maxVertex; vertexData += _numberOfDatas)
			{
				// 
				// Pour chaque point
				// 
				if (face[vertexData] + 1 > newOffset[0])
					newOffset[0] = face[vertexData] + 1;
				if (face[vertexData + 1] + 1 > newOffset[1])
					newOffset[1] = face[vertexData + 1] + 1;
				if (face[vertexData + 2] + 1 > newOffset[2])
					newOffset[2] = face[vertexData + 2] + 1;
				if (face[vertexData + 3] + 1 > newOffset[3])
					newOffset[3] = face[vertexData + 3] + 1;
					
				face[vertexData] 		-= (offset[0]);
				face[vertexData + 1] 	-= (offset[1]);
				face[vertexData + 2] 	-= (offset[2]);
				face[vertexData + 3] 	-= (offset[3]);
				
				NUplets nouveauPoint = new NUplets(face, vertexData, _numberOfDatas);
				
				boolean flagAlreadyExist = false;
				// 
				// On verifie que ce point n'existe pas deja
				// 
				long p2 = System.currentTimeMillis();
				for (int counterNUplets = 0, max = _allVertexIndex.size(); 
						counterNUplets < max; 
						counterNUplets++)//NUplets nuplet : _allVertexIndex)
				{
					if (nouveauPoint.equals(_allVertexIndex.get(counterNUplets)))
					{
						nouveauPoint.setIndex(_allVertexIndex.get(counterNUplets).getIndex());
						flagAlreadyExist = true;
						break;
					}
				}
				nouveauPoint.rewind();
				long p3 = System.currentTimeMillis();
				timeInAlgo += p3 - p2;
				if (!flagAlreadyExist)
				{
					nouveauPoint.setIndex(indexPoint);
					indexPoint++;
				}
				
				_allVertexIndex.add(nouveauPoint);
			}
			long p4 = System.currentTimeMillis();
			timeOutAlgo += p4 - p1;
		}
		logger.debug("End of iteration index");
		
		offset = newOffset;
		
		return indexPoint;*/
		return 0;
	}


	public Importer compile (ImporterSetter importer, InputStreamGetter reader, String path, String filename) {
		return null;
	}


}
