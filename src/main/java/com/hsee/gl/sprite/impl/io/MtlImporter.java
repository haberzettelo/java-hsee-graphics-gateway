package com.hsee.gl.sprite.impl.io;

import static com.hsee.gl.sprite.impl.io.ObjUtilParser.parseFloat;
import static com.hsee.gl.sprite.impl.io.ObjUtilParser.parseThreeFloat;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.hsee.gl.sprite.impl.TextureImpl;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.InputStreamGetterDefault;

public class MtlImporter {
	private static Logger logger = Logger.getLogger(MtlImporter.class);
	public static MtlImporter importer = new MtlImporter();
	private MtlImporter () {
	}
	
	public List<MaterialSetter> readMtlFile(String path, String filename) {
		InputStream inputFileMTL = null;
        if (filename != null && (filename.substring(0, 2).equals("./")))
        	filename = filename.substring(2);
        else if (filename != null && filename.substring(0, 3).equals("../"))
        	filename = filename.substring(3);
        try {
            inputFileMTL = new FileInputStream(path + filename);
        } catch (FileNotFoundException e) {
			logger.error("The material located:\""+path + filename+"\" file has encountered a problem during first opening.");
            return new ArrayList<MaterialSetter>();
        }
        return readMtlFile(InputStreamGetterDefault.instance, inputFileMTL, path, filename);
	}
	
	public List<MaterialSetter> readMtlFile(InputStreamGetter reader, String path, String filename)  {
		InputStream input = reader.open(path + filename);
		if (input == null) {
			logger.error("The material located:\""+path + filename+"\" file has encountered a problem during first opening.");
			return new ArrayList<MaterialSetter>();
		}
		return readMtlFile(reader, input, path, filename);
	}
	
	protected List<MaterialSetter> readMtlFile(InputStreamGetter getter, InputStream inputStream, String path, String filename) {
		BufferedReader inputFileMTL = new BufferedReader(new InputStreamReader(inputStream));
		 /**
         * Treatment of material file
         */
        logger.debug("Process material : \"" + path + filename + "\"");
        List<MaterialSetter> material = new ArrayList<MaterialSetter>();
        //List<MaterialSetter> result = new ArrayList<MaterialSetter>();
        String ligne;
        try {
			while ((ligne = inputFileMTL.readLine()) != null) {
			    String[] ligneParse = ligne.split(" ");
			    String type = ligneParse[0];
			    if (type.equals("newmtl")) {
			        /**
			         * New material
			         */
			        material.add(new MaterialImpl().setName(ligneParse[1]));
			    } else if (type.equals("Ka")) {
			        /**
			         * The ambient reflectivity of material (type Ka r g b)
			         */
			        material.get(material.size()-1).setAmbientReflexivity(parseThreeFloat(ligneParse));
			        //TODO type Ka spectral file.rfl factor AND Ka xyz x y z 
			    } else if (type.equals("Kd")) {
			        /**
			         * The diffuse reflectivity of material (type Kd r g b)
			         */
			        material.get(material.size()-1).setDiffuseReflexivity(parseThreeFloat(ligneParse));
			        //TODO type Kd spectral file.rfl factor AND Kd xyz x y z 
			    } else if (type.equals("Ks")) {
			        /**
			         * The specular reflectivity of material (type Ks r g b)
			         */
			        material.get(material.size()-1).setSpecularReflexivity(parseThreeFloat(ligneParse));
			        //TODO type Ks spectral file.rfl factor AND Ks xyz x y z 
			    } else if (type.equals("Tf")) {
			        /**
			         * The transmission filter of material (type Tf r g b)
			         */
			        material.get(material.size()-1).setTransmissionFilter(parseThreeFloat(ligneParse));
			        //TODO type Tf spectral file.rfl factor AND Tf xyz x y z 
			    } else if (type.equals("d")) {
			        /**
			         * The dissolve (transparency) of material (type Tf r g b)
			         */
			        material.get(material.size()-1).setDissolve(parseFloat(ligneParse));
			        //TODO type Tf spectral file.rfl factor AND Tf xyz x y z 
			    } else if (type.equals("illum")) {
			        /**
			         * The illumination type 0 Color on and Ambient off 1 Color on
			         * and Ambient on 2 Highlight on 3 Reflection on and Ray trace
			         * on 4 Transparency: Glass on and Reflection: Ray trace on 5
			         * Reflection: Fresnel on and Ray trace on 6 Transparency:
			         * Refraction on and Reflection: Fresnel off and Ray trace on 7
			         * Transparency: Refraction on and Reflection: Fresnel on and
			         * Ray trace on 8 Reflection on and Ray trace off 9
			         * Transparency: Glass on and Reflection: Ray trace off 10 Casts
			         * shadows onto invisible surfaces
			         */
			        material.get(material.size()-1).setIlluminationModel((int) parseFloat(ligneParse));
			    } else if (type.equals("Ns")) {
			        /**
			         * The specular exponent of material value 0 to 1000
			         */
			        material.get(material.size()-1).setSpecularExponent((int) parseFloat(ligneParse));
			    } else if (type.equals("Ni")) {
			        /**
			         * The optical density of material value 0.001 to 10
			         */
			        material.get(material.size()-1).setOpticalDensity(parseFloat(ligneParse));
			    } else if (type.equals("map_Ka")) {
			        /**
			         * The texture file
			         */
			    	String filenameTexture = path + ligneParse[ligneParse.length - 1];
			    	material.get(material.size()-1).setAmbientTexture(new TextureImpl(filenameTexture));
			    } else if (type.equals("map_Kd")) {
			        /**
			         * The texture file
			         */
			    	material.get(material.size()-1).setDiffuseTexture(new TextureImpl(getter.open(path + ligneParse[ligneParse.length - 1]), ligneParse[ligneParse.length - 1]));
			    } else if (type.equals("map_Ks")) {
			        /**
			         * The texture file
			         */
			    	material.get(material.size()-1).setSpecularTexture(new TextureImpl(getter.open(path + ligneParse[ligneParse.length - 1]), ligneParse[ligneParse.length - 1]));
			    }else if (type.equals("map_Ns")) {
			        /**
			         * The texture file
			         */
			    	material.get(material.size()-1).setSpecularExponentTexture(new TextureImpl(getter.open(path + ligneParse[ligneParse.length - 1]), ligneParse[ligneParse.length - 1]));
			    }else if (type.equals("map_bump") || type.equals("bump")) {
			        /**
			         * The texture file
			         */
			    	material.get(material.size()-1).setBumpTexture(new TextureImpl(getter.open(path + ligneParse[ligneParse.length - 1]), ligneParse[ligneParse.length - 1]));
			    }else if (type.equals("map_d")) {
			        /**
			         * The texture file
			         */
			    	material.get(material.size()-1).setDissolveTexture(new TextureImpl(path + ligneParse[ligneParse.length - 1]));
			    }
			}
		}
		catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        try {
			inputFileMTL.close();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
        /*for (MaterialSetter material2 : material) {
			result.add(material2);
		}*/

        logger.debug("End of process material : \"" + path + filename + "\"");
		return material;
	}
}
