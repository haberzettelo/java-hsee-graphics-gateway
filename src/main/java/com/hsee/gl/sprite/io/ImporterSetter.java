package com.hsee.gl.sprite.io;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;

import com.hsee.gl.sprite.Material;
import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.Sprite.ObjectFaces;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * ImporterSetter is the extension of Importer to add the setter control on this contract.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 10 mai 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ImporterSetter extends Importer
{
	/**
	 * Set the vertex positions from List of FloatBuffer
	 * @param pos Positions
	 */
	void setPositions(List<FloatBuffer> pos);
	
	/**
	 * Set the vertex normals from List of FloatBuffer
	 * @param norm Normals
	 */
	void setNormals(List<FloatBuffer> norm);
	
	/**
	 * Set the vertex colors from List of FloatBuffer
	 * @param col Colors
	 */
	void setColors(List<FloatBuffer> col);
	
	/**
	 * Set the vertex texture coordinates from List of FloatBuffer
	 * @param tex Texture coordinates
	 */
	void setTextureCoordinates(List<FloatBuffer> tex);
	
	
	void setIndexes(List<List<ObjectFaces>> ids);
	/**
	 * Set the vertex indexes from List of IntBuffer
	 * @param ids Indexes
	 */
	//void setIndexes(List<IntBuffer> ids);
	
	/**
	 * Set the materials of sub objects from a List of Material
	 * @param mat Materials
	 */
	void setMaterials(List<MaterialSetter> mat);
	
	/**
	 * Return the restart index used to render Triangle strip/fan correctly
	 * @return Retart index
	 */
	int getRestartIndex();
	
	/**
	 * Set the restart index used to render Triangle strip/fan correctly
	 * @param restartId Restart index
	 */
	void setRestartIndex(int restartId);
}
