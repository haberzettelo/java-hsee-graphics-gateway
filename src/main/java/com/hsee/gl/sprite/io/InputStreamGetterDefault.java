package com.hsee.gl.sprite.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class InputStreamGetterDefault implements InputStreamGetter {
	public static InputStreamGetter instance = new InputStreamGetterDefault();
	@Override
	public InputStream open (String fileName) {
		try {
			return new FileInputStream(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

}
