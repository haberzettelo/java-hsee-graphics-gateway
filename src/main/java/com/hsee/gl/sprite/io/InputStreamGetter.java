package com.hsee.gl.sprite.io;

import java.io.InputStream;

public interface InputStreamGetter {
	/**
	 * Get the input stream from the file name
	 * @param fileName name of the file you want to bind
	 * @return The inputstream binded with the file, null if file is not found
	 */
	public InputStream open(String fileName);
}
