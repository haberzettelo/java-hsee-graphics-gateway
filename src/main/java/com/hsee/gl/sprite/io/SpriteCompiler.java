package com.hsee.gl.sprite.io;

import com.hsee.gl.sprite.impl.io.NormalComputer;



/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Compiler is a contract to compile an object, imported of native. Compiler can 
 * compile an ImporterSetter contract and import the compiled ImporterSetter contract.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 10 mai 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface SpriteCompiler
{
	/**
	 * Return true if the specified object is already compiled, false otherwise.
	 * @param path Path of the compiled file
	 * @param filename Name of compile file. The extension will be removed.
	 */
	boolean alreadyCompiled(String path, String filename);
	
	/**
	 * Compile the ImporterSetter object into the specified file. The format of the compiled file depend of the implementation used.
	 * @param importer Object to compile
	 * @param path Path of the file
	 * @param filename File name
	 * @return The object you have specified down to Importer.
	 */
	Importer compile(ImporterSetter importer, String path, String filename);
	
	/**
	 * Import a specified compiled file into a ImporterSetter.
	 * @param dest ImporterSetter object to store the reading
	 * @param path Path of the compiled file
	 * @param filename File name of the compiled file, the extension will be removed and will be replaced by the implementation format
	 * @return The Read object down to Importer.
	 */
	Importer importCompiledFile(ImporterSetter dest, String path, String filename);
	Importer importCompiledFile(ImporterSetter dest, InputStreamGetter reader, String path, String filename);

	boolean alreadyCompiled (InputStreamGetter reader, String path, String filename);

	Importer compile (ImporterSetter importer, InputStreamGetter reader, String path, String filename);
}
