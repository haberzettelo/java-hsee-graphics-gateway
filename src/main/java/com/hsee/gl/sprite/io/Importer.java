package com.hsee.gl.sprite.io;

import java.io.InputStream;
import java.nio.FloatBuffer;
import java.util.List;

import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.Sprite.ObjectFaces;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Importer is the intermediate object, between the importation and the storage.
 * All Importer instances should be in a Sprite.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 10 mai 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Importer
{
	/**
	 * Import an object from a file. Only OBJ format is implement at the moment.
	 * @param filename File name include the path of the file
	 */
	void importFile(String filenameIncludePath);
	
	/**
	 * Import an object from a file. Only OBJ format is implement at the moment.
	 * @param filename File name include the path of the file
	 */
	void importFile(InputStreamGetter reader, String filenameIncludePath);
	
	
	/**
	 * Import an object from an input stream. Only OBJ format is implement at the moment.
	 * @param inputStream 
	 * @param shaderNameToDebug Name to debug, can be the file name if input stream come from file
	 */
	void importFromInputStream(InputStream inputStream, String shaderNameToDebug);
	
	/**
	 * Return all vertex positions of the object.
	 */
	List<FloatBuffer> getPositions();
	
	/**
	 * Return all vertex normals of the object.
	 */
	List<FloatBuffer> getNormals();
	
	/**
	 * Return all vertex colors of the object.
	 */
	List<FloatBuffer> getColors();
	
	/**
	 * Return all vertex texture coordinates of the object.
	 */
	List<FloatBuffer> getTextureCoordinates();
	
	
	List<List<ObjectFaces>> getIndexes();
	/**
	 * Return all vertex indexes of the object.
	 */
	//List<IntBuffer> getIndexes();
	
	/**
	 * Return all materials of the object.
	 */
	List<MaterialSetter> getMaterials();

	/**
	 * Return the file name of the material file, path included is relative to the imported file.
	 */
	String getMtlFilename();
}
