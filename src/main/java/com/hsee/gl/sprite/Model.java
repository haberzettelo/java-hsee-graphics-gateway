package com.hsee.gl.sprite;

import com.hsee.gl.math.IHasMatrix;
import com.hsee.gl.math.MatrixTransform;

public interface Model extends IHasMatrix, MatrixTransform
{
}
