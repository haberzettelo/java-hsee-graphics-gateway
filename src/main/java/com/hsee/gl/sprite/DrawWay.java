package com.hsee.gl.sprite;

public enum DrawWay
{
	TrianglesStrip, Triangles, LinesStrip, Lines, Points;
}
