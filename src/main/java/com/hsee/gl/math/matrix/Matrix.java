package com.hsee.gl.math.matrix;

import org.apache.log4j.Logger;

import com.hsee.gl.math.Valuable;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Permit to create matrix 4x4. A matrix is represented by array of float with
 * 4*4=16 elements.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Create matrix with constructor and use this with another matrix to perform
 * your operation. A matrix can be sended at a shader if you set its id with the
 * method setIndexMatrixInShader().
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 13 mai 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Matrix implements Valuable<Float>
{

	@SuppressWarnings ("unused")
	private final static Logger	logger	= Logger.getLogger(Matrix.class);
	private float[]				matrix;
	protected float[]			operation;
	protected float[]			tempResult;

	/**
	 * Instantiate identity matrix
	 */
	public Matrix ()
	{
		identity();
	}

	/**
	 * Instanciate new matrix with your data
	 * 
	 * @param data
	 *            Size must be 16, be caution
	 */
	public Matrix (float[] data)
	{
		matrix = data;
	}

	/**
	 * Define this matrix to identity
	 */
	public void identity () {
		float[] result = {1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,};
		matrix = result;
	}

	@Override
	public Matrix clone () {
		return new Matrix(this.matrix.clone());
	}

	/**
	 * Return the real value in float[16] array
	 * 
	 * @return
	 */
	public float[] getValues () {
		return matrix;
	}

	/**
	 * Set value directly with array
	 * 
	 * @param data
	 *            Size must be 16, be caution
	 */
	public void setValues (float[] data) {
		matrix = data;
	}

	long	time	= 0;

	/**
	 * Send matrix ModelViewProjection at specified shader
	 * 
	 * @param shader
	 *            Shader which will gone ModelViewProjection matrix
	 * @throws Exception
	 *             If ModelViewMatrix isn't identified in Shader
	 */
	/*
	 * public void sendMatrix (Shader shader) throws Exception,
	 * HseeMatrixException { if (idMatrix != -1) { shader.sendMatrix(this, true,
	 * idMatrix); } else { throw new HseeException(
	 * "IdMatrix = -1, use Matrix.setIndexMatrixInShader(int) method before Matrix.sendMatrix(Shader)."
	 * ); } }
	 */

	/**
	 * Calculate the determinant of a 3x3 matrix
	 * 
	 * @return result
	 */
	private static float determinant3x3 (float t00, float t01, float t02,
			float t10, float t11, float t12, float t20, float t21, float t22) {
		return t00 * (t11 * t22 - t12 * t21) + t01 * (t12 * t20 - t10 * t22)
				+ t02 * (t10 * t21 - t11 * t20);
	}

	/**
	 * @return the determinant of the matrix
	 */
	public float determinant () {
		float f = matrix[0]
				* ((matrix[5] * matrix[10] * matrix[15] + matrix[6]
						* matrix[11] * matrix[13] + matrix[7] * matrix[9]
						* matrix[14])
						- matrix[7]
						* matrix[10]
						* matrix[13]
						- matrix[5]
						* matrix[11] * matrix[14] - matrix[6] * matrix[9]
						* matrix[15]);
		f -= matrix[1]
				* ((matrix[4] * matrix[10] * matrix[15] + matrix[6]
						* matrix[11] * matrix[12] + matrix[7] * matrix[8]
						* matrix[14])
						- matrix[7]
						* matrix[10]
						* matrix[12]
						- matrix[4]
						* matrix[11] * matrix[14] - matrix[6] * matrix[8]
						* matrix[15]);
		f += matrix[2]
				* ((matrix[4] * matrix[9] * matrix[15] + matrix[5] * matrix[11]
						* matrix[12] + matrix[7] * matrix[8] * matrix[13])
						- matrix[7]
						* matrix[9]
						* matrix[12]
						- matrix[4]
						* matrix[11] * matrix[13] - matrix[5] * matrix[8]
						* matrix[15]);
		f -= matrix[3]
				* ((matrix[4] * matrix[9] * matrix[14] + matrix[5] * matrix[10]
						* matrix[12] + matrix[6] * matrix[8] * matrix[13])
						- matrix[6]
						* matrix[9]
						* matrix[12]
						- matrix[4]
						* matrix[10] * matrix[13] - matrix[5] * matrix[8]
						* matrix[14]);
		return f;
	}

	/**
	* Invert this matrix
	* @return this if successful, null otherwise
	*/
	public Matrix invert() {
		return invert(this, this);
	}
	
	/**
	 * Not yet implemented, This operation is discouraged, it is best to use
	 * this operation in shader.
	 */
	public static Matrix invert (Matrix src, Matrix dest) {
		float determinant = src.determinant();
		if (determinant != 0) {
			/*
			 * m00 m01 m02 m03 m10 m11 m12 m13 m20 m21 m22 m23 m30 m31 m32 m33
			 */
			if (dest == null)
				dest = new Matrix();
			float determinant_inv = 1f / determinant;
			// first row
			float t00 = determinant3x3(src.matrix[5], src.matrix[6],
					src.matrix[7], src.matrix[9], src.matrix[10],
					src.matrix[11], src.matrix[13], src.matrix[14],
					src.matrix[15]);
			float t01 = -determinant3x3(src.matrix[4], src.matrix[6],
					src.matrix[7], src.matrix[8], src.matrix[10],
					src.matrix[11], src.matrix[12], src.matrix[14],
					src.matrix[15]);
			float t02 = determinant3x3(src.matrix[4], src.matrix[5],
					src.matrix[7], src.matrix[8], src.matrix[9],
					src.matrix[11], src.matrix[12], src.matrix[13],
					src.matrix[15]);
			float t03 = -determinant3x3(src.matrix[4], src.matrix[5],
					src.matrix[6], src.matrix[8], src.matrix[9],
					src.matrix[10], src.matrix[12], src.matrix[13],
					src.matrix[14]);
			// second row
			float t10 = -determinant3x3(src.matrix[1], src.matrix[2],
					src.matrix[3], src.matrix[9], src.matrix[10],
					src.matrix[11], src.matrix[13], src.matrix[14],
					src.matrix[15]);
			float t11 = determinant3x3(src.matrix[0], src.matrix[2],
					src.matrix[3], src.matrix[8], src.matrix[10],
					src.matrix[11], src.matrix[12], src.matrix[14],
					src.matrix[15]);
			float t12 = -determinant3x3(src.matrix[0], src.matrix[1],
					src.matrix[3], src.matrix[8], src.matrix[9],
					src.matrix[11], src.matrix[12], src.matrix[13],
					src.matrix[15]);
			float t13 = determinant3x3(src.matrix[0], src.matrix[1],
					src.matrix[2], src.matrix[8], src.matrix[9],
					src.matrix[10], src.matrix[12], src.matrix[13],
					src.matrix[14]);
			// third row
			float t20 = determinant3x3(src.matrix[1], src.matrix[2],
					src.matrix[3], src.matrix[5], src.matrix[6], src.matrix[7],
					src.matrix[13], src.matrix[14], src.matrix[15]);
			float t21 = -determinant3x3(src.matrix[0], src.matrix[2],
					src.matrix[3], src.matrix[4], src.matrix[6], src.matrix[7],
					src.matrix[12], src.matrix[14], src.matrix[15]);
			float t22 = determinant3x3(src.matrix[0], src.matrix[1],
					src.matrix[3], src.matrix[4], src.matrix[5], src.matrix[7],
					src.matrix[12], src.matrix[13], src.matrix[15]);
			float t23 = -determinant3x3(src.matrix[0], src.matrix[1],
					src.matrix[2], src.matrix[4], src.matrix[5], src.matrix[6],
					src.matrix[12], src.matrix[13], src.matrix[14]);
			// fourth row
			float t30 = -determinant3x3(src.matrix[1], src.matrix[2],
					src.matrix[3], src.matrix[5], src.matrix[6], src.matrix[7],
					src.matrix[9], src.matrix[10], src.matrix[11]);
			float t31 = determinant3x3(src.matrix[0], src.matrix[2],
					src.matrix[3], src.matrix[4], src.matrix[6], src.matrix[7],
					src.matrix[8], src.matrix[10], src.matrix[11]);
			float t32 = -determinant3x3(src.matrix[0], src.matrix[1],
					src.matrix[3], src.matrix[4], src.matrix[5], src.matrix[7],
					src.matrix[8], src.matrix[9], src.matrix[11]);
			float t33 = determinant3x3(src.matrix[0], src.matrix[1],
					src.matrix[2], src.matrix[4], src.matrix[5], src.matrix[6],
					src.matrix[8], src.matrix[9], src.matrix[10]);
			// transpose and divide by the determinant
			dest.matrix[0] = t00 * determinant_inv;
			dest.matrix[5] = t11 * determinant_inv;
			dest.matrix[10] = t22 * determinant_inv;
			dest.matrix[15] = t33 * determinant_inv;
			dest.matrix[1] = t10 * determinant_inv;
			dest.matrix[4] = t01 * determinant_inv;
			dest.matrix[8] = t02 * determinant_inv;
			dest.matrix[2] = t20 * determinant_inv;
			dest.matrix[6] = t21 * determinant_inv;
			dest.matrix[9] = t12 * determinant_inv;
			dest.matrix[3] = t30 * determinant_inv;
			dest.matrix[12] = t03 * determinant_inv;
			dest.matrix[7] = t31 * determinant_inv;
			dest.matrix[13] = t13 * determinant_inv;
			dest.matrix[14] = t23 * determinant_inv;
			dest.matrix[11] = t32 * determinant_inv;
			return dest;
		}
		else
			return null;
	}

	/**
	 * Rotate at angle theta on specified axis
	 * 
	 * @param theta
	 *            Angle rotate
	 * @param x
	 *            float value
	 * @param y
	 *            float value
	 * @param z
	 *            float value
	 * @param mat
	 *            matrix to be rotated
	 * @return Matrix rotated
	 */
	protected float[] rotate (float theta, float x, float y, float z, Matrix mat) {
		float cosTheta = (float) Math.cos(theta);
		float unMoinsCosTheta = 1 - cosTheta;
		float sinTheta = (float) Math.sin(theta);
		operation = new float[] {x * x * (unMoinsCosTheta) + cosTheta,
				x * y * (unMoinsCosTheta) - z * sinTheta,
				x * z * (unMoinsCosTheta) + y * sinTheta, 0f,
				y * x * (unMoinsCosTheta) + z * sinTheta,
				y * y * (unMoinsCosTheta) + cosTheta,
				y * z * (unMoinsCosTheta) - x * sinTheta, 0f,
				x * z * (unMoinsCosTheta) - y * sinTheta,
				y * z * (unMoinsCosTheta) + x * sinTheta,
				z * z * (unMoinsCosTheta) + cosTheta, 0f, 0f, 0f, 0f, 1f};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(mat.getValues(), operation, tempResult);
		return tempResult;
	}

	/**
	 * Translate at specified value
	 * 
	 * @param x
	 *            Translate on x axe
	 * @param y
	 *            Translate on y axe
	 * @param z
	 *            Translate on z axe
	 */
	protected float[] translate (float x, float y, float z, Matrix mat) {
		operation = new float[] {1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(mat.getValues(), operation, tempResult);
		return tempResult;
	}

	/**
	 * Modify the scale at specified values
	 * 
	 * @param x
	 *            Resize on x axe
	 * @param y
	 *            Resize on y axe
	 * @param z
	 *            Resize on z axe
	 */
	protected float[] scale (float x, float y, float z, Matrix mat) {
		operation = new float[] {x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(mat.getValues(), operation, tempResult);
		return tempResult;
	}

	public String toString () {
		return " /" + matrix[0] + " " + matrix[1] + " " + matrix[2] + " "
				+ matrix[3] + "\\\n" + "| " + matrix[4] + " " + matrix[5] + " "
				+ matrix[6] + " " + matrix[7] + " |\n" + "| " + matrix[8] + " "
				+ matrix[9] + " " + matrix[10] + " " + matrix[11] + " |\n"
				+ " \\" + matrix[12] + " " + matrix[13] + " " + matrix[14]
				+ " " + matrix[15] + "/\n";
	}

	@Override
	public Float[] get () {
		return new Float[]{matrix[0], matrix[1], matrix[2], matrix[3], matrix[4], matrix[5], matrix[6], matrix[7], matrix[8], matrix[9], matrix[10], matrix[11], matrix[12], matrix[13], matrix[14], matrix[15]};
	}
}
