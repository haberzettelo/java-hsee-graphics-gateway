package com.hsee.gl.math.quaternion.test;
import static org.junit.Assert.*;
import static java.lang.Math.*;

import org.junit.BeforeClass;
import org.junit.Test;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.math.quaternion.DualQuaternion;
import com.hsee.gl.math.quaternion.Quaternion;



public class DualQuaternionTest
{
    static float epsilon = 0.00001f;
    private static Quaternion _q1/*, _q2*/;
    private static Vector3 _v0, /*_v1, */_v2/*, _v3*/;
    private static DualQuaternion _dq;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
		_q1 = new Quaternion(new Vector3(1, 0, 0), (float)PI/2);
		//_q2 = new Quaternion(new Vector3(0, 1, 0), (float)PI/4);
		_v0 = new Vector3(0, 0, 0);
		//_v1 = new Vector3(1, 1, 0);
		_v2 = new Vector3(0, 0, 1);
		//_v3 = new Vector3(1, 1, 1);
    }

    @Test
    public void testNoTranslation()
    {
		_dq = new DualQuaternion(_q1, _v0);
		Quaternion q = new Quaternion();
		Vector3 v = _dq.toRotationTranslation(q);
		assertEquals(_q1.getX(), q.getX(), epsilon);
		assertEquals(_q1.getY(), q.getY(), epsilon);
		assertEquals(_q1.getZ(), q.getZ(), epsilon);
		assertEquals(_q1.getW(), q.getW(), epsilon);
		
		assertEquals(0, v.getX(), epsilon);
		assertEquals(0, v.getY(), epsilon);
		assertEquals(0, v.getZ(), epsilon);
    }

    @Test
    public void testTranslationZ() throws Exception
    {
		_dq = new DualQuaternion(_q1, _v2);
		Quaternion q = new Quaternion();
		Vector3 v = _dq.toRotationTranslation(q);
		assertEquals(_q1.getX(), q.getX(), epsilon);
		assertEquals(_q1.getY(), q.getY(), epsilon);
		assertEquals(_q1.getZ(), q.getZ(), epsilon);
		assertEquals(_q1.getW(), q.getW(), epsilon);
		
		assertEquals(0, v.getX(), epsilon);
		assertEquals(0, v.getY(), epsilon);
		assertEquals(1, v.getZ(), epsilon);
    }
}
