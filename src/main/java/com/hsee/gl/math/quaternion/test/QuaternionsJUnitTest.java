package com.hsee.gl.math.quaternion.test;
import static java.lang.Math.PI;
import static java.lang.Math.sin;
import static java.lang.Math.cos;
import static junit.framework.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.math.quaternion.Quaternion;


public class QuaternionsJUnitTest
{
    static float epsilon = 0.000001f,
	    sqrtOneOverSqrtFive = 0;
    static Quaternion q;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
	sqrtOneOverSqrtFive = (float)Math.sqrt(0.5);
	q = new Quaternion(0, 0, 0, 0);
    }
    
    @SuppressWarnings ("deprecation")
	@Test
    public void testGetters() throws Exception
    {
		float x = -12f, y = 0.547f, z = -521.7f, w1 = 0f, w2 = 0.789f;
		Vector3 vec = new Vector3(54f, -59.0214f, 14f);
		q = new Quaternion(x, y, z, w1);
		assertEquals(x, q.getX(), epsilon);
		assertEquals(y, q.getY(), epsilon);
		assertEquals(z, q.getZ(), epsilon);
		assertEquals(w1, q.getW(), epsilon);
		
		q = new Quaternion(vec, w2);
		float angleOverTwo = w2 / 2f, sinAngleOverTwo = (float)sin(angleOverTwo), cosAngleOverTwo = (float)cos(angleOverTwo);
		assertEquals(vec.getX() * sinAngleOverTwo, q.getX(), epsilon);
		assertEquals(vec.getY() * sinAngleOverTwo, q.getY(), epsilon);
		assertEquals(vec.getZ() * sinAngleOverTwo, q.getZ(), epsilon);
		assertEquals(cosAngleOverTwo, q.getW(), epsilon);
    }

    @SuppressWarnings ("deprecation")
	@Test
    public void testNinetyDegreesAroundX()
    {
		q.fromRotationVector(new Vector3(1, 0, 0), (float)PI/2);
		assertEquals(sqrtOneOverSqrtFive, q.getW(), epsilon);
		assertEquals(sqrtOneOverSqrtFive, q.getX(), epsilon);
		assertEquals(0.0f, q.getY(), epsilon);
		assertEquals(0f, q.getZ(), epsilon);
    }

    @SuppressWarnings ("deprecation")
    @Test
    public void testNinetyDegreesAroundY()
    {
		q = new Quaternion(new Vector3(0, 1, 0), (float)PI/2);
		assertEquals(sqrtOneOverSqrtFive, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(sqrtOneOverSqrtFive, q.getY(), epsilon);
		assertEquals(0f, q.getZ(), epsilon);
    }

    @SuppressWarnings ("deprecation")
    @Test
    public void testNinetyDegreesAroundZ()
    {
		q.fromRotationVector(new Vector3(0, 0, 1), (float)PI/2);
		assertEquals(sqrtOneOverSqrtFive, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(0.0f, q.getY(), epsilon);
		assertEquals(sqrtOneOverSqrtFive, q.getZ(), epsilon);
    }
    
    @SuppressWarnings ("deprecation")
    @Test
    public void testMinusNinetyDegreesAroundX()
    {
		q = new Quaternion(new Vector3(1, 0, 0), -(float)PI/2);
		assertEquals(sqrtOneOverSqrtFive, q.getW(), epsilon);
		assertEquals(-sqrtOneOverSqrtFive, q.getX(), epsilon);
		assertEquals(0.0f, q.getY(), epsilon);
		assertEquals(0f, q.getZ(), epsilon);
    }
    
    @SuppressWarnings ("deprecation")
    @Test
    public void testMinusNinetyDegreesAroundY()
    {
		q.fromRotationVector(new Vector3(0, 1, 0), -(float)PI/2);
		assertEquals(sqrtOneOverSqrtFive, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(-sqrtOneOverSqrtFive, q.getY(), epsilon);
		assertEquals(0f, q.getZ(), epsilon);
    }
    
    @SuppressWarnings ("deprecation")
    @Test
    public void testMinusNinetyDegreesAroundZ()
    {
		q = new Quaternion(new Vector3(0, 0, 1), -(float)PI/2);
		assertEquals(sqrtOneOverSqrtFive, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(0, q.getY(), epsilon);
		assertEquals(-sqrtOneOverSqrtFive, q.getZ(), epsilon);
    }
    
    @SuppressWarnings ("deprecation")
    @Test
    public void testOneHundredDegresAroundX()
    {
		q.fromRotationVector(new Vector3(1, 0, 0), (float)PI);
		assertEquals(0, q.getW(), epsilon);
		assertEquals(1, q.getX(), epsilon);
		assertEquals(0, q.getY(), epsilon);
		assertEquals(0, q.getZ(), epsilon);
    }
    @SuppressWarnings ("deprecation")
    @Test
    public void testOneHundredDegresAroundY()
    {
		q = new Quaternion(new Vector3(0, 1, 0), (float)PI);
		assertEquals(0, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(1, q.getY(), epsilon);
		assertEquals(0, q.getZ(), epsilon);
    }
    @SuppressWarnings ("deprecation")
    @Test
    public void testOneHundredDegresAroundZ()
    {
		q.fromRotationVector(new Vector3(0, 0, 1), (float)PI);
		assertEquals(0, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(0, q.getY(), epsilon);
		assertEquals(1, q.getZ(), epsilon);
    }
    @SuppressWarnings ("deprecation")
    @Test
    public void testIdentity()
    {
		q = new Quaternion(new Vector3(0, 0, 0), 0);
		assertEquals(1, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(0, q.getY(), epsilon);
		assertEquals(0, q.getZ(), epsilon);
		
		q.fromRotationVector(new Vector3(0.1234f, 5.6789f, 10.111213f), 0);
		assertEquals(1, q.getW(), epsilon);
		assertEquals(0, q.getX(), epsilon);
		assertEquals(0, q.getY(), epsilon);
		assertEquals(0, q.getZ(), epsilon);
    }
    
    @SuppressWarnings ("deprecation")
    @Test
    public void testConjugate() throws Exception
    {
		float x = 1.07f, y = 7, z = -458.5f;
		q = new Quaternion(new Vector3(x, y, z), 0.7f);
		x = q.getRotationVector().getX();
		y = q.getRotationVector().getY();
		z = q.getRotationVector().getZ();
		q.conjugate();
		assertEquals(q.getX(), -x, epsilon);
		assertEquals(q.getY(), -y, epsilon);
		assertEquals(q.getZ(), -z, epsilon);
    }
    
    @SuppressWarnings ("deprecation")
    @Test
    public void testConjugateStatic() 
    {
		q = new Quaternion(new Vector3(1, 7, -458.5f), 0.7f);
		Quaternion conj = Quaternion.conjugateStatic(q);
		assertEquals(q.getX(), -conj.getX(), epsilon);
		assertEquals(q.getY(), -conj.getY(), epsilon);
		assertEquals(q.getZ(), -conj.getZ(), epsilon);
    }
}
