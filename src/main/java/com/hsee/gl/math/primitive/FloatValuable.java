package com.hsee.gl.math.primitive;

import com.hsee.gl.math.Valuable;

public class FloatValuable implements Valuable<Float>
{
	public float a;
	@Override
	public Float[] get () {
		return new Float[] {a};
	}

}
