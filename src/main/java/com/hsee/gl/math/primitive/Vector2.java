package com.hsee.gl.math.primitive;

import java.util.List;

import com.hsee.gl.math.Valuable;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Reprensent 3 coordinates on space, (x, y, z)
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Vector2 implements Valuable<Float>
{
	private float[] _coordinate;
	
	/**
	 * Contruct a vector, (0, 0)
	 */
	public Vector2 ()
	{
		_coordinate = new float[2];
		_coordinate[0] = 0;
		_coordinate[1] = 0;
	}
	
	/**
	 * Construct a vector (x, y, z)
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param z Z coordinate
	 */
	public Vector2 (float x, float y)
	{
		_coordinate = new float[2];
		_coordinate[0] = x;
		_coordinate[1] = y;
	}

	/**
	 * Construct a vector by copy of you specified vector
	 * @param vec
	 */
	public Vector2 (Vector2 vec)
	{
		_coordinate = new float[2];
		_coordinate[0] = vec.getX();
		_coordinate[1] = vec.getY();
	}

	public Vector2 (float[] vec2)
	{
		_coordinate = new float[2];
		_coordinate[0] = vec2[0];
		_coordinate[1] = vec2[1];
	}

	/**
	 * Return the coordinates of vector
	 * @return
	 */
	public float[] getCoordinates ()
	{
		return _coordinate;
	}
	
	/**
	 * Return the x coordinate
	 * @return
	 */
	public float getX()
	{
		return _coordinate[0];
	}
	
	/**
	 * Return the y coordinate
	 * @return
	 */
	public float getY()
	{
		return _coordinate[1];
	}
	
	/**
	 * Set all coordinates of this vector and return the same instance
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public Vector2 set(float x, float y) {
		_coordinate[0] = x;
		_coordinate[1] = y;
		return this;
	}
	
	/**
	 * Sub this vector by another vector and put result in dest vector, state of this vector will be stayed same
	 * @param toSub
	 * @param dest
	 * @return
	 */
	public Vector2 sub (Vector2 toSub, Vector2 dest) {
		dest._coordinate[0] = _coordinate[0] - toSub._coordinate[0];
		dest._coordinate[1] = _coordinate[1] - toSub._coordinate[1];
		return dest;
	}
	
	/**
	 * Sub this vector by another vector, state of this vector will be change
	 * @param toSub
	 * @return This instance changed
	 */
	public Vector2 sub (Vector2 toSub) {
		_coordinate[0] = _coordinate[0] - toSub._coordinate[0];
		_coordinate[1] = _coordinate[1] - toSub._coordinate[1];
		return this;
	}
	
	/**
	 * Add all vector together and dest will contain the result of addition (including dest if not 0)
	 * @param dest
	 * @param toAdd
	 * @return
	 */
	public static Vector2 add(Vector2 dest, Vector2... toAdd) {
		for (Vector2 vector2 : toAdd) {
			dest.add(vector2, dest);
		}
		return dest;
	}
	
	/**
	 * Add all vector together and dest will contain the result of addition (including dest if not 0)
	 * @param dest
	 * @param toAdd
	 * @return
	 */
	public static Vector2 add(Vector2 dest, List<Vector2> toAdd) {
		for (Vector2 vector2 : toAdd) {
			dest.add(vector2, dest);
		}
		return dest;
	}
	
	/**
	 * Add this vector by another vector and put result in dest vector, state of this vector will be stayed same
	 * @param toAdd
	 * @param dest
	 * @return
	 */
	public Vector2 add(Vector2 toAdd, Vector2 dest) {
		dest._coordinate[0] = _coordinate[0] + toAdd._coordinate[0];
		dest._coordinate[1] = _coordinate[1] + toAdd._coordinate[1];
		return dest;
	}
	
	/**
	 * Oppose this vector and return the same instance modified
	 * @return Chaining
	 */
	public Vector2 opposite() {
		_coordinate[0] = -_coordinate[0];
		_coordinate[1] = -_coordinate[1];
		return this;
	}
	
	/**
	 * Oppose this vector and put the result into dest. This instance will no change
	 * @return Result of the operation
	 */
	public Vector2 opposite(Vector2 dest) {
		dest._coordinate[0] = -_coordinate[0];
		dest._coordinate[1] = -_coordinate[1];
		return dest;
	}
	
	public String toString ()
	{
		return "Vector2 (" + _coordinate[0] + ", " + _coordinate[1] + ")";
	}
	
	public static boolean equals(Vector2 vec1, Vector2 vec2)
	{
		return Vector2.equals(vec1, vec2, 0.0001f);
	}
	
	public static boolean equals(Vector2 vec1, Vector2 vec2, float delta)
	{
		return (Math.abs(vec1.getX() - vec2.getX()) <= delta) &&
				(Math.abs(vec1.getY() - vec2.getY()) <= delta);
	}

	@Override
	public Float[] get () {
		return new Float[]{_coordinate[0], _coordinate[1]};
	}
}
