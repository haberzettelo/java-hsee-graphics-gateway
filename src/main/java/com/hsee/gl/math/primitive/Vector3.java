package com.hsee.gl.math.primitive;

import java.util.List;

import com.hsee.gl.math.Valuable;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Reprensent 3 coordinates on space, (x, y, z)
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Vector3 implements Valuable<Float>
{
	public static Vector3 NULL_VECTOR = new Vector3(),
			UNITY_VECTOR = new Vector3(1, 1, 1);
	private float[] _coordinate;
	
	/**
	 * Contruct a vector, (0, 0, 0)
	 */
	public Vector3 ()
	{
		_coordinate = new float[3];
		_coordinate[0] = 0;
		_coordinate[1] = 0;
		_coordinate[2] = 0;
	}
	
	/**
	 * Construct a vector (x, y, z)
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param z Z coordinate
	 */
	public Vector3 (float x, float y, float z)
	{
		_coordinate = new float[3];
		_coordinate[0] = x;
		_coordinate[1] = y;
		_coordinate[2] = z;
	}

	/**
	 * Construct a vector by copy of you specified vector
	 * @param vec
	 */
	public Vector3 (Vector3 vec)
	{
		_coordinate = new float[3];
		_coordinate[0] = vec.getX();
		_coordinate[1] = vec.getY();
		_coordinate[2] = vec.getZ();
	}

	public Vector3 (float[] vec3)
	{
		_coordinate = new float[3];
		_coordinate[0] = vec3[0];
		_coordinate[1] = vec3[1];
		_coordinate[2] = vec3[2];
	}

	/**
	 * Return the coordinates of vector
	 * @return
	 */
	public float[] getCoordinates ()
	{
		return _coordinate;
	}
	
	/**
	 * Return the x coordinate
	 * @return
	 */
	public float getX()
	{
		return _coordinate[0];
	}
	
	/**
	 * Return the y coordinate
	 * @return
	 */
	public float getY()
	{
		return _coordinate[1];
	}
	
	/**
	 * Return the z coordinate
	 * @return
	 */
	public float getZ()
	{
		return _coordinate[2];
	}
	
	/**
	 * Set all coordinates of this vector and return the same instance
	 * @param x
	 * @param y
	 * @param z
	 * @return
	 */
	public Vector3 set(float x, float y, float z) {
		_coordinate[0] = x;
		_coordinate[1] = y;
		_coordinate[2] = z;
		return this;
	}
	
	/**
	 * Sub this vector by another vector and put result in dest vector, state of this vector will be stayed same
	 * @param toSub
	 * @param dest
	 * @return
	 */
	public Vector3 sub (Vector3 toSub, Vector3 dest) {
		dest._coordinate[0] = _coordinate[0] - toSub._coordinate[0];
		dest._coordinate[1] = _coordinate[1] - toSub._coordinate[1];
		dest._coordinate[2] = _coordinate[2] - toSub._coordinate[2];
		return dest;
	}
	
	/**
	 * Sub this vector by another vector, state of this vector will be change
	 * @param toSub
	 * @return This instance changed
	 */
	public Vector3 sub (Vector3 toSub) {
		_coordinate[0] = _coordinate[0] - toSub._coordinate[0];
		_coordinate[1] = _coordinate[1] - toSub._coordinate[1];
		_coordinate[2] = _coordinate[2] - toSub._coordinate[2];
		return this;
	}
	
	/**
	 * Add all vector together and dest will contain the result of addition (including dest if not 0)
	 * @param dest
	 * @param toAdd
	 * @return
	 */
	public static Vector3 add(Vector3 dest, Vector3... toAdd) {
		for (Vector3 vector3 : toAdd) {
			dest.add(vector3, dest);
		}
		return dest;
	}
	
	/**
	 * Add all vector together and dest will contain the result of addition (including dest if not 0)
	 * @param dest
	 * @param toAdd
	 * @return
	 */
	public static Vector3 add(Vector3 dest, List<Vector3> toAdd) {
		for (Vector3 vector3 : toAdd) {
			dest.add(vector3, dest);
		}
		return dest;
	}
	
	/**
	 * Add this vector by another vector and put result in dest vector, state of this vector will be stayed same
	 * @param toAdd
	 * @param dest
	 * @return
	 */
	public Vector3 add(Vector3 toAdd, Vector3 dest) {
		dest._coordinate[0] = _coordinate[0] + toAdd._coordinate[0];
		dest._coordinate[1] = _coordinate[1] + toAdd._coordinate[1];
		dest._coordinate[2] = _coordinate[2] + toAdd._coordinate[2];
		return dest;
	}
	
	/**
	 * Oppose this vector and return the same instance modified
	 * @return Chaining
	 */
	public Vector3 opposite() {
		_coordinate[0] = -_coordinate[0];
		_coordinate[1] = -_coordinate[1];
		_coordinate[2] = -_coordinate[2];
		return this;
	}
	
	/**
	 * Oppose this vector and put the result into dest. This instance will no change
	 * @return Result of the operation
	 */
	public Vector3 opposite(Vector3 dest) {
		dest._coordinate[0] = -_coordinate[0];
		dest._coordinate[1] = -_coordinate[1];
		dest._coordinate[2] = -_coordinate[2];
		return dest;
	}
	
	public String toString ()
	{
		return "Vector3 (" + _coordinate[0] + ", " + _coordinate[1] + ", " + _coordinate[2] + ")";
	}
	
	public static boolean equals(Vector3 vec1, Vector3 vec2)
	{
		return Vector3.equals(vec1, vec2, 0.0001f);
	}
	
	public static boolean equals(Vector3 vec1, Vector3 vec2, float delta)
	{
		return (Math.abs(vec1.getX() - vec2.getX()) <= delta) &&
				(Math.abs(vec1.getY() - vec2.getY()) <= delta) &&
				(Math.abs(vec1.getZ() - vec2.getZ()) <= delta);
	}

	@Override
	public Float[] get () {
		return new Float[]{_coordinate[0], _coordinate[1], _coordinate[2]};
	}
}
