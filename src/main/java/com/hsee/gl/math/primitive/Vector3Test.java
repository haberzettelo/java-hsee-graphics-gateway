package com.hsee.gl.math.primitive;

import static org.junit.Assert.*;

import org.junit.Test;

public class Vector3Test
{

	@Test
	public void testVector3 ()
	{
		Vector3 vec = new Vector3();
		assertTrue(vec instanceof Vector3);
		assertEquals(0, vec.getX(), 0.00001f);
		assertEquals(0, vec.getY(), 0.00001f);
		assertEquals(0, vec.getZ(), 0.00001f);
	}
	
	@Test
	public void testVector3Vector3 ()
	{
		Vector3 vec2 = new Vector3(1.3574f, -777469.1254f, 0);
		Vector3 vec1 = new Vector3(vec2);
		assertTrue(vec1 instanceof Vector3);
		assertTrue(vec2 instanceof Vector3);
		assertFalse(vec1 == vec2);
		assertEquals(vec2.getX(), vec1.getX(), 0.00001f);
		assertEquals(vec2.getY(), vec1.getY(), 0.00001f);
		assertEquals(vec2.getZ(), vec1.getZ(), 0.00001f);
	}

	@Test
	public void testVector3FloatFloatFloat ()
	{
		Vector3 vec = new Vector3(1.3574f, -777469.1254f, 0);
		assertTrue(vec instanceof Vector3);
		assertEquals(1.3574f, vec.getX(), 0.00001f);
		assertEquals(-777469.1254f, vec.getY(), 0.00001f);
		assertEquals(0, vec.getZ(), 0.00001f);
	}

	@Test
	public void testGetCoordinates ()
	{
		Vector3 vec = new Vector3(1.3574f, -777469.1254f, 0);
		float[] temp = vec.getCoordinates();
		assertTrue(temp.length == 3);
		assertEquals(1.3574f, temp[0], 0.00001f);
		assertEquals(-777469.1254f, temp[1], 0.00001f);
		assertEquals(0, temp[2], 0.00001f);
		
	}
	
	@Test
	public void testEquals ()
	{
		Vector3 vec1 = new Vector3(1.3574f, -777469.1254f, 0);
		Vector3 vec2 = new Vector3(1.3574f, -777469.1254f, 0);
		Vector3 vec5 = new Vector3(1.3574f, -777469.1255f, 0);
		Vector3 vec4 = new Vector3(1.3574f, -777469.1254f, 1);
		Vector3 vec3 = new Vector3(0, -777469.1254f, 0);
		assertTrue(Vector3.equals(vec1, vec2));
		assertTrue(Vector3.equals(vec1, vec5));
		assertFalse(Vector3.equals(vec1, vec4));
		assertTrue(Vector3.equals(vec1, vec4, 1f));
		assertFalse(Vector3.equals(vec1, vec3));
		
	}

}
