package com.hsee.gl.math;

import com.hsee.gl.math.matrix.Matrix;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Projection is the operation done between your virtual scene and your camera.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 3 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Projection extends IHasMatrix {
	/**
	 * Construct perspective matrix to render 3D scene
	 * @param angle The angle with you can see the scene with your screen 
	 * @param ratio Ratio is the coefficient between width and height of your screen<br>
	 * 		Ratio of windows (often 4/3 ou 16/9) <br>
	 *            !! Caution, if you insert 4/3 = 1 because int/int => int, you
	 *            must cast one of value to float or double !!
	 * @param near
	 *            Value represent the part of 3D scene will be not display near
	 *            the camera
	 * @param far
	 *            Value represent the part of 3D scene will be not display far
	 *            the camera {@link source 
	 *            http://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml}
	 * @return The result Matrix
	 */
	public Projection perspective (float angle, float ratio, float near, float far);
}
