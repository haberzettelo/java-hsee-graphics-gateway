package com.hsee.gl.math;


import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.math.quaternion.Quaternion;

public interface MatrixTransform
{
	void translate(float x, float y, float z);
	void translate(Vector3 translation);
	
	void rotate(Quaternion rotation);
	void rotate(float angle, float x, float y, float z);
	void rotate(float angle, Vector3 angleVector);
	
	/**
	 * Modify the scale at specified values
	 * @param x Resize on x axe
	 * @param y Resize on y axe
	 * @param z Resize on z axe
	 */
	void scale (float x, float y, float z);
	
	/**
	 * Modify the scale at specified values
	 * @param scaleVector Scale vector
	 */
	void scale (Vector3 scaleVector);
	
	/**
	 * Modify the scale at specified values
	 * @param scale Scale 
	 */
	void scale (float scale);
	
	void updateMatrix(Matrix updateMatrix);
}
