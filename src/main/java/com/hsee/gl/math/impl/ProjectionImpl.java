package com.hsee.gl.math.impl;

import com.hsee.gl.math.Projection;
import com.hsee.gl.math.matrix.Matrix;

public class ProjectionImpl implements Projection
{
	protected Matrix _projection = null;
	protected static Projection _instance = null;
	
	public static Projection getInstance() {
		if (_instance == null)
			_instance = new ProjectionImpl();
		return _instance;
	}
	
	private ProjectionImpl (){
	}
	@Override
	public Matrix getMatrix (){
		return _projection;
	}

	@Override
	public Projection perspective (float angle, float ratio, float near, float far) {
		double angleRadians = angle * Math.PI / 180;
		float f = (float) (1 / (Math.tan(angleRadians / 2)));

		_projection = new Matrix(new float[] {f / ratio, 0, 0, 0, 0, f, 0, 0, 0, 0,
				(near + far) / (near - far), (2 * near * far) / (near - far),
				0, 0, -1, 0});
		
		return this;
	}

}
