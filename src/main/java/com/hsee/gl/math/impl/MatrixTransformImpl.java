package com.hsee.gl.math.impl;


import com.hsee.gl.math.MatrixTransform;
import com.hsee.gl.math.matrix.MathOperation;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.math.quaternion.Quaternion;

import static com.hsee.gl.math.matrix.MathOperation.*;

public class MatrixTransformImpl implements MatrixTransform
{
	protected Matrix _matrix;
	protected MatrixTransformImpl (Matrix mat)
	{
		_matrix = mat;
	}

	@Override
	public void translate (float x, float y, float z) {
		operation = new float[] {1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}

	@Override
	public void translate (Vector3 translation) {
		operation = new float[] {1, 0, 0, translation.getX(), 0, 1, 0, translation.getY(), 0, 0, 1, translation.getZ(), 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}

	@Override
	public void rotate (Quaternion rotation) {
		Matrix rotMat = rotation.toRotationMatrix();
		Matrix result = multiply4x4(_matrix, rotMat, new Matrix());
		_matrix.setValues(result.getValues());
	}

	@Override
	public void rotate (float angle, float x, float y, float z) {
		float cosTheta = (float) Math.cos(angle);
		float unMoinsCosTheta = 1 - cosTheta;
		float sinTheta = (float) Math.sin(angle);
		operation = new float[] {x * x * (unMoinsCosTheta) + cosTheta,
				x * y * (unMoinsCosTheta) - z * sinTheta,
				x * z * (unMoinsCosTheta) + y * sinTheta, 0f,
				y * x * (unMoinsCosTheta) + z * sinTheta,
				y * y * (unMoinsCosTheta) + cosTheta,
				y * z * (unMoinsCosTheta) - x * sinTheta, 0f,
				x * z * (unMoinsCosTheta) - y * sinTheta,
				y * z * (unMoinsCosTheta) + x * sinTheta,
				z * z * (unMoinsCosTheta) + cosTheta, 0f, 0f, 0f, 0f, 1f};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}

	@Override
	public void rotate (float angle, Vector3 angleVector) {
		float cosTheta = (float) Math.cos(angle);
		float unMoinsCosTheta = 1 - cosTheta;
		float sinTheta = (float) Math.sin(angle);
		operation = new float[] {angleVector.getX() * angleVector.getX() * (unMoinsCosTheta) + cosTheta,
				angleVector.getX() * angleVector.getY() * (unMoinsCosTheta) - angleVector.getZ() * sinTheta,
				angleVector.getX() * angleVector.getZ() * (unMoinsCosTheta) + angleVector.getY() * sinTheta, 0f,
				angleVector.getY() * angleVector.getX() * (unMoinsCosTheta) + angleVector.getZ() * sinTheta,
				angleVector.getY() * angleVector.getY() * (unMoinsCosTheta) + cosTheta,
				angleVector.getY() * angleVector.getZ() * (unMoinsCosTheta) - angleVector.getX() * sinTheta, 0f,
				angleVector.getX() * angleVector.getZ() * (unMoinsCosTheta) - angleVector.getY() * sinTheta,
				angleVector.getY() * angleVector.getZ() * (unMoinsCosTheta) + angleVector.getX() * sinTheta,
				angleVector.getZ() * angleVector.getZ() * (unMoinsCosTheta) + cosTheta, 0f, 0f, 0f, 0f, 1f};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MathOperation.multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}

	private float[] operation, tempResult;
	@Override
	public void scale (float x, float y, float z) {
		operation = new float[] {x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}

	@Override
	public void scale (Vector3 scaleVector) {
		operation = new float[] {scaleVector.getX(), 0, 0, 0, 0, scaleVector.getY(), 0, 0, 0, 0, scaleVector.getZ(), 0, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}

	@Override
	public void scale (float scale) {
		operation = new float[] {scale, 0, 0, 0, 
				0, scale, 0, 0, 
				0, 0, scale, 0, 
				0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		multiply4x4(_matrix.getValues(), operation, tempResult);
		_matrix.setValues(tempResult);
	}
	
	@Override
	public void updateMatrix (Matrix updateMatrix) {
		_matrix.setValues(updateMatrix.getValues());
	}
}
