package com.hsee.gl.math.interpolation;

public interface QuadricBezierCurveInterpolation<T extends Number> extends Interpolation
{
	public QuadricBezierCurveInterpolation<T> init(T begin);
	public QuadricBezierCurveInterpolation<T> addPoint(T controlPoint, T point);
}
