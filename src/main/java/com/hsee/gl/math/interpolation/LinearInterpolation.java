package com.hsee.gl.math.interpolation;

import com.hsee.gl.math.Valuable;

public interface LinearInterpolation<T extends Valuable> extends Interpolation
{
	public LinearInterpolation<T> addPoint(T point);
}
