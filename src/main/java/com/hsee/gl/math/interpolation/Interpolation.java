package com.hsee.gl.math.interpolation;

import com.hsee.gl.math.Valuable;
import com.hsee.gl.math.primitive.Vector4;


/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Represent all interpolations methods available.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 3 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Interpolation<T extends Valuable<? extends Number>> {
	/**
	 * Interpolate valuable between begin and end valuablee, coefficient cursor allow you to interpolate
	 * the result that you want.<br>
	 * When cursor=0, result=begin, when cursor=1, result=end
	 * @param begin Valuable represent the beginning of the interpolation
	 * @param end Vector represent the ending of the interpolation
	 * @param cursor Position of the cursor, between 0 and 1
	 * @return Interpolated vector computed
	 */
	public T mix(T begin, T end, double cursor);
}
