package com.hsee.gl.math.interpolation;

public interface CubicBezierCurveInterpolation<T extends Number> extends Interpolation
{
	public CubicBezierCurveInterpolation<T> init(T begin);
	public CubicBezierCurveInterpolation<T> addPoint(T controlPoint1, T controlPoint2, T point);
}
