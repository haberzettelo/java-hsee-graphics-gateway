package com.hsee.gl.math.interpolation;

public interface SplineInterpolation<T extends Number> extends Interpolation
{
	public SplineInterpolation<T> addPoint(T point, T velocity);
}
