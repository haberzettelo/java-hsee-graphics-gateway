package com.hsee.gl.math;

public interface Valuable<T extends Number>
{
	public T[] get();
}
