package com.hsee.gl.math;

import com.hsee.gl.math.matrix.Matrix;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * This interface represent the models contains a matrix.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Just call get matrix on object implement this interface to get this transformation
 * matrix.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 3 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface IHasMatrix {
	/**
	 * Get the transformation matrix of your object
	 * @return Transformation matrix
	 */
	public Matrix getMatrix ();
}
