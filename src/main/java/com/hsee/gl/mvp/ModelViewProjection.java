 package com.hsee.gl.mvp;

import com.hsee.gl.camera.View;
import com.hsee.gl.math.Projection;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Model;

public interface ModelViewProjection extends ModelViewProjectionListener, ShaderVariable<Matrix>
{
	public void setModel(Model model);
	public void setView(View view);
	public void setProjection(Projection proj);
}
