package com.hsee.gl.mvp.impl;

import java.util.Hashtable;
import java.util.Map;

import com.hsee.gl.camera.View;
import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.math.Projection;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Model;

import static com.hsee.gl.math.matrix.MathOperation.*;

public abstract class ModelViewProjectionImpl implements ModelViewProjection
{
	public static String MVP_NAME = VAR_PREFIX + "mvp", 
			NORM_MATRIX_NAME = VAR_PREFIX + "normalMatrix",
			MODEL_MATRIX_NAME = VAR_PREFIX + "modelMatrix", 
			VIEWMODEL_MATRIX_NAME = VAR_PREFIX + "modelViewMatrix";
	protected Matrix _model, _view, _projection, _mvp, _viewModel, _normalMatrix;
	protected Hashtable<ShaderProgram, int[]> _locations;
	
	protected boolean _mvpUpdated, _normalMatrixUpdated;
	
	public ModelViewProjectionImpl ()
	{
		_model = new Matrix();
		_view = new Matrix();
		_projection = new Matrix();
		_mvp = new Matrix();
		_viewModel = new Matrix();
		_normalMatrix = new Matrix();
		_mvpUpdated = true;
		_normalMatrixUpdated = true;
		_locations = new Hashtable<ShaderProgram, int[]>();
	}
	
	@Override
	public void update (Projection projection)
	{
		_projection.setValues(projection.getMatrix().getValues());
		_mvpUpdated = false;
	}

	@Override
	public void update (View view)
	{
		_view.setValues(view.getMatrix().getValues());
		_mvpUpdated = false;
	}

	@Override
	public void update (Model model)
	{
		_model.setValues(model.getMatrix().getValues());
		_mvpUpdated = false;
	}


	@Override
	public void setModel (Model model)
	{
		_model = model.getMatrix();
		_mvpUpdated = false;
	}

	@Override
	public void setView (View view)
	{
		_view = view.getMatrix();
		_mvpUpdated = false;
	}

	@Override
	public void setProjection (Projection proj)
	{
		_projection = proj.getMatrix();
		_mvpUpdated = false;
	}

	@Override
	public abstract ShaderVariable<Matrix> addShaderProgram (ShaderProgram program) throws HseeShaderStructureException;

	@Override
	public Matrix getValue ()
	{
		if (!_mvpUpdated){
			_viewModel = multiply4x4(_view, _model, _viewModel);
			_mvp = multiply4x4(_projection, _viewModel, _mvp);
			_mvpUpdated = true;
			_normalMatrixUpdated = false;
		}
		return _mvp;
	}

	@Override
	public ShaderVariable<Matrix> setValue (Matrix newValue)
	{
		_mvp = newValue;
		return this;
	}
	
	protected Matrix getNormalMatrix() {
		if (_normalMatrixUpdated)
			return _normalMatrix;
		_normalMatrixUpdated = true;
		Matrix.invert(_viewModel, _normalMatrix);
		return _normalMatrix;
	}
	
	protected Matrix getModelViewMatrix () {
		return _viewModel;
	}

	protected Matrix getModelMatrix() {
		return _model;
	}
	
	@Override
	public abstract ShaderVariable<Matrix> updateShaderData (ShaderProgram updater);

	@Override
	public ShaderVariable<Matrix> notifyUpdated ()
	{
		for (Map.Entry<ShaderProgram, int[]> entry : _locations.entrySet())
			entry.getKey().addShaderVariableToUpdate(this);
		return this;
	}

	public String toString () {
		return /*"Projection : \n" + _projection + "\n" + "View :\n" + _view + "\n" + "Model :\n" + _model + "\n" + "result :\n" + */_mvp + "";
	}
}
