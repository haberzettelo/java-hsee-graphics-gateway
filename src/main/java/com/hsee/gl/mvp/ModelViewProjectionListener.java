package com.hsee.gl.mvp;

import com.hsee.gl.camera.View;
import com.hsee.gl.math.Projection;
import com.hsee.gl.sprite.Model;

public interface ModelViewProjectionListener
{
	public void update(Projection projection);
	public void update(View view);
	public void update(Model model);
}
