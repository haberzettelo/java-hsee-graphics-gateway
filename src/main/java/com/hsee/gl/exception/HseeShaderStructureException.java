package com.hsee.gl.exception;

public class HseeShaderStructureException extends HseeException
{

	/**
	 * 
	 */
	private static final long	serialVersionUID	= -781028930261841125L;

	public HseeShaderStructureException (String message)
	{
		super(message);
	}

}
