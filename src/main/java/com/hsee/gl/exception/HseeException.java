package com.hsee.gl.exception;

public class HseeException extends Throwable{
	protected int _errorCode = 0;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HseeException (String message)
	{
		super(message);
	}
}
