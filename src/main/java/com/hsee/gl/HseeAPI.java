package com.hsee.gl;

import com.hsee.gl.light.DirectionalLight;
import com.hsee.gl.light.PointLight;
import com.hsee.gl.light.SpotLight;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderFactory;
import com.hsee.gl.shader.ShaderVariableFactory;
import com.hsee.gl.shader.impl.ShaderProgramFactory;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;

@SuppressWarnings("rawtypes")
public interface HseeAPI {
	
	
	DirectionalLight createDirectionalLight(String shaderVariableName);
	PointLight createPointLight(String shaderVariableName);
	SpotLight createSpotLight(String shaderVariableName);
	
	ShaderFactory getShaderFactory();
	ShaderProgramFactory getShaderProgramFactory();
	ShaderVariableFactory getShaderVariableFactory();
	
	AbstractCube createCube(String name);
	AbstractPlan createPlan(String name, int size);
	
	Sprite createSprite(ImporterSetter importer, SpriteCompiler compiler, String name, String path, String filename);
	Sprite createSprite(ImporterSetter importer, SpriteCompiler compiler, InputStreamGetter reader, String name, String path, String filename);
	ModelViewProjection createModelViewProjection();
	
	
	GraphicsGateway getGraphicsGateway();
	Object getUnderlayedAPI();
}
