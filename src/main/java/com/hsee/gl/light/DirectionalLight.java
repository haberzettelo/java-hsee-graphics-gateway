package com.hsee.gl.light;

import com.hsee.gl.camera.Camera;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.ShaderVariable;

public interface DirectionalLight extends Light
{
	public static String TYPENAMEDIRECTIONAL = ShaderVariable.TYPE_PREFIX + "DirectionalLight",
			VAR_DIRECTION = "direction", VAR_HALF = "halfVector";
	
	public DirectionalLight setDirection(Vector3 dir);
	public DirectionalLight updateHalfVector(Camera view);
}
