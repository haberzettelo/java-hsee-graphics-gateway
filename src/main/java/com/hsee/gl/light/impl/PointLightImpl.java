package com.hsee.gl.light.impl;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.light.PointLight;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.jogamp.common.nio.Buffers;

public abstract class PointLightImpl extends LightImpl implements PointLight
{
	static List<Shader> _structImporter = new ArrayList<Shader>();
	protected FloatBuffer _diffuse, _specular, _position;
	protected float _attConstant, _attLinear, _attExpo;
	protected int _specLoc, _posLoc, _diffLoc, _attConstLoc, _attLineLoc, _attExpLoc;
	
	public PointLightImpl (String name) {
		super(name);
		_diffuse = Buffers.newDirectFloatBuffer(3);
		_specular = Buffers.newDirectFloatBuffer(3);
		_position = Buffers.newDirectFloatBuffer(3);
		_attConstant = 0;
		_attLinear = 2;
		_attExpo = 0f;
	}

	@Override
	public PointLight setDiffuseColor (Vector3 color) {
		_diffuse.rewind();
		_diffuse.put(color.getCoordinates());
		return this;
	}

	@Override
	public PointLight setSpecularColor (Vector3 color) {
		_specular.rewind();
		_specular.put(color.getCoordinates());
		return this;
	}
	
	@Override
	public PointLight setPosition (Vector3 dir) {
		_position.rewind();
		_position.put(dir.getCoordinates());
		return this;
	}
	
	@Override
	public PointLight setAttenuationConstant (float attenuation) {
		_attConstant = attenuation;
		return this;
	}
	
	@Override
	public PointLight setAttenuationLinear (float attenuation) {
		_attLinear = attenuation;
		return this;
	}
	
	@Override
	public PointLight setAttenuationExp (float attenuation) {
		_attExpo = attenuation;
		return this;
	}
	
	
	
	@Override
	protected void initLocationsByApi (ShaderProgram updater) {
		String nameP = _name + ".";
		_ambLoc = updater.getUniformLocation(nameP + VAR_AMBIENT);
		_diffLoc = updater.getUniformLocation(nameP + VAR_DIFFUSECOLOR);
		_posLoc = updater.getUniformLocation(nameP + VAR_POSITION);
		_specLoc = updater.getUniformLocation(nameP + VAR_SPECULARCOLOR);
		_attConstLoc = updater.getUniformLocation(nameP + VAR_ATT_CONST);
		_attLineLoc = updater.getUniformLocation(nameP + VAR_ATT_LIN);
		_attExpLoc = updater.getUniformLocation(nameP + VAR_ATT_EXP);
		//_halfLoc = updater.getLocation(nameP + VAR_CAMPOS);
		_prgs.add(updater);
		if (_diffLoc < 0 || _posLoc < 0 || _specLoc < 0 ||
				_attConstLoc < 0 || _attLineLoc < 0 || _attExpLoc < 0 || _ambLoc < 0)
			System.err.println("Directional light initialized \n" + nameP + VAR_DIFFUSECOLOR + ":" + _diffLoc + "\n" + nameP + VAR_POSITION + ":" + _posLoc + "\n" + nameP + VAR_SPECULARCOLOR + ":" + _specLoc
					+ nameP + VAR_ATT_CONST + ":" + _attConstLoc + "\n" + nameP + VAR_ATT_LIN + ":" + _attLineLoc + "\n" + nameP + VAR_ATT_EXP + ":" + _attExpLoc + "\n" + nameP + VAR_AMBIENT + ":" + _ambLoc
					/* + "\n" + nameP + VAR_CAMPOS + ":" + _halfLoc*/);
	}
	
	@Override
	protected List<Shader> getStaticListOfShaderIncludeStruct () {
		return _structImporter;
	}
}
