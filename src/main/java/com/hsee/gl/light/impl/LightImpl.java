package com.hsee.gl.light.impl;

import java.util.List;

import com.hsee.gl.light.Light;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;

public abstract class LightImpl extends AmbientImpl implements Light
{
	public LightImpl (String name) {
		super(name);
	}

	@Override
	public Light setDiffuseColor (Vector3 color) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Light setSpecularColor (Vector3 color) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	protected abstract List<Shader> getStaticListOfShaderIncludeStruct ();
}
