package com.hsee.gl.light.impl;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.light.Ambient;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.impl.structure.ShaderStructureImpl;
import com.jogamp.common.nio.Buffers;

public abstract class AmbientImpl extends ShaderStructureImpl implements Ambient
{
	private static List<Shader> _listOfShaderIncludeStruct = new ArrayList<Shader>();
	protected String _name;
	protected int _ambLoc;
	protected FloatBuffer _ambientColor;
	public AmbientImpl (String name)
	{
		_name = name;
		_ambientColor = Buffers.newDirectFloatBuffer(3);
	}

	@Override
	public Ambient setAmbiantColor (Vector3 color) {
		_ambientColor.rewind();
		_ambientColor.put(color.getCoordinates());
		return this;
	}

	
	//@Override
	public ShaderVariable updateShaderData (ShaderProgram updater) {
		if (!_prgs.contains(updater)) {
			initLocationsByApi(updater);
			_prgs.add(updater);
		}
		updateShaderDataByApi(updater);
		return this;
	}
	
	@Override
	public void initShader (Shader shader) {
		if (!getStaticListOfShaderIncludeStruct().contains(shader)) {
			getStaticListOfShaderIncludeStruct().add(shader);
			initShaderByApi(shader);
		}
	}
	
	protected abstract void initShaderByApi(Shader shader);
	
	protected abstract void updateShaderDataByApi(ShaderProgram updater); 
	protected abstract void initLocationsByApi(ShaderProgram updater); 
	
	protected List<Shader> getStaticListOfShaderIncludeStruct() {
		return _listOfShaderIncludeStruct;
	}
}
