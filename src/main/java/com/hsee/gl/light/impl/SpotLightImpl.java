package com.hsee.gl.light.impl;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.light.SpotLight;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.jogamp.common.nio.Buffers;

public abstract class SpotLightImpl extends PointLightImpl implements SpotLight
{
	static List<Shader> _structImporterSpot = new ArrayList<Shader>();
	

	protected FloatBuffer _coneDirection;
	protected float _spotCutOff, _spotExponent;
	protected int _coneLoc, _spotCutLoc, _spotExpLoc;
	public SpotLightImpl (String name) {
		super(name);
		_coneDirection = Buffers.newDirectFloatBuffer(3);
	}

	@Override
	public SpotLight setSpotCutoff (float alpha) {
		_spotCutOff = alpha;
		return this;
	}

	@Override
	public SpotLight setSpotExponent (float alpha) {
		_spotExponent = alpha;
		return this;
	}

	@Override
	public SpotLight setConeDirection (Vector3 direction) {
		_coneDirection.rewind();
		_coneDirection.put(direction.getCoordinates());
		return this;
	}
	
	@Override
	protected void initLocationsByApi (ShaderProgram updater) {
		super.initLocationsByApi(updater);
		String nameP = _name + ".";
		_coneLoc = updater.getUniformLocation(nameP + VAR_CONE_DIR);
		_spotCutLoc = updater.getUniformLocation(nameP + VAR_CUT_OFF);
		_spotExpLoc = updater.getUniformLocation(nameP + VAR_EXPONENT);
		//_halfLoc = updater.getLocation(nameP + VAR_CAMPOS);
		_prgs.add(updater);
		if (_coneLoc < 0 || _spotCutLoc < 0 || _spotExpLoc < 0)
			System.err.println("Spot light initialized \n" + nameP + VAR_CONE_DIR + ":" + _coneLoc + "\n" + nameP + VAR_CUT_OFF + ":" + _spotCutLoc + "\n" + nameP + VAR_EXPONENT + ":" + _spotExpLoc);
	}

	@Override
	protected List<Shader> getStaticListOfShaderIncludeStruct () {
		return _structImporterSpot;
	}
}
