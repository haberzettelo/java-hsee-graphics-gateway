package com.hsee.gl.light.impl;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.camera.Camera;
import com.hsee.gl.light.DirectionalLight;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.jogamp.common.nio.Buffers;

public abstract class DirectionalLightImpl extends LightImpl implements DirectionalLight
{
	static List<Shader> _structImporter = new ArrayList<Shader>();

	protected int _dirLoc, _specLoc, _diffLoc, _halfLoc;
	protected FloatBuffer _diffuseColor, _specularColor, _direction, _halfvector;
	protected Vector3 temp = new Vector3();
	
	public DirectionalLightImpl (String name)
	{
		super(name);
		_diffuseColor = Buffers.newDirectFloatBuffer(3);
		_specularColor = Buffers.newDirectFloatBuffer(3);
		_direction = Buffers.newDirectFloatBuffer(3);
		_halfvector = Buffers.newDirectFloatBuffer(3);
	}
	
	@Override
	public DirectionalLight setDiffuseColor (Vector3 color) {
		_diffuseColor.rewind();
		_diffuseColor.put(color.getCoordinates());
		return this;
	}

	@Override
	public DirectionalLight setSpecularColor (Vector3 color) {
		_specularColor.rewind();
		_specularColor.put(color.getCoordinates());
		return this;
	}
	
	/*@Override
	public Light setHalfVector (Vector3 halfVector) {
		_halfvector.rewind();
		_halfvector.put(halfVector.getCoordinates());
		return this;
	}*/
/*
	@Override
	public final ShaderVariable updateShaderData (ShaderProgram updater) {
		if (!_prgs.contains(updater)) {
			initLocationsByApi(updater);
			_prgs.add(updater);
		}
		updateShaderDataByApi(updater);
		return this;
	}*/

	@Override
	public DirectionalLight setDirection (Vector3 dir) {
		_direction.rewind();
		dir.opposite(temp);
		_direction.put(temp.getCoordinates());
		return this;
	}
	
	@Override
	protected void initLocationsByApi (ShaderProgram updater) {
		String nameP = _name + ".";
		_ambLoc = updater.getUniformLocation(nameP + VAR_AMBIENT);
		_diffLoc = updater.getUniformLocation(nameP + VAR_DIFFUSECOLOR);
		_dirLoc = updater.getUniformLocation(nameP + VAR_DIRECTION);
		_specLoc = updater.getUniformLocation(nameP + VAR_SPECULARCOLOR);
		_halfLoc = updater.getUniformLocation(nameP + VAR_HALF);
		
		if (_ambLoc < 0 || _diffLoc < 0 || _dirLoc < 0 || _specLoc < 0 || _halfLoc < 0)
			System.err.println("Directional light initialized \n" + nameP + VAR_DIFFUSECOLOR + ":" + _diffLoc + "\n" + nameP + VAR_DIRECTION + ":" + _dirLoc + "\n" + nameP + VAR_SPECULARCOLOR + ":" + _specLoc + "\n" + nameP + VAR_HALF + ":" + _halfLoc + "\n" + nameP + VAR_AMBIENT + ":" + _ambLoc);
	}

	@Override
	protected List<Shader> getStaticListOfShaderIncludeStruct () {
		return _structImporter;
	}
	
	@Override
	public DirectionalLight updateHalfVector (Camera view) {
		Vector3 eye = view.getPosition().sub(view.getLookPoint());
		eye.sub(temp.set(_direction.get(0), _direction.get(1), _direction.get(2)));
		_halfvector.rewind();
		_halfvector.put(eye.getCoordinates());
		return this;
	}
}
