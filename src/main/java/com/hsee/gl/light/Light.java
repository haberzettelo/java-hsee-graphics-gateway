package com.hsee.gl.light;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.structure.ShaderStructure;

public interface Light extends ShaderStructure, Ambient{
	public static String VAR_DIFFUSECOLOR = "diffuse",
			VAR_SPECULARCOLOR = "specular";
	public Light setDiffuseColor(Vector3 color);
	public Light setSpecularColor(Vector3 color);
}
