package com.hsee.gl.light;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.ShaderVariable;

public interface PointLight extends Light
{
	public static String TYPENAMEPOINT = ShaderVariable.TYPE_PREFIX + "PointLight", VAR_POSITION = "position",
			VAR_ATT_CONST = "attConstant", VAR_ATT_LIN = "attLinear", VAR_ATT_EXP = "attExponential";
	public PointLight setPosition(Vector3 dir);
	public PointLight setAttenuationConstant(float attenuation);
	public PointLight setAttenuationLinear(float attenuation);
	public PointLight setAttenuationExp(float attenuation);
}
