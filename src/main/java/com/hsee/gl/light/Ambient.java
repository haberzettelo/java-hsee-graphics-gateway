package com.hsee.gl.light;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.structure.ShaderStructure;

public interface Ambient extends ShaderStructure
{
	public static String TYPENAMEAMBIENT = ShaderVariable.TYPE_PREFIX + "AmbientLight", VAR_AMBIENT = "ambient";
	public Ambient setAmbiantColor(Vector3 color);
}
