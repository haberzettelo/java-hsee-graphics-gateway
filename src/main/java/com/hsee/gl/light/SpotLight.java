package com.hsee.gl.light;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.ShaderVariable;

public interface SpotLight extends PointLight
{
	public static String TYPENAMESPOT = ShaderVariable.TYPE_PREFIX + "SpotLight",
			VAR_CONE_DIR = VAR_PREFIX + "coneDirection",
			VAR_CUT_OFF = VAR_PREFIX + "cutoff",
			VAR_EXPONENT = VAR_PREFIX + "exponent";
	/**
	 * Set the angle of your spot light
	 * @param alpha Max value visible 1.166185f
	 * @return Chaining
	 */
	public SpotLight setSpotCutoff(float alpha);
	
	/**
	 * Set the exponent of your spot light, correspond to the concentration of the light at the middle of the cone
	 * @param alpha
	 * @return Chaining
	 */
	public SpotLight setSpotExponent(float exp);
	
	/**
	 * Set the direction of the cone spot light
	 * @param direction
	 * @return Chaining
	 */
	public SpotLight setConeDirection(Vector3 direction);
}
