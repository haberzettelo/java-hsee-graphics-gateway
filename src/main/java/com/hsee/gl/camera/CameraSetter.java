package com.hsee.gl.camera;

import com.hsee.gl.math.primitive.Vector3;

/** 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * It's the setter of the camera, allow you to modify your camera instance.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 3 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface CameraSetter extends Camera
{
	/**
	 * Set new position vector of your camera
	 * @param newPosition Position vector
	 */
	public void setPosition(Vector3 newPosition);
	
	/**
	 * Set new direction vector of your camera
	 * @param newDirection Direction vector
	 */
	public void setLookPoint(Vector3 newDirection);
	
	/**
	 * Set new up vector
	 * @param newUp Up vector
	 */
	public void setUp(Vector3 newUp);
}
