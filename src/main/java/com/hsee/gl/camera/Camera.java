package com.hsee.gl.camera;

import com.hsee.gl.math.primitive.Vector3;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Camera is an eye on your scene. 
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 3 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Camera extends View {
	/**
	 * Get the current position of the camera
	 * @return Copy of the position vector
	 */
	public Vector3 getPosition();
	
	/**
	 * Get the current eye direction of the camera
	 * @return Copy of the eye direction vector
	 */
	public Vector3 getLookPoint();
	
	/**
	 * Get the current up vector
	 * @return Copy of the up vector
	 */
	public Vector3 getUp();
}
