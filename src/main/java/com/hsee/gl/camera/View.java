package com.hsee.gl.camera;

import com.hsee.gl.math.IHasMatrix;
import com.hsee.gl.math.MatrixTransform;

public interface View extends IHasMatrix, MatrixTransform
{

}
