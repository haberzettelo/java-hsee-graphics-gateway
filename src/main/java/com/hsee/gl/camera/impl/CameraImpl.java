package com.hsee.gl.camera.impl;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;

import com.hsee.gl.camera.CameraSetter;
import com.hsee.gl.math.impl.MatrixTransformImpl;
import com.hsee.gl.math.matrix.MathOperation;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.math.primitive.Vector3;

public class CameraImpl extends MatrixTransformImpl implements CameraSetter
{
	protected FloatBuffer _eyeCenterUp;
	//protected Matrix _view;
	protected boolean _upToDate;
	
	public CameraImpl ()
	{
		super(new Matrix());
		_eyeCenterUp = ByteBuffer.allocateDirect(36).asFloatBuffer();
		_upToDate = true;
		_matrix = new Matrix();
	}
	
	/**
	 * Initialize camera
	 * @param eyeX Position of camera on x axis
	 * @param eyeY Position of camera on y axis
	 * @param eyeZ Position of camera on z axis
	 * @param centerX Point look by camera
	 * @param centerY Point look by camera
	 * @param centerZ Point look by camera
	 * @param upX Vertical axis for camera
	 * @param upY Vertical axis for camera
	 * @param upZ Vertical axis for camera
	 */
	protected void lookAt (float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ) {
		float[] regard = new float[3], normal = new float[3], newAxe = new float[3];
		//Calcul de regard : center - eye
		regard = new float[]{centerX - eyeX, centerY - eyeY, centerZ - eyeZ};
		//Calcul de normal : regard /\ up
		MathOperation.normalizeVec3(regard);
		MathOperation.crossProductVec3(regard, new float[] {upX ,upY, upZ}, normal);
		//Calcul de newAxe : normal /\ regard
		MathOperation.normalizeVec3(normal);
		MathOperation.crossProductVec3(normal, regard, newAxe);
		
		
		_matrix.setValues(new float[] {normal[0], 	normal[1], 	normal[2], 	0,
									newAxe[0], 	newAxe[1], 	newAxe[2], 	0,
									-regard[0], -regard[1], -regard[2], 0,
									0, 				0, 				0, 	1});
		
		translate(-eyeX, -eyeY, -eyeZ);
	}
	
	private Vector3 _temp = new Vector3(), _temp2 = new Vector3(), _temp3 = new Vector3();
	
	@Override
	public Vector3 getPosition ()
	{
		return _temp.set(_eyeCenterUp.get(0), _eyeCenterUp.get(1), _eyeCenterUp.get(2));
	}

	@Override
	public Vector3 getLookPoint ()
	{
		return _temp2.set(_eyeCenterUp.get(3), _eyeCenterUp.get(4), _eyeCenterUp.get(5));
	}

	@Override
	public Vector3 getUp ()
	{
		return _temp3.set(_eyeCenterUp.get(6), _eyeCenterUp.get(7), _eyeCenterUp.get(8));
	}

	@Override
	public Matrix getMatrix ()
	{
		if (!_upToDate) {
			_eyeCenterUp.rewind();
			lookAt(_eyeCenterUp.get(), _eyeCenterUp.get(), _eyeCenterUp.get(), 
					_eyeCenterUp.get(), _eyeCenterUp.get(), _eyeCenterUp.get(), +
					_eyeCenterUp.get(), _eyeCenterUp.get(), _eyeCenterUp.get());
			_upToDate = true;
		}
		return _matrix;
	}

	@Override
	public void setPosition (Vector3 newPosition)
	{
		_eyeCenterUp.position(0);
		_eyeCenterUp.put(newPosition.getCoordinates(), 0, 3);
		_upToDate = false;
	}

	@Override
	public void setLookPoint (Vector3 newDirection)
	{
		_eyeCenterUp.position(3);
		_eyeCenterUp.put(newDirection.getCoordinates(), 0, 3);
		_upToDate = false;
	}

	@Override
	public void setUp (Vector3 newUp)
	{
		_eyeCenterUp.position(6);
		_eyeCenterUp.put(newUp.getCoordinates(), 0, 3);
		_upToDate = false;
	}
	
	
}
