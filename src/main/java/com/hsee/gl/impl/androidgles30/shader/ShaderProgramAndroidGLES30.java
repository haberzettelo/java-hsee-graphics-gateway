package com.hsee.gl.impl.androidgles30.shader;

import android.opengl.GLES30;
import android.opengl.GLES30;
import android.util.Log;

import com.hsee.gl.exception.HseeProgramShaderException;
import com.hsee.gl.impl.androidgles30.exception.HseeProgramShaderExceptionAbdroidgles30;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderProgramPipeline;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.impl.ShaderProgramImpl;
import com.jogamp.common.nio.Buffers;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static android.opengl.GLES30.*;

public abstract class ShaderProgramAndroidGLES30 extends ShaderProgramImpl
{
	protected boolean separable;
	protected static IntBuffer _temp = Buffers.newDirectIntBuffer(1);

	ShaderProgramAndroidGLES30(GLES30 gl)
	{
		super();
		_programId = GLES30.glCreateProgram();
	}

	@Override
	public int getUniformLocation (String varName)
	{
		return GLES30.glGetUniformLocation(_programId, varName);
	}

	@Override
	public int[] getUniformLocation (String[] varsName)
	{
		int[] result = new int[varsName.length];
		for (int i = 0, max = varsName.length; i < max; i++)
			result[i] = GLES30.glGetUniformLocation(_programId, varsName[i]);
		return result;
	}

	@Override
	protected ShaderProgram attachShaderByApi (Shader shader)
	{
		GLES30.glAttachShader(_programId, shader.getShaderId());
		
		return this;
	}

	@Override
	protected ShaderProgram detachShaderByApi (Shader shader)
	{
		GLES30.glDetachShader(_programId, shader.getShaderId());
		return this;
	}
	
	@Override
	public ShaderProgram link () throws HseeProgramShaderException {
		
        // Link the two shaders together into a program.
        glLinkProgram(_programId);

        // Get the link status.
        final int[] linkStatus = new int[1];
        glGetProgramiv(_programId, GL_LINK_STATUS, linkStatus, 0);

            // Print the program info log to the Android log output.
            Log.v("", "Results of linking program:\n" 
                + glGetProgramInfoLog(_programId));		

        // Verify the link status.
        if (linkStatus[0] == 0) {
            // If it failed, delete the program object.
            glDeleteProgram(_programId);
            Log.w("", "Linking of program failed.");
        }
        
        return this;
		
		/*GLES30.glLinkProgram(_programId);
		if (!getStatus(GLES30.GL_LINK_STATUS)) {
			int error;
			if ((error = GLES30.glGetError()) != GLES30.GL_NO_ERROR) {
				throw HseeProgramShaderExceptionAbdroidgles30.getLinkError(error, _programId);
			}
			throw new HseeProgramShaderException("Error while linking : " + getLogMessage());
		}
		int error;
		
		GLES30.glValidateProgram(_programId);
		if (!getStatus(GLES30.GL_VALIDATE_STATUS)) {
			if ((error = GLES30.glGetError()) != GLES30.GL_NO_ERROR) {
				throw HseeProgramShaderExceptionAbdroidgles30.getValidateError(error, _programId);
			}
			throw new HseeProgramShaderException("Error while validate : " + getLogMessage());
		}
		return this;*/
		
	}

	@Override
	protected int getAttribLocationByApi (String varName)
	{
		return GLES30.glGetAttribLocation(_programId, varName);
	}
	
	@Override
	public int getStorageBufferLocation (String ssbName) {
		// Get blockname to bytebuffer
		// TODO Adapt to version
		/*ByteBuffer nameBuf = StandardCharsets.US_ASCII.encode(ssbName);
		nameBuf.rewind();

		return GLES30.glGetProgramResourceIndex(_programId, GLES30.GL_SHADER_STORAGE_BLOCK, nameBuf);*/
		return -1;
	}
	
	@Override
	protected void finalize ()
	{
		// TODO Auto-generated method stub
		
	}
	
	private boolean getStatus(int param) {
		_temp.rewind();
		GLES30.glGetProgramiv(_programId, param, _temp);
		if (_temp.get(0) == GLES30.GL_FALSE)
			return false;
		return true;
	}
	
	private String getLogMessage() {
		/*_temp.rewind();
		GLES30.glGetProgramiv(_programId, GLES30.GL_INFO_LOG_LENGTH, _temp);
		System.out.println("sizeOfBuffer + " + _temp.get(0));
		ByteBuffer byteBuffer = Buffers.newDirectByteBuffer(_temp.get(0));*/
		String error = "";
		error = GLES30.glGetProgramInfoLog(_programId);

		return error;
	}
}

class ShaderProgramSimpleAndroidGLES30 extends ShaderProgramAndroidGLES30 {

	ShaderProgramSimpleAndroidGLES30(GLES30 gl)
	{
		super(gl);
	}

	@Override
	public boolean isSeparable ()
	{
		return false;
	}

	@Override
	protected ShaderProgram useProgramByApi ()
	{
		GLES30.glUseProgram(_programId);
		return this;
	}

	@Override
	protected ShaderProgram updateShaderDataByApi (@SuppressWarnings("rawtypes") ShaderVariable var)
	{
		var.updateShaderData(this);
		return this;
	}
}

class ShaderProgramSeparableAndroidGLES30 extends ShaderProgramAndroidGLES30 {
	ShaderProgramPipeline pipeline;

	ShaderProgramSeparableAndroidGLES30(GLES30 gl)
	{
		super(gl);
		// TODO Adapt
		//GLES30.glProgramParameteri(_programId, GLES30.GL_PROGRAM_SEPARABLE, GLES30.GL_TRUE);
		pipeline = null;
	}

	@Override
	public boolean isSeparable ()
	{
		return true;
	}

	@Override
	protected ShaderProgram useProgramByApi ()
	{
		// Don't bind, the pipeline program must be take the hand
		GLES30.glUseProgram(0);
		return this;
	}

	ShaderProgramSeparableAndroidGLES30 setShaderProgramPipeline (ShaderProgramPipeline pipeline) {
		this.pipeline = pipeline;
		return this;
	}
	
	@Override
	protected ShaderProgram updateShaderDataByApi (@SuppressWarnings("rawtypes") ShaderVariable var)
	{
		// TODO Adapt
		/*if (pipeline != null)
			GLES30.glActiveShaderProgram(pipeline.getShaderProgramPipelineId(), _programId);
		var.updateShaderData(this);*/
		return this;
	}
	
}
