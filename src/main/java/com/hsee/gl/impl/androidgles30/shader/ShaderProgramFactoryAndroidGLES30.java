package com.hsee.gl.impl.androidgles30.shader;

import android.opengl.GLES30;

import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.impl.ShaderProgramFactory;

public class ShaderProgramFactoryAndroidGLES30 implements ShaderProgramFactory
{
	public static ShaderProgramFactoryAndroidGLES30 factory = null;
	public static void initFactory(GLES30 ctx) {
		factory = new ShaderProgramFactoryAndroidGLES30(ctx);
	}
	
	private ShaderProgramFactoryAndroidGLES30 (GLES30 context) {
	}
	@Override
	public ShaderProgram newProgram ()
	{
		return new ShaderProgramSimpleAndroidGLES30(null);
	}

	@Override
	public ShaderProgram newSeparateProgram ()
	{
		return new ShaderProgramSimpleAndroidGLES30(null);
	}

}
