package com.hsee.gl.impl.androidgles30;

import android.opengl.GLES30;

import com.hsee.gl.GraphicsGateway;

public class GraphicsGatewayGLES30 implements GraphicsGateway {

	@Override
	public GraphicsGateway clearColor(float red, float green, float blue,
			float alpha) {
		GLES30.glClearColor(red, green, blue, alpha);
		return this;
	}

	@Override
	public GraphicsGateway clear(int target) {
		GLES30.glClear(target);
		return this;
	}

	@Override
	public GraphicsGateway enable(int targets) {
		GLES30.glEnable(targets);
		return this;
	}

	@Override
	public GraphicsGateway flush() {
		GLES30.glFlush();
		return this;
	}

	@Override
	public GraphicsGateway blendFunc(int sourceFactor, int destinationFactor) {
		GLES30.glBlendFunc(sourceFactor, destinationFactor);
		return this;
	}

}
