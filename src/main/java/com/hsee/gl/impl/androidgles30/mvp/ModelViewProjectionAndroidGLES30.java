package com.hsee.gl.impl.androidgles30.mvp;

import android.opengl.GLES30;
import android.opengl.GLES30;

import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.mvp.impl.ModelViewProjectionImpl;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;

public class ModelViewProjectionAndroidGLES30 extends ModelViewProjectionImpl
{
	public ModelViewProjectionAndroidGLES30(GLES30 gl)
	{
	}

	@Override
	public ShaderVariable<Matrix> addShaderProgram (ShaderProgram program) throws HseeShaderStructureException  
	{
		int location = GLES30.glGetUniformLocation(program.getProgramId(), MVP_NAME), 
				locationNormal = GLES30.glGetUniformLocation(program.getProgramId(), NORM_MATRIX_NAME),
				locationModel = GLES30.glGetUniformLocation(program.getProgramId(), MODEL_MATRIX_NAME),
				locationViewModel = GLES30.glGetUniformLocation(program.getProgramId(), VIEWMODEL_MATRIX_NAME);
		if (location < 0)
			throw new HseeShaderStructureException("Variable \"" + MVP_NAME + "\" was not found in program id : " + program.getProgramId());
		_locations.put(program, new int[] {location, locationNormal, locationModel, locationViewModel});
		return this;
	}

	@Override
	public ShaderVariable<Matrix> updateShaderData (ShaderProgram updater)
	{
		int[] index = _locations.get(updater);
		GLES30.glUniformMatrix4fv(index[0], 1, false, getValue().getValues(), 0);
		
		if (index[1] > -1)
			GLES30.glUniformMatrix4fv(index[1], 1, false, getNormalMatrix().getValues(), 0);
		if (index[2] > -1)
			GLES30.glUniformMatrix4fv(index[2], 1, false, getModelMatrix().getValues(), 0);
		if (index[3] > -1)
			GLES30.glUniformMatrix4fv(index[3], 1, false, getModelViewMatrix().getValues(), 0);
		
		return this;
	}

}
