package com.hsee.gl.impl.androidgles30.sprite.shape;

import android.opengl.GLES30;

import com.hsee.gl.impl.androidgles30.sprite.renderer.SpriteRendererAndroidGLES30;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public class Cube extends AbstractCube<GLES30>
{
	public Cube (GLES30 g, String name)
	{
		super(null, name);
	}

	@Override
	protected SpriteRenderer<GLES30> newRendererByApi (GLES30 g) {
		return new SpriteRendererAndroidGLES30();
	}

}
