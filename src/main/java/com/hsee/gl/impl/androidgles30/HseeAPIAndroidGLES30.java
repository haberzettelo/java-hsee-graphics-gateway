package com.hsee.gl.impl.androidgles30;

import android.opengl.GLES30;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;
import com.hsee.gl.impl.androidgles30.light.DirectionalLightAndroidGLES30;
import com.hsee.gl.impl.androidgles30.light.PointLightAndroidGLES30;
import com.hsee.gl.impl.androidgles30.light.SpotLightAndroidGLES30;
import com.hsee.gl.impl.androidgles30.mvp.ModelViewProjectionAndroidGLES30;
import com.hsee.gl.impl.androidgles30.shader.ShaderFactoryAndroidGLES30;
import com.hsee.gl.impl.androidgles30.shader.ShaderProgramFactoryAndroidGLES30;
import com.hsee.gl.impl.androidgles30.shader.ShaderVariableFactoryAndroidGLES30;
import com.hsee.gl.impl.androidgles30.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.impl.androidgles30.sprite.io.SpriteImporterAndroidGLES30;
import com.hsee.gl.impl.androidgles30.sprite.shape.Cube;
import com.hsee.gl.impl.androidgles30.sprite.shape.Plan;
import com.hsee.gl.light.DirectionalLight;
import com.hsee.gl.light.PointLight;
import com.hsee.gl.light.SpotLight;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderFactory;
import com.hsee.gl.shader.ShaderVariableFactory;
import com.hsee.gl.shader.impl.ShaderProgramFactory;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;

@SuppressWarnings ("rawtypes")
public class HseeAPIAndroidGLES30 implements HseeAPI {
	private GLES30 g;
	private GraphicsGatewayGLES30 ggateway;
	public HseeAPIAndroidGLES30() {
		g = null;//(GLES30)javax.microedition.khronos.egl.EGLContext.getEGL();
		
		ggateway = new GraphicsGatewayGLES30();
		ShaderFactoryAndroidGLES30.initFactory(g);
		ShaderVariableFactoryAndroidGLES30.initFactory(g);
		ShaderProgramFactoryAndroidGLES30.initFactory(g);
		
		MaterialSSB.initMaterialSsb(g);
	}
	
	@Override
	public DirectionalLight createDirectionalLight(String shaderVariableName) {
		return new DirectionalLightAndroidGLES30(g, shaderVariableName);
	}

	@Override
	public PointLight createPointLight(String shaderVariableName) {
		return new PointLightAndroidGLES30(g, shaderVariableName);
	}

	@Override
	public SpotLight createSpotLight(String shaderVariableName) {
		return new SpotLightAndroidGLES30(g, shaderVariableName);
	}

	@Override
	public ShaderFactory getShaderFactory() {
		return ShaderFactoryAndroidGLES30.factory;
	}

	@Override
	public ShaderProgramFactory getShaderProgramFactory() {
		return ShaderProgramFactoryAndroidGLES30.factory;
	}

	@Override
	public ShaderVariableFactory getShaderVariableFactory() {
		return ShaderVariableFactoryAndroidGLES30.factory;
	}

	@Override
	public AbstractCube createCube(String name) {
		return new Cube(g, name);
	}

	@Override
	public AbstractPlan createPlan(String name, int size) {
		return new Plan(g, name, size);
	}

	@Override
	public Sprite createSprite(ImporterSetter importer,
			SpriteCompiler compiler, String name, String path, String filename) {
		return new SpriteImporterAndroidGLES30(g, importer, compiler, name, path, filename);
	}
	@Override
	public Sprite createSprite (ImporterSetter importer,
			SpriteCompiler compiler, InputStreamGetter reader, String name,
			String path, String filename) {
		return new SpriteImporterAndroidGLES30(g, importer, compiler, reader, name, path, filename);
	}

	@Override
	public ModelViewProjection createModelViewProjection() {
		return new ModelViewProjectionAndroidGLES30(g);
	}

	@Override
	public Object getUnderlayedAPI() {
		return g;
	}

	@Override
	public GraphicsGateway getGraphicsGateway() {
		return ggateway;
	}


}
