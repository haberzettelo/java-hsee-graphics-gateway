package com.hsee.gl.impl.androidgles30.sprite.shape;

import android.opengl.GLES30;

import com.hsee.gl.impl.androidgles30.sprite.renderer.SpriteRendererAndroidGLES30;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public class Plan extends AbstractPlan<GLES30>
{

	public Plan (GLES30 g, String name, float size)
	{
		super(null, name, size);
	}

	@Override
	protected SpriteRenderer<GLES30> newRendererByApi (GLES30 g) {
		return new SpriteRendererAndroidGLES30();
	}

}
