package com.hsee.gl.impl.androidgles30.scene;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.impl.androidgles30.HseeAPIAndroidGLES30;
import com.hsee.gl.scene.impl.SceneImpl;

public class SceneAndroidGLES30 extends SceneImpl {
	protected GLSurfaceView surfaceView;
	private float relativeSpeed = 0;
	
	public SceneAndroidGLES30(GLSurfaceView view) {
		super((GraphicsGateway)null);
		this.surfaceView = view;
		super.api = new HseeAPIAndroidGLES30();
		super.graphicsGateway = super.api.getGraphicsGateway();
		view.setEGLContextClientVersion(2);
		view.setRenderer(new Renderer() {
			private int counter = 0;
			@Override
			public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
				SceneAndroidGLES30.this.surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
				SceneAndroidGLES30.this.api = new HseeAPIAndroidGLES30();
				SceneAndroidGLES30.this.graphicsGateway = SceneAndroidGLES30.this.api.getGraphicsGateway();
				
				final int listenersSize = SceneAndroidGLES30.this.listeners.size();
				for (counter = 0; counter < listenersSize; counter++)
					SceneAndroidGLES30.this.listeners.get(counter).initialize(SceneAndroidGLES30.this.api, SceneAndroidGLES30.this.graphicsGateway);
			}
			
			@Override
			public void onSurfaceChanged(GL10 arg0, int arg1, int arg2) {
				final int listenersSize = SceneAndroidGLES30.this.listeners.size();
				for (counter = 0; counter < listenersSize; counter++)
					SceneAndroidGLES30.this.listeners.get(counter).reshape(SceneAndroidGLES30.this.api, SceneAndroidGLES30.this.graphicsGateway, 0, 0, arg1, arg2);
			}
			
			@Override
			public void onDrawFrame(GL10 arg0) {
				final int listenersSize = SceneAndroidGLES30.this.listeners.size();
				for (counter = 0; counter < listenersSize; counter++)
					SceneAndroidGLES30.this.listeners.get(counter).render(SceneAndroidGLES30.this.api, SceneAndroidGLES30.this.graphicsGateway, SceneAndroidGLES30.this.relativeSpeed);
			}
		});
	}

	@Override
	public void display() {
		this.surfaceView.requestRender();
	}

	@Override
	public void display(float relativeSpeed) {
		this.relativeSpeed = relativeSpeed;
		this.display();
	}

	@Override
	public void waitEndRender() {
		super.graphicsGateway.flush();
	}

}
