package com.hsee.gl.impl.androidgles30.shader;

import android.opengl.GLES30;
import android.opengl.GLES30;

import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderFactory;

public class ShaderFactoryAndroidGLES30 implements ShaderFactory
{
	public static ShaderFactoryAndroidGLES30 factory = null;
	
	public static void initFactory(GLES30 ctx) {
		factory = new ShaderFactoryAndroidGLES30(null);
	}
	
	private ShaderFactoryAndroidGLES30 (GLES30 context) {
	}
	
	@Override
	public Shader newComputeShader () 
	{
		/*return new ShaderAndroidGLES30(graphicalContext, GLES30.GL_COMPUTE_SHADER) {
			@Override
			public boolean isComputeShader () {
				return true;
			}
		};*/
		return null;
	}

	@Override
	public Shader newFragmentShader ()
	{
		return new ShaderAndroidGLES30(null, GLES30.GL_FRAGMENT_SHADER) {
			@Override
			public boolean isFragmentShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newVertexShader ()
	{
		return new ShaderAndroidGLES30(null, GLES30.GL_VERTEX_SHADER) {
			@Override
			public boolean isVertexShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newGeometryShader ()
	{
		/*return new ShaderAndroidGLES30(graphicalContext, GLES30.GL_GEOMETRY_SHADER) {
			@Override
			public boolean isGeometryShader () {
				return true;
			}
		};*/
		return null;
	}

	@Override
	public Shader newTesselationControlShader ()
	{
		/*return new ShaderAndroidGLES30(graphicalContext, GLES30.GL_TESS_CONTROL_SHADER) {
			@Override
			public boolean isTesselationControlShader () {
				return true;
			}
		};*/
		return null;
	}

	@Override
	public Shader newTesselationEvaluationShader ()
	{
		/*return new ShaderAndroidGLES30(graphicalContext, GLES30.GL_TESS_EVALUATION_SHADER) {
			@Override
			public boolean isTesselationEvaluationShader () {
				return true;
			}
		};*/
		return null;
	}
}
