package com.hsee.gl.impl.androidgles30.exception;


import org.apache.log4j.Logger;

import android.opengl.GLES30;

import com.hsee.gl.exception.HseeException;


/**
 * Exception qui est envoyé lorsqu'il y a une erreur avec OpenGL.
 * @author Olivier HABERZETTEL, Jean-Clément NIGUES
 */
public class HseeExceptionImpl extends HseeException {

    /**
	 * 
	 */
	private static final long	serialVersionUID	= -8941027105458787072L;
	private final static Logger logger = Logger.getLogger(HseeExceptionImpl.class);
    private String error;
    private int code;
    
    /**
     * Remonte l'erreur OpenGL via une exception Java
     * @param err Message d'erreur
     */
    public HseeExceptionImpl(String err) {
    	super(err);
    	error = err;
    	this.code = -1;
        //logger.fatal("CODE = " + code + "\n\t" + err);
    }
    
    /**
     * Remonte l'erreur OpenGL via une exception Java
     * @param err Message d'erreur
     * @param code Code de l'erreur
     */
    public HseeExceptionImpl(String err, int code) {
    	super(err);
    	error = err;
    	this.code = code;
        //logger.fatal("CODE = " + code + "\n\t" + err);
    }
    
    public void displayGLException ()
    {
    	logger.fatal("CODE = " + code + "\n\t" + error);
    }

    /*public static boolean checkGlError(GL3 gl){
		int error = gl.glGetError();
		if(error!=0){
			StackTraceElement[] stack = Thread.currentThread().getStackTrace();
			System.err.println("GL ERROR : " + error + " " + errorMap.get(error));
			for(int i=2; i< stack.length; i++){
				System.err.println( stack[i]);
			}
			System.err.println("  ");
		}
		// return true on error
		return error==GL.GL_NO_ERROR?false:true;
	}
*/
    
    public static void getOpenGLError(GLES30 gl) throws HseeExceptionImpl {
        int error = GLES30.glGetError();

        if (error == GLES30.GL_NO_ERROR) {
        	
        } else if ((error & 0x507) == GLES30.GL_INVALID_ENUM) {
            throw new HseeExceptionImpl(
                    "GL_INVALID_ENUM\r\t"
            		+ "An unacceptable value is specified for an enumerated argument.\r\tThe offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flaGLES30.", GLES30.GL_INVALID_ENUM);
        } else if ((error & 0x0507) == GLES30.GL_INVALID_VALUE) {
            throw new HseeExceptionImpl(
                    "GL_INVALID_VALUE\r\t"
            		+ "A numeric argument is out of range.\r\tThe offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flaGLES30.", GLES30.GL_INVALID_ENUM);
        } else if ((error & 0x0507) == GLES30.GL_INVALID_OPERATION) {
            throw new HseeExceptionImpl(
                    "GL_INVALID_OPERATION\r\t"
            		+ "The specified operation is not allowed in the current state.\r\t"
                    + "The offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flaGLES30.", GLES30.GL_INVALID_OPERATION);
        } /*else if ((error & 0x0507) == GLES30.GL_STACK_OVERFLOW) {
            throw new HseeExceptionImpl(
                    "GL_STACK_OVERFLOW\r\t"
                    + "This command would cause a stack overflow.\r\t"
                    + "The offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flaGLES30.", GLES30.GL_STACK_OVERFLOW);
        } else if ((error & 0x0507) == GLES30.GL_STACK_UNDERFLOW) {
            throw new HseeExceptionImpl(
                    "GL_STACK_UNDERFLOW\r\t"
                    + "This command would cause a stack underflow.\r\t"
                    + "The offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flaGLES30.", GLES30.GL_STACK_UNDERFLOW);
        } */else if ((error & 0x0507) == GLES30.GL_OUT_OF_MEMORY) {
            throw new HseeExceptionImpl(
                    "GL_OUT_OF_MEMORY\r\t"
                    + "There is not enough memory left to execute the command.\r\t"
                    + "The state of the GL is undefined,\r\t"
                    + "except for the state of the error flags,\r\t"
                    + "after this error is recorded.", GLES30.GL_OUT_OF_MEMORY);
        } /*else if ((error & 0x8031) == GLES30.GL_TABLE_TOO_LARGE) {
            throw new HseeExceptionImpl(
                    "GL_TABLE_TOO_LARGE\r\t"
                    + "The specified table exceeds the implementation's maximum supported table\r\t"
                    + "size.  The offending command is ignored and has no other side effect\r\tthan to set the error flaGLES30.",
                    GLES30.GL_TABLE_TOO_LARGE);
        }*/ else {
            // Undefined error
            throw new HseeExceptionImpl("Erreur non implementation => error="+error, -1);
        }
        /*
         GL_OUT_OF_MEMORY
         There is not enough memory left to execute the command.
         The state of the GL is undefined,
         except for the state of the error flags,
         after this error is recorded.
         GL_TABLE_TOO_LARGE*/
    }

    public static void clearOpenGLError(GLES30 gl) {
        GLES30.glGetError();
    }
}
