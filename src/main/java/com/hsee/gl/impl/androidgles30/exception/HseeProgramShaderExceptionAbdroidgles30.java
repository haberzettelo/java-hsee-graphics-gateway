package com.hsee.gl.impl.androidgles30.exception;

import android.opengl.GLES30;

import com.hsee.gl.exception.HseeProgramShaderException;

public class HseeProgramShaderExceptionAbdroidgles30 extends HseeProgramShaderException
{
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	public HseeProgramShaderExceptionAbdroidgles30(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub

	}
	
	public static HseeProgramShaderException getLinkError (int code, int programId) {
		String message = "Error while linking program (program id : " + programId + ")\n";
		switch (code) {
		case GLES30.GL_INVALID_VALUE:
			return new HseeProgramShaderException(message + "GL_INVALID_VALUE : \nError generated if program is not a value generated by OpenGL");
		case GLES30.GL_INVALID_OPERATION:
			return new HseeProgramShaderException(message + "GL_INVALID_OPERATION : \nError generated if program is not a program object\nOR\ngenerated if program is the currently active program object and transform feedback mode is active");
		default:
			return new HseeProgramShaderException(message + "Error code not implemented : " + code);
		}
	}
	
	public static HseeProgramShaderException getValidateError (int code, int programId) {
		String message = "Error while validating program (program id : " + programId + ")\n";
		switch (code) {
		case GLES30.GL_INVALID_VALUE:
			return new HseeProgramShaderException(message + "GL_INVALID_VALUE : \nError generated if program is not a value generated by OpenGL");
		case GLES30.GL_INVALID_OPERATION:
			return new HseeProgramShaderException(message + "GL_INVALID_OPERATION : \nError generated if program is not a program object\nOR\ngenerated if program is the currently active program object and transform feedback mode is active");
		default:
			return new HseeProgramShaderException(message + "Error code not implemented : " + code);
		}
	}
}
