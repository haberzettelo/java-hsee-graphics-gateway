package com.hsee.gl.impl.androidgles30.sprite.io;


import android.opengl.GLES30;

import com.hsee.gl.impl.androidgles30.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.impl.androidgles30.sprite.renderer.SpriteRendererAndroidGLES30;
import com.hsee.gl.sprite.DrawWay;
import com.hsee.gl.sprite.SpriteSetter;
import com.hsee.gl.sprite.impl.io.SpriteImporterImpl;
import com.hsee.gl.sprite.impl.shape.Shape;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.hsee.gl.sprite.io.SpriteCompiler;

public class SpriteImporterAndroidGLES30 extends SpriteImporterImpl<GLES30>
{
	public SpriteImporterAndroidGLES30 (GLES30 g, ImporterSetter importer,
			SpriteCompiler compiler, String name, String path, String filename) {
		super(null, importer, compiler, name, path, filename);
		MaterialSSB.ssb.addMaterial(importer.getMaterials());
	}
	
	public SpriteImporterAndroidGLES30 (GLES30 g, ImporterSetter importer,
			SpriteCompiler compiler, InputStreamGetter reader, String name, String path, String filename) {
		super(null, importer, compiler, reader, name, path, filename);
		MaterialSSB.ssb.addMaterial(importer.getMaterials());
	}

	@Override
	public DrawWay getDrawWay () {
		return DrawWay.TrianglesStrip;
	}

	@Override
	public boolean indexContinue () {
		return true;
	}

	@Override
	public SpriteSetter<GLES30> newChild (String name) {
		return new Shape<GLES30>(null, this, _renderer, name);
	}

	@Override
	protected SpriteRenderer<GLES30> newRendererByApi (GLES30 g) {
		return new SpriteRendererAndroidGLES30();
	}

}
