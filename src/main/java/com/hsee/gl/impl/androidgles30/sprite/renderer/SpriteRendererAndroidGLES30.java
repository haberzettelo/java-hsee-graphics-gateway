package com.hsee.gl.impl.androidgles30.sprite.renderer;

import static android.opengl.GLES30.GL_ARRAY_BUFFER;
import static android.opengl.GLES30.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES30.GL_FLOAT;
import static android.opengl.GLES30.GL_LINEAR;
import static android.opengl.GLES30.GL_REPEAT;
import static android.opengl.GLES30.GL_RGB;
import static android.opengl.GLES30.GL_STATIC_DRAW;
import static android.opengl.GLES30.GL_TEXTURE_2D;
import static android.opengl.GLES30.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES30.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES30.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES30.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES30.GL_UNSIGNED_BYTE;

import java.nio.IntBuffer;

import android.opengl.GLES30;
import android.util.Log;

import com.hsee.gl.impl.androidgles30.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.Texture;
import com.hsee.gl.sprite.impl.renderer.SpriteRendererImpl;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.common.nio.Buffers;

public class SpriteRendererAndroidGLES30 extends SpriteRendererImpl<GLES30> implements
		SpriteRenderer<GLES30> {
	static int		SIZEOF_FLOAT	= Buffers.SIZEOF_FLOAT;
	int				_positionPointer, _normalPointer, _texPointer,
			_colorPointer, _vao, _wayToDraw, _nbObjectsToDraw, _text;
	IntBuffer _temp, _count, _baseVertex;
	long[]	_arrayOfIndices;
	ShaderProgram	_previousProgram;
	Sprite<GLES30>		_parent;
	
	static MaterialSSB _ssbMaterial = null;

	public SpriteRendererAndroidGLES30() {
		super();
		_previousProgram = null;
		_temp = Buffers.newDirectIntBuffer(1);
	}

	@Override
	public void initByApi (GLES30 g, ShaderProgram program) {
		if (_ssbMaterial == null)
			_ssbMaterial = MaterialSSB.ssb;
		if (_previousProgram != program) {
			initLocationPointers(program);
			_previousProgram = program;
		}
		_parent = _objects.get(0);

		switch (_parent.getDrawWay()) {
		case TrianglesStrip:
			_wayToDraw = GLES30.GL_TRIANGLE_STRIP;
			break;
		case Triangles:
			_wayToDraw = GLES30.GL_TRIANGLES;
			break;
		case Points:
			_wayToDraw = GLES30.GL_POINTS;
			break;
		case Lines:
			_wayToDraw = GLES30.GL_LINES;
			break;
		case LinesStrip:
			_wayToDraw = GLES30.GL_LINE_STRIP;
			break;
		}

		//_ssbMaterial.addMaterial((MaterialImpl)sprite.getVertexIndexes().get(0).material);

		boolean hasPos = false, hasNor = false, hasCol = false, hasTex = false, indContinued = _parent
				.indexContinue();
		int nbBuffers = 1;

		_temp.rewind();
		// TODO Adapt
		/*GLES30.glGenVertexArrays(1, _temp);

		GLES30.glEnable(GLES30.GL_PRIMITIVE_RESTART);
		GLES30.glPrimitiveRestartIndex(getRestartIndex());
		_vao = _temp.get(0);
		// Save all following states
		GLES30.glBindVertexArray(_vao);*/


		if (_parent.getVertexPosition().capacity() > 0) {
			GLES30.glEnableVertexAttribArray(_positionPointer);
			hasPos = true;
			nbBuffers++;
		}

		if (_parent.getVertexNormal().capacity() > 0) {
			GLES30.glEnableVertexAttribArray(_normalPointer);
			hasNor = true;
			nbBuffers++;
		}

		if (_parent.getVertexColor().capacity() > 0) {
			GLES30.glEnableVertexAttribArray(_colorPointer);
			hasCol = true;
			nbBuffers++;
		}

		if (_parent.getVertexTextureCoordinates().capacity() > 0) {
			GLES30.glEnableVertexAttribArray(_texPointer);
			hasTex = true;
			nbBuffers++;
		}

		_temp = Buffers.newDirectIntBuffer(nbBuffers);
		GLES30.glGenBuffers(nbBuffers, _temp);

		int posSize = 0, colSize = 0, norSize = 0, texSize = 0, indSize = 0;
		// First loop to get all size to attribute to buffers
		for (Sprite<GLES30> sprite : _objects) {
			posSize += sprite.getVertexPosition().capacity();
			colSize += sprite.getVertexColor().capacity();
			norSize += sprite.getVertexNormal().capacity();
			texSize += sprite.getVertexTextureCoordinates().capacity();
			
			for (ObjectFaces object : sprite.getVertexIndexes()) {
				indSize += object.faces.capacity();				
			}
		}
		posSize *= SIZEOF_FLOAT;
		colSize *= SIZEOF_FLOAT;
		norSize *= SIZEOF_FLOAT;
		texSize *= SIZEOF_FLOAT;
		indSize *= Buffers.SIZEOF_INT;

		_temp.rewind();
		if (hasPos) {
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glBufferData(GL_ARRAY_BUFFER, posSize, null, GL_STATIC_DRAW);
		}
		if (hasCol) {
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glBufferData(GL_ARRAY_BUFFER, colSize, null, GL_STATIC_DRAW);
		}
		if (hasNor) {
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glBufferData(GL_ARRAY_BUFFER, norSize, null, GL_STATIC_DRAW);
		}
		if (hasTex) {
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glBufferData(GL_ARRAY_BUFFER, texSize, null, GL_STATIC_DRAW);
		}
		GLES30.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
		GLES30.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indSize, null, GL_STATIC_DRAW);

		int offsetInd = 0, offsetPos = 0, offsetNor = 0, offsetCol = 0, offsetTex = 0;
		int size;

		// TODO
		_baseVertex = Buffers.newDirectIntBuffer(_objects.size());
		_baseVertex.rewind();
		
		_nbObjectsToDraw = 0;
		for (Sprite<GLES30> sprite : _objects) {
			_nbObjectsToDraw += sprite.getVertexIndexes().size();
		}

		_count = Buffers.newDirectIntBuffer(_nbObjectsToDraw);
		_count.rewind();

		_arrayOfIndices = new long[_nbObjectsToDraw];
		int iArrrayOfIndice = 0;

		for (Sprite<GLES30> sprite : _objects) {
			_temp.rewind();
			if (hasPos) {
				GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexPosition().capacity() * SIZEOF_FLOAT;
				GLES30.glBufferSubData(GL_ARRAY_BUFFER, offsetPos, size, sprite
						.getVertexPosition().rewind());

				GLES30.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false,
						0, 0);

				if (!indContinued)
					_baseVertex.put(offsetPos / 3);

				offsetPos += size;
			}
			if (hasCol) {
				GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexColor().capacity() * SIZEOF_FLOAT;
				GLES30.glBufferSubData(GL_ARRAY_BUFFER, offsetCol, size, sprite.getVertexColor().rewind());
				GLES30.glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, false, 0, 0);
				offsetCol += size;
			}
			if (hasNor) {
				GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexNormal().capacity() * SIZEOF_FLOAT;
				GLES30.glBufferSubData(GL_ARRAY_BUFFER, offsetNor, size, sprite.getVertexNormal().rewind());
				GLES30.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, true, 0, 0);
				offsetNor += size;
			}
			if (hasTex) {
				GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexTextureCoordinates().capacity() * SIZEOF_FLOAT;
				GLES30.glBufferSubData(GL_ARRAY_BUFFER, offsetTex, size, sprite.getVertexTextureCoordinates().rewind());
				GLES30.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, true, 0, 0);
				offsetTex += size;
			}
			
			
			int idBindElement = _temp.get();
			GLES30.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idBindElement);
			//TODO
			for (int i = 0, max = sprite.getVertexIndexes().size(); i < max; i++) {
				ObjectFaces object = sprite.getVertexIndexes().get(i);
				_count.put(object.faces.capacity());
	
				_arrayOfIndices[iArrrayOfIndice++] = offsetInd;
	
				size = object.faces.capacity()
						* Buffers.SIZEOF_INT;
				GLES30.glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offsetInd, size, object.faces.rewind());
				offsetInd += size;
			}
			// for(int i = 0; i < sprite.getVertexIndexes().capacity(); i++)
			// System.out.println(sprite.getVertexIndexes().get(i));
			//System.out.println(sprite.getVertexIndexes().get(0).material);
		}

		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null) {
			Texture t = _parent.getVertexIndexes().get(0).material
					.getDiffuseTexture();
			IntBuffer intb = Buffers.newDirectIntBuffer(1);
			_text = intb.get(0);
			GLES30.glGenTextures(1, intb);

			GLES30.glEnable(GL_TEXTURE_2D);
			GLES30.glBindTexture(GL_TEXTURE_2D, _text);
			//GLES30.glActiveTexture(GL_TEXTURE0);
			GLES30.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GLES30.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			GLES30.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, t.getWidth(),
					t.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, t.getData().rewind());
			GLES30.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			GLES30.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}

		//GLES30.glBindVertexArray(0);
		_ssbMaterial.bindShaderProgram(program, "materials");
		_ssbMaterial.updateSsbo();
		_materialLocation = program.getUniformLocation("materialId");
	}
	int _materialLocation;
	
	@Override
	public void draw (GLES30 g, ShaderProgram program) {
		_count.rewind();
		_temp.rewind();
		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null)
			GLES30.glBindTexture(GL_TEXTURE_2D, _text);
		//GLES30.glBindVertexArray(_vao);
		if (_parent.indexContinue()) {
			/*GLES30.glMultiDrawElements(_wayToDraw, _count, GL4.GL_UNSIGNED_INT,
					_arrayOfIndices, _nbObjectsToDraw);*/
			_temp.rewind();
			_parent.getVertexPosition().rewind();
			StringBuilder b = new StringBuilder(_parent.getVertexPosition().capacity()*6);
			for (int i = 0, max = _parent.getVertexPosition().capacity(); i < max; i++) {
				b.append(_parent.getVertexPosition().get());
				b.append(", ");
			}
			Log.d("positions : ", b.toString());
			_parent.getVertexPosition().rewind();
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false, 0, _parent.getVertexPosition());
			GLES30.glEnableVertexAttribArray(_positionPointer);
			
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, false, 0, 0);
			GLES30.glEnableVertexAttribArray(_normalPointer);
			
			GLES30.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES30.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, false, 0, 0);
			GLES30.glEnableVertexAttribArray(_texPointer);
			
			GLES30.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
			/*GLES30.glMultiDrawElements(_wayToDraw, _count, GL4.GL_UNSIGNED_INT,
					_arrayOfIndices, _nbObjectsToDraw);*/
			// Display opaque elements before
			int iArrayOfIndices = 0;
			for (Sprite<GLES30> sprite : _objects)
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					long arrayOfIndex = _arrayOfIndices[iArrayOfIndices++];
					if (object.material.getDissolve() > 0.999f) {
						GLES30.glUniform1i(_materialLocation, object.material.getIdSsb());
						Log.d("", "display " + count + " opaques elements");
						GLES30.glDrawArrays(_wayToDraw, 0, count);
						//GLES30.glDrawElements(_wayToDraw, count, GL_UNSIGNED_INT, (int)arrayOfIndex);
					}
				}
			_count.rewind();
			iArrayOfIndices = 0;
			// Display transparent element after
			for (Sprite<GLES30> sprite : _objects)
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					long arrayOfIndex = _arrayOfIndices[iArrayOfIndices++];
					if (object.material.getDissolve() < 0.9999f) {
						GLES30.glUniform1i(_materialLocation, object.material.getIdSsb());
						GLES30.glDrawArrays(_wayToDraw, 0, count);
						Log.d("", "display " + count + " transparent elements");
						//GLES30.glDrawElements(_wayToDraw, count, GL_UNSIGNED_INT, (int)arrayOfIndex);
					}
				}
			
		}
		else {
			_baseVertex.rewind();
			// TODO Adapt
			/*GLES30.glMultiDrawElementsBaseVertex(_wayToDraw, _count,
					GLES30.GL_UNSIGNED_INT, _arrayOfIndices, _nbObjectsToDraw,
					_baseVertex);*/
		}
		//GLES30.glBindVertexArray(0);
	}

	private void initLocationPointers (ShaderProgram program) {
		GLES30.glBindAttribLocation(program.getProgramId(), 0, ShaderVariable.BUILTIN_POSITION);
		GLES30.glBindAttribLocation(program.getProgramId(), 1, ShaderVariable.BUILTIN_NORMAL);
		GLES30.glBindAttribLocation(program.getProgramId(), 2, ShaderVariable.BUILTIN_COLOR);
		GLES30.glBindAttribLocation(program.getProgramId(), 3, ShaderVariable.BUILTIN_TEXCOORDINATES);
		
		_positionPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_POSITION);
		_normalPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_NORMAL);
		_colorPointer = program.getAttribLocation(ShaderVariable.BUILTIN_COLOR);
		_texPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_TEXCOORDINATES);
	}

	@Override
	protected void removeByApi (Sprite<GLES30> sprite) {
		// TODO Auto-generated method stub
	}
}
