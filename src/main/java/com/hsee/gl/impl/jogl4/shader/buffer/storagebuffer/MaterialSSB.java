package com.hsee.gl.impl.jogl4.shader.buffer.storagebuffer;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.sprite.Material;
import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL4;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * This class represent all material of all sprites, all materials of the application
 * will be stored in a ShaderStorageBufferObject.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 mai 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class MaterialSSB extends ShaderStorageBufferJogl4 {
	private static Logger logger = Logger.getLogger(MaterialSSB.class); 
	private List<Material> _materials = null;
	
	private int _nbMaterial = 0;
	/**
	 * ambientColor (3 + 1) + 
	 * diffuseColor (3 + 1) +
	 * specularColor(3 + 1) +
	 * trFilter		(3 + 1) +
	 * specExponent (1) +
	 * sharpness	(1) +
	 * opticalDensit(1) +
	 * dissolve		(1)
	 * 	==
	 * 20
	 */
	private static int MATERIAL_SIZE = 20 * Buffers.SIZEOF_FLOAT;
	public static int MAX_MATERIAL = 256;
	
	public static MaterialSSB ssb = null;
	public static void initMaterialSsb (GL4 g) {
		ssb = new MaterialSSB(g);
	}
	
	private MaterialSSB (GL4 g) {
		super(g, MAX_MATERIAL * MATERIAL_SIZE, 0);
		addMaterial(new MaterialImpl());
	}

	@Override
	public void initShader (Shader shader) {
		shader.append(
				  "struct " + BUFFER_PREFIX + "Material {\n"
				+ "	vec4 ambientColor;\n"
				+ "	vec4 diffuseColor;\n"
				+ "	vec4 specularColor;\n"
				+ "	vec4 transmissionFilter;\n"
				+ "	float specularExponent;\n"
				+ "	float sharpness;\n"
				+ "	float opticalDensity;\n"
				+ "	float dissolve;\n"
				+ "};\n\n"
				+ "layout (std430) buffer Material {\n"
				+ "	" + BUFFER_PREFIX + "Material materials[];\n"
				+ "};\n"
				);
	}

	/**
	 * Add one material to all material on gpu
	 * @param mat
	 */
	public void addMaterial(MaterialSetter mat) {
		if (_materials == null)
			_materials = new ArrayList<Material>();
		else if (_materials.size() >= MAX_MATERIAL) {
			logger.error("Limit material reached (" + MAX_MATERIAL + "), material " + mat.getName() + " ignored");
			return ;
		}
			
		mat.setIdSsb(_nbMaterial++);
		_materials.add(mat);
	}
	
	/**
	 * Add a list of material to all material on gpu
	 * @param mat
	 */
	public void addMaterial(List<MaterialSetter> mat) {
		if (_materials == null) {
			_materials = new ArrayList<Material>(mat.size());
			for (MaterialSetter materialSetter : mat){
				materialSetter.setIdSsb(_nbMaterial++);
				_materials.add(materialSetter);
			}
		}
		else
			for (MaterialSetter materialImpl : mat) {
				if (_materials.size() >= MAX_MATERIAL) {
					logger.error("Limit material reached (" + MAX_MATERIAL + "), material " + materialImpl.getName() + " ignored");
				} else {
					materialImpl.setIdSsb(_nbMaterial++);
					_materials.add(materialImpl);
				}
				
			}
	}
	
	/**
	 * Update the Shader storage buffer object in gpu memory
	 */
	public void updateSsbo () {
		ByteBuffer buffer = mapData(false, true);
		buffer.rewind();
		for (Material material : _materials) {
			putVector(buffer, material.getAmbientReflexivity());
			putVector(buffer, material.getDiffuseReflexivity());
			putVector(buffer, material.getSpecularReflexivity());
			putVector(buffer, material.getTransmissionFilter());
			buffer.putFloat(material.getSpecularExponent());
			buffer.putFloat(material.getSharpness());
			buffer.putFloat(material.getOpticalDensity());
			buffer.putFloat(material.getDissolve());
		}
		unmapData();
	}
	
	private static ByteBuffer putVector(ByteBuffer b, Vector3 v) {
		float[] va;
		if (v == null)
			va = Vector3.NULL_VECTOR.getCoordinates();
		else
			va = v.getCoordinates();
		b.putFloat(va[0]);
		b.putFloat(va[1]);
		b.putFloat(va[2]);
		b.putFloat(0);
		return b;
	}
}
