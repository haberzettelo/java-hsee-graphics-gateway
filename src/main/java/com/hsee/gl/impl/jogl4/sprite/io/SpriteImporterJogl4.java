package com.hsee.gl.impl.jogl4.sprite.io;

import com.hsee.gl.impl.jogl4.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.impl.jogl4.sprite.renderer.SpriteRendererJogl4;
import com.hsee.gl.sprite.DrawWay;
import com.hsee.gl.sprite.SpriteSetter;
import com.hsee.gl.sprite.impl.io.SpriteImporterImpl;
import com.hsee.gl.sprite.impl.shape.Shape;
import com.hsee.gl.sprite.io.SpriteCompiler;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.opengl.GL4;

public class SpriteImporterJogl4 extends SpriteImporterImpl<GL4>
{
	GL4 g;
	public SpriteImporterJogl4 (GL4 g, ImporterSetter importer,
			SpriteCompiler compiler, String name, String path, String filename)
	{
		super(g, importer, compiler, name, path, filename);
		MaterialSSB.ssb.addMaterial(importer.getMaterials());
		this.g = g;
	}

	@Override
	public DrawWay getDrawWay () {
		return DrawWay.TrianglesStrip;
	}

	@Override
	public boolean indexContinue () {
		return true;
	}

	@Override
	public SpriteSetter<GL4> newChild (String name) {
		return new Shape<GL4>(g, this, _renderer, name);
	}

	@Override
	protected SpriteRenderer<GL4> newRendererByApi (GL4 g) {
		return new SpriteRendererJogl4();
	}

}
