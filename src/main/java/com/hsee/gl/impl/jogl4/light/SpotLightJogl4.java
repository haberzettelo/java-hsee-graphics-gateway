package com.hsee.gl.impl.jogl4.light;

import com.hsee.gl.light.impl.SpotLightImpl;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.jogamp.opengl.GL4;

public class SpotLightJogl4 extends SpotLightImpl
{
	GL4 g;
	public SpotLightJogl4 (GL4 g, String name)
	{
		super(name);
		this.g = g;
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void initShaderByApi (Shader shader) {
		shader.append("struct " + TYPENAMESPOT + "{\n"
				+ "	vec3 " + VAR_AMBIENT + ";\n"
				+ "	vec3 " + VAR_DIFFUSECOLOR + ";\n"
				+ "	vec3 " + VAR_SPECULARCOLOR + ";\n"
				+ "	float " + VAR_ATT_CONST + ";\n"
				+ "	float " + VAR_ATT_LIN + ";\n"
				+ "	float " + VAR_ATT_EXP + ";\n"
				+ "	vec3 " + VAR_CONE_DIR + ";\n"
				+ "	float " + VAR_CUT_OFF + ";\n"
				+ "	float " + VAR_EXPONENT + ";\n"
				+ "	vec3 " + VAR_POSITION + ";\n};\n");
	}

	@Override
	protected void updateShaderDataByApi (ShaderProgram updater) {
		// Same as PointLightJogl4
		_diffuse.rewind();
		_specular.rewind();
		_position.rewind();
		_ambientColor.rewind();
		
		g.glUniform3f(_diffLoc, _diffuse.get(),_diffuse.get(),_diffuse.get());
		g.glUniform3f(_specLoc, _specular.get(),_specular.get(),_specular.get());
		g.glUniform3f(_posLoc, _position.get(),_position.get(),_position.get());
		g.glUniform3f(_ambLoc, _ambientColor.get(),_ambientColor.get(),_ambientColor.get());
		
		g.glUniform1f(_attConstLoc, _attConstant);
		g.glUniform1f(_attLineLoc, _attLinear);
		g.glUniform1f(_attExpLoc, _attExpo);
		
		// Add for spot
		_coneDirection.rewind();
		g.glUniform3f(_coneLoc, _coneDirection.get(),_coneDirection.get(),_coneDirection.get());
		g.glUniform1f(_spotCutLoc, _spotCutOff);
		g.glUniform1f(_spotExpLoc, _spotExponent);
	}

}
