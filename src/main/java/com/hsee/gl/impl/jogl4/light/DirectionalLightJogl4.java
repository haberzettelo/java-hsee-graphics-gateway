package com.hsee.gl.impl.jogl4.light;

import com.jogamp.opengl.GL4;
import com.hsee.gl.light.impl.DirectionalLightImpl;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;

public class DirectionalLightJogl4 extends DirectionalLightImpl
{
	GL4 g;
	public DirectionalLightJogl4 (GL4 g, String name) {
		super(name);
		this.g = g;
	}
	

	
	@Override
	protected void updateShaderDataByApi (ShaderProgram updater) {
		_diffuseColor.rewind();
		_specularColor.rewind();
		_direction.rewind();
		_halfvector.rewind();
		_ambientColor.rewind();
	
		g.glUniform3f(_diffLoc, _diffuseColor.get(), _diffuseColor.get(), _diffuseColor.get());
		g.glUniform3f(_specLoc, _specularColor.get(), _specularColor.get(), _specularColor.get());
		g.glUniform3f(_dirLoc, _direction.get(), _direction.get(), _direction.get());
		g.glUniform3f(_halfLoc, _halfvector.get(), _halfvector.get(), _halfvector.get());
		g.glUniform3f(_ambLoc, _ambientColor.get(), _ambientColor.get(), _ambientColor.get());
	}

	@Override
	protected void initShaderByApi (Shader shader) {
		shader.append("struct " + TYPENAMEDIRECTIONAL + "{\n"
				+ "	vec3 " + VAR_AMBIENT + ";\n"
				+ "	vec3 " + VAR_DIFFUSECOLOR + ";\n"
				+ "	vec3 " + VAR_SPECULARCOLOR + ";\n"
				+ "	vec3 " + VAR_HALF + ";\n"
				+ "	vec3 " + VAR_DIRECTION + ";\n};\n");
	}
}
