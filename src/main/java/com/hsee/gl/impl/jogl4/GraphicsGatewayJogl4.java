package com.hsee.gl.impl.jogl4;

import com.hsee.gl.GraphicsGateway;
import com.jogamp.opengl.GL4;

public class GraphicsGatewayJogl4 implements GraphicsGateway {
	private GL4 g;
	public GraphicsGatewayJogl4(GL4 g) {
		this.g = g;
	}
	
	@Override
	public GraphicsGateway clearColor(float red, float green, float blue,
			float alpha) {
		g.glClearColor(red, green, blue, alpha);
		return this;
	}

	@Override
	public GraphicsGateway clear(int targets) {
		g.glClear(targets);
		return this;
	}

	@Override
	public GraphicsGateway enable(int targets) {
		g.glEnable(targets);
		return this;
	}

	@Override
	public GraphicsGateway flush() {
		g.glFlush();
		return this;
	}

	@Override
	public GraphicsGateway blendFunc(int sourceFactor, int destinationFactor) {
		g.glBlendFunc(sourceFactor, destinationFactor);
		return this;
	}
}
