package com.hsee.gl.impl.jogl4.mvp;

import com.jogamp.opengl.GL4;
import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.mvp.impl.ModelViewProjectionImpl;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;

public class ModelViewProjectionJogl4 extends ModelViewProjectionImpl
{
	private GL4 g;
	public ModelViewProjectionJogl4 (GL4 gl)
	{
		g = gl;
	}

	@Override
	public ShaderVariable<Matrix> addShaderProgram (ShaderProgram program) throws HseeShaderStructureException  
	{
		int location = g.glGetUniformLocation(program.getProgramId(), MVP_NAME), 
				locationNormal = g.glGetUniformLocation(program.getProgramId(), NORM_MATRIX_NAME),
				locationModel = g.glGetUniformLocation(program.getProgramId(), MODEL_MATRIX_NAME),
				locationViewModel = g.glGetUniformLocation(program.getProgramId(), VIEWMODEL_MATRIX_NAME);
		if (location < 0)
			throw new HseeShaderStructureException("Variable \"" + MVP_NAME + "\" was not found in program id : " + program.getProgramId());
		_locations.put(program, new int[] {location, locationNormal, locationModel, locationViewModel});
		return this;
	}

	@Override
	public ShaderVariable<Matrix> updateShaderData (ShaderProgram updater)
	{
		int[] index = _locations.get(updater);
		g.glUniformMatrix4fv(index[0], 1, true, getValue().getValues(), 0);
		
		if (index[1] > -1)
			g.glUniformMatrix4fv(index[1], 1, false, getNormalMatrix().getValues(), 0);
		if (index[2] > -1)
			g.glUniformMatrix4fv(index[2], 1, true, getModelMatrix().getValues(), 0);
		if (index[3] > -1)
			g.glUniformMatrix4fv(index[3], 1, true, getModelViewMatrix().getValues(), 0);
		
		return this;
	}

}
