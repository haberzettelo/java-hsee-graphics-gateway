package com.hsee.gl.impl.jogl4.scene;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.impl.jogl4.HseeAPIJogl4;
import com.hsee.gl.scene.impl.SceneImpl;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;

public class SceneJogl4 extends SceneImpl/*
														 * implements
														 * GLEventListener
														 */{
	private GLCanvas canvas;
	protected Runnable rnable;
	
	public SceneJogl4(GLCanvas canvas) {
		super((GraphicsGateway) null);
		this.canvas = canvas;
		initByApi();
	}
	
	private float relativeSpeed = 0;
	
	public void initByApi() {
		if (this.canvas != null)
			canvas.addGLEventListener(new GLEventListener() {
				private int counter = 0;
				@Override
				public void reshape(GLAutoDrawable drawable, int x, int y, int width,
						int height) {
					final int listenersSize = SceneJogl4.this.listeners.size();
					for (counter = 0; counter < listenersSize; counter++)
						SceneJogl4.this.listeners.get(counter).reshape(SceneJogl4.this.api, SceneJogl4.this.graphicsGateway, x, y, width, height);
				}
				
				@Override
				public void init(GLAutoDrawable drawable) {
					SceneJogl4.this.api = new HseeAPIJogl4(drawable.getGL().getGL4());
					SceneJogl4.this.graphicsGateway = api.getGraphicsGateway();
					final int listenersSize = SceneJogl4.this.listeners.size();
					for (counter = 0; counter < listenersSize; counter++)
						SceneJogl4.this.listeners.get(counter).initialize(SceneJogl4.this.api, SceneJogl4.this.getGraphicsGateway());
				}
				
				@Override
				public void dispose(GLAutoDrawable drawable) {
				}
				
				@Override
				public void display(GLAutoDrawable drawable) {
					final int listenersSize = SceneJogl4.this.listeners.size();
					for (counter = 0; counter < listenersSize; counter++)
						SceneJogl4.this.listeners.get(counter).render(SceneJogl4.this.api, SceneJogl4.this.getGraphicsGateway(), SceneJogl4.this.relativeSpeed);
				}
			});
	}
	/*@Override
	public void start() {
		rnable = getRunnable(fpsSpeed, new Render() {
			
			@Override
			public void render(GraphicsGateway graphics, float relativeSpeed) {
				SceneLoopJogl4.this.relativeSpeed = relativeSpeed;
				SceneLoopJogl4.this.canvas.display();
			}

			@Override
			public void dislay() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void waitEndRender() {
				SceneLoopJogl4.this.canvas.flushGLRunnables();
			}
		});
		new Thread(rnable).start();
	}*/
	
	@Override
	public void waitEndRender() {
		SceneJogl4.this.canvas.flushGLRunnables();
	}
	
	@Override
	public void display() {
		SceneJogl4.this.canvas.display();
	}
	
	@Override
	public void display(float relativeSpeed) {
		SceneJogl4.this.relativeSpeed = relativeSpeed;
		this.display();
	}
}
