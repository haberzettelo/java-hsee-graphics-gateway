package com.hsee.gl.impl.jogl4.light;

import com.hsee.gl.light.impl.PointLightImpl;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.jogamp.opengl.GL4;

public class PointLightJogl4 extends PointLightImpl
{
	private GL4 g;
	public PointLightJogl4 (GL4 g, String name) {
		super(name);
		this.g = g;
	}

	@Override
	protected void initShaderByApi (Shader shader) {
		shader.append("struct " + TYPENAMEPOINT + "{\n"
				+ "	vec3 " + VAR_AMBIENT + ";\n"
				+ "	vec3 " + VAR_DIFFUSECOLOR + ";\n"
				+ "	vec3 " + VAR_SPECULARCOLOR + ";\n"
				+ "	float " + VAR_ATT_CONST + ";\n"
				+ "	float " + VAR_ATT_LIN + ";\n"
				+ "	float " + VAR_ATT_EXP + ";\n"
				//+ "	vec3 " + VAR_CAMPOS + ";\n"
				+ "	vec3 " + VAR_POSITION + ";\n};\n");
	}

	@Override
	protected void updateShaderDataByApi (ShaderProgram updater) {
		_diffuse.rewind();
		_specular.rewind();
		_position.rewind();
		_ambientColor.rewind();
		
		g.glUniform3f(_diffLoc, _diffuse.get(),_diffuse.get(),_diffuse.get());
		g.glUniform3f(_specLoc, _specular.get(),_specular.get(),_specular.get());
		g.glUniform3f(_posLoc, _position.get(),_position.get(),_position.get());
		g.glUniform3f(_ambLoc, _ambientColor.get(),_ambientColor.get(),_ambientColor.get());
		
		g.glUniform1f(_attConstLoc, _attConstant);
		g.glUniform1f(_attLineLoc, _attLinear);
		g.glUniform1f(_attExpLoc, _attExpo);
	}

}
