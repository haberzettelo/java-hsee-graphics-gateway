package com.hsee.gl.impl.jogl4.shader;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import com.jogamp.opengl.GL4;
import com.hsee.gl.exception.HseeShaderException;
import com.hsee.gl.shader.impl.ShaderImpl;
import com.jogamp.common.nio.Buffers;

public abstract class ShaderJogl4 extends ShaderImpl
{
	private GL4 gl;
	private boolean _showSource = true;
	/**
	 * Construct a shader
	 * @param context Graphical context
	 * @param shaderType Specifies the type of shader to be created. Must be one of GL_COMPUTE_SHADER, <br>
	 * GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, or GL_FRAGMENT_SHADER.
	 */
	ShaderJogl4 (GL4 context, int shaderType)
	{
		gl = context;
		_shaderId = gl.glCreateShader(shaderType);
	}

	
	
	@Override
	public void compileShader () throws HseeShaderException
	{
		IntBuffer buf = Buffers.newDirectIntBuffer(1);
		buf.put(_sourceShader.length());
		buf.rewind();
		gl.glShaderSource(_shaderId, 1, new String[] {_sourceShader}, buf);
		
		gl.glCompileShader(_shaderId);
		// Take the buffer to store the result of the compilation
		buf.rewind();
		gl.glGetShaderiv(_shaderId, GL4.GL_COMPILE_STATUS, buf);
		if (buf.get(0) == GL4.GL_FALSE)
		{
			// Take the buffer to store the length of log
			buf.rewind();
			gl.glGetShaderiv(_shaderId, GL4.GL_INFO_LOG_LENGTH, buf);
			ByteBuffer byteBuffer = Buffers.newDirectByteBuffer(buf.get(0));
			gl.glGetShaderInfoLog(_shaderId, buf.get(), buf, byteBuffer);
			
			String error = "";
			for (int i = 0, max = byteBuffer.capacity(); i < max; i++) {
                error += ((char) byteBuffer.get());
            }
			String addMessage = "";
			if (_showSource)
				addMessage = " of source : \n" + this;
			throw new HseeShaderException("Error while compilation" + addMessage + "\nCompilation output : \n" + error);
		}
	}

	@Override
	protected int getShaderIdByApi ()
	{
		return _shaderId;
	}



	@Override
	protected void finalize ()
	{
		// Don't remove shader here, remove when program is deleted
		//gl.glDeleteShader(_shaderId);
	}

}
