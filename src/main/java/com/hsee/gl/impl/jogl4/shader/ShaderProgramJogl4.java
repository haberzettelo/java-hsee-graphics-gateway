package com.hsee.gl.impl.jogl4.shader;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.StandardCharsets;

import com.hsee.gl.exception.HseeProgramShaderException;
import com.hsee.gl.impl.jogl4.exception.HseeProgramShaderExceptionJogl4;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderProgramPipeline;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.impl.ShaderProgramImpl;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL4;

public abstract class ShaderProgramJogl4 extends ShaderProgramImpl
{
	protected GL4 g;
	protected boolean separable;
	protected static IntBuffer _temp = Buffers.newDirectIntBuffer(1);
	ShaderProgramJogl4 (GL4 gl)
	{
		super();
		g = gl;
		_programId = gl.glCreateProgram();
	}

	@Override
	public int getUniformLocation (String varName)
	{
		return g.glGetUniformLocation(_programId, varName);
	}

	@Override
	public int[] getUniformLocation (String[] varsName)
	{
		int[] result = new int[varsName.length];
		for (int i = 0, max = varsName.length; i < max; i++)
			result[i] = g.glGetUniformLocation(_programId, varsName[i]);
		return result;
	}

	@Override
	protected ShaderProgram attachShaderByApi (Shader shader)
	{
		g.glAttachShader(_programId, shader.getShaderId());
		return this;
	}

	@Override
	protected ShaderProgram detachShaderByApi (Shader shader)
	{
		g.glDetachShader(_programId, shader.getShaderId());
		return this;
	}
	
	@Override
	public ShaderProgram link () throws HseeProgramShaderException
	{
		g.glLinkProgram(_programId);
		if (!getStatus(GL4.GL_LINK_STATUS))
			throw new HseeProgramShaderException("Error while linking : " + getLogMessage());
		int error;
		if ((error = g.glGetError()) != GL4.GL_NO_ERROR) {
			throw HseeProgramShaderExceptionJogl4.getLinkError(error, _programId);
		}
		g.glValidateProgram(_programId);
		if (!getStatus(GL4.GL_VALIDATE_STATUS))
			throw new HseeProgramShaderException("Error while validate : " + getLogMessage());
		if ((error = g.glGetError()) != GL4.GL_NO_ERROR) {
			throw HseeProgramShaderExceptionJogl4.getValidateError(error, _programId);
		}
		return this;
	}

	@Override
	protected int getAttribLocationByApi (String varName)
	{
		return g.glGetAttribLocation(_programId, varName);
	}
	
	@Override
	public int getStorageBufferLocation (String ssbName) {
		// Get blockname to bytebuffer
		ByteBuffer nameBuf = StandardCharsets.US_ASCII.encode(ssbName);
		nameBuf.rewind();

		return g.glGetProgramResourceIndex(_programId, GL4.GL_SHADER_STORAGE_BLOCK, nameBuf);
	}
	
	@Override
	protected void finalize ()
	{
		// TODO Auto-generated method stub
		
	}
	
	private boolean getStatus(int param) {
		_temp.rewind();
		g.glGetProgramiv(_programId, param, _temp);
		if (_temp.get(0) == GL4.GL_TRUE)
			return true;
		return false;
	}
	
	private String getLogMessage() {
		_temp.rewind();
		g.glGetProgramiv(_programId, GL4.GL_INFO_LOG_LENGTH,  _temp);
		System.out.println("sizeOfBuffer + " + _temp.get(0));
		ByteBuffer byteBuffer = Buffers.newDirectByteBuffer(_temp.get(0));
		g.glGetProgramInfoLog(_programId, _temp.get(0), _temp, byteBuffer);
		
		String error = "";
		for (int i = 0, max = byteBuffer.capacity(); i < max; i++) {
            error += ((char) byteBuffer.get());
        }
		return error;
	}
}

class ShaderProgramSimpleJogl4 extends ShaderProgramJogl4 {

	ShaderProgramSimpleJogl4 (GL4 gl)
	{
		super(gl);
	}

	@Override
	public boolean isSeparable ()
	{
		return false;
	}

	@Override
	protected ShaderProgram useProgramByApi ()
	{
		g.glUseProgram(_programId);
		return this;
	}

	@Override
	protected ShaderProgram updateShaderDataByApi (@SuppressWarnings ("rawtypes") ShaderVariable var)
	{
		var.updateShaderData(this);
		return this;
	}
}

class ShaderProgramSeparableJogl4 extends ShaderProgramJogl4 {
	ShaderProgramPipeline pipeline;
	ShaderProgramSeparableJogl4 (GL4 gl)
	{
		super(gl);
		g.glProgramParameteri(_programId, GL4.GL_PROGRAM_SEPARABLE, GL4.GL_TRUE);
		pipeline = null;
	}

	@Override
	public boolean isSeparable ()
	{
		return true;
	}

	@Override
	protected ShaderProgram useProgramByApi ()
	{
		// Don't bind, the pipeline program must be take the hand
		g.glUseProgram(0);
		return this;
	}

	ShaderProgramSeparableJogl4 setShaderProgramPipeline (ShaderProgramPipeline pipeline) {
		this.pipeline = pipeline;
		return this;
	}
	
	@Override
	protected ShaderProgram updateShaderDataByApi (@SuppressWarnings ("rawtypes") ShaderVariable var)
	{
		if (pipeline != null)
			g.glActiveShaderProgram(pipeline.getShaderProgramPipelineId(), _programId);
		var.updateShaderData(this);
		return this;
	}
	
}
