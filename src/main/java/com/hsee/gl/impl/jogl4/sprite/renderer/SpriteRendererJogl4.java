package com.hsee.gl.impl.jogl4.sprite.renderer;

import java.nio.IntBuffer;
import java.util.List;

import com.jogamp.opengl.GL4;

import static com.jogamp.opengl.GL4.*;

import com.hsee.gl.impl.jogl4.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Texture;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.impl.renderer.SpriteRendererImpl;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.common.nio.Buffers;
import com.jogamp.common.nio.PointerBuffer;

public class SpriteRendererJogl4 extends SpriteRendererImpl<GL4> implements
		SpriteRenderer<GL4> {
	static int		SIZEOF_FLOAT	= Buffers.SIZEOF_FLOAT;
	int				_positionPointer, _normalPointer, _texPointer,
			_colorPointer, _vao, _wayToDraw, _nbObjectsToDraw, _text;
	IntBuffer		_temp, _count, _baseVertex;
	PointerBuffer	_arrayOfIndices;
	ShaderProgram	_previousProgram;
	Sprite<GL4>		_parent;
	
	static MaterialSSB _ssbMaterial = null;

	public SpriteRendererJogl4 () {
		super();
		_previousProgram = null;
		_temp = Buffers.newDirectIntBuffer(1);
	}

	@Override
	public void initByApi (GL4 g, ShaderProgram program) {
		if (_ssbMaterial == null)
			_ssbMaterial = MaterialSSB.ssb;
		if (_previousProgram != program) {
			initLocationPointers(program);
			_previousProgram = program;
		}
		_parent = _objects.get(0);

		switch (_parent.getDrawWay()) {
		case TrianglesStrip:
			_wayToDraw = GL4.GL_TRIANGLE_STRIP;
			break;
		case Triangles:
			_wayToDraw = GL4.GL_TRIANGLES;
			break;
		case Points:
			_wayToDraw = GL4.GL_POINTS;
			break;
		case Lines:
			_wayToDraw = GL4.GL_LINES;
			break;
		case LinesStrip:
			_wayToDraw = GL4.GL_LINE_STRIP;
			break;
		}

		//_ssbMaterial.addMaterial((MaterialImpl)sprite.getVertexIndexes().get(0).material);

		boolean hasPos = false, hasNor = false, hasCol = false, hasTex = false, indContinued = _parent
				.indexContinue();
		int nbBuffers = 1;

		_temp.rewind();
		g.glGenVertexArrays(1, _temp);

		g.glEnable(GL_PRIMITIVE_RESTART);
		g.glPrimitiveRestartIndex(getRestartIndex());
		_vao = _temp.get(0);
		// Save all following states
		g.glBindVertexArray(_vao);

		if (_parent.getVertexPosition().capacity() > 0) {
			g.glEnableVertexAttribArray(_positionPointer);
			hasPos = true;
			nbBuffers++;
		}

		if (_parent.getVertexNormal().capacity() > 0) {
			g.glEnableVertexAttribArray(_normalPointer);
			hasNor = true;
			nbBuffers++;
		}

		if (_parent.getVertexColor().capacity() > 0) {
			g.glEnableVertexAttribArray(_colorPointer);
			hasCol = true;
			nbBuffers++;
		}

		if (_parent.getVertexTextureCoordinates().capacity() > 0) {
			g.glEnableVertexAttribArray(_texPointer);
			hasTex = true;
			nbBuffers++;
		}

		_temp = Buffers.newDirectIntBuffer(nbBuffers);
		g.glGenBuffers(nbBuffers, _temp);

		int posSize = 0, colSize = 0, norSize = 0, texSize = 0, indSize = 0;
		// First loop to get all size to attribute to buffers
		for (Sprite<GL4> sprite : _objects) {
			posSize += sprite.getVertexPosition().capacity();
			colSize += sprite.getVertexColor().capacity();
			norSize += sprite.getVertexNormal().capacity();
			texSize += sprite.getVertexTextureCoordinates().capacity();
			
			for (ObjectFaces object : sprite.getVertexIndexes()) {
				indSize += object.faces.capacity();				
			}
		}
		posSize *= SIZEOF_FLOAT;
		colSize *= SIZEOF_FLOAT;
		norSize *= SIZEOF_FLOAT;
		texSize *= SIZEOF_FLOAT;
		indSize *= Buffers.SIZEOF_INT;

		_temp.rewind();
		if (hasPos) {
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glBufferData(GL_ARRAY_BUFFER, posSize, null, GL_STATIC_DRAW);
		}
		if (hasCol) {
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glBufferData(GL_ARRAY_BUFFER, colSize, null, GL_STATIC_DRAW);
		}
		if (hasNor) {
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glBufferData(GL_ARRAY_BUFFER, norSize, null, GL_STATIC_DRAW);
		}
		if (hasTex) {
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glBufferData(GL_ARRAY_BUFFER, texSize, null, GL_STATIC_DRAW);
		}
		g.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
		g.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indSize, null, GL_STATIC_DRAW);

		int offsetInd = 0, offsetPos = 0, offsetNor = 0, offsetCol = 0, offsetTex = 0;
		int size;

		// TODO
		_baseVertex = Buffers.newDirectIntBuffer(_objects.size());
		_baseVertex.rewind();
		
		_nbObjectsToDraw = 0;
		for (Sprite<GL4> sprite : _objects) {
			_nbObjectsToDraw += sprite.getVertexIndexes().size();
		}

		_count = Buffers.newDirectIntBuffer(_nbObjectsToDraw);
		_count.rewind();

		_arrayOfIndices = PointerBuffer.allocateDirect(_nbObjectsToDraw);
		_arrayOfIndices.rewind();

		for (Sprite<GL4> sprite : _objects) {
			_temp.rewind();
			if (hasPos) {
				g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexPosition().capacity() * SIZEOF_FLOAT;
				g.glBufferSubData(GL_ARRAY_BUFFER, offsetPos, size, sprite
						.getVertexPosition().rewind());

				g.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false,
						0, 0);

				if (!indContinued)
					_baseVertex.put(offsetPos / 3);

				offsetPos += size;
			}
			if (hasCol) {
				g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexColor().capacity() * SIZEOF_FLOAT;
				g.glBufferSubData(GL_ARRAY_BUFFER, offsetCol, size, sprite.getVertexColor().rewind());
				g.glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, false, 0, 0);
				offsetCol += size;
			}
			if (hasNor) {
				g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexNormal().capacity() * SIZEOF_FLOAT;
				g.glBufferSubData(GL_ARRAY_BUFFER, offsetNor, size, sprite.getVertexNormal().rewind());
				g.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, true, 0, 0);
				offsetNor += size;
			}
			if (hasTex) {
				g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexTextureCoordinates().capacity() * SIZEOF_FLOAT;
				g.glBufferSubData(GL_ARRAY_BUFFER, offsetTex, size, sprite.getVertexTextureCoordinates().rewind());
				g.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, true, 0, 0);
				offsetTex += size;
			}
			
			
			int idBindElement = _temp.get();
			g.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idBindElement);
			//TODO
			for (int i = 0, max = sprite.getVertexIndexes().size(); i < max; i++) {
				ObjectFaces object = sprite.getVertexIndexes().get(i);
				_count.put(object.faces.capacity());
	
				_arrayOfIndices.put(offsetInd);
	
				size = object.faces.capacity()
						* Buffers.SIZEOF_INT;
				g.glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offsetInd, size, object.faces.rewind());
				offsetInd += size;
			}
			// for(int i = 0; i < sprite.getVertexIndexes().capacity(); i++)
			// System.out.println(sprite.getVertexIndexes().get(i));
			//System.out.println(sprite.getVertexIndexes().get(0).material);
		}

		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null) {
			Texture t = _parent.getVertexIndexes().get(0).material
					.getDiffuseTexture();
			IntBuffer intb = Buffers.newDirectIntBuffer(1);
			_text = intb.get(0);
			g.glGenTextures(1, intb);

			g.glEnable(GL_TEXTURE_2D);
			g.glBindTexture(GL_TEXTURE_2D, _text);
			//g.glActiveTexture(GL_TEXTURE0);
			g.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			g.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			g.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, t.getWidth(),
					t.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, t.getData().rewind());
			g.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			g.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}

		g.glBindVertexArray(0);
		_ssbMaterial.bindShaderProgram(program, "materials");
		_ssbMaterial.updateSsbo();
		_materialLocation = program.getUniformLocation("materialId");
	}
	int _materialLocation;
	
	@Override
	public void draw (GL4 g, ShaderProgram program) {
		_count.rewind();
		_arrayOfIndices.rewind();
		_temp.rewind();
		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null)
			g.glBindTexture(GL_TEXTURE_2D, _text);
		g.glBindVertexArray(_vao);
		if (_parent.indexContinue()) {
			/*g.glMultiDrawElements(_wayToDraw, _count, GL4.GL_UNSIGNED_INT,
					_arrayOfIndices, _nbObjectsToDraw);*/
			_temp.rewind();
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false, 0, 0);
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, false, 0, 0);
			g.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			g.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, false, 0, 0);
			g.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
			/*g.glMultiDrawElements(_wayToDraw, _count, GL4.GL_UNSIGNED_INT,
					_arrayOfIndices, _nbObjectsToDraw);*/
			// Display opaque elements before
			for (Sprite<GL4> sprite : _objects) 
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					long arrayOfIndex = _arrayOfIndices.get();
					if (object.material.getDissolve() > 0.999f) {
						g.glUniform1i(_materialLocation, object.material.getIdSsb());
						g.glDrawElements(_wayToDraw, count, GL_UNSIGNED_INT, arrayOfIndex);
					}
				}
			_count.rewind();
			_arrayOfIndices.rewind();
			// Display transparent element after
			for (Sprite<GL4> sprite : _objects) 
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					long arrayOfIndex = _arrayOfIndices.get();
					if (object.material.getDissolve() < 0.9999f) {
						g.glUniform1i(_materialLocation, object.material.getIdSsb());
						g.glDrawElements(_wayToDraw, count, GL_UNSIGNED_INT, arrayOfIndex);
					}
				}
			
		}
		else {
			_baseVertex.rewind();
			g.glMultiDrawElementsBaseVertex(_wayToDraw, _count,
					GL4.GL_UNSIGNED_INT, _arrayOfIndices, _nbObjectsToDraw,
					_baseVertex);
		}
		g.glBindVertexArray(0);
	}

	private void initLocationPointers (ShaderProgram program) {
		_positionPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_POSITION);
		_normalPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_NORMAL);
		_colorPointer = program.getAttribLocation(ShaderVariable.BUILTIN_COLOR);
		_texPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_TEXCOORDINATES);
	}

	@Override
	protected void removeByApi (Sprite<GL4> sprite) {
		// TODO Auto-generated method stub
	}
}
