package com.hsee.gl.impl.jogl4;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;
import com.hsee.gl.impl.jogl4.light.DirectionalLightJogl4;
import com.hsee.gl.impl.jogl4.light.PointLightJogl4;
import com.hsee.gl.impl.jogl4.light.SpotLightJogl4;
import com.hsee.gl.impl.jogl4.mvp.ModelViewProjectionJogl4;
import com.hsee.gl.impl.jogl4.shader.ShaderFactoryJogl4;
import com.hsee.gl.impl.jogl4.shader.ShaderProgramFactoryJogl4;
import com.hsee.gl.impl.jogl4.shader.ShaderVariableFactoryJogl4;
import com.hsee.gl.impl.jogl4.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.impl.jogl4.sprite.io.SpriteImporterJogl4;
import com.hsee.gl.impl.jogl4.sprite.shape.Cube;
import com.hsee.gl.impl.jogl4.sprite.shape.Plan;
import com.hsee.gl.light.DirectionalLight;
import com.hsee.gl.light.PointLight;
import com.hsee.gl.light.SpotLight;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderFactory;
import com.hsee.gl.shader.ShaderVariableFactory;
import com.hsee.gl.shader.impl.ShaderProgramFactory;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;
import com.jogamp.opengl.GL4;

public class HseeAPIJogl4 implements HseeAPI {
	private GL4 g;
	private GraphicsGatewayJogl4 ggateway;

	public HseeAPIJogl4() {
	}
	
	public HseeAPIJogl4(GL4 glContext) {
		g = glContext;

		ggateway = new GraphicsGatewayJogl4(g);
		
		ShaderFactoryJogl4.initFactory(g);
		ShaderVariableFactoryJogl4.initFactory(g);
		ShaderProgramFactoryJogl4.initFactory(g);
		
		MaterialSSB.initMaterialSsb(g);
	}
	
	@Override
	public DirectionalLight createDirectionalLight(String shaderVariableName) {
		return new DirectionalLightJogl4(g, shaderVariableName);
	}

	@Override
	public PointLight createPointLight(String shaderVariableName) {
		return new PointLightJogl4(g, shaderVariableName);
	}

	@Override
	public SpotLight createSpotLight(String shaderVariableName) {
		return new SpotLightJogl4(g, shaderVariableName);
	}

	@Override
	public ShaderFactory getShaderFactory() {
		return ShaderFactoryJogl4.factory;
	}

	@Override
	public ShaderProgramFactory getShaderProgramFactory() {
		return ShaderProgramFactoryJogl4.factory;
	}

	@Override
	public ShaderVariableFactory getShaderVariableFactory() {
		return ShaderVariableFactoryJogl4.factory;
	}

	@Override
	public AbstractCube createCube(String name) {
		return new Cube(g, name);
	}

	@Override
	public AbstractPlan createPlan(String name, int size) {
		return new Plan(g, name, size);
	}

	@Override
	public Sprite createSprite(ImporterSetter importer,
			SpriteCompiler compiler, String name, String path, String filename) {
		return new SpriteImporterJogl4(g, importer, compiler, name, path, filename);
	}

	@Override
	public ModelViewProjection createModelViewProjection() {
		return new ModelViewProjectionJogl4(g);
	}

	@Override
	public Object getUnderlayedAPI() {
		return g;
	}

	@Override
	public GraphicsGateway getGraphicsGateway() {
		return ggateway;
	}

	@Override
	public Sprite createSprite (ImporterSetter importer,
			SpriteCompiler compiler, InputStreamGetter reader, String name,
			String path, String filename) {
		return null;
	}

}
