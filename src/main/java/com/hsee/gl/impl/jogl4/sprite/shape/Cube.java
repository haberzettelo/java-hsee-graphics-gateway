package com.hsee.gl.impl.jogl4.sprite.shape;

import com.hsee.gl.impl.jogl4.sprite.renderer.SpriteRendererJogl4;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.opengl.GL4;

public class Cube extends AbstractCube<GL4>
{
	public Cube (GL4 g, String name)
	{
		super(g, name);
	}

	@Override
	protected SpriteRenderer<GL4> newRendererByApi (GL4 g) {
		return new SpriteRendererJogl4();
	}

}
