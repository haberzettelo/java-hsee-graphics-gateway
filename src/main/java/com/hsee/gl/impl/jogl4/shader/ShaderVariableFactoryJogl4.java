package com.hsee.gl.impl.jogl4.shader;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.jogamp.opengl.GL4;
import com.hsee.gl.math.matrix.Matrix;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.ShaderVariableFactory;
import com.hsee.gl.shader.impl.ShaderVariableImpl;

public class ShaderVariableFactoryJogl4 implements ShaderVariableFactory
{
	public static ShaderVariableFactory factory = null;
	
	public static void initFactory(GL4 ctx) {
		factory = new ShaderVariableFactoryJogl4(ctx);
	}
	
	private ShaderVariableFactoryJogl4 (GL4 context) {
		this.g = context;
	}
	GL4 g;

	@Override
	public ShaderVariable<Float> newFloat (Object varNameOrLocation)
	{
		return new ShaderVariableImpl<Float>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (
					int location)
			{
				g.glUniform1f(location, _var);
			}
		};
	}

	@Override
	public ShaderVariable<Integer> newInteger (Object varNameOrLocation)
	{
		return new ShaderVariableImpl<Integer>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (
					int location)
			{
				g.glUniform1i(location, _var);
			}
		};
	}

	@Override
	public ShaderVariable<Boolean> newBoolean (Object varNameOrLocation)
	{
		return new ShaderVariableImpl<Boolean>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (int location)
			{
				if (_var)
					g.glUniform1i(location, 1);
				else
					g.glUniform1i(location, 0);
					
			}
		};
	}

	@Override
	public ShaderVariable<Double> newDouble (Object varNameOrLocation)
	{
		return new ShaderVariableImpl<Double>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (int location)
			{
				g.glUniform1d(location, _var);
			}
		};
	}

	@Override
	public ShaderVariable<FloatBuffer> newFloatArray (Object varNameOrLocation, int size)
	{
		return new ShaderVariableImpl<FloatBuffer>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (int location)
			{
				g.glUniform1fv(location, _var.capacity(), _var);
			}
		};
	}

	@Override
	public ShaderVariable<IntBuffer> newIntArray (Object varNameOrLocation, int size)
	{
		return new ShaderVariableImpl<IntBuffer>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (int location)
			{
				g.glUniform1iv(location, _var.capacity(), _var);
			}
		};
	}

	@Override
	public ShaderVariable<IntBuffer> newBooleanArray (Object varNameOrLocation, int size)
	{
		return new ShaderVariableImpl<IntBuffer>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (int location)
			{
				g.glUniform1iv(location, _var.capacity(), _var);
			}
		};
	}

	@Override
	public ShaderVariable<DoubleBuffer> newDoubleArray (Object varNameOrLocation, int size)
	{
		return new ShaderVariableImpl<DoubleBuffer>(varNameOrLocation)
		{
			@Override
			protected void updateShaderDataByApi (int location)
			{
				g.glUniform1dv(location, _var.capacity(), _var);
			}
		};
	}

	@Override
	public ShaderVariable<FloatBuffer> newFloatVec (Object varNameOrLocation, int size)
	{
		if (size == 1)
			return new ShaderVariableImpl<FloatBuffer>(varNameOrLocation)
					{
						@Override
						protected void updateShaderDataByApi (int location)
						{
							g.glUniform1f(location, _var.get(0));
						}
					};
		else if (size == 2) 
			return new ShaderVariableImpl<FloatBuffer>(varNameOrLocation)
					{
						@Override
						protected void updateShaderDataByApi (int location)
						{
							_var.rewind();
							g.glUniform2f(location, _var.get(), _var.get());
						}
					};
		else if (size == 3) 
			return new ShaderVariableImpl<FloatBuffer>(varNameOrLocation)
					{
						@Override
						protected void updateShaderDataByApi (int location)
						{
							_var.rewind();
							g.glUniform3f(location, _var.get(), _var.get(), _var.get());
						}
					};
		else if (size == 4) 
			return new ShaderVariableImpl<FloatBuffer>(varNameOrLocation)
					{
						@Override
						protected void updateShaderDataByApi (int location)
						{
							_var.rewind();
							g.glUniform4f(location, _var.get(), _var.get(), _var.get(), _var.get());
						}
					};
		else return null;
}

	@Override
	public ShaderVariable<IntBuffer> newIntVec (Object varNameOrLocation, int size)
			{
		return new ShaderVariableImpl<IntBuffer>(varNameOrLocation)
				{
					@Override
					protected void updateShaderDataByApi (int location)
					{
						//TODO implementation
						//g.glUniform1dv(location, _var.capacity(), _var);
					}
				};
	}

	@Override
	public ShaderVariable<DoubleBuffer> newDoubleVec (Object varNameOrLocation, int size)
	{
		return new ShaderVariableImpl<DoubleBuffer>(varNameOrLocation)
				{
					@Override
					protected void updateShaderDataByApi (int location)
					{
						//TODO implementation
						//g.glUniform1dv(location, _var.capacity(), _var);
					}
				};
	}

	@Override
	public ShaderVariable<Matrix> newMatrix (Object varNameOrLocation)
	{
		return new ShaderVariableImpl<Matrix>(varNameOrLocation)
				{
					@Override
					protected void updateShaderDataByApi (int location)
					{
						//TODO implementation
						//g.glUniform1dv(location, _var.capacity(), _var);
					}
				};
	}

}
