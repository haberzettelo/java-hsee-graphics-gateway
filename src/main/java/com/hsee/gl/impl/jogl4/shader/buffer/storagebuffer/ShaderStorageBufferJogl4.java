package com.hsee.gl.impl.jogl4.shader.buffer.storagebuffer;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import com.hsee.gl.impl.jogl4.exception.HseeExceptionImpl;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.buffer.storagebuffer.ShaderStorageBuffer;
import com.hsee.gl.shader.impl.structure.ShaderStructureImpl;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL4;

public abstract class ShaderStorageBufferJogl4 extends ShaderStructureImpl implements ShaderStorageBuffer {
	private GL4 g;
	private int _ssbId, _bindingId;
	public ShaderStorageBufferJogl4 (GL4 g, int sizeOfSsb, int bindingPoint) {
		this.g = g;
		try {
			IntBuffer b = Buffers.newDirectIntBuffer(1);
			g.glGenBuffers(1, b);
			_ssbId = b.get(0);
			initData(sizeOfSsb);
			bindBlock(bindingPoint);
			HseeExceptionImpl.getOpenGLError(g);
		}
		catch (HseeExceptionImpl e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void bindBlock (int bindingPoint) {
		_bindingId = bindingPoint;
		g.glBindBufferBase(GL4.GL_SHADER_STORAGE_BUFFER, bindingPoint, _ssbId);
	}

	@Override
	public void bindShaderProgram (ShaderProgram program, String ssbName) {
		int location = program.getStorageBufferLocation(ssbName);
		g.glShaderStorageBlockBinding(program.getProgramId(), location, _bindingId);
	}

	
	
	@Override
	public ByteBuffer mapData (boolean read, boolean write) {
		g.glBindBuffer(GL4.GL_SHADER_STORAGE_BUFFER, _ssbId);
		int state = GL4.GL_READ_ONLY;
		if(read && write) {
			state = GL4.GL_READ_WRITE;
		} else if(!read && write) {
			state = GL4.GL_WRITE_ONLY;
		}
		return g.glMapBuffer(GL4.GL_SHADER_STORAGE_BUFFER, state);
	}

	@Override
	public void unmapData () {
		g.glUnmapBuffer(GL4.GL_SHADER_STORAGE_BUFFER);
		g.glBindBuffer(GL4.GL_SHADER_STORAGE_BUFFER, 0);
	}
	
	@Override
	public void initData (int size) {
		g.glBindBuffer(GL4.GL_SHADER_STORAGE_BUFFER, _ssbId); 
		g.glBufferData(GL4.GL_SHADER_STORAGE_BUFFER, size, Buffers.newDirectByteBuffer(size), GL4.GL_STATIC_DRAW); 
		g.glBindBuffer(GL4.GL_SHADER_STORAGE_BUFFER, 0);
	}
	
	@Override
	public ShaderVariable updateShaderData (ShaderProgram updater) {
		return null;
	}
}
