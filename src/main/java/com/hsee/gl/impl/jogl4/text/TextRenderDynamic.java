package com.hsee.gl.impl.jogl4.text;

import java.awt.Rectangle;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import com.hsee.gl.text.Glyphs;
import com.jogamp.common.nio.Buffers;

public class TextRenderDynamic
{
	/*VertexBufferObject _vboData;
	ShaderStorageGlyphsObject _ssgo;
	IntBuffer _charData;
	GL4 g;
	Mat4 matProjection = new Mat4();
	
	int _attributeLocation, _matrixLocation, _deviceLocation, _nbChars;
	Texture _texture;
	ShaderProgram _program;
	String _path = "resources/shaders/test/";
	Glyphs _glyphs;
	
	public TextRenderDynamic(GL4 g, int width, int height, ByteBuffer texture, Glyphs glyphs, int maxChars)
	{
		//System.out.println("width:"+width + ", height:" + height + ", cap:" + texture.capacity() + ", nbpixels:" + (width*height));
		_vboData = new VertexBufferObject(g, GL4.GL_ARRAY_BUFFER, GL4.GL_STATIC_DRAW);
		//_vboData.setData(Buffers.newDirectIntBuffer(maxChars));
		_charData = Buffers.newDirectIntBuffer(maxChars*2);
		
		
		_glyphs = glyphs;
		this.g = g;
		_texture = new Texture(g, width, height, texture);
		
		
		matProjection.loadIdentity();
		//matProjection.scale(new Vec3(0.2f, 0.2f, 0.2f));
		
		VertexShader vs = new VertexShader(g);
		GeometryShader gs = new GeometryShader(g);
		FragmentShader fs = new FragmentShader(g);
		vs.loadFromFile("./" + _path + "dynamicText.vs");
		gs.loadFromFile("./" + _path + "dynamicText.gs");
		fs.loadFromFile("./" + _path + "dynamicText.fs");
		vs.compile();
		gs.compile();
		fs.compile();
		
		
		_program = new ShaderProgram(g, false);
		_program.addShader(vs);
		_program.addShader(gs);
		_program.addShader(fs);
		_program.compileProgram();
		_program.linkProgram();
		_program.validateProgram();

		_attributeLocation = _program.getAttributeLocation("characterIn");
		
		_program.enable();
		_deviceLocation = _program.getConstantLocation("device");
		_matrixLocation = _program.getConstantLocation("mvp");
		_program.setConstantMat4D(_matrixLocation, matProjection.toArray());
		_program.setConstant4D("textPosition", 1, -1, 0, 0);
		_ssgo = new ShaderStorageGlyphsObject(g, glyphs, 5, "Glyphs");
		_ssgo.bind(_program.getId());
		
		_program.disable();
	}
	
	public void setText (String text) {
		if (text.length()*2 > _charData.capacity())
			_charData = Buffers.newDirectIntBuffer(text.length()*2);
		_charData.rewind();
		int offset = 0;
		for (char c : text.toCharArray()) {
			_charData.put(c);
			_charData.put(offset);
			offset += _glyphs.width[c];
		}
		_charData.rewind();
		
		_nbChars = text.length();
		
		_vboData.bind();
		_vboData.bindIntegerAttribute(_attributeLocation, GL4.GL_INT, 2, 0, 0);
		_vboData.setData(_charData);
		_vboData.unBind();
	}
	
	public void setModelViewProjection(Mat4 mat) {
		_program.enable();
		_program.setConstantMat4D(_matrixLocation, mat.toArray());		
		_program.disable();
	}
	
	public void setPosition (Vec3 pos) {
		_program.enable();
		_program.setConstant4D("textPosition", (float)pos.getX(), (float)pos.getY(), (float)pos.getZ(), 0);	
		_program.disable();
	}
	
	public void draw(RenderViewInfo info) {
		_program.enable();
		Rectangle device = info.getDeviceBound();
		_program.setConstant4D(_deviceLocation, (float)device.getMinX(), (float)device.getMinY(), (float)device.getWidth(), (float)device.getHeight());
		_texture.bind();
		_vboData.bind();

		g.glEnable(GL.GL_BLEND);
		g.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
		g.glDrawArrays(GL4.GL_POINTS, 0, _nbChars);
		
		_vboData.unBind();
		_texture.unBind();
		_program.disable();
	}*/
}
