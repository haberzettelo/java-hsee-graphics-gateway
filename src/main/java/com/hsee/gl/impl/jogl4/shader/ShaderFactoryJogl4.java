package com.hsee.gl.impl.jogl4.shader;

import com.jogamp.opengl.GL4;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderFactory;

public class ShaderFactoryJogl4 implements ShaderFactory
{
	public static ShaderFactoryJogl4 factory = null;
	public static void initFactory(GL4 ctx) {
		factory = new ShaderFactoryJogl4(ctx);
	}
	
	private ShaderFactoryJogl4 (GL4 context) {
		this.graphicalContext = context;
	}
	GL4 graphicalContext;
	
	@Override
	public Shader newComputeShader () 
	{
		return new ShaderJogl4(graphicalContext, GL4.GL_COMPUTE_SHADER) {
			@Override
			public boolean isComputeShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newFragmentShader ()
	{
		return new ShaderJogl4(graphicalContext, GL4.GL_FRAGMENT_SHADER) {
			@Override
			public boolean isFragmentShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newVertexShader ()
	{
		return new ShaderJogl4(graphicalContext, GL4.GL_VERTEX_SHADER) {
			@Override
			public boolean isVertexShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newGeometryShader ()
	{
		return new ShaderJogl4(graphicalContext, GL4.GL_GEOMETRY_SHADER) {
			@Override
			public boolean isGeometryShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newTesselationControlShader ()
	{
		return new ShaderJogl4(graphicalContext, GL4.GL_TESS_CONTROL_SHADER) {
			@Override
			public boolean isTesselationControlShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newTesselationEvaluationShader ()
	{
		return new ShaderJogl4(graphicalContext, GL4.GL_TESS_EVALUATION_SHADER) {
			@Override
			public boolean isTesselationEvaluationShader () {
				return true;
			}
		};
	}
}
