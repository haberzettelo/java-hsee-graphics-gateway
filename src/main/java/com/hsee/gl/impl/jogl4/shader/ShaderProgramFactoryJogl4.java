package com.hsee.gl.impl.jogl4.shader;

import com.jogamp.opengl.GL4;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.impl.ShaderProgramFactory;

public class ShaderProgramFactoryJogl4 implements ShaderProgramFactory
{
	public static ShaderProgramFactoryJogl4 factory = null;
	
	public static void initFactory(GL4 ctx) {
		factory = new ShaderProgramFactoryJogl4(ctx);
	}
	
	private ShaderProgramFactoryJogl4 (GL4 context) {
		this.g = context;
	}
	GL4 g;
	
	@Override
	public ShaderProgram newProgram ()
	{
		return new ShaderProgramSimpleJogl4(g);
	}

	@Override
	public ShaderProgram newSeparateProgram ()
	{
		return new ShaderProgramSimpleJogl4(g);
	}

}
