package com.hsee.gl.impl.jogl4.sprite.shape;

import com.hsee.gl.impl.jogl4.sprite.renderer.SpriteRendererJogl4;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.opengl.GL4;

public class Plan extends AbstractPlan<GL4>
{

	public Plan (GL4 g, String name, float size)
	{
		super(g, name, size);
	}

	@Override
	protected SpriteRenderer<GL4> newRendererByApi (GL4 g) {
		return new SpriteRendererJogl4();
	}

}
