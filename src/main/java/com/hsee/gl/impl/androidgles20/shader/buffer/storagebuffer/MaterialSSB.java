package com.hsee.gl.impl.androidgles20.shader.buffer.storagebuffer;

import android.opengl.GLES20;
import android.util.Log;
import static android.opengl.GLES20.*;

import com.hsee.gl.exception.HseeException;
import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.impl.androidgles20.exception.HseeExceptionImpl;
import com.hsee.gl.math.primitive.Vector3;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.sprite.Material;
import com.hsee.gl.sprite.MaterialSetter;
import com.hsee.gl.sprite.impl.MaterialImpl;
import com.jogamp.common.nio.Buffers;

import org.apache.log4j.Logger;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * This class represent all material of all sprites, all materials of the application
 * will be stored in a ShaderStorageBufferObject.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 mai 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class MaterialSSB extends ShaderStorageBufferAndroidGLES20 {
	private static Logger logger = Logger.getLogger(MaterialSSB.class);
	private static String MATERIAL_NAME = BUFFER_PREFIX + "Material", MATERIAL_VAR_NAME = "materials";
	private static int NB_VAR_MATERIAL = 8;
	private static String[] MATERIAL_VARS_NAME = new String[] {
					"ambientColor",
					"diffuseColor",
					"specularColor",
					"transmissionFilter",
					"specularExponent",
					"sharpness",
					"opticalDensity",
					"dissolve"}; 
	private List<Material> _materials = null;
	
	private int _nbMaterial = 0, _nbMaterialInitialized = 0;
	/**
	 * ambientColor (3 + 1) + 
	 * diffuseColor (3 + 1) +
	 * specularColor(3 + 1) +
	 * trFilter		(3 + 1) +
	 * specExponent (1) +
	 * sharpness	(1) +
	 * opticalDensit(1) +
	 * dissolve		(1)
	 * 	==
	 * 20
	 */
	private static int MATERIAL_SIZE = 20 * Buffers.SIZEOF_FLOAT;
	public static int MAX_MATERIAL = 16;
	private ArrayList<Integer> _locations = new ArrayList<Integer>(MAX_MATERIAL * NB_VAR_MATERIAL);
	
	public static MaterialSSB ssb = null;
	public static void initMaterialSsb (GLES20 g) {
		ssb = new MaterialSSB(null);
	}
	
	private MaterialSSB (GLES20 g) {
		super(null, MAX_MATERIAL * MATERIAL_SIZE, 0);
		addMaterial(new MaterialImpl());
	}

	@Override
	public void initShader (Shader shader) {
		shader.append(
				  "struct " + MATERIAL_NAME + " {\n"
				+ "	mediump vec4 "+MATERIAL_VARS_NAME[0]+";\n"
				+ "	mediump vec4 "+MATERIAL_VARS_NAME[1]+";\n"
				+ "	mediump vec4 "+MATERIAL_VARS_NAME[2]+";\n"
				+ "	mediump vec4 "+MATERIAL_VARS_NAME[3]+";\n"
				+ "	mediump float "+MATERIAL_VARS_NAME[4]+";\n"
				+ "	mediump float "+MATERIAL_VARS_NAME[5]+";\n"
				+ "	mediump float "+MATERIAL_VARS_NAME[6]+";\n"
				+ "	mediump float "+MATERIAL_VARS_NAME[7]+";\n"
				+ "};\n\n"
				+ "uniform mediump " + MATERIAL_NAME + " " + MATERIAL_VAR_NAME + "["+MAX_MATERIAL+"];\n"
				);
	}

	/**
	 * Add one material to all material on gpu
	 * @param mat
	 */
	public void addMaterial(MaterialSetter mat) {
		if (_materials == null)
			_materials = new ArrayList<Material>();
		else if (_materials.size() >= MAX_MATERIAL) {
			logger.error("Limit material reached (" + MAX_MATERIAL + "), material " + mat.getName() + " ignored");
			return ;
		}
			
		mat.setIdSsb(_nbMaterial++);
		_materials.add(mat);
	}
	
	/**
	 * Add a list of material to all material on gpu
	 * @param mat
	 */
	public void addMaterial(List<MaterialSetter> mat) {
		if (_materials == null) {
			_materials = new ArrayList<Material>(mat.size());
			for (MaterialSetter materialSetter : mat){
				materialSetter.setIdSsb(_nbMaterial++);
				_materials.add(materialSetter);
			}
		}
		else
			for (MaterialSetter materialImpl : mat) {
				if (_materials.size() >= MAX_MATERIAL) {
					logger.error("Limit material reached (" + MAX_MATERIAL + "), material " + materialImpl.getName() + " ignored");
				} else {
					materialImpl.setIdSsb(_nbMaterial++);
					_materials.add(materialImpl);
				}
				
			}
	}
	
	/**
	 * Update the Shader storage buffer object in gpu memory
	 */
	public void updateSsbo () {
		//ByteBuffer buffer = mapData(false, true);
		//buffer.rewind();
		int counter = 0;
		try {
			HseeExceptionImpl.getOpenGLError(null);
		}
		catch (HseeExceptionImpl e) {
			Log.e("debug material", e.getMessage());
		}
		for (ShaderProgram shaderProgram : _prgs) {
			shaderProgram.useProgram();
			for (Material material : _materials) {
				putVector(this._locations.get(counter++), material.getAmbientReflexivity());
				putVector(this._locations.get(counter++), material.getDiffuseReflexivity());
				putVector(this._locations.get(counter++), material.getSpecularReflexivity());
				putVector(this._locations.get(counter++), material.getTransmissionFilter());
				glUniform1f(this._locations.get(counter++), material.getSpecularExponent());
				glUniform1f(this._locations.get(counter++), material.getSharpness());
				glUniform1f(this._locations.get(counter++), material.getOpticalDensity());
				glUniform1f(this._locations.get(counter++), material.getDissolve());
			}
		}
		//unmapData();
		try {
			HseeExceptionImpl.getOpenGLError(null);
		}
		catch (HseeExceptionImpl e) {
			Log.e("debug material", e.getMessage());
		}
	}
	
	private static void putVector(int location, Vector3 v) {
		float[] va;
		if (v == null)
			va = Vector3.NULL_VECTOR.getCoordinates();
		else
			va = v.getCoordinates();
		glUniform4f(location, va[0], va[1], va[2], 0);
	}

	
	@Override
	public void bindShaderProgram (ShaderProgram program, String ssbName) {
		int counter = 0;
		String prefix = MATERIAL_VAR_NAME + "[.";
		Log.d("debug material", "beginning of get location for material");
		
		
		StringBuilder builder = new StringBuilder(prefix);
		StringBuilder builderOneVar = new StringBuilder(builder);
		builderOneVar.append("].");
		try {
			HseeExceptionImpl.getOpenGLError(null);
		}
		catch (HseeExceptionImpl e) {
			Log.e("debug material", e.getMessage());
		}
		for (int k = 0, max = _materials.size() - _nbMaterialInitialized; k < max; k++) {
			int nbOfPreviousDigit = String.valueOf((int)(k>0?k-1:0)).length();
			builder.delete(builder.length() - nbOfPreviousDigit, builder.length());
			builder.append(counter);
			
			builderOneVar.delete(0, builderOneVar.length());
			builderOneVar.append(builder);
			builderOneVar.append("].h");
			for(int i = 0; i < NB_VAR_MATERIAL; i++) {
				builderOneVar.delete(builder.length() + 2, builderOneVar.length());
				builderOneVar.append(MATERIAL_VARS_NAME[i]);
				this._locations.add(GLES20.glGetUniformLocation(program.getProgramId(), builderOneVar.toString()));
				Log.d("debug material", builderOneVar.toString());
			}
			counter++;
		}
		try {
			HseeExceptionImpl.getOpenGLError(null);
		}
		catch (HseeExceptionImpl e) {
			Log.e("debug material", e.getMessage());
		}
		if (!_prgs.contains(program))
			_prgs.add(program);
		
		_nbMaterialInitialized = _materials.size();
	}

	@Override
	public void initData (int size) {
		
	}
	
}
