package com.hsee.gl.impl.androidgles20.sprite.renderer;

import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_REPEAT;
import static android.opengl.GLES20.GL_RGB;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import android.opengl.GLES20;
import android.util.Log;

import com.hsee.gl.impl.androidgles20.exception.HseeExceptionImpl;
import com.hsee.gl.impl.androidgles20.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.Texture;
import com.hsee.gl.sprite.impl.renderer.SpriteRendererImpl;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.common.nio.Buffers;

public class RecupSpriteRendererAndroidGLES20 extends SpriteRendererImpl<GLES20> implements
		SpriteRenderer<GLES20> {
	static int		SIZEOF_FLOAT	= Buffers.SIZEOF_FLOAT;
	int				_positionPointer, _normalPointer, _texPointer,
			_colorPointer, _vao, _wayToDraw, _nbObjectsToDraw, _text;
	IntBuffer _count, _baseVertex;
	List<ShortBuffer> _indexes;
	long[]	_arrayOfIndices;
	ShaderProgram	_previousProgram;
	Sprite<GLES20>		_parent;
	protected boolean hasPos = false, hasNor = false, hasCol = false, hasTex = false;
	
	static MaterialSSB _ssbMaterial = null;

	public RecupSpriteRendererAndroidGLES20() {
		super();
		_previousProgram = null;
	}

	@Override
	public void initByApi (GLES20 g, ShaderProgram program) {
		if (_ssbMaterial == null)
			_ssbMaterial = MaterialSSB.ssb;
		if (_previousProgram != program) {
			initLocationPointers(program);
			_previousProgram = program;
		}
		_parent = _objects.get(0);

		switch (_parent.getDrawWay()) {
		case TrianglesStrip:
			_wayToDraw = GLES20.GL_TRIANGLE_STRIP;
			break;
		case Triangles:
			_wayToDraw = GLES20.GL_TRIANGLES;
			break;
		case Points:
			_wayToDraw = GLES20.GL_POINTS;
			break;
		case Lines:
			_wayToDraw = GLES20.GL_LINES;
			break;
		case LinesStrip:
			_wayToDraw = GLES20.GL_LINE_STRIP;
			break;
		}

		//_ssbMaterial.addMaterial((MaterialImpl)sprite.getVertexIndexes().get(0).material);

		boolean indContinued = _parent
				.indexContinue();
		int nbBuffers = 1;
		// TODO Adapt
		/*GLES20.glGenVertexArrays(1, _temp);

		GLES20.glEnable(GLES20.GL_PRIMITIVE_RESTART);
		GLES20.glPrimitiveRestartIndex(getRestartIndex());
		_vao = _temp.get(0);
		// Save all following states
		GLES20.glBindVertexArray(_vao);*/


		if (_parent.getVertexPosition().capacity() > 0) {
			hasPos = _positionPointer > -1;
			if (hasPos) {
				GLES20.glEnableVertexAttribArray(_positionPointer);
				//nbBuffers++;
			}
		}

		if (_parent.getVertexNormal().capacity() > 0) {
			hasNor = _normalPointer > -1;
			if (hasNor) {
				GLES20.glEnableVertexAttribArray(_normalPointer);
				//nbBuffers++;
			}
		}

		if (_parent.getVertexColor().capacity() > 0) {
			hasCol = _colorPointer > -1;
			if(hasCol) {
				GLES20.glEnableVertexAttribArray(_colorPointer);
				//nbBuffers++;
			}
		}

		if (_parent.getVertexTextureCoordinates().capacity() > 0) {
			hasTex = _texPointer > -1;
			if (hasTex) {
				GLES20.glEnableVertexAttribArray(_texPointer);
				//nbBuffers++;
			}
		}
		
		int posSize = 0, colSize = 0, norSize = 0, texSize = 0, indSize = 0, nbSubObject = 0;
		// First loop to get all size to attribute to buffers
		for (Sprite<GLES20> sprite : _objects) {
			posSize += sprite.getVertexPosition().capacity();
			colSize += sprite.getVertexColor().capacity();
			norSize += sprite.getVertexNormal().capacity();
			texSize += sprite.getVertexTextureCoordinates().capacity();
			
			for (ObjectFaces object : sprite.getVertexIndexes()) {
				indSize += object.faces.capacity();
				nbSubObject++;
			}
		}
		this._indexes = new ArrayList<ShortBuffer>(nbSubObject);
		posSize *= SIZEOF_FLOAT;
		colSize *= SIZEOF_FLOAT;
		norSize *= SIZEOF_FLOAT;
		texSize *= SIZEOF_FLOAT;
		indSize *= Buffers.SIZEOF_INT;

		/*if (hasPos) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, posSize, null, GL_STATIC_DRAW);
		}
		if (hasCol) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, colSize, null, GL_STATIC_DRAW);
		}
		if (hasNor) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, norSize, null, GL_STATIC_DRAW);
		}
		if (hasTex) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, texSize, null, GL_STATIC_DRAW);
		}*/
		//GLES20.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
		//GLES20.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indSize, null, GL_STATIC_DRAW);

		int offsetInd = 0, offsetPos = 0, offsetNor = 0, offsetCol = 0, offsetTex = 0;
		int size;

		// TODO
		_baseVertex = Buffers.newDirectIntBuffer(_objects.size());
		_baseVertex.rewind();
		
		_nbObjectsToDraw = 0;
		for (Sprite<GLES20> sprite : _objects) {
			_nbObjectsToDraw += sprite.getVertexIndexes().size();
		}

		_count = Buffers.newDirectIntBuffer(_nbObjectsToDraw);
		_count.rewind();

		_arrayOfIndices = new long[_nbObjectsToDraw];
		int iArrrayOfIndice = 0;

		for (Sprite<GLES20> sprite : _objects) {
			if (hasPos) {
				//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexPosition().capacity() * SIZEOF_FLOAT;
				//GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetPos, size, sprite
				//		.getVertexPosition().rewind());

				//GLES20.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false,
				//		0, sprite.getVertexPosition().rewind());

				if (!indContinued)
					_baseVertex.put(offsetPos / 3);

				offsetPos += size;
			}
			if (hasCol) {
				//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexColor().capacity() * SIZEOF_FLOAT;
				//GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetCol, size, sprite.getVertexColor().rewind());
				//GLES20.glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, false, 0, sprite.getVertexColor().rewind());
				offsetCol += size;
			}
			if (hasNor) {
				//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexNormal().capacity() * SIZEOF_FLOAT;
				//GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetNor, size, sprite.getVertexNormal().rewind());
				//GLES20.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, true, 0, sprite.getVertexNormal().rewind());
				offsetNor += size;
			}
			if (hasTex) {
				//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexTextureCoordinates().capacity() * SIZEOF_FLOAT;
				//GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetTex, size, sprite.getVertexTextureCoordinates().rewind());
				//GLES20.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, true, 0, sprite.getVertexTextureCoordinates().rewind());
				offsetTex += size;
			}
			
			
			//GLES20.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idBindElement);
			//TODO
			for (int i = 0, max = sprite.getVertexIndexes().size(); i < max; i++) {
				ObjectFaces object = sprite.getVertexIndexes().get(i);
				_count.put(object.faces.capacity());
	
				_arrayOfIndices[iArrrayOfIndice++] = offsetInd;
	
				size = object.faces.capacity()
						* Buffers.SIZEOF_SHORT;
				object.faces.rewind();
				ShortBuffer buf = Buffers.newDirectShortBuffer(object.faces.capacity());
				for (int j = 0, max2 = buf.capacity(); j < max2; j++)
					buf.put((short)object.faces.get());
				buf.rewind();
				this._indexes.add(buf);
				//GLES20.glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offsetInd, size, buf);
				offsetInd += size;
			}
			// for(int i = 0; i < sprite.getVertexIndexes().capacity(); i++)
			// System.out.println(sprite.getVertexIndexes().get(i));
			//System.out.println(sprite.getVertexIndexes().get(0).material);
		}

		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null) {
			Texture t = _parent.getVertexIndexes().get(0).material
					.getDiffuseTexture();
			IntBuffer intb = Buffers.newDirectIntBuffer(1);
			_text = intb.get(0);
			GLES20.glGenTextures(1, intb);

			GLES20.glEnable(GL_TEXTURE_2D);
			GLES20.glBindTexture(GL_TEXTURE_2D, _text);
			//GLES20.glActiveTexture(GL_TEXTURE0);
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			GLES20.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, t.getWidth(),
					t.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, t.getData().rewind());
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}

		//GLES20.glBindVertexArray(0);
		_ssbMaterial.bindShaderProgram(program, "materials");
		_ssbMaterial.updateSsbo();
		_materialLocation = program.getUniformLocation("materialId");
	}
	int _materialLocation;
	
	@Override
	public void draw (GLES20 g, ShaderProgram program){
		_count.rewind();
		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null)
			GLES20.glBindTexture(GL_TEXTURE_2D, _text);
		if (_parent.indexContinue()) {
			_parent.getVertexPosition().rewind();

			int c = 0;
			Sprite<GLES20> spriteBase = _objects.get(0);
				_parent.getVertexPosition().rewind();
				//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				GLES20.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false, 0, spriteBase.getVertexPosition().rewind());
				GLES20.glEnableVertexAttribArray(_positionPointer);
				if (this.hasCol) {
					//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
					GLES20.glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, false, 0, spriteBase.getVertexColor().rewind());
					GLES20.glEnableVertexAttribArray(_colorPointer);
				}
				
				if (this.hasNor) {
					//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
					GLES20.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, false, 0, spriteBase.getVertexNormal().rewind());
					GLES20.glEnableVertexAttribArray(_normalPointer);
				}
				
				if (this.hasTex) {
					//GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
					GLES20.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, false, 0, spriteBase.getVertexTextureCoordinates().rewind());
					GLES20.glEnableVertexAttribArray(_texPointer);
				}
				
			for (Sprite<GLES20> sprite : _objects) {
				// Display opaque elements before
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					ShortBuffer b = this._indexes.get(c++);
					if (object.material.getDissolve() > 0.999f && count > 0) {
						GLES20.glUniform1i(_materialLocation, object.material.getIdSsb());
						//GLES20.glDrawArrays(_wayToDraw, 0, count);
						GLES20.glDrawElements(_wayToDraw,count, GLES20.GL_UNSIGNED_SHORT, b.rewind());
						try {
							HseeExceptionImpl.getOpenGLError(null);
						}
						catch (HseeExceptionImpl e) {
							Log.e("andoid", e.getLocalizedMessage());
						}
					}
				}				
			}
		}
		else {
			_baseVertex.rewind();
		}
}

	private void initLocationPointers (ShaderProgram program) {
		GLES20.glBindAttribLocation(program.getProgramId(), 0, ShaderVariable.BUILTIN_POSITION);
		GLES20.glBindAttribLocation(program.getProgramId(), 1, ShaderVariable.BUILTIN_NORMAL);
		GLES20.glBindAttribLocation(program.getProgramId(), 2, ShaderVariable.BUILTIN_COLOR);
		GLES20.glBindAttribLocation(program.getProgramId(), 3, ShaderVariable.BUILTIN_TEXCOORDINATES);
		
		_positionPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_POSITION);
		_normalPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_NORMAL);
		_colorPointer = program.getAttribLocation(ShaderVariable.BUILTIN_COLOR);
		_texPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_TEXCOORDINATES);
	}

	@Override
	protected void removeByApi (Sprite<GLES20> sprite) {
		// TODO Auto-generated method stub
	}
}
