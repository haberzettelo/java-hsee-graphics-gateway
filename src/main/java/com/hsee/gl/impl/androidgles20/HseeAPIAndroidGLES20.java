package com.hsee.gl.impl.androidgles20;

import android.opengl.GLES20;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.HseeAPI;
import com.hsee.gl.impl.androidgles20.light.DirectionalLightAndroidGLES20;
import com.hsee.gl.impl.androidgles20.light.PointLightAndroidGLES20;
import com.hsee.gl.impl.androidgles20.light.SpotLightAndroidGLES20;
import com.hsee.gl.impl.androidgles20.mvp.ModelViewProjectionAndroidGLES20;
import com.hsee.gl.impl.androidgles20.shader.ShaderFactoryAndroidGLES20;
import com.hsee.gl.impl.androidgles20.shader.ShaderProgramFactoryAndroidGLES20;
import com.hsee.gl.impl.androidgles20.shader.ShaderVariableFactoryAndroidGLES20;
import com.hsee.gl.impl.androidgles20.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.impl.androidgles20.sprite.io.SpriteImporterAndroidGLES20;
import com.hsee.gl.impl.androidgles20.sprite.shape.Cube;
import com.hsee.gl.impl.androidgles20.sprite.shape.Plan;
import com.hsee.gl.light.DirectionalLight;
import com.hsee.gl.light.PointLight;
import com.hsee.gl.light.SpotLight;
import com.hsee.gl.mvp.ModelViewProjection;
import com.hsee.gl.shader.ShaderFactory;
import com.hsee.gl.shader.ShaderVariableFactory;
import com.hsee.gl.shader.impl.ShaderProgramFactory;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.io.SpriteCompiler;

@SuppressWarnings ("rawtypes")
public class HseeAPIAndroidGLES20 implements HseeAPI {
	private GLES20 g;
	private GraphicsGatewayGLES20 ggateway;
	public HseeAPIAndroidGLES20() {
		g = null;//(GLES20)javax.microedition.khronos.egl.EGLContext.getEGL();
		
		ggateway = new GraphicsGatewayGLES20();
		ShaderFactoryAndroidGLES20.initFactory(g);
		ShaderVariableFactoryAndroidGLES20.initFactory(g);
		ShaderProgramFactoryAndroidGLES20.initFactory(g);
		
		MaterialSSB.initMaterialSsb(g);
	}
	
	@Override
	public DirectionalLight createDirectionalLight(String shaderVariableName) {
		return new DirectionalLightAndroidGLES20(g, shaderVariableName);
	}

	@Override
	public PointLight createPointLight(String shaderVariableName) {
		return new PointLightAndroidGLES20(g, shaderVariableName);
	}

	@Override
	public SpotLight createSpotLight(String shaderVariableName) {
		return new SpotLightAndroidGLES20(g, shaderVariableName);
	}

	@Override
	public ShaderFactory getShaderFactory() {
		return ShaderFactoryAndroidGLES20.factory;
	}

	@Override
	public ShaderProgramFactory getShaderProgramFactory() {
		return ShaderProgramFactoryAndroidGLES20.factory;
	}

	@Override
	public ShaderVariableFactory getShaderVariableFactory() {
		return ShaderVariableFactoryAndroidGLES20.factory;
	}

	@Override
	public AbstractCube createCube(String name) {
		return new Cube(g, name);
	}

	@Override
	public AbstractPlan createPlan(String name, int size) {
		return new Plan(g, name, size);
	}

	@Override
	public Sprite createSprite(ImporterSetter importer,
			SpriteCompiler compiler, String name, String path, String filename) {
		return new SpriteImporterAndroidGLES20(g, importer, compiler, name, path, filename);
	}
	@Override
	public Sprite createSprite (ImporterSetter importer,
			SpriteCompiler compiler, InputStreamGetter reader, String name,
			String path, String filename) {
		return new SpriteImporterAndroidGLES20(g, importer, compiler, reader, name, path, filename);
	}

	@Override
	public ModelViewProjection createModelViewProjection() {
		return new ModelViewProjectionAndroidGLES20(g);
	}

	@Override
	public Object getUnderlayedAPI() {
		return g;
	}

	@Override
	public GraphicsGateway getGraphicsGateway() {
		return ggateway;
	}


}
