package com.hsee.gl.impl.androidgles20.sprite.renderer;

import static android.opengl.GLES20.GL_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_ELEMENT_ARRAY_BUFFER;
import static android.opengl.GLES20.GL_FLOAT;
import static android.opengl.GLES20.GL_LINEAR;
import static android.opengl.GLES20.GL_REPEAT;
import static android.opengl.GLES20.GL_RGB;
import static android.opengl.GLES20.GL_STATIC_DRAW;
import static android.opengl.GLES20.GL_TEXTURE_2D;
import static android.opengl.GLES20.GL_TEXTURE_MAG_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_MIN_FILTER;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_S;
import static android.opengl.GLES20.GL_TEXTURE_WRAP_T;
import static android.opengl.GLES20.GL_UNSIGNED_BYTE;

import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import android.opengl.GLES20;
import android.util.Log;

import com.hsee.gl.exception.HseeException;
import com.hsee.gl.impl.androidgles20.exception.HseeExceptionImpl;
import com.hsee.gl.impl.androidgles20.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.sprite.Sprite;
import com.hsee.gl.sprite.Sprite.ObjectFaces;
import com.hsee.gl.sprite.Texture;
import com.hsee.gl.sprite.impl.renderer.SpriteRendererImpl;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.jogamp.common.nio.Buffers;

public class SpriteRendererAndroidGLES20 extends SpriteRendererImpl<GLES20> implements
		SpriteRenderer<GLES20> {
	static int		SIZEOF_FLOAT	= Buffers.SIZEOF_FLOAT;
	int				_positionPointer, _normalPointer, _texPointer,
			_colorPointer, _vao, _wayToDraw, _nbObjectsToDraw, _text;
	IntBuffer _temp, _count, _baseVertex;
	int[]	_arrayOfIndices;
	List<ShortBuffer> _indexes;
	ShaderProgram	_previousProgram;
	Sprite<GLES20>		_parent;
	boolean hasPos = false, hasNor = false, hasCol = false, hasTex = false;
	
	static MaterialSSB _ssbMaterial = null;

	public SpriteRendererAndroidGLES20() {
		super();
		_previousProgram = null;
		_temp = Buffers.newDirectIntBuffer(1);
	}

	@Override
	public void initByApi (GLES20 g, ShaderProgram program) {
		if (_ssbMaterial == null)
			_ssbMaterial = MaterialSSB.ssb;
		if (_previousProgram != program) {
			initLocationPointers(program);
			_previousProgram = program;
		}
		_parent = _objects.get(0);

		switch (_parent.getDrawWay()) {
		case TrianglesStrip:
			_wayToDraw = GLES20.GL_TRIANGLE_STRIP;
			break;
		case Triangles:
			_wayToDraw = GLES20.GL_TRIANGLES;
			break;
		case Points:
			_wayToDraw = GLES20.GL_POINTS;
			break;
		case Lines:
			_wayToDraw = GLES20.GL_LINES;
			break;
		case LinesStrip:
			_wayToDraw = GLES20.GL_LINE_STRIP;
			break;
		}

		//_ssbMaterial.addMaterial((MaterialImpl)sprite.getVertexIndexes().get(0).material);

		boolean indContinued = _parent
				.indexContinue();
		int nbBuffers = 1;

		_temp.rewind();
		// TODO Adapt
		/*GLES20.glGenVertexArrays(1, _temp);

		GLES20.glEnable(GLES20.GL_PRIMITIVE_RESTART);
		GLES20.glPrimitiveRestartIndex(getRestartIndex());
		_vao = _temp.get(0);
		// Save all following states
		GLES20.glBindVertexArray(_vao);*/


		if (_parent.getVertexPosition().capacity() > 0) {
			hasPos = _positionPointer > -1;
			if (hasPos) {
				GLES20.glEnableVertexAttribArray(_positionPointer);
				nbBuffers++;
			}
		}

		if (_parent.getVertexNormal().capacity() > 0) {
			hasNor = _normalPointer > -1;
			if (hasNor) {
				GLES20.glEnableVertexAttribArray(_normalPointer);
				nbBuffers++;
			}
		}

		if (_parent.getVertexColor().capacity() > 0) {
			hasCol = _colorPointer > -1;
			if(hasCol) {
				GLES20.glEnableVertexAttribArray(_colorPointer);
				nbBuffers++;
			}
		}

		if (_parent.getVertexTextureCoordinates().capacity() > 0) {
			hasTex = _texPointer > -1;
			if (hasTex) {
				GLES20.glEnableVertexAttribArray(_texPointer);
				nbBuffers++;
			}
		}

		_temp = Buffers.newDirectIntBuffer(nbBuffers);
		GLES20.glGenBuffers(nbBuffers, _temp);

		int posSize = 0, colSize = 0, norSize = 0, texSize = 0, indSize = 0, nbSubObject = 0;
		// First loop to get all size to attribute to buffers
		for (Sprite<GLES20> sprite : _objects) {
			posSize += sprite.getVertexPosition().capacity();
			colSize += sprite.getVertexColor().capacity();
			norSize += sprite.getVertexNormal().capacity();
			texSize += sprite.getVertexTextureCoordinates().capacity();
			
			for (ObjectFaces object : sprite.getVertexIndexes()) {
				indSize += object.faces.capacity();	
				nbSubObject++;			
			}
		}
		posSize *= SIZEOF_FLOAT;
		colSize *= SIZEOF_FLOAT;
		norSize *= SIZEOF_FLOAT;
		texSize *= SIZEOF_FLOAT;
		indSize *= Buffers.SIZEOF_SHORT;
		this._indexes = new ArrayList<ShortBuffer>(nbSubObject);

		_temp.rewind();
		if (hasPos) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, posSize, null, GL_STATIC_DRAW);
		}
		try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
		
		if (hasCol) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, colSize, null, GL_STATIC_DRAW);
		}
		try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
		
		if (hasNor) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, norSize, null, GL_STATIC_DRAW);
		}
		try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
		
		if (hasTex) {
			GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
			GLES20.glBufferData(GL_ARRAY_BUFFER, texSize, null, GL_STATIC_DRAW);
		}
		try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
		
		GLES20.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
		GLES20.glBufferData(GL_ELEMENT_ARRAY_BUFFER, indSize, null, GL_STATIC_DRAW);

		int offsetInd = 0, offsetPos = 0, offsetNor = 0, offsetCol = 0, offsetTex = 0;
		int size;

		// TODO
		_baseVertex = Buffers.newDirectIntBuffer(_objects.size());
		_baseVertex.rewind();
		
		_nbObjectsToDraw = 0;
		for (Sprite<GLES20> sprite : _objects) {
			_nbObjectsToDraw += sprite.getVertexIndexes().size();
		}

		_count = Buffers.newDirectIntBuffer(_nbObjectsToDraw);
		_count.rewind();

		_arrayOfIndices = new int[_nbObjectsToDraw];
		int iArrrayOfIndice = 0;

		for (Sprite<GLES20> sprite : _objects) {
			try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
			_temp.rewind();
			if (hasPos) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexPosition().capacity() * SIZEOF_FLOAT;
				GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetPos, size, sprite
						.getVertexPosition().rewind());

				GLES20.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false,
						0, 0);

				if (!indContinued)
					_baseVertex.put(offsetPos / 3);

				offsetPos += size;
			}
			if (hasCol) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexColor().capacity() * SIZEOF_FLOAT;
				GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetCol, size, sprite.getVertexColor().rewind());
				GLES20.glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, false, 0, 0);
				offsetCol += size;
			}try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
			
			if (hasNor) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexNormal().capacity() * SIZEOF_FLOAT;
				GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetNor, size, sprite.getVertexNormal().rewind());
				GLES20.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, true, 0, 0);
				offsetNor += size;
			}try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
			
			if (hasTex) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				size = sprite.getVertexTextureCoordinates().capacity() * SIZEOF_FLOAT;
				GLES20.glBufferSubData(GL_ARRAY_BUFFER, offsetTex, size, sprite.getVertexTextureCoordinates().rewind());
				GLES20.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, true, 0, 0);
				offsetTex += size;
			}try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
			
			
			try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
			int idBindElement = _temp.get();
			GLES20.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idBindElement);
			//TODO
			for (int i = 0, max = sprite.getVertexIndexes().size(); i < max; i++) {
				try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
				ObjectFaces object = sprite.getVertexIndexes().get(i);
				_count.put(object.faces.capacity());
	
				_arrayOfIndices[iArrrayOfIndice++] = offsetInd;
	
				size = object.faces.capacity() * Buffers.SIZEOF_SHORT;
	
				object.faces.rewind();
				ShortBuffer buf = Buffers.newDirectShortBuffer(object.faces.capacity());
				for (int j = 0, max2 = buf.capacity(); j < max2; j++)
					buf.put((short)object.faces.get());
				buf.rewind();
	
				GLES20.glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offsetInd, size, buf);
				this._indexes.add(buf);
				//GLES20.glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offsetInd, size, buf);
				offsetInd += size;
			}
			// for(int i = 0; i < sprite.getVertexIndexes().capacity(); i++)
			// System.out.println(sprite.getVertexIndexes().get(i));
			//System.out.println(sprite.getVertexIndexes().get(0).material);
		}
		try { HseeExceptionImpl.getOpenGLError(null); } catch (HseeExceptionImpl e) {Log.e("", e.getMessage());}
		
		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null) {
			Texture t = _parent.getVertexIndexes().get(0).material
					.getDiffuseTexture();
			IntBuffer intb = Buffers.newDirectIntBuffer(1);
			_text = intb.get(0);
			GLES20.glGenTextures(1, intb);

			//GLES20.glEnable(GL_TEXTURE_2D);
			GLES20.glBindTexture(GL_TEXTURE_2D, _text);
			//GLES20.glActiveTexture(GL_TEXTURE0);
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			GLES20.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, t.getWidth(),
					t.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, t.getData().rewind());
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			GLES20.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}

		//GLES20.glBindVertexArray(0);
		_ssbMaterial.bindShaderProgram(program, "materials");
		_ssbMaterial.updateSsbo();
		_materialLocation = program.getUniformLocation("materialId");
	}
	int _materialLocation;
	
	@Override
	public void draw (GLES20 g, ShaderProgram program) {
		_count.rewind();
		_temp.rewind();
		if (_parent.getVertexIndexes().get(0).material.getDiffuseTexture() != null)
			GLES20.glBindTexture(GL_TEXTURE_2D, _text);
		//GLES20.glBindVertexArray(_vao);
		if (_parent.indexContinue()) {
			/*GLES20.glMultiDrawElements(_wayToDraw, _count, GL4.GL_UNSIGNED_INT,
					_arrayOfIndices, _nbObjectsToDraw);*/
			if (hasPos) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				GLES20.glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, false, 0, 0);
				GLES20.glEnableVertexAttribArray(_positionPointer);
			}
			if (hasCol) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				GLES20.glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, false, 0, 0);
				GLES20.glEnableVertexAttribArray(_colorPointer);
			}
			if (hasNor) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				GLES20.glVertexAttribPointer(_normalPointer, 3, GL_FLOAT, false, 0, 0);
				GLES20.glEnableVertexAttribArray(_normalPointer);
			}
			if (hasTex) {
				GLES20.glBindBuffer(GL_ARRAY_BUFFER, _temp.get());
				GLES20.glVertexAttribPointer(_texPointer, 3, GL_FLOAT, false, 0, 0);
				GLES20.glEnableVertexAttribArray(_texPointer);
			}
			
			GLES20.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _temp.get());
			/*GLES20.glMultiDrawElements(_wayToDraw, _count, GL4.GL_UNSIGNED_INT,
					_arrayOfIndices, _nbObjectsToDraw);*/
			// Display opaque elements before
			int iArrayOfIndices = 0;
			int c = 0;
			for (Sprite<GLES20> sprite : _objects)
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					int arrayOfIndex = _arrayOfIndices[iArrayOfIndices++];
					ShortBuffer b1 = this._indexes.get(c++);
					if (object.material.getDissolve() > 0.999f && count > 0) {
						object.faces.rewind();
						object.faces.rewind();
						GLES20.glUniform1i(_materialLocation, object.material.getIdSsb());
						//GLES20.glDrawArrays(_wayToDraw, 0, count);
						GLES20.glDrawElements(_wayToDraw, count, GLES20.GL_UNSIGNED_SHORT, arrayOfIndex);
					}
				}
			_count.rewind();
			iArrayOfIndices = 0;
			c = 0;
			// Display transparent element after
			for (Sprite<GLES20> sprite : _objects)
				for (ObjectFaces object : sprite.getVertexIndexes()){
					int count = _count.get();
					int arrayOfIndex = _arrayOfIndices[iArrayOfIndices++];
					ShortBuffer b1 = this._indexes.get(c++);
					if (object.material.getDissolve() < 0.9999f) {
						GLES20.glUniform1i(_materialLocation, object.material.getIdSsb());
						//GLES20.glDrawArrays(_wayToDraw, 0, count);
						//Log.d("", "display " + count + " transparent elements");
						GLES20.glDrawElements(_wayToDraw, count, GLES20.GL_UNSIGNED_SHORT, arrayOfIndex);
					}
				}
			
		}
		else {
			_baseVertex.rewind();
			// TODO Adapt
			/*GLES20.glMultiDrawElementsBaseVertex(_wayToDraw, _count,
					GLES20.GL_UNSIGNED_INT, _arrayOfIndices, _nbObjectsToDraw,
					_baseVertex);*/
		}
		//GLES20.glBindVertexArray(0);
	}

	private void initLocationPointers (ShaderProgram program) {
		GLES20.glBindAttribLocation(program.getProgramId(), 0, ShaderVariable.BUILTIN_POSITION);
		GLES20.glBindAttribLocation(program.getProgramId(), 1, ShaderVariable.BUILTIN_NORMAL);
		GLES20.glBindAttribLocation(program.getProgramId(), 2, ShaderVariable.BUILTIN_COLOR);
		GLES20.glBindAttribLocation(program.getProgramId(), 3, ShaderVariable.BUILTIN_TEXCOORDINATES);
		
		_positionPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_POSITION);
		_normalPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_NORMAL);
		_colorPointer = program.getAttribLocation(ShaderVariable.BUILTIN_COLOR);
		_texPointer = program
				.getAttribLocation(ShaderVariable.BUILTIN_TEXCOORDINATES);
	}

	@Override
	protected void removeByApi (Sprite<GLES20> sprite) {
		// TODO Auto-generated method stub
	}
}
