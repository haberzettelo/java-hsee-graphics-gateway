package com.hsee.gl.impl.androidgles20.shader;

import android.opengl.GLES20;
import android.util.Log;

import com.hsee.gl.exception.HseeProgramShaderException;
import com.hsee.gl.impl.androidgles20.exception.HseeExceptionImpl;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderProgramPipeline;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.impl.ShaderProgramImpl;
import com.jogamp.common.nio.Buffers;

import java.nio.IntBuffer;

import static android.opengl.GLES20.*;

public abstract class ShaderProgramAndroidGLES20 extends ShaderProgramImpl
{
	protected boolean separable;
	protected static IntBuffer _temp = Buffers.newDirectIntBuffer(1);

	ShaderProgramAndroidGLES20(GLES20 gl)
	{
		super();
		_programId = GLES20.glCreateProgram();
	}

	@Override
	public int getUniformLocation (String varName)
	{
		return GLES20.glGetUniformLocation(_programId, varName);
	}

	@Override
	public int[] getUniformLocation (String[] varsName)
	{
		int[] result = new int[varsName.length];
		for (int i = 0, max = varsName.length; i < max; i++)
			result[i] = GLES20.glGetUniformLocation(_programId, varsName[i]);
		return result;
	}

	@Override
	protected ShaderProgram attachShaderByApi (Shader shader)
	{
		GLES20.glAttachShader(_programId, shader.getShaderId());
		
		return this;
	}

	@Override
	protected ShaderProgram detachShaderByApi (Shader shader)
	{
		GLES20.glDetachShader(_programId, shader.getShaderId());
		return this;
	}
	
	@Override
	public ShaderProgram link () throws HseeProgramShaderException {
		
        // Link the two shaders together into a program.
        glLinkProgram(_programId);

        // Get the link status.
        final int[] linkStatus = new int[1];
        glGetProgramiv(_programId, GL_LINK_STATUS, linkStatus, 0);

            // Print the program info log to the Android log output.
            Log.v("", "Results of linking program:\n" 
                + glGetProgramInfoLog(_programId));		

        // Verify the link status.
        if (linkStatus[0] == 0) {
            // If it failed, delete the program object.
            glDeleteProgram(_programId);
            Log.w("", "Linking of program failed.");
        }
        
        return this;
		
		/*GLES20.glLinkProgram(_programId);
		if (!getStatus(GLES20.GL_LINK_STATUS)) {
			int error;
			if ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
				throw HseeProgramShaderExceptionAbdroidgles30.getLinkError(error, _programId);
			}
			throw new HseeProgramShaderException("Error while linking : " + getLogMessage());
		}
		int error;
		
		GLES20.glValidateProgram(_programId);
		if (!getStatus(GLES20.GL_VALIDATE_STATUS)) {
			if ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
				throw HseeProgramShaderExceptionAbdroidgles30.getValidateError(error, _programId);
			}
			throw new HseeProgramShaderException("Error while validate : " + getLogMessage());
		}
		return this;*/
		
	}

	@Override
	protected int getAttribLocationByApi (String varName)
	{
		return GLES20.glGetAttribLocation(_programId, varName);
	}
	
	@Override
	public int getStorageBufferLocation (String ssbName) {
		// Get blockname to bytebuffer
		// TODO Adapt to version
		/*ByteBuffer nameBuf = StandardCharsets.US_ASCII.encode(ssbName);
		nameBuf.rewind();

		return GLES20.glGetProgramResourceIndex(_programId, GLES20.GL_SHADER_STORAGE_BLOCK, nameBuf);*/
		return -1;
	}
	
	@Override
	protected void finalize ()
	{
		// TODO Auto-generated method stub
		
	}
	
	private boolean getStatus(int param) {
		_temp.rewind();
		GLES20.glGetProgramiv(_programId, param, _temp);
		if (_temp.get(0) == GLES20.GL_FALSE)
			return false;
		return true;
	}
	
	private String getLogMessage() {
		/*_temp.rewind();
		GLES20.glGetProgramiv(_programId, GLES20.GL_INFO_LOG_LENGTH, _temp);
		System.out.println("sizeOfBuffer + " + _temp.get(0));
		ByteBuffer byteBuffer = Buffers.newDirectByteBuffer(_temp.get(0));*/
		String error = "";
		error = GLES20.glGetProgramInfoLog(_programId);

		return error;
	}
}

class ShaderProgramSimpleAndroidGLES20 extends ShaderProgramAndroidGLES20 {

	ShaderProgramSimpleAndroidGLES20(GLES20 gl)
	{
		super(gl);
	}

	@Override
	public boolean isSeparable ()
	{
		return false;
	}

	@Override
	protected ShaderProgram useProgramByApi ()
	{
		GLES20.glUseProgram(_programId);
		return this;
	}

	@Override
	protected ShaderProgram updateShaderDataByApi (@SuppressWarnings("rawtypes") ShaderVariable var)
	{
		var.updateShaderData(this);
		return this;
	}
}

class ShaderProgramSeparableAndroidGLES20 extends ShaderProgramAndroidGLES20 {
	ShaderProgramPipeline pipeline;

	ShaderProgramSeparableAndroidGLES20(GLES20 gl)
	{
		super(gl);
		// TODO Adapt
		//GLES20.glProgramParameteri(_programId, GLES20.GL_PROGRAM_SEPARABLE, GLES20.GL_TRUE);
		pipeline = null;
	}

	@Override
	public boolean isSeparable ()
	{
		return true;
	}

	@Override
	protected ShaderProgram useProgramByApi ()
	{
		// Don't bind, the pipeline program must be take the hand
		GLES20.glUseProgram(0);
		return this;
	}

	ShaderProgramSeparableAndroidGLES20 setShaderProgramPipeline (ShaderProgramPipeline pipeline) {
		this.pipeline = pipeline;
		return this;
	}
	
	@Override
	protected ShaderProgram updateShaderDataByApi (@SuppressWarnings("rawtypes") ShaderVariable var)
	{
		// TODO Adapt
		/*if (pipeline != null)
			GLES20.glActiveShaderProgram(pipeline.getShaderProgramPipelineId(), _programId);
		var.updateShaderData(this);*/
		return this;
	}
	
}
