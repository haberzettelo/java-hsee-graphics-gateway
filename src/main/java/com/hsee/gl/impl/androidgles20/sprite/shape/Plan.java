package com.hsee.gl.impl.androidgles20.sprite.shape;

import android.opengl.GLES20;

import com.hsee.gl.impl.androidgles20.sprite.renderer.SpriteRendererAndroidGLES20;
import com.hsee.gl.sprite.impl.shape.AbstractPlan;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public class Plan extends AbstractPlan<GLES20>
{

	public Plan (GLES20 g, String name, float size)
	{
		super(null, name, size);
	}

	@Override
	protected SpriteRenderer<GLES20> newRendererByApi (GLES20 g) {
		return new SpriteRendererAndroidGLES20();
	}

}
