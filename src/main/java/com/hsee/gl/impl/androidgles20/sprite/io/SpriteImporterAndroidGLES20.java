package com.hsee.gl.impl.androidgles20.sprite.io;


import android.opengl.GLES20;

import com.hsee.gl.impl.androidgles20.shader.buffer.storagebuffer.MaterialSSB;
import com.hsee.gl.impl.androidgles20.sprite.renderer.SpriteRendererAndroidGLES20;
import com.hsee.gl.sprite.DrawWay;
import com.hsee.gl.sprite.SpriteSetter;
import com.hsee.gl.sprite.impl.io.SpriteImporterImpl;
import com.hsee.gl.sprite.impl.shape.Shape;
import com.hsee.gl.sprite.io.ImporterSetter;
import com.hsee.gl.sprite.io.InputStreamGetter;
import com.hsee.gl.sprite.renderer.SpriteRenderer;
import com.hsee.gl.sprite.io.SpriteCompiler;

public class SpriteImporterAndroidGLES20 extends SpriteImporterImpl<GLES20>
{
	public SpriteImporterAndroidGLES20 (GLES20 g, ImporterSetter importer,
			SpriteCompiler compiler, String name, String path, String filename) {
		super(null, importer, compiler, name, path, filename);
		MaterialSSB.ssb.addMaterial(importer.getMaterials());
	}
	
	public SpriteImporterAndroidGLES20 (GLES20 g, ImporterSetter importer,
			SpriteCompiler compiler, InputStreamGetter reader, String name, String path, String filename) {
		super(null, importer, compiler, reader, name, path, filename);
		MaterialSSB.ssb.addMaterial(importer.getMaterials());
	}

	@Override
	public DrawWay getDrawWay () {
		return DrawWay.TrianglesStrip;
	}

	@Override
	public boolean indexContinue () {
		return true;
	}

	@Override
	public SpriteSetter<GLES20> newChild (String name) {
		return new Shape<GLES20>(null, this, _renderer, name);
	}

	@Override
	protected SpriteRenderer<GLES20> newRendererByApi (GLES20 g) {
		return new SpriteRendererAndroidGLES20();
	}

}
