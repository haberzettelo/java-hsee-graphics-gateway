package com.hsee.gl.impl.androidgles20.shader;

import android.opengl.GLES20;

import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderFactory;

public class ShaderFactoryAndroidGLES20 implements ShaderFactory
{
	public static ShaderFactoryAndroidGLES20 factory = null;
	
	public static void initFactory(GLES20 ctx) {
		factory = new ShaderFactoryAndroidGLES20(null);
	}
	
	private ShaderFactoryAndroidGLES20 (GLES20 context) {
	}
	
	@Override
	public Shader newComputeShader () 
	{
		/*return new ShaderAndroidGLES20(graphicalContext, GLES20.GL_COMPUTE_SHADER) {
			@Override
			public boolean isComputeShader () {
				return true;
			}
		};*/
		return null;
	}

	@Override
	public Shader newFragmentShader ()
	{
		return new ShaderAndroidGLES20(null, GLES20.GL_FRAGMENT_SHADER) {
			@Override
			public boolean isFragmentShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newVertexShader ()
	{
		return new ShaderAndroidGLES20(null, GLES20.GL_VERTEX_SHADER) {
			@Override
			public boolean isVertexShader () {
				return true;
			}
		};
	}

	@Override
	public Shader newGeometryShader ()
	{
		/*return new ShaderAndroidGLES20(graphicalContext, GLES20.GL_GEOMETRY_SHADER) {
			@Override
			public boolean isGeometryShader () {
				return true;
			}
		};*/
		return null;
	}

	@Override
	public Shader newTesselationControlShader ()
	{
		/*return new ShaderAndroidGLES20(graphicalContext, GLES20.GL_TESS_CONTROL_SHADER) {
			@Override
			public boolean isTesselationControlShader () {
				return true;
			}
		};*/
		return null;
	}

	@Override
	public Shader newTesselationEvaluationShader ()
	{
		/*return new ShaderAndroidGLES20(graphicalContext, GLES20.GL_TESS_EVALUATION_SHADER) {
			@Override
			public boolean isTesselationEvaluationShader () {
				return true;
			}
		};*/
		return null;
	}
}
