package com.hsee.gl.impl.androidgles20.sprite.shape;

import android.opengl.GLES20;

import com.hsee.gl.impl.androidgles20.sprite.renderer.SpriteRendererAndroidGLES20;
import com.hsee.gl.sprite.impl.shape.AbstractCube;
import com.hsee.gl.sprite.renderer.SpriteRenderer;

public class Cube extends AbstractCube<GLES20>
{
	public Cube (GLES20 g, String name)
	{
		super(null, name);
	}

	@Override
	protected SpriteRenderer<GLES20> newRendererByApi (GLES20 g) {
		return new SpriteRendererAndroidGLES20();
	}

}
