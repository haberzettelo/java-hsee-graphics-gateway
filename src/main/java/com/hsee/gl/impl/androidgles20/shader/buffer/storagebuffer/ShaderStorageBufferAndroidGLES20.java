package com.hsee.gl.impl.androidgles20.shader.buffer.storagebuffer;

import android.opengl.GLES20;

import com.hsee.gl.impl.androidgles20.exception.HseeExceptionImpl;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.buffer.storagebuffer.ShaderStorageBuffer;
import com.hsee.gl.shader.impl.structure.ShaderStructureImpl;
import com.jogamp.common.nio.Buffers;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public abstract class ShaderStorageBufferAndroidGLES20 extends ShaderStructureImpl implements ShaderStorageBuffer {
	private int _ssbId, _bindingId;
	public ShaderStorageBufferAndroidGLES20(GLES20 g, int sizeOfSsb, int bindingPoint) {
		try {
			IntBuffer b = Buffers.newDirectIntBuffer(1);
			GLES20.glGenBuffers(1, b);
			_ssbId = b.get(0);
			initData(sizeOfSsb);
			bindBlock(bindingPoint);
			HseeExceptionImpl.getOpenGLError(g);
		}
		catch (HseeExceptionImpl e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void bindBlock (int bindingPoint) {
		_bindingId = bindingPoint;
		// TODO Adapt to lower opengl version
		//GLES20.glBindBufferBase(GLES20.GL_SHADER_STORAGE_BUFFER, bindingPoint, _ssbId);
	}

	@Override
	public abstract void bindShaderProgram (ShaderProgram program, String ssbName);/* {
		int location = program.getStorageBufferLocation(ssbName);
		// TODO Adapt to lower opengl version
		//GLES20.glShaderStorageBlockBinding(program.getProgramId(), location, _bindingId);
	}*/

	
	
	@Override
	public ByteBuffer mapData (boolean read, boolean write) {
		// TODO Adapt to lower opengl version
		/*GLES20.glBindBuffer(GLES20.GL_SHADER_STORAGE_BUFFER, _ssbId);
		int state = GLES20.GL_READ_ONLY;
		if(read && write) {
			state = GLES20.GL_READ_WRITE;
		} else if(!read && write) {
			state = GLES20.GL_WRITE_ONLY;
		}
		return GLES20.glMapBuffer(GLES20.GL_SHADER_STORAGE_BUFFER, state);*/
		return null;
	}

	@Override
	public void unmapData () {
		// TODO Adapt to lower opengl version
		/*GLES20.glUnmapBuffer(GLES20.GL_SHADER_STORAGE_BUFFER);
		GLES20.glBindBuffer(GLES20.GL_SHADER_STORAGE_BUFFER, 0);*/
	}
	
	@Override
	public abstract void initData (int size);/* {
		// TODO Adapt to lower opengl version
		//GLES20.glBindBuffer(GLES20.GL_SHADER_STORAGE_BUFFER, _ssbId);
		//GLES20.glBufferData(GLES20.GL_SHADER_STORAGE_BUFFER, size, Buffers.newDirectByteBuffer(size), GLES20.GL_STATIC_DRAW);
		//GLES20.glBindBuffer(GLES20.GL_SHADER_STORAGE_BUFFER, 0);
	}*/
	
	@Override
	public ShaderVariable updateShaderData (ShaderProgram updater) {
		return null;
	}
}
