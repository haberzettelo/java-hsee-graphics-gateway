package com.hsee.gl.impl.androidgles20.scene;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;

import com.hsee.gl.GraphicsGateway;
import com.hsee.gl.impl.androidgles20.HseeAPIAndroidGLES20;
import com.hsee.gl.scene.impl.SceneImpl;

public class SceneAndroidGLES20 extends SceneImpl {
	protected GLSurfaceView surfaceView;
	private float relativeSpeed = 0;
	
	public SceneAndroidGLES20(GLSurfaceView view) {
		super((GraphicsGateway)null);
		this.surfaceView = view;
		super.api = new HseeAPIAndroidGLES20();
		super.graphicsGateway = super.api.getGraphicsGateway();
		view.setEGLContextClientVersion(2);
		view.setRenderer(new Renderer() {
			private int counter = 0;
			@Override
			public void onSurfaceCreated(GL10 arg0, EGLConfig arg1) {
				SceneAndroidGLES20.this.surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
				
				SceneAndroidGLES20.this.api = new HseeAPIAndroidGLES20();
				SceneAndroidGLES20.this.graphicsGateway = SceneAndroidGLES20.this.api.getGraphicsGateway();
				
				final int listenersSize = SceneAndroidGLES20.this.listeners.size();
				for (counter = 0; counter < listenersSize; counter++)
					SceneAndroidGLES20.this.listeners.get(counter).initialize(SceneAndroidGLES20.this.api, SceneAndroidGLES20.this.graphicsGateway);
			}
			
			@Override
			public void onSurfaceChanged(GL10 arg0, int arg1, int arg2) {
				final int listenersSize = SceneAndroidGLES20.this.listeners.size();
				for (counter = 0; counter < listenersSize; counter++)
					SceneAndroidGLES20.this.listeners.get(counter).reshape(SceneAndroidGLES20.this.api, SceneAndroidGLES20.this.graphicsGateway, 0, 0, arg1, arg2);
			}
			
			@Override
			public void onDrawFrame(GL10 arg0) {
				final int listenersSize = SceneAndroidGLES20.this.listeners.size();
				for (counter = 0; counter < listenersSize; counter++)
					SceneAndroidGLES20.this.listeners.get(counter).render(SceneAndroidGLES20.this.api, SceneAndroidGLES20.this.graphicsGateway, SceneAndroidGLES20.this.relativeSpeed);
			}
		});
	}

	@Override
	public void display() {
		this.surfaceView.requestRender();
	}

	@Override
	public void display(float relativeSpeed) {
		this.relativeSpeed = relativeSpeed;
		this.display();
	}

	@Override
	public void waitEndRender() {
		super.graphicsGateway.flush();
	}

}
