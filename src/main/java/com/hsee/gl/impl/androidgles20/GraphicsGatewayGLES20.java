package com.hsee.gl.impl.androidgles20;

import android.opengl.GLES20;

import com.hsee.gl.GraphicsGateway;

public class GraphicsGatewayGLES20 implements GraphicsGateway {

	@Override
	public GraphicsGateway clearColor(float red, float green, float blue,
			float alpha) {
		GLES20.glClearColor(red, green, blue, alpha);
		return this;
	}

	@Override
	public GraphicsGateway clear(int target) {
		GLES20.glClear(target);
		return this;
	}

	@Override
	public GraphicsGateway enable(int targets) {
		GLES20.glEnable(targets);
		return this;
	}

	@Override
	public GraphicsGateway flush() {
		GLES20.glFlush();
		return this;
	}

	@Override
	public GraphicsGateway blendFunc(int sourceFactor, int destinationFactor) {
		GLES20.glBlendFunc(sourceFactor, destinationFactor);
		return this;
	}

}
