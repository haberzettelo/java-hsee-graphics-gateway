package com.hsee.gl.impl.androidgles20.light;

import android.opengl.GLES20;

import com.hsee.gl.light.impl.PointLightImpl;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;

public class PointLightAndroidGLES20 extends PointLightImpl
{
	public PointLightAndroidGLES20 (GLES20 g, String name) {
		super(name);
	}

	@Override
	protected void initShaderByApi (Shader shader) {
		shader.append("struct " + TYPENAMEPOINT + "{\n"
				+ "	vec3 " + VAR_AMBIENT + ";\n"
				+ "	vec3 " + VAR_DIFFUSECOLOR + ";\n"
				+ "	vec3 " + VAR_SPECULARCOLOR + ";\n"
				+ "	float " + VAR_ATT_CONST + ";\n"
				+ "	float " + VAR_ATT_LIN + ";\n"
				+ "	float " + VAR_ATT_EXP + ";\n"
				//+ "	vec3 " + VAR_CAMPOS + ";\n"
				+ "	vec3 " + VAR_POSITION + ";\n};\n");
	}

	@Override
	protected void updateShaderDataByApi (ShaderProgram updater) {
		_diffuse.rewind();
		_specular.rewind();
		_position.rewind();
		_ambientColor.rewind();
		
		GLES20.glUniform3f(_diffLoc, _diffuse.get(),_diffuse.get(),_diffuse.get());
		GLES20.glUniform3f(_specLoc, _specular.get(),_specular.get(),_specular.get());
		GLES20.glUniform3f(_posLoc, _position.get(),_position.get(),_position.get());
		GLES20.glUniform3f(_ambLoc, _ambientColor.get(),_ambientColor.get(),_ambientColor.get());
		
		GLES20.glUniform1f(_attConstLoc, _attConstant);
		GLES20.glUniform1f(_attLineLoc, _attLinear);
		GLES20.glUniform1f(_attExpLoc, _attExpo);
	}

}
