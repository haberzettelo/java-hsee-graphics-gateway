package com.hsee.gl.impl.androidgles20.light;

import android.opengl.GLES20;

import com.hsee.gl.light.impl.DirectionalLightImpl;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;

public class DirectionalLightAndroidGLES20 extends DirectionalLightImpl
{
	public DirectionalLightAndroidGLES20(GLES20 g, String name) {
		super(name);
	}
	

	
	@Override
	protected void updateShaderDataByApi (ShaderProgram updater) {
		_diffuseColor.rewind();
		_specularColor.rewind();
		_direction.rewind();
		_halfvector.rewind();
		_ambientColor.rewind();
	
		GLES20.glUniform3f(_diffLoc, _diffuseColor.get(), _diffuseColor.get(), _diffuseColor.get());
		GLES20.glUniform3f(_specLoc, _specularColor.get(), _specularColor.get(), _specularColor.get());
		GLES20.glUniform3f(_dirLoc, _direction.get(), _direction.get(), _direction.get());
		GLES20.glUniform3f(_halfLoc, _halfvector.get(), _halfvector.get(), _halfvector.get());
		GLES20.glUniform3f(_ambLoc, _ambientColor.get(), _ambientColor.get(), _ambientColor.get());
	}

	@Override
	protected void initShaderByApi (Shader shader) {
		shader.append("struct " + TYPENAMEDIRECTIONAL + "{\n"
				+ "	vec3 " + VAR_AMBIENT + ";\n"
				+ "	vec3 " + VAR_DIFFUSECOLOR + ";\n"
				+ "	vec3 " + VAR_SPECULARCOLOR + ";\n"
				+ "	vec3 " + VAR_HALF + ";\n"
				+ "	vec3 " + VAR_DIRECTION + ";\n};\n");
	}
}
