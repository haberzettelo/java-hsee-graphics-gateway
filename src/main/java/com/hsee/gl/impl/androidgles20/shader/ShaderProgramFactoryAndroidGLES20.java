package com.hsee.gl.impl.androidgles20.shader;

import android.opengl.GLES20;

import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.impl.ShaderProgramFactory;

public class ShaderProgramFactoryAndroidGLES20 implements ShaderProgramFactory
{
	public static ShaderProgramFactoryAndroidGLES20 factory = null;
	public static void initFactory(GLES20 ctx) {
		factory = new ShaderProgramFactoryAndroidGLES20(ctx);
	}
	
	private ShaderProgramFactoryAndroidGLES20 (GLES20 context) {
	}
	@Override
	public ShaderProgram newProgram ()
	{
		return new ShaderProgramSimpleAndroidGLES20(null);
	}

	@Override
	public ShaderProgram newSeparateProgram ()
	{
		return new ShaderProgramSimpleAndroidGLES20(null);
	}

}
