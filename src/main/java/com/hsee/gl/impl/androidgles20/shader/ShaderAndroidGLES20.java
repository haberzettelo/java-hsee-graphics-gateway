package com.hsee.gl.impl.androidgles20.shader;

import android.opengl.GLES20;

import com.hsee.gl.exception.HseeShaderException;
import com.hsee.gl.shader.impl.ShaderImpl;
import com.jogamp.common.nio.Buffers;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

public abstract class ShaderAndroidGLES20 extends ShaderImpl
{
	private boolean _showSource = true;
	/**
	 * Construct a shader
	 * @param context Graphical context
	 * @param shaderType Specifies the type of shader to be created. Must be one of GL_COMPUTE_SHADER, <br>
	 * GL_VERTEX_SHADER, GL_TESS_CONTROL_SHADER, GL_TESS_EVALUATION_SHADER, GL_GEOMETRY_SHADER, or GL_FRAGMENT_SHADER.
	 */
	ShaderAndroidGLES20(GLES20 context, int shaderType)
	{
		_shaderId = GLES20.glCreateShader(shaderType);
	}

	
	
	@Override
	public void compileShader () throws HseeShaderException
	{
		IntBuffer buf = Buffers.newDirectIntBuffer(1);
		buf.put(_sourceShader.length());
		buf.rewind();
		// Desktop version
		//GLES20.glShaderSource(_shaderId, 1, new String[] {_sourceShader}, buf);
		// Android version
		GLES20.glShaderSource(_shaderId, _sourceShader);

		GLES20.glCompileShader(_shaderId);
		// Take the buffer to store the result of the compilation
		buf.rewind();
		GLES20.glGetShaderiv(_shaderId, GLES20.GL_COMPILE_STATUS, buf);
		if (buf.get(0) == GLES20.GL_FALSE)
		{
			// Take the buffer to store the length of log
			buf.rewind();
			GLES20.glGetShaderiv(_shaderId, GLES20.GL_INFO_LOG_LENGTH, buf);
			ByteBuffer byteBuffer = Buffers.newDirectByteBuffer(buf.get(0));
			String error = "";
			error = GLES20.glGetShaderInfoLog(_shaderId);
//			for (int i = 0, max = byteBuffer.capacity(); i < max; i++) {
//				error += ((char) byteBuffer.get());
//			}
			String addMessage = "";
			if (_showSource)
				addMessage = " of source : \n" + this;
			throw new HseeShaderException("Error while compilation" + addMessage + "\nCompilation output : \n" + error);
		}
		String message = "";
		if (!(message = GLES20.glGetShaderInfoLog(_shaderId)).equals(""))
			throw new HseeShaderException("Shader log : " + message );
	}

	@Override
	protected int getShaderIdByApi ()
	{
		return _shaderId;
	}



	@Override
	protected void finalize ()
	{
		// Don't remove shader here, remove when program is deleted
		//GLES20.glDeleteShader(_shaderId);
	}

}
