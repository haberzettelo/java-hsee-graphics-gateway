package com.hsee.gl.impl.androidgles20.sprite.io;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

import android.content.res.AssetManager;

import com.hsee.gl.sprite.io.InputStreamGetter;

public class InputStreamGetterAndroid implements InputStreamGetter {
	private static Logger logger = Logger.getLogger(InputStreamGetterAndroid.class);
	public static InputStreamGetterAndroid reader = new InputStreamGetterAndroid();
	private AssetManager manager = null;
	private InputStreamGetterAndroid () {
	}
	
	public void setAssetManager(AssetManager manager) {
		this.manager = manager;
	}
	
	@Override
	public InputStream open (String fileName) {
		InputStream input = null;
		try {
			input = this.manager.open(fileName);
		} catch (IOException e) {
			logger.warn("The file : \"" + fileName + "\" was not found.\nError catched : " + e.getMessage());
		}
		return input;
	}

}
