package com.hsee.gl.shader;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * A shader program pipeline is a program that contain one or more shader program,
 * this is an extension of shader program. You can use this to make a program that
 * contain sub program, and reuse several sub programs in another shader program 
 * pipeline.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Create your shader programs and link your shaders to this. Next, create shader
 * programs pipeline with method newShaderProgramPipeline(). When it's done, add 
 * shader programs to your shader program pipeline that you have created with 
 * addShaderProgram() method.
 * To use this shader program pipeline, use useProgramPipeline() method. you can't 
 * use a shader program at the same time, if you do that, shader program pipeline
 * will be ignored instead of shader program.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 9 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ShaderProgramPipeline
{
	/**
	 * Factory of ShaderProgramPipeline<br>
	 * Create some shader program pipeline
	 * @param numberToGenerate The number of instance you will generate
	 * @return Array with new instance
	 */
	public ShaderProgramPipeline[] newShaderProgramPipeline (int numberToGenerate);
	
	/**
	 * Add a shader program to your shader program pipeline
	 * @param program Shader program with one or more stage of shader
	 */
	public void addShaderProgram(ShaderProgram program);
	
	/**
	 * Return the id of the program pipeline
	 * @return Id
	 */
	public int getShaderProgramPipelineId();
	
	/**
	 * Use the shader program pipeline to the render of your application
	 */
	public void useProgramPipeline();
}
