package com.hsee.gl.shader;

public interface ShaderProgramListener
{
	/**
	 * Update this variable in your program. !! DON'T USE THIS OUTSIDE ShaderProgram : use notifyUpdated() method !!
	 * @return Chaining
	 */
	@SuppressWarnings ("rawtypes")
	public ShaderVariable updateShaderData (ShaderProgram updater);
	
	/**
	 * Notify this variable has changed to all referenced Shader Program
	 * @return Chaining
	 */
	@SuppressWarnings ("rawtypes")
	public ShaderVariable notifyUpdated();
}
