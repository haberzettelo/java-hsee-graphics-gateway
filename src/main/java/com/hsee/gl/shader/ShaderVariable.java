package com.hsee.gl.shader;

import com.hsee.gl.exception.HseeShaderStructureException;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * This contract represent a variable contained in shader. These variable are
 * the bind between shader and you application. 
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * To use a shader variable in your application, you must create your shader and
 * your shader program first, then you can use it.
 * Now, instantiate your shader variable by the constructor of the implementation
 * of the type you want, by example, to create a float variable, use the constructor
 * of ShaderFloat.
 * 
 * Next, specify to your variable the shader program with setShaderProgram() method
 * and the location of this variable, either with location directly (already 
 * specified in your shader), or with the Shader. If you use Shader to detect
 * the location, you must set the variable name before with setVarName() method.
 * 
 * Then, you can update you variable depending on your implementation and 
 * notify to all shader program that you have update the variable by using
 * notifyUpdated() method.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 17 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ShaderVariable<T> extends ShaderProgramListener
{
	public static String VAR_PREFIX = "",
						CONST_PREFIX = "HSEE_",
						BUILTIN_PREFIX = "in_",
						TYPE_PREFIX = "Hsee_",
						BUFFER_PREFIX = "HBuffer_";
	public static String BUILTIN_POSITION = BUILTIN_PREFIX + "position",
			BUILTIN_COLOR = BUILTIN_PREFIX + "color", 
			BUILTIN_TEXCOORDINATES = BUILTIN_PREFIX + "texcoord",
			BUILTIN_NORMAL = BUILTIN_PREFIX + "normal";
	/**
	 * Set the location of this variable.<br>
	 * !! Be caution, if you set location directly, you can't set var name. <br>
	 * In this case, you must have the same location of this variable in each program. !!
	 * @param location The location of this variable in the shader
	 * @return Chaining
	 */
	//public ShaderVariable setLocation (int location);

	/**
	 * Add a shader program. You must set this before use of updateShaderData() method.
	 * @param program Shader program
	 * @throws HseeShaderStructureException If a problem occurred when the binding of this variable to the specified shader, an exception will be thrown.
	 * @return Chaining
	 */
	public ShaderVariable<T> addShaderProgram (ShaderProgram program) throws HseeShaderStructureException;
	
	/**
	 * Get the value of this variable. To improve performance (to reduce charge of garbage collector), if you<br>
	 * want to modify the variable value, use the getter, set the value and notify the change with notifyUpdated() method.
	 * @return The instance of variable
	 */
	public T getValue();
	
	/**
	 * Set the value of this variable. To improve performance (to reduce charge of garbage collector), try to<br>
	 * don't create a new instance and use the getter to modify value.<br>
	 * If you use Primitive type, ignore this advise !
	 * @return Chaining
	 */
	public ShaderVariable<T> setValue(T newValue);
	
	
	/**
	 * Set the variable name correspond to the name in your shader
	 * @param name Variable name
	 * @return Chaining
	 */
	//public ShaderVariable setVarName(String name);
}
