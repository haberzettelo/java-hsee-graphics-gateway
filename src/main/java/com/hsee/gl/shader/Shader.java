package com.hsee.gl.shader;

import java.io.InputStream;

import com.hsee.gl.exception.HseeShaderException;
import com.hsee.gl.sprite.io.InputStreamGetter;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * A shader is a program in some of 3D api that let us a control before drawing.
 * Some of stage are available, the most known are vertex shader and fragment
 * shader. By example, the vertex shader is called before the calculation of the 
 * position vertex, and fragment shader is called before the color of the pixel 
 * was computed.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Construct your shader with the implementation of shader that you want. Next,
 * you can add one or more file to your shader with addFileName() method.
 * When it's good, you must compile your shader with compileShader() method.
 * 
 * If you have some variable in your shader that you want to set, use getLocation()
 * method to take the address of these variables.
 * 
 * You can now add your shader to a shader program to use it.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 9 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface Shader {	
	/**
	 * Add a file to your shader
	 * @param fileName File name with the extension
	 * @throws HseeShaderException If the file was not found
	 */
	public void addFileName (String fileName) throws HseeShaderException;
	
	/**
	 * Add a file to your shader
	 * @param reader Reader
	 * @param fileName File name with the extension
	 * @throws HseeShaderException If the file was not found
	 */
	public void addFileName (InputStreamGetter reader, String fileName) throws HseeShaderException;
	
	/**
	 * Add a shader source by input stream
	 * @param inputStream
	 * @param shaderNameToDebug Name to debug, can be the file name if input stream come from file
	 * @throws HseeShaderException If the file was not found
	 */
	//public void addInputStream(InputStream inputStream, String shaderNameToDebug) throws HseeShaderException;
	
	/**
	 * Append to your shader the specified string
	 * @param tail
	 */
	public void append (String tail);
	
	/**
	 * Compile this shader
	 * @throws HseeShaderException If a problem occurred
	 */
	public void compileShader () throws HseeShaderException;
	
	/**
	 * Return the adress of the shader
	 * @return
	 */
	int getShaderId();

	boolean isFragmentShader();
	boolean isVertexShader();
	boolean isGeometryShader();
	boolean isComputeShader();
	boolean isTesselationControlShader();
	boolean isTesselationEvaluationShader();
}
