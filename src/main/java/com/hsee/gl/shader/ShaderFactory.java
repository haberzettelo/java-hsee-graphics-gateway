package com.hsee.gl.shader;

public interface ShaderFactory
{
	public Shader newComputeShader();
	public Shader newFragmentShader();
	public Shader newVertexShader();
	public Shader newGeometryShader();
	public Shader newTesselationControlShader();
	public Shader newTesselationEvaluationShader();
	
}
