package com.hsee.gl.shader;

import com.hsee.gl.exception.HseeProgramShaderException;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Shader program is a program that contain one or more shader. Each shader in shader program
 * can be at different stage of shader. If two shader are added to shader program which
 * be at the same stage, only one of these shader must have main method.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Create your shader in first time. When it's done, you must create a shader program
 * with on of the two factories provided :
 * 	-newProgram() : create a simple shader program
 * 	-newSeparateProgram() : create a part of shader program which will be contain 
 * 	some shader stage.
 * 
 * After, you can add your shaders to your shader program with attachShader() method.
 * If a shader must be detached from your shader program, use method :
 * 	-detachShader() : detach one shader at time
 * 	-detachAllShader() : detach all shader at time
 * 
 * When you have attached all your shaders to your shader program, you can link these
 * by using method link(). If an error occurred when this process, an exception will
 * be throw. 
 * 
 * Now, you must use a shader program with one of the two ways:
 * 	-If you have a simple shader, created by newProgram() method, use useProgram()
 * 	method to use your shader program
 * 	-If you have a part of shader program created by newSeparateProgram() method,
 * 	you must now have a shader program pipeline to continue, and go to javadoc 
 * 	attached to this interface.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 9 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ShaderProgram
{
	/**
	 * Add shader to this program. You can add more of one shader by stage,<br>
	 * if you do that, you must have single main function in entire shader stage.
	 * @param shader Shader you want to add
	 * @return New instance of ShaderProgram
	 */
	public ShaderProgram attachShader (Shader shader);
	
	/**
	 * Remove shader to this program
	 * @param shader Shader you want to remove
	 * @return New instance of ShaderProgram
	 */
	public ShaderProgram detachShader (Shader shader);
	
	/**
	 * Remove all shader referenced in this program
	 * @return New instance of ShaderProgram
	 */
	//public ShaderProgram detachAllShader ();
	
	/**
	 * Link this program to API, when this method was performed, you can't add more<br>
	 * or remove shader in this program
	 * @throws HseeProgramShaderException
	 * @return New instance of ShaderProgram
	 */
	public ShaderProgram link () throws HseeProgramShaderException;
	
	/**
	 * Use the shader program to render your application
	 * @return New instance of ShaderProgram
	 */
	public ShaderProgram useProgram();
	
	/**
	 * Add shader variable to update when this program was used
	 * @param variable Your Shader Variable to update
	 * @return Chaining
	 */
	public ShaderProgram addShaderVariableToUpdate(@SuppressWarnings ("rawtypes") ShaderVariable variable);	

	/**
	 * Get the location the built-in variable you have provided, in or varying variables
	 * @param attribVarName Built-in variable name
	 * @return Location of the built-in variable
	 */
	public int getAttribLocation(String attribVarName);
	
	/**
	 * Get the location the shader storage buffer you have provided, buffer variable
	 * @param ssbName Shader storage buffer name
	 * @return Location of the ssb
	 */
	public int getStorageBufferLocation(String ssbName);
	
	/**
	 * Get the location of the variable you have provided, uniform variable
	 * @param varName Variable name
	 * @return Location of the variable
	 */
	public int getUniformLocation (String varName);
	
	/**
	 * Get the locations of the variables you have provided, uniform variables
	 * @param varsName Variables name
	 * @return Locations of the variables
	 */
	public int[] getUniformLocation (String[] varsName);
	
	/**
	 * Return true if the program can be separated, else return false
	 * @return 
	 */
	public boolean isSeparable ();
	
	/*
	 * Put theses method in the implementation (abstract)
	 */
	int getProgramId();
	boolean hasFragmentShader();
	boolean hasVertexShader();
	boolean hasGeometryShader();
	boolean hasComputeShader();
	boolean hasTesselationControlShader();
	boolean hasTesselationEvaluateShader();
}
