package com.hsee.gl.shader.buffer.storagebuffer;

import java.nio.ByteBuffer;

import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.structure.ShaderStructure;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Shader Storage Buffer is a buffer that can be read and write in shader, and 
 * by application. They has no very limitation for the memory, that depend on 
 * your hardware graphic memory. 
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 mai 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ShaderStorageBuffer extends ShaderStructure{
	/**
	 * Bind this SSB to the specified point
	 * @param bindingPoint
	 */
	void bindBlock(int bindingPoint);
	
	/**
	 * Bind a shader storage buffer object of a shader program to this SSB.
	 * @param program
	 */
	void bindShaderProgram(ShaderProgram program, String ssbName);
	
	/**
	 * Specify the size of the ssb in byte
	 * @param size
	 */
	void initData(int size);
	
	/**
	 * Map the data to the returned ByteBuffer
	 * @param read Set at true if you want to read value of the buffer
	 * @param write Set at true if you want to write values on the buffer
	 */
	ByteBuffer mapData(boolean read, boolean write);
	
	/**
	 * Unmap the data from the previously mapped data
	 */
	void unmapData();
}
