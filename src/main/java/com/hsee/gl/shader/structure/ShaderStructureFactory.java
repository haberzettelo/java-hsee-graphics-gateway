package com.hsee.gl.shader.structure;

import com.hsee.gl.shader.ShaderProgram;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Shader structure factory allow you to create one or many structure variable
 * correspond to your shader structure type.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Use newShaderStructureVariable method to create your structure. You must
 * specify Shader program in these methods so you must create your shaders before 
 * use this factory.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 16 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ShaderStructureFactory<T extends ShaderStructure>
{
	/**
	 * Create new Shader Structure Variable
	 * @param variableName Variable name in your shader
	 * @param program Shader program to update this variable
	 * @return Your Shader Structure Variable
	 */
	public T newShaderStructureVariable(String variableName, ShaderProgram program);
	/**
	 * Create new Shader Structure Variables
	 * @param variableName Variable name in your shader
	 * @param numberOfVar  Number of variables you want to generate
	 * @param program Shader program to update this variable
	 * @return Your Shader Structure Variables, length correspond to numberOfVar
	 */
	public T[] newShadetStructureVariable(String variableName, int numberOfVar, ShaderProgram program);
}
