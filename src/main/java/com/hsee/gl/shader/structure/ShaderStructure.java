package com.hsee.gl.shader.structure;

import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderVariable;

/**
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Shader Structure represent a structure from shader.
 * A shader structure is a complex variable which be declared in your shader. A
 * structure is composed of any primitives type provided by your shader language,
 * and may be add a structure variable in another structure.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * You must create your Shader and your shader program first, next you must use 
 * the factory associated with your structure type.
 * Use this into your factory.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 17 mars 2015
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface ShaderStructure extends ShaderVariable<ShaderStructure> {
	/**
	 * InitShader() method allow to your shader to have the structure type into it.<br>
	 * Call this method more one time by shader and structure type produce ignore call.
	 * @param shader
	 */
	public void initShader(Shader shader);
}
