package com.hsee.gl.shader;

import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import com.hsee.gl.math.matrix.Matrix;

public interface  ShaderVariableFactory{
	public ShaderVariable<Float> newFloat(Object varNameOrLocation);
	public ShaderVariable<Integer> newInteger(Object varNameOrLocation);
	public ShaderVariable<Boolean> newBoolean(Object varNameOrLocation);
	public ShaderVariable<Double> newDouble(Object varNameOrLocation);
	
	public ShaderVariable<FloatBuffer> newFloatArray(Object varNameOrLocation, int size);
	public ShaderVariable<IntBuffer> newIntArray(Object varNameOrLocation, int size);
	public ShaderVariable<IntBuffer> newBooleanArray(Object varNameOrLocation, int size);
	public ShaderVariable<DoubleBuffer> newDoubleArray(Object varNameOrLocation, int size);

	public ShaderVariable<FloatBuffer> newFloatVec(Object varNameOrLocation, int size);
	public ShaderVariable<IntBuffer> newIntVec(Object varNameOrLocation, int size);
	public ShaderVariable<DoubleBuffer> newDoubleVec(Object varNameOrLocation, int size);
	
	public ShaderVariable<Matrix> newMatrix(Object varNameOrLocation);
	// TODO Complete for texture and maybe other types
}