package com.hsee.gl.shader.impl;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.hsee.gl.shader.Shader;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;

public abstract class ShaderProgramImpl implements ShaderProgram
{
	@SuppressWarnings ("rawtypes")
	protected List<ShaderVariable> _varToUpdate;
	protected Hashtable<String, Integer> _builtinVar;
	protected int _programId;
	// In the order : vertex, geometry; fragment, compute, tess evaluation, tess control
	protected boolean[] _hasShader;
	public ShaderProgramImpl ()
	{
		_varToUpdate = new ArrayList<ShaderVariable>();
		_builtinVar = new Hashtable<String, Integer>();
		_programId = -1;
		_hasShader = new boolean[] {false, false, false, false, false, false};
	}
	
	@Override
	public ShaderProgram attachShader (Shader shader)
	{
		setShaderStage(shader, true);
		attachShaderByApi(shader);
		return this;
	}
	
	protected abstract ShaderProgram attachShaderByApi (Shader shader);
	protected abstract ShaderProgram detachShaderByApi (Shader shader);
	protected abstract ShaderProgram useProgramByApi ();
	protected abstract ShaderProgram updateShaderDataByApi (@SuppressWarnings ("rawtypes") ShaderVariable var);

	@Override
	public ShaderProgram detachShader (Shader shader)
	{
		setShaderStage(shader, false);
		detachShaderByApi(shader);
		return this;
	}
	
	@Override
	public ShaderProgram useProgram ()
	{
		useProgramByApi();
		for (int i = 0, max = _varToUpdate.size(); i < max; i++)
		{
			@SuppressWarnings ("rawtypes")
			ShaderVariable var = _varToUpdate.get(i);
			updateShaderDataByApi(var);
		}
		_varToUpdate.removeAll(_varToUpdate);
		return this;
	}

	@Override
	public ShaderProgram addShaderVariableToUpdate (@SuppressWarnings ("rawtypes") ShaderVariable variable)
	{
		if (!_varToUpdate.contains(variable))
			_varToUpdate.add(variable);
		return this;
	}

	@Override
	public int getProgramId ()
	{
		return _programId;
	}

	@Override
	public boolean hasFragmentShader ()
	{
		return _hasShader[2];
	}

	@Override
	public boolean hasVertexShader ()
	{
		return _hasShader[0];
	}

	@Override
	public boolean hasGeometryShader ()
	{
		return _hasShader[1];
	}

	@Override
	public boolean hasComputeShader ()
	{
		return _hasShader[3];
	}

	@Override
	public boolean hasTesselationControlShader ()
	{
		return _hasShader[5];
	}

	@Override
	public boolean hasTesselationEvaluateShader ()
	{
		return _hasShader[4];
	}
	
	protected void setShaderStage(Shader shader, boolean newState) {
		if (shader.isVertexShader()) 
			_hasShader[0] = newState;
		else if (shader.isGeometryShader())
			_hasShader[1] = newState;
		else if (shader.isFragmentShader())
			_hasShader[2] = newState;
		else if (shader.isComputeShader())
			_hasShader[3] = newState;
		else if (shader.isTesselationEvaluationShader())
			_hasShader[4] = newState;
		else if (shader.isTesselationControlShader())
			_hasShader[5] = newState;
	}
	
	@Override
	public int getAttribLocation (String attribVarName)
	{
		if (!_builtinVar.containsKey(attribVarName))
			_builtinVar.put(attribVarName, getAttribLocationByApi(attribVarName));
		return _builtinVar.get(attribVarName);
	}

	protected abstract int getAttribLocationByApi(String varName);
	
	@Override
	protected abstract void finalize();
}
