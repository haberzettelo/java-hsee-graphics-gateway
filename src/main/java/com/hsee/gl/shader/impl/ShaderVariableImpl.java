package com.hsee.gl.shader.impl;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;

public abstract class ShaderVariableImpl<T> implements ShaderVariable<T>
{
	private static Logger logger = Logger.getLogger(ShaderVariableImpl.class);
	protected ArrayList<ShaderProgram> _prgs;
	protected T _var;
	
	protected boolean _dynamicLocationActivate = false;
	
	protected int _staticLocation;
	protected ArrayList<Integer> _dynamicLocation;
	protected String _varName;
	
	public ShaderVariableImpl (Object object) {
		_prgs = new ArrayList<ShaderProgram>();
		if(object instanceof Integer)
			initLocation((Integer)object);
		else if(object instanceof String)
			initVarName((String)object);
		else
			logger.fatal("A shaderVariable can't be initialized, cause : excpect Integer represent ID or String represent variable name.");
	}
	
	public void initLocation (Integer location)
	{
		_dynamicLocationActivate = false;
		_staticLocation = location;
		
	}
	public void initVarName (String varName)
	{
		_dynamicLocationActivate = true;
		_varName = varName;
		_dynamicLocation = new ArrayList<Integer>();
	}
		
	protected abstract void updateShaderDataByApi (int varLocation);
	
	@Override
	public ShaderVariable<T> updateShaderData (ShaderProgram updater)
	{
		/*if (_dynamicLocationActivate)
		{
			for (int i = 0, max = _prgs.size(); i < max; i++)
			{
				_prgs.get(i).useProgram();
				updateShaderDataByApi(_dynamicLocation.get(i));
			}
		}
		else
			for (ShaderProgram shaderProgram : _prgs)
			{
				shaderProgram.useProgram();
				updateShaderDataByApi(_staticLocation);
			}*/
		int index = _prgs.indexOf(updater);

		if (_dynamicLocationActivate)
			updateShaderDataByApi(_dynamicLocation.get(index));
		else
			updateShaderDataByApi(_staticLocation);
			
		return this;
	}

	@Override
	public ShaderVariable<T> notifyUpdated ()
	{
		for (ShaderProgram shaderProgram : _prgs)
			shaderProgram.addShaderVariableToUpdate(this);
		return this;
	}

	@Override
	public ShaderVariable<T> addShaderProgram (ShaderProgram program)
			throws HseeShaderStructureException
	{
		if (_dynamicLocationActivate) {
			int location = program.getUniformLocation(_varName);
			if (location < 0)
			{
				if (location == -1)
					throw new HseeShaderStructureException("The variable named \"" + _varName + "\" was not found or name isn't authorized.");
				else
					throw new HseeShaderStructureException("The variable named \"" + _varName + "\" encountred an error while searching.");
			}
			_prgs.add(program);
			_dynamicLocation.add(location);
		}
		else
			_prgs.add(program);
		return this;
	}

	@Override
	public T getValue ()
	{
		return _var;
	}

	@Override
	public ShaderVariable<T> setValue (T newValue)
	{
		_var = newValue;
		return this;
	}
}
