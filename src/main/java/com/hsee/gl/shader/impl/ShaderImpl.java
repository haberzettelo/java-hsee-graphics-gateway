package com.hsee.gl.shader.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.hsee.gl.exception.HseeShaderException;
import com.hsee.gl.shader.Shader;
import com.hsee.gl.sprite.io.InputStreamGetter;

public abstract class ShaderImpl implements Shader {
	private static Logger logger = Logger.getLogger(ShaderImpl.class);
	protected int _shaderId = -1;
	protected String _sourceShader = "", _files = "";
	
	protected void addInputStream (InputStream inputStream,
			String shaderNameToDebug) throws HseeShaderException {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			
			String line;
				line = reader.readLine();
			if (line != null)
				if (line.trim().codePointAt(0) == (int)'#')
					_sourceShader = line + "\n" + _sourceShader;
				else
					_sourceShader += line + "\n";
			while((line = reader.readLine()) != null)
				_sourceShader += line + "\n";
			
			if (_files != "")
				_files += "|";
			_files += shaderNameToDebug;
		}
		catch (IOException e) {
			throw new HseeShaderException("The input stream \"" + inputStream.toString() + "\" was not found in " + shaderNameToDebug + "\n Error catched : " + e.getMessage());
		}
	}
	@Override
	public void addFileName (InputStreamGetter reader, String fileName)
			throws HseeShaderException {
		InputStream input = reader.open(fileName);
		
		if (input == null) {
			logger.warn("The file \"" + fileName + "\" was not found.");
			throw new HseeShaderException("The file \"" + fileName + "\" was not found.");
		}
		addInputStream(input, fileName);
	}
	
	@Override
	public void addFileName (String fileName) throws HseeShaderException {
		File f = new File(fileName);
		try {
			addInputStream(new FileInputStream(f), fileName);
		} catch (IOException e) {
			logger.warn("The file \"" + fileName + "\" was not found in " + f.getAbsolutePath() + "\n Error catched : " + e.getMessage());
			throw new HseeShaderException("The file \"" + fileName + "\" was not found in " + f.getAbsolutePath() + "\n Error catched : " + e.getMessage());
		}
	}
	
	@Override
	public void append (String tail) {
		_sourceShader += "\n" + tail;
	}
	
	@Override
	public int getShaderId ()
	{
		if (_shaderId == -1)
			_shaderId = getShaderIdByApi();
		return _shaderId;
	}
	

	public boolean isFragmentShader() {
		return false;
	}
	public boolean isVertexShader() {
		return false;
	}
	public boolean isGeometryShader() {
		return false;
	}
	public boolean isComputeShader() {
		return false;
	}
	public boolean isTesselationControlShader() {
		return false;
	}
	public boolean isTesselationEvaluationShader() {
		return false;
	}
	
	public String toString() {
		String result = "" + String.format("% 5d", 1) + " | ";
		char[] stream = _sourceShader.toCharArray();
		int lineNumber = 2;
		// Add the line number
		for (char c : stream) {
			result += c;
			if (c == '\n')
				result += "" + String.format("% 5d", lineNumber++) + " | ";
		}
		return "Shader source from file(s) \""+_files+"\" :\n" + result;
	}
	
	@Override
	protected abstract void finalize ();
	protected abstract int getShaderIdByApi();
}
