package com.hsee.gl.shader.impl.structure;

import java.util.ArrayList;
import java.util.List;

import com.hsee.gl.exception.HseeShaderStructureException;
import com.hsee.gl.shader.ShaderProgram;
import com.hsee.gl.shader.ShaderVariable;
import com.hsee.gl.shader.structure.ShaderStructure;

public abstract class ShaderStructureImpl implements ShaderStructure
{
	protected List<ShaderProgram> _prgs;
	
	public ShaderStructureImpl () {
		_prgs = new ArrayList<ShaderProgram>();
	}
	
	@Override
	public ShaderVariable<ShaderStructure> addShaderProgram (ShaderProgram program)
			throws HseeShaderStructureException {
		_prgs.add(program);
		return this;
	}

	@Override
	public ShaderStructure getValue () {
		return this; 
	}

	@Override
	public ShaderVariable<ShaderStructure> setValue (ShaderStructure newValue) {
		return this;
	}

	@Override
	public ShaderVariable<ShaderStructure> notifyUpdated () {
		for (ShaderProgram shaderProgram : _prgs) {
			//System.out.println("notify updated by shader structure");
			shaderProgram.addShaderVariableToUpdate(this);
		}
		return this;
	}

}
