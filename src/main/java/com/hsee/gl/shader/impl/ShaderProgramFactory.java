package com.hsee.gl.shader.impl;

import com.hsee.gl.shader.ShaderProgram;

public interface ShaderProgramFactory
{
	/**
	 * Factory of ShaderProgram<br>
	 * Create a new simple program
	 * @return New instance of ShaderProgram
	 */
	public ShaderProgram newProgram();
	
	/**
	 * Factory of ShaderProgram<br>
	 * Create a part of shader program which will be contain some shader stage.
	 * @return New instance of ShaderProgram
	 */
	public ShaderProgram newSeparateProgram();
}
