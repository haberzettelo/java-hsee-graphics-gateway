/*
    uniform = read-only depuis le programme
    in = attribute (obselète!!) = read-only depuis le programme (vertex only)
    out = varying (obselète) = read-write
        >>> non initialisé dans le vertex shader
        >>> envoyé ensuite au fragment shader (read-only)

   dot(vec, vec) = Produit scalaire
   
   Structures des diférentes lumières :
   	Spot light : 
   	struct SpotLight
	{
		vec3 color;
		vec3 position;
		vec3 direction;
		float cut;
	};
	
	Positional light :
	struct PosLight
	{
		vec3 color;
		vec3 position;
	};
	
	Directional light :
	struct DirLight
	{
		vec3 color;
		vec3 direction;
	};
*/