#version 430
smooth in vec3 N;
uniform vec3 cameraPosition;
uniform mat4 Hsee_modelViewMatrix;
uniform sampler2D sampler;
uniform int materialId;
in vec4 frag_color;
in vec3 frag_position;
in vec4 P;
in vec2 T;
out vec4 outColor;
uniform Hsee_DirectionalLight light;
uniform Hsee_PointLight pointLight;
uniform Hsee_SpotLight spotLight;

vec4 dirLightCalc (Hsee_DirectionalLight light, HBuffer_Material mat) {
	vec3 Nn = normalize(N);
	vec3 L = normalize(light.direction.xyz);
	vec3 ambient = light.ambient * frag_color.xyz * mat.ambientColor.xyz;
	
	vec3 diffuse = max(dot(Nn, L), 0) * light.diffuse * mat.diffuseColor.xyz;
	
	vec3 V = (cameraPosition - P.xyz);
	V = normalize(V);
	vec3 R = reflect(-L, Nn);
	vec3 specular = pow(max(dot(V, R), 0), mat.specularExponent) * light.specular * mat.specularColor.xyz;
	return vec4(ambient + diffuse + specular, 1);
}

vec4 pointLightCalc(Hsee_PointLight light, HBuffer_Material mat) {
	vec3 Nn = normalize(N);
	vec3 L = light.position.xyz - P.xyz;
	float dist = length(L);
	L = normalize(L);
	vec3 ambient = light.ambient.xyz * frag_color.xyz * mat.ambientColor.xyz;
	
	vec3 diffuse = max(dot(Nn, L), 0) * light.diffuse * mat.diffuseColor.xyz ;
	
	vec3 V = (cameraPosition - P.xyz);
	V = normalize(V);
	
	vec3 R = reflect(-L, Nn);
	vec3 specular = pow(max(dot(V, R), 0), mat.specularExponent) * light.specular  * mat.specularColor.xyz;
	
	float attenuation = light.attConstant + light.attLinear * dist + 
						light.attExponential * dist * dist;	
	return vec4((ambient + diffuse + specular)/attenuation, 1);
}

vec4 spotLightCalc(Hsee_SpotLight light, HBuffer_Material mat) {
	vec3 Nn = normalize(N);
	vec3 L = light.position - P.xyz;
	float dist = length(L);
	L = normalize(L);
	vec3 ambient = light.ambient * frag_color.xyz * mat.ambientColor.xyz;
	
	float attenuation = 1/(light.attConstant + light.attLinear * dist + 
						light.attExponential * dist * dist);	
	float spotCos = dot(L, -light.coneDirection);
	if (spotCos < light.cutoff)
		attenuation = 0;
	else
		attenuation *= pow(spotCos, light.exponent);

	vec3 R = reflect(-L, Nn);
	vec3 V = (cameraPosition - P.xyz);
	V = normalize(V);

	vec3 diffuse = max(dot(Nn, L), 0) * light.diffuse * mat.diffuseColor.xyz;
	vec3 specular = pow(max(dot(V, R), 0),  mat.specularExponent ) * light.specular * mat.specularColor.xyz;

	return vec4((ambient + diffuse + specular) * attenuation, 1);
}

void main ()
{		
	vec3 gamma = vec3(1);//vec3(1.0/2.2);
	HBuffer_Material mat = materials[materialId];
	outColor = (vec4(texture(sampler, vec2(T.x, T.y)).xyz, mat.dissolve) + vec4(vec3(spotLightCalc(spotLight, mat)) + vec3(dirLightCalc(light, mat)) + vec3(pointLightCalc(pointLight, mat)), /*frag_color.w**/mat.dissolve));
}
