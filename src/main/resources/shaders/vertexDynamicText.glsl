#version 430

uniform mat4 mvp;
uniform vec4 textPosition;
uniform vec4 device;
in ivec2 characterIn;
out int character;
out int position;

void main() {
	character = characterIn.x;
	position = gl_VertexID + characterIn.y;
	vec4 pos = mvp * vec4(vec3(textPosition.x, textPosition.y, textPosition.z), 1);
	gl_Position = vec4(pos.x + (characterIn.y) / (device.z), pos.y, pos.z, pos.w);
}