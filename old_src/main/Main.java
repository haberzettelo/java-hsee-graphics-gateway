package main;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.jogamp.opengl.JoglVersion;

import echiquier.exceptions.EchecsException;
import echiquier.Echiquier;
import echiquier.Joueur;
import echiquier.Piece;
import echiquier.gui.FenetrePrincipal;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import java_3d_engine.engine.animation.interpolation.Linear;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;

/**
 * Instancie une fenêtre avec le rendu OpenGL
 */
public final class Main {

    /**
     * Logger qui sert à debugger l'application.
     */
    private final static Logger logger = Logger.getLogger(Main.class);
    
    /**
     * Contient l'instance de la fenêtre principale
     */
    public static FenetrePrincipal view;
    
    /**
     * Créer une nouvelle scène 3D
     */
    public static Annim an;

    /**
     * @param args Arguments de l'application sous la forme :\n args[0] = "gui"
     * Mode graphique ou non\n args[1] = niveau de debuggage [1 = ALERT, 2 =
     * CRITICAL, 3 = ERROR, 4 = WARNING, 5 = NOTICE, 6 = INFO, 7 = DEBUG] (voir
     * RFC 3164)
     */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
    	
        an = new Annim();
        PropertyConfigurator.configure("log4.properties");
        logger.info("Version JOGL : " + JoglVersion.getInstance().getImplementationVersion());
        logger.info("Démarrage de l'application");
        logger.info("Nombre d'arguments : " + args.length + ", liste :");
        //Linear lin = new Linear(new float[] {0,0,0,0,  1,1,1,1});
        for (String arg : args) {
            logger.debug("Argument : " + arg);
        }
        // Mode "graphique"
        if (args.length == 0 || !args[0].equals("nogui")) {
            logger.debug("Jeu en mode 'graphique'");
            view = new FenetrePrincipal();
            view.init();
            // Mode "console"
        } else {
            if (args[0].equals("nogui")) {
                logger.debug("Jeu en mode 'console'");
                // Desactive le logger
                List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
                loggers.add(LogManager.getRootLogger());
                for (Logger logger : loggers) {
                    logger.setLevel(Level.OFF);
                }
                creeModeConsole();
            }
        }
    }

    private static void creeModeConsole() {
        Scanner s = new Scanner(System.in);
        System.out.println("Mode console - Jeu d'echec");
        System.out.println("-= Menu principal =-");
        System.out.println("[1] Nouvelle partie");
        System.out.println("[2] Charger une partie");
        System.out.println("[3] Quitter");
        System.out.print("Saisir un nombre (1 ou 2 ou 3) : ");
        int choix = s.nextInt();
        while (choix != 1 && choix != 2 && choix != 3) {
            System.out.println("Choix inconnu, Recommencez...");
            System.out.print("Saisir un nombre (1 ou 2 ou 3) : ");
            choix = s.nextInt();
        }
        if (choix == 3) {
            System.exit(-1);
        } else {
            System.out.println("Choix de votre couleur : B ou N ?");
            String choixCouleur = s.nextLine();
            while (!choixCouleur.equals("B") && !choixCouleur.equals("N")) {
                choixCouleur = s.nextLine();
            }
            int couleur = (choixCouleur.equals("B")) ? Piece.BLANC : Piece.NOIR;
            int couleurAdv = (couleur == Piece.BLANC) ? Piece.NOIR : Piece.BLANC;
            Joueur moi = new Joueur(couleur);
            Joueur adv = new Joueur(couleurAdv);
            Echiquier plateau = Echiquier.getInstance();
            plateau.nouvellePartieEchec(moi, adv);
            if (choix == 2) {
                File file = null;
                String filename;
                while (file == null) {
                    System.out.println("Chemin relatif ou absolue vers le fichier : ");
                    filename = s.nextLine();
                    try {
                        file = new File(filename);
                        plateau.chargerPartie(file);
                    } catch (Exception e) {
                        System.out.println("Fichier introuvable.");
                    }
                }
            }
            System.out.println(plateau);
            System.out.println("Vous avez les pièces " + moi.getCouleur());
            System.out.println("C'est au tour du Joueur " + plateau.getTourJoueur().getCouleur());
            String c;
            int x, y;
            Piece courante;
            while (plateau.partieEnCours()) {
                System.out.println("Taper les coordonnées du pion à déplacer");
                System.out.println("Coordonnée de la forme XY (ex: 47, 52, 00,...)");
                c = s.nextLine();
                x = Character.getNumericValue(c.charAt(0));
                y = Character.getNumericValue(c.charAt(1));
                courante = plateau.getPiece(x, y);
                if (courante == null) {
                    System.out.println("Il n'y a aucune pièece en " + x + "," + y);
                } else if (!courante.getCouleur().equalsIgnoreCase(plateau.getTourJoueur().getCouleur())) {
                    System.out.println("Cette pièece ne vous appartient pas !");
                } else {
                    System.out.print("On déplace la pièce ");
                    System.out.print(courante.getNom() + " (" + x + "|" + y + ") sur");
                    System.out.println(" quelle case ? (XY)");
                    c = s.nextLine();
                    x = Character.getNumericValue(c.charAt(0));
                    y = Character.getNumericValue(c.charAt(1));
                    try {
                        plateau.deplacer(courante, x, y);
                        System.out.println(plateau);
                        System.out.println("Au tour du Joueur " + plateau.getTourJoueur().getCouleur());
                    } catch (EchecsException e) {
                        System.out.println(e);
                    }
                }
            }
            System.out.println("Fin de la partie");
            if (plateau.verifiePartieNull()) {
                System.out.println("Partie null. Aucun gagnant");
            } else if (plateau.getEtatPartie() == Echiquier.ECHEC_ET_MAT) {
                System.out.println("Joueur avec les pions " + plateau.getTourJoueur().getCouleur() + " gagne !");
                System.out.println("Nombre de coups : " + plateau.getTourJoueur().getCoups());
            } else {
                System.out.println("Inattendu, une erreur est survenue !");
            }
        }
    }
}
