package main;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.jogamp.opengl.util.FPSAnimator;

public class MainNew
{
	private static Logger logger = Logger.getLogger(MainNew.class);
	public static void main (String[] args)
	{
		PropertyConfigurator.configure("log4.properties");
		logger.info("------------D�but du programme-----------");
		logger.info("-----------------------------------------");
		Frame frame = new Frame("JOGL");
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing (WindowEvent e)
			{
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		GLCapabilities capabilities = new GLCapabilities(null);
		
		GLProfile glp = GLProfile.getDefault();
        GLCapabilities caps = new GLCapabilities(glp);
        GLCanvas canvas = new GLCanvas(caps);
        Scene scene = new Scene(600, 600, capabilities);
        canvas.addGLEventListener(scene);
        canvas.addKeyListener(scene);
		
	    capabilities.setRedBits(8);
	    capabilities.setBlueBits(8);
	    capabilities.setGreenBits(8);
	    capabilities.setAlphaBits(8);
		frame.add(canvas);
		frame.setSize(600, 600);
		frame.setVisible(true);
		
		FPSAnimator animator = new FPSAnimator(canvas, 60);
		animator.start();
		
	}
}
