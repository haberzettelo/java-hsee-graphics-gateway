package main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java_3d_engine.event.Observable;
import java_3d_engine.matrix.mvp.DataTransform;

import javax.media.opengl.awt.GLCanvas;

import org.apache.log4j.Logger;

public class ReadKey extends Observable<DataTransform> implements KeyListener 
{

    /**
     * Logger qui sert à debugger l'application.
     */
    private final static Logger logger = Logger.getLogger(ReadKey.class);
    
    private boolean left, right, up, down, space, plus, moins, huit, quatre, six, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z;
    private int codeLeft, codeRight, codeDown, codeUp, codeSpace, codePlus, codeMoins, codeHuit, codeQuatre, codeSix;
    private int keyPress, keyPressed;

    public ReadKey(GLCanvas canvas) {
        canvas.addKeyListener(this);
        canvas.requestFocus(); // Perte du focus à cause de ça, sous gnome 3 (linux) !
        b = false;
        left = false;
        right = false;
        up = false;
        down = false;
        space = false;
        plus = false;
        moins = false;
        a = false;
        c = false;
        d = false;
        e = false;
        f = false;
        g = false;
        h = false;
        i = false;
        j = false;
        k = false;
        l = false;
        m = false;
        n = false;
        o = false;
        p = false;
        q = false;
        r = false;
        s = false;
        t = false;
        u = false;
        v = false;
        w = false;
        x = false;
        y = false;
        z = false;

        codeLeft = 37;
        codeRight = 39;
        codeUp = 38;
        codeDown = 40;
        codeSpace = 32;
        codePlus = KeyEvent.VK_NUMPAD0;
        codeMoins = KeyEvent.VK_NUMPAD1;

        keyPress = 0;
        keyPressed = 0;

    }

    @Override
	public void keyTyped(KeyEvent event) 
    {
    }

    @Override
	public void keyPressed(KeyEvent event) {
        // TODO Auto-generated method stub
        keyPressed = keyPress;
        int val = event.getKeyCode();

        float deplacement = 1;

        if (val == 27) {
            System.exit(0);
        } else if (val == codeLeft) {
            left = true;
        } else if (val == codeRight) {
            right = true;
        } else if (val == codeUp) {
        	/*
        	 * Je sais pas trop comment changer le délai de répétition
        	 * Je propose de créer un thread tant que up = true
        	 */
        	notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {0,0,deplacement}));
            up = true;
        } else if (val == codeDown) {
        	notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {0,0,-deplacement}));
        	down = true;
        } else if (val == codeSpace) {
            space = true;
        } else if (val == codePlus) {
            plus = true;
        } else if (val == codeMoins) {
            moins = true;
        } else if (val == KeyEvent.VK_B) {
            b = true;
        } else if (val == KeyEvent.VK_SPACE) {
            space = true;
        } else if (val == KeyEvent.VK_NUMPAD2) {
        	notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {0,-deplacement,0}));
        } else if (val == KeyEvent.VK_NUMPAD4) {
        	notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {-deplacement,0,0}));
            quatre = true;
        } else if (val == KeyEvent.VK_NUMPAD6) {
        	notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {deplacement,0,0}));
            six = true;
        } else if (val == KeyEvent.VK_NUMPAD8) {
        	notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {0,deplacement,0}));
            huit = true;
        } else if (val == KeyEvent.VK_A) {
            a = true;
        } else if (val == KeyEvent.VK_B) {
            b = true;
        } else if (val == KeyEvent.VK_C) {
            c = true;
        } else if (val == KeyEvent.VK_D) {
            d = true;
        } else if (val == KeyEvent.VK_E) {
            e = true;
        } else if (val == KeyEvent.VK_F) {
            f = true;
        } else if (val == KeyEvent.VK_G) {
            g = true;
        } else if (val == KeyEvent.VK_H) {
            h = true;
        } else if (val == KeyEvent.VK_I) {
            i = true;
        } else if (val == KeyEvent.VK_J) {
            j = true;
        } else if (val == KeyEvent.VK_K) {
            k = true;
        } else if (val == KeyEvent.VK_L) {
            l = true;
        } else if (val == KeyEvent.VK_M) {
            m = true;
        } else if (val == KeyEvent.VK_N) {
            n = true;
        } else if (val == KeyEvent.VK_O) {
            o = true;
        } else if (val == KeyEvent.VK_P) {
            p = true;
        } else if (val == KeyEvent.VK_Q) {
            q = true;
        } else if (val == KeyEvent.VK_R) {
            r = true;
        } else if (val == KeyEvent.VK_S) {
            s = true;
        } else if (val == KeyEvent.VK_T) {
            t = true;
        } else if (val == KeyEvent.VK_U) {
            u = true;
        } else if (val == KeyEvent.VK_V) {
            v = true;
        } else if (val == KeyEvent.VK_W) {
            w = true;
        } else if (val == KeyEvent.VK_X) {
            x = true;
        } else if (val == KeyEvent.VK_Y) {
            y = true;
        } else if (val == KeyEvent.VK_Z) {
            z = true;
        }

        keyPress = val;

        if (keyPressed != keyPress) {
            logger.debug("Touche pressée : "+KeyEvent.getKeyText(event.getKeyCode()));
        }
    }

    public boolean LeftPressed() {
        return this.left;
    }

    public boolean RightPressed() {
        return this.right;
    }

    public boolean DownPressed() {
        return this.down;
    }

    public boolean UpPressed() {
        return this.up;
    }

    public boolean PlusPressed() {
        return this.plus;
    }

    public boolean LessPressed() {
        return this.moins;
    }

    public boolean BPressed() {
        return this.b;
    }

    public boolean SpacePressed() {
        return this.space;
    }

    public boolean QuatrePressed() {
        return this.quatre;
    }

    public boolean SixPressed() {
        return this.six;
    }

    public boolean HuitPressed() {
        return this.huit;
    }

    public boolean APressed() {
        return this.a;
    }

    public boolean CPressed() {
        return this.c;
    }

    public boolean DPressed() {
        return this.d;
    }

    public boolean EPressed() {
        return this.e;
    }

    public boolean FPressed() {
        return this.f;
    }

    public boolean GPressed() {
        return this.g;
    }

    public boolean HPressed() {
        return this.h;
    }

    public boolean IPressed() {
        return this.i;
    }

    public boolean JPressed() {
        return this.j;
    }

    public boolean KPressed() {
        return this.k;
    }

    public boolean LPressed() {
        return this.l;
    }

    public boolean MPressed() {
        return this.m;
    }

    public boolean NPressed() {
        return this.n;
    }

    public boolean OPressed() {
        return this.o;
    }

    public boolean PPressed() {
        return this.p;
    }

    public boolean QPressed() {
        return this.q;
    }

    public boolean RPressed() {
        return this.e;
    }

    public boolean SPressed() {
        return this.s;
    }

    public boolean TPressed() {
        return this.t;
    }

    public boolean UPressed() {
        return this.u;
    }

    public boolean VPressed() {
        return this.v;
    }

    public boolean WPressed() {
        return this.w;
    }

    public boolean XPressed() {
        return this.x;
    }

    public boolean YPressed() {
        return this.y;
    }

    public boolean ZPressed() {
        return this.z;
    }

    @Override
	public void keyReleased(KeyEvent event) {
        int val = event.getKeyCode();
        if (val == 27) {
            System.exit(0);
        } else if (val == codeLeft) {
            left = false;
        } else if (val == codeRight) {
            right = false;
        } else if (val == codeUp) {
            up = false;
        } else if (val == codeDown) {
            down = false;
        } else if (val == codeSpace) {
            space = false;
        } else if (val == codePlus) {
            plus = false;
        } else if (val == codeMoins) {
            moins = false;
        } else if (val == KeyEvent.VK_B) {
            b = false;
        } else if (val == KeyEvent.VK_SPACE) {
            space = false;
        } else if (val == KeyEvent.VK_4) {
            quatre = false;
        } else if (val == KeyEvent.VK_6) {
            six = false;
        } else if (val == KeyEvent.VK_8) {
            huit = false;
        } else if (val == KeyEvent.VK_A) {
            a = false;
        } else if (val == KeyEvent.VK_B) {
            b = false;
        } else if (val == KeyEvent.VK_C) {
            c = false;
        } else if (val == KeyEvent.VK_D) {
            d = false;
        } else if (val == KeyEvent.VK_E) {
            e = false;
        } else if (val == KeyEvent.VK_F) {
            f = false;
        } else if (val == KeyEvent.VK_G) {
            g = false;
        } else if (val == KeyEvent.VK_H) {
            h = false;
        } else if (val == KeyEvent.VK_I) {
            i = false;
        } else if (val == KeyEvent.VK_J) {
            j = false;
        } else if (val == KeyEvent.VK_K) {
            k = false;
        } else if (val == KeyEvent.VK_L) {
            l = false;
        } else if (val == KeyEvent.VK_M) {
            m = false;
        } else if (val == KeyEvent.VK_N) {
            n = false;
        } else if (val == KeyEvent.VK_O) {
            o = false;
        } else if (val == KeyEvent.VK_P) {
            p = false;
        } else if (val == KeyEvent.VK_Q) {
            q = false;
        } else if (val == KeyEvent.VK_R) {
            r = false;
        } else if (val == KeyEvent.VK_S) {
            s = false;
        } else if (val == KeyEvent.VK_T) {
            t = false;
        } else if (val == KeyEvent.VK_U) {
            u = false;
        } else if (val == KeyEvent.VK_V) {
            v = false;
        } else if (val == KeyEvent.VK_W) {
            w = false;
        } else if (val == KeyEvent.VK_X) {
            x = false;
        } else if (val == KeyEvent.VK_Y) {
            y = false;
        } else if (val == KeyEvent.VK_Z) {
            z = false;
        }

        keyPress = 0;
        keyPressed = 0;
    }
}
