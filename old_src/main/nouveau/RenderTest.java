package main.nouveau;

import _v2test.matrix.CubeGeometry;
import _v2test.matrix.Geometry;
import _v2test.matrix.Mesh;
import _v2test.matrix.MeshBasicMaterial;
import _v2test.matrix.Renderer;
import _v2test.matrix.Scene;
import _v2test.matrix.PerspectiveCamera;

import com.jogamp.opengl.util.FPSAnimator;

import java_3d_engine.interfaces.GraphicLibrary;

import javax.media.opengl.GL;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JLabel;

import org.apache.log4j.Logger;

/**
 *
 * @author JeanClement
 */
public class RenderTest implements GLEventListener {
    
    /**
     * Logger qui sert à debugger l'application.
     */
    public final static Logger logger = Logger.getLogger(RenderTest.class);
    
    private static RenderTest instance = null;
    
    // DEBUG
    private JLabel status = null;
    
    /**
     * Indique si l'animator est démarré ou non.
     */
    private boolean started = false;
    
    /**
     * Scène 3D et "animation"
     */
    private MainNew scene = null;
    private GraphicLibrary gl = null;
    private GLCanvas canvas = null;
    private FPSAnimator animator = null;
    private PerspectiveCamera camera = null;
    private MouseHandleV2 mouse = null;
    private Geometry geometry = null;
    private Renderer renderer = null;
    private Mesh cube = null;
    private MeshBasicMaterial material = null;
    
    public static RenderTest getInstance() {
        if(instance == null)
            instance = new RenderTest();
        
        return instance;
    }
    
    private RenderTest() {
        logger.debug("Création de la scène 3D");
        this.started = false;
        this.canvas = new GLCanvas();
        this.canvas.addGLEventListener(this);
    }
    
    public void start() throws Exception {
        if(started)
            throw new Exception("La scène 3D est déjà initialisé !");
        this.animator = new FPSAnimator(canvas, 60);
        this.animator.start();
        this.started = true;
        logger.debug("Début de l'animation (60 FPS)");
    }
    
    public void stop() {
        logger.debug("Arrêt de l'animation");
        this.animator.stop();
    }
    
    public GLCanvas getCanvas() {
        return this.canvas;
    }
    
    public void setStatus(JLabel status) {
        this.status = status;
    }

    @Override
    public void init(GLAutoDrawable glad) {
        logger.debug("init method call");
        
        gl = new GraphicLibrary(glad.getGL());
        
        logger.info("Version d'OpenGL : " + gl.getGLx().glGetString(GL.GL_VERSION));
        logger.info("Version GLSL : " + gl.getGLx().glGetString(GL3.GL_SHADING_LANGUAGE_VERSION));
        
        // Gestion des erreurs
        //glad.setGL(new DebugGL(glad.getGL()));
        
        // Camera, Scenes, etc
        this.scene = new MainNew();
        this.camera = new PerspectiveCamera(75, canvas.getWidth() / canvas.getHeight(), 0.1f, 1000);
        //this.renderer = new Renderer(gl);
        
        this.material = new MeshBasicMaterial();
        this.cube = new Mesh(new CubeGeometry(1, 1, 1), material);
        
        scene.add(cube);
        
        this.camera.getPosition().setZ(-5f);
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        logger.debug("dispose method call");
    }

    @Override
    public void display(GLAutoDrawable glad) {
        GraphicLibrary gl = new GraphicLibrary(glad.getGL());
        //this.renderer.render(this.scene, this.camera, true);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
        logger.debug("reshape method call");
    }
}
