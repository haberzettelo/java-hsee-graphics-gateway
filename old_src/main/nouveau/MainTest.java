package main.nouveau;

import audio.AudioManager;
import com.jogamp.opengl.JoglVersion;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author JeanClement
 */
public class MainTest extends JFrame {
    
    public final static Logger logger = Logger.getLogger(MainTest.class);
    
    private static MainTest instance = null;
    
    public static MainTest getInstance() {
        if(instance == null)
            instance = new MainTest();
        
        return instance;
    }
    
    private MainTest() {
        
    }
    
    public static void main(String... args) throws Exception {
        
        PropertyConfigurator.configure("log4.properties");
        logger.info("Version JOGL : " + JoglVersion.getInstance().getImplementationVersion());
        logger.info("Démarrage de l'application");
        logger.info("Nombre d'arguments : " + args.length + ", liste :");
        for (String arg : args) {
            logger.debug("Argument : " + arg);
        }
        
        MainTest view = MainTest.getInstance();
        RenderTest render = RenderTest.getInstance();
        AudioManager audio = AudioManager.getInstance();
        JLabel status = new JLabel("none.");
        view.add(status);
        view.add(render.getCanvas());
        render.setStatus(status);
        render.start();
        view.setSize(500, 500);
        view.setResizable(false);
        view.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        view.setVisible(true);
        //audio.addPlay("audio/bgm/ejay_project1.wav");
        //audio.play();
    }
}
