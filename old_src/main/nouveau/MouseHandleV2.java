package main.nouveau;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import java_3d_engine.matrix.mvp.ModelViewProjectionMatrix;

import javax.media.opengl.awt.GLCanvas;

import main.ReadMouse;

import org.apache.log4j.Logger;

/**
 *
 * @author JeanClement
 */
public class MouseHandleV2 
    implements MouseListener, MouseMotionListener, ActionListener, MouseWheelListener {

    private final static Logger logger = Logger.getLogger(MouseHandleV2.class);
    
    private static MouseHandleV2 instance = null;
    
    private boolean initialized = false;
    private GLCanvas canvas = null;
    private ModelViewProjectionMatrix mvp = null;
    
    private int boutonPresse = -1;
    
    public MouseHandleV2 getInstance() {
        if(instance == null)
            instance = new MouseHandleV2();
        
        return instance;
    }
    
    private MouseHandleV2() {
        // Singleton
    }
    
    public void init(GLCanvas canvas, ModelViewProjectionMatrix mvp) {
        this.canvas = canvas;
        this.mvp = mvp;
        this.initialized = true;
    }
    
    @Override
    public void mouseClicked(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
        boutonPresse = me.getButton();
        if(boutonPresse == MouseEvent.BUTTON1) {
            logger.debug("Bouton gauche enfoncé !");
        } else if(boutonPresse == MouseEvent.BUTTON2) {
            logger.debug("Rolette enfoncé !");
        } else if(boutonPresse == MouseEvent.BUTTON3) {
            logger.debug("Bouton droit enfoncé !");
        }
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
        boutonPresse = -1;
        logger.debug("Bouton de la souris relaché !");
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
    }

    @Override
    public void mouseExited(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
    }

    @Override
    public void mouseMoved(MouseEvent me) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
        if(!this.initialized) {
            logger.warn("Souris non initialisé !!");
            return;
        }
        
    }
}
