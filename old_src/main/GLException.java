package main;

import java_3d_engine.interfaces.GraphicLibrary;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES1;

import org.apache.log4j.Logger;

/**
 * Exception qui est envoyé lorsqu'il y a une erreur avec OpenGL.
 * @author Olivier HABERZETTEL, Jean-Clément NIGUES
 */
public class GLException extends Exception {

    private final static Logger logger = Logger.getLogger(GLException.class);
    private String error;
    private int code;
    
    /**
     * Remonte l'erreur OpenGL via une exception Java
     * @param err Message d'erreur
     */
    public GLException(String err) {
    	error = err;
    	this.code = -1;
        //logger.fatal("CODE = " + code + "\n\t" + err);
    }
    
    /**
     * Remonte l'erreur OpenGL via une exception Java
     * @param err Message d'erreur
     * @param code Code de l'erreur
     */
    public GLException(String err, int code) {
    	error = err;
    	this.code = code;
        //logger.fatal("CODE = " + code + "\n\t" + err);
    }
    
    public void displayGLException ()
    {
    	logger.fatal("CODE = " + code + "\n\t" + error);
    }

    public static void getOpenGLError(GraphicLibrary gl) throws GLException {
        int error = gl.getGLx().glGetError();

        if (error == GL.GL_NO_ERROR) {
        	
        } else if ((error & 0x507) == GL.GL_INVALID_ENUM) {
            throw new GLException(
                    "GL_INVALID_ENUM\r\t"
            		+ "An unacceptable value is specified for an enumerated argument.\r\tThe offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flag.", GL.GL_INVALID_ENUM);
        } else if ((error & 0x0507) == GL.GL_INVALID_VALUE) {
            throw new GLException(
                    "GL_INVALID_VALUE\r\t"
            		+ "A numeric argument is out of range.\r\tThe offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flag.", GL.GL_INVALID_ENUM);
        } else if ((error & 0x0507) == GL.GL_INVALID_OPERATION) {
            throw new GLException(
                    "GL_INVALID_OPERATION\r\t"
            		+ "The specified operation is not allowed in the current state.\r\t"
                    + "The offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flag.", GL.GL_INVALID_OPERATION);
        } else if ((error & 0x0507) == GL2ES1.GL_STACK_OVERFLOW) {
            throw new GLException(
                    "GL_STACK_OVERFLOW\r\t"
                    + "This command would cause a stack overflow.\r\t"
                    + "The offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flag.", GL2ES1.GL_STACK_OVERFLOW);
        } else if ((error & 0x0507) == GL2ES1.GL_STACK_UNDERFLOW) {
            throw new GLException(
                    "GL_STACK_UNDERFLOW\r\t"
                    + "This command would cause a stack underflow.\r\t"
                    + "The offending command is ignored\r\t"
                    + "and has no other side effect than to set the error flag.", GL2ES1.GL_STACK_UNDERFLOW);
        } else if ((error & 0x0507) == GL.GL_OUT_OF_MEMORY) {
            throw new GLException(
                    "GL_OUT_OF_MEMORY\r\t"
                    + "There is not enough memory left to execute the command.\r\t"
                    + "The state of the GL is undefined,\r\t"
                    + "except for the state of the error flags,\r\t"
                    + "after this error is recorded.", GL.GL_OUT_OF_MEMORY);
        } else if ((error & 0x8031) == GL2.GL_TABLE_TOO_LARGE) {
            throw new GLException(
                    "GL_TABLE_TOO_LARGE\r\t"
                    + "The specified table exceeds the implementation's maximum supported table\r\t"
                    + "size.  The offending command is ignored and has no other side effect\r\tthan to set the error flag.",
                    GL2.GL_TABLE_TOO_LARGE);
        } else {
            // Undefined error
            throw new GLException("Erreur non implémenté => error="+error, -1);
        }
        /*
         GL_OUT_OF_MEMORY
         There is not enough memory left to execute the command.
         The state of the GL is undefined,
         except for the state of the error flags,
         after this error is recorded.
         GL_TABLE_TOO_LARGE*/
    }

    public static void clearOpenGLError(GraphicLibrary gl) {
        gl.getGLx().glGetError();
    }
}
