package main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import gl.hsee.engine.jogl.display.DisplayJoglVbo;
import gl.hsee.engine.math.matrix.mvp.ModelViewProjectionMatrix;
import gl.hsee.engine.object.AbstractObject;
import gl.hsee.engine.object.Cube;
import gl.hsee.engine.object.ImportObj;
import gl.hsee.engine.shader.Shader;

import javax.media.opengl.GL4;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;

import static javax.media.opengl.GL4.*;

public class Scene extends GLCanvas  implements GLEventListener, KeyListener
{
	ModelViewProjectionMatrix _mvp;
	Shader _shader;
	Cube _cube = null;
	AbstractObject _pyramide = null;
	
	float _camX, _camY, _camZ;
	
	public Scene (int width, int height, GLCapabilities capabilities)
	{
        super(capabilities);
        setSize(width, height);
        
        _camX = -30;
        _camY = 10;
        _camZ = 00;
        
        _mvp = new ModelViewProjectionMatrix();
		_mvp.getProjection().perspective(45, (float) (width) / height, 0.5f, 100);
		_mvp.loadIdentity();
		
	}
	
	@Override
	public void display (GLAutoDrawable drawable)
	{
	    update();
	    render(drawable);
	}

	@Override
	public void dispose (GLAutoDrawable arg0)
	{
	}
	
	
	private void update ()
	{
		if (down)
			_camY -= move;
		if (up)
			_camY += move;
			
		if (left)
			_camX -= move;
		if (right)
			_camX += move;
			
		if(z)
			_camZ += move;
		if (s)
			_camZ -= move;
	}
	
	private void render(GLAutoDrawable drawable)
	{
		_mvp.getView().lookAt(-1, 2, -1, 0, 0, 0, 0, 1, 0);
		System.out.println(_mvp);
		GL4 gl = drawable.getContext().getGL().getGL4();
		/*if (_cube == null)
			_cube = new Cube(new DisplayJoglVbo(gl));
		*/if (_pyramide == null)
			_pyramide = new ImportObj(new DisplayJoglVbo(gl), "audi.obj");
        gl.glClearColor(0, 0, 0, 1);
		gl.glEnable(GL_DEPTH_TEST);
        

        gl.glClear(GL_DEPTH_BUFFER_BIT);
        gl.glClear(GL_COLOR_BUFFER_BIT);
	    /*GL2 gl = drawable.getGL().getGL2();
	    
	    // draw a triangle filling the window
	    gl.glBegin(GL_TRIANGLES);
	    gl.glColor3f(1, 0, 0);
	    gl.glVertex2f(-1, -1);
	    gl.glColor3f(0, 1, 0);
	    gl.glVertex2f(0, 1);
	    gl.glColor3f(0, 0, 1);
	    gl.glVertex2f(1, -1);
	    gl.glEnd();*/
	    
	    try
		{
			_shader.useProgram();
			_mvp.sendMVPMatrix(_shader);
			
			//_cube.display(_shader);
			_pyramide.display(_shader);
		}
		catch (Exception e)
		{
			// T�ODO Auto-generated catch block
			e.printStackTrace();
		}
	    gl.glFlush();
	}

	@Override
	public void init (GLAutoDrawable arg0)
	{
		
		_shader = new Shader(arg0.getContext().getGL().getGL4(), "shaders/vertex.glsl", "shaders/fragmentSimple.glsl");
		_shader.initShaders();
		
		AbstractObject.setPath("modeles3d/");
	}

	@Override
	public void reshape (GLAutoDrawable arg0, int arg1, int arg2, int arg3,
			int arg4)
	{
		
	}
	
	private boolean down = false, up = false, left = false, right = false, z = false, s = false;
	private float move = 0.1f;
	
	@Override
	public void keyPressed (KeyEvent arg0)
	{
		switch (arg0.getKeyCode())
		{
		case KeyEvent.VK_DOWN:
			down = true;
			break;
		case KeyEvent.VK_UP:
			up = true;
			
			break;
		case KeyEvent.VK_LEFT:
			left = true;
			
			break;
		case KeyEvent.VK_RIGHT:
			right = true;
			break;
			
		case KeyEvent.VK_Z:
			z = true;
			break;
		case KeyEvent.VK_S:
			s = true;
			break;
			
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		default:
			break;
		}
	}

	@Override
	public void keyReleased (KeyEvent arg0)
	{
		switch (arg0.getKeyCode())
		{
		case KeyEvent.VK_DOWN:
			down = false;
			break;
		case KeyEvent.VK_UP:
			up = false;
			
			break;
		case KeyEvent.VK_LEFT:
			left = false;
			
			break;
		case KeyEvent.VK_RIGHT:
			right = false;
			break;
			
		case KeyEvent.VK_Z:
			z = false;
			break;
		case KeyEvent.VK_S:
			s = false;
			break;
			
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
		default:
			break;
		}
		
	}

	@Override
	public void keyTyped (KeyEvent arg0)
	{
	}
	
}
