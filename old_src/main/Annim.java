package main;

import java.io.IOException;
import java.nio.Buffer;
import java.nio.IntBuffer;

import java_3d_engine.engine.animation.IInterpolation;
import java_3d_engine.engine.animation.interpolation.Linear;
import java_3d_engine.engine.light.Directional;
import java_3d_engine.engine.light.Punctual;
import java_3d_engine.engine.light.Spot;
import java_3d_engine.engine.light.Sprite3D;
import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;
import java_3d_engine.matrix.mvp.Model;
import java_3d_engine.matrix.mvp.ModelViewProjectionMatrix;
import java_3d_engine.models.buffers.FrameBufferObject;
import java_3d_engine.models.buffers.TextureObject;
import java_3d_engine.models.object.ObjectManager;
import java_3d_engine.models.object.ObjectModel;
import java_3d_engine.shader.Shader;
import java_3d_engine.util.Util;

import javax.media.opengl.GL;
import javax.media.opengl.GL2ES1;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLCanvas;
import javax.media.opengl.fixedfunc.GLLightingFunc;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.FPSAnimator;

import echiquier.exceptions.EchecsException;
import echiquier.Echiquier;
import echiquier.Piece;

import java.util.logging.Level;

import javax.swing.JOptionPane;

public class Annim implements GLEventListener {

    /**
     * Logger qui sert à debugger l'application.
     */
    public final static Logger logger = Logger.getLogger(Annim.class);
    
    /**
     * Scène 3D
     */
    private GLCanvas canvas = null;

    /**
     * Entrée du clavier
     */
    private ReadKey key = null;
    
    /**
     * Entrée de la souris
     */
    private ReadMouse mouse = null;
    private static String path = null; // Chemin vers les modèles 3D
    private Shader shaderBlack = null;
    private Shader shaderWhite = null;
    private Shader shaderBoard = null; 
    private Shader shaderSimple = null; 
    private Shader shaderFBO = null;
    private Shader shaderPosCase = null;
    
    private Spot spotLight = null;
    private Directional dirLight = null;
    private Punctual posLight = null;
    
    /**
     * Definit la matrice Model View Projection
     */
    private ModelViewProjectionMatrix mvp = null;
    private int fenetre_largeur = 700, fenetre_hauteur = 700;
    private Piece pieceSelectionnee = null;
    
    private ObjectManager objectManager = new ObjectManager();
    
    /**
     * Definit les deplacement possible pour les afficher sur le plateau
     */
    private boolean[][] deplacementPossible = new boolean[8][8];
    
    /**
     * Definit les couleurs du masque de collision des cases du plateau et des pieces
     */
    private int[][][] casesBounding = new int[8][8][3];
    private int[][][] piecesBounding = new int[2][16][3];
    
    /**
     * Definit les pieces restantes a placer sur le plateau de jeu
     * [couleur]
     *      [0] pour les pions
     *      [1] pour les tours
     *      [2] pour les cavaliers
     *      [3] pour les fous
     * 	    [4] pour la reine
     * 	    [5] pour le roi
     */
    private int[][] piecesRestantes;;
    
    /**
     * Definition de tous les chemins d'acces
     */
    private String pathChess = "../3DChessModels/";// + "chess/";
    private String[] Bishop = {pathChess, "Bishop"};
    private String[] Checker = {pathChess, "Checker"};
    private String[] DoubleChecker = {pathChess, "DoubleChecker"};
    private String[] King = {pathChess, "King"};
    private String[] Knight = {pathChess, "Knight"};
    private String[] Pawn = {pathChess, "Pawn"};
    private String[] Queen = {pathChess, "Queen"};
    private String[] Rook = {pathChess, "Rook"};
    private String[] casePlateau = {pathChess, "caseBoard"};
    //Pawn = Cube;

    private String[] Plateau = {pathChess, "board"};
    private String[] Table = {pathChess, "tableGround"};
    
    /**
     * Active (ou non) les shaderBlacks.
     */
    private boolean shade = true;

    public static String getPath() {
        return path;
    }
    
    /**
     * Models 3D charges a partir des fichiers obj ou objcompile
     */
    private ObjectModel bishopObject;
    private ObjectModel kingObject;
    private ObjectModel knightObject;
    private ObjectModel pawnObject;
    private ObjectModel queenObject;
    private ObjectModel rookObject;
    private ObjectModel casePlateauObject;
    private ObjectModel plateauObject;
    private ObjectModel tableObject;
    
    
    private Sprite3D pieces[][];
    private Sprite3D caseBoard[][];
    private Sprite3D plateau;
    private Sprite3D table;
    
    private boolean loaded = false;

    public Annim() {
    }
    
    public void initAnnim ()
    {
        path = "../3DChessModels/";

        logger.debug("Chemin vers les modèles 3D (.obj) => " + path);

        canvas = new GLCanvas();

        //frame.add(canvas);

        canvas.addGLEventListener(this);

        logger.debug("Creation de la matrice de projection (mvp).");

        mvp = new ModelViewProjectionMatrix();
        key = new ReadKey(canvas);
        mouse = new ReadMouse(canvas, mvp);
        mouse.addObsever(mvp);
        mouse.setGraphicLibrary(gl);
        key.addObsever(mvp);

        logger.debug("Affiche la 'frame' (JFrame)");

        final FPSAnimator animator = new FPSAnimator(canvas, 60);

        /*
         * On parametre nos variables de couleurs de limitation des pieces (bounding)
         */
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                casesBounding[i][j] = new int[]{255, 80, 255 - (i * 8 + j) * 4};
            }
        }
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 16; j++) {
                piecesBounding[i][j] = new int[]{(i * 100), ((i * 16 + j) * 7), (255 - j * 15)};
            }
        }
        
        //animator.start();
        animator.start();

        logger.debug("Début de l'animation (animator.start()) !");
    }
    
    public GLCanvas getCanvas() {
        return canvas;
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        update();
        try {
            render(drawable);
        } catch (IOException e) {
            logger.fatal("Impossible de (re)dessiner | IOException - stack:", e);
        } catch (Exception e) {
            try {
                GLException.getOpenGLError(gl);
            } catch (GLException e1) {
                e1.displayGLException();
            }
            logger.fatal("Impossible de (re)dessiner | Exception - stack:", e);
        }
    }
    
    /**
     * Initialise piecesRestantes à 
     * 		[0] -> 8 pour les pions
     * 		[1] -> 2 pour les tours
     * 		[2] -> 2 pour les cavaliers
     * 		[3] -> 2 pour les fous
     * 		[4] -> 1 pour la reine
     * 		[5] -> 1 pour le roi
     */
    public void initPecesRestantAPlacer()
    {
    	piecesRestantes = new int[2][6];
    	for (int i = 0; i < 2; i++)
    	{
    		piecesRestantes[i][0] = 8;
    		piecesRestantes[i][1] = 2;
    		piecesRestantes[i][2] = 2;
    		piecesRestantes[i][3] = 2;
    		piecesRestantes[i][4] = 1;
    		piecesRestantes[i][5] = 1;
    	}
    }
    
    private void loadModels() {
        logger.info("Version d'OpenGL : " + gl.getGLx().glGetString(GL.GL_VERSION));
        logger.info("Version GLSL : " + gl.getGLx().glGetString(GL2ES2.GL_SHADING_LANGUAGE_VERSION));
        if (!loaded) {
            /**
             * On charge tous les objects 3D en mémoire
             */
            try {
            	/**
            	 * Mode mono thread
            	 */
            	
                /*bishopObject = ObjectModel.createInVBO(Bishop[0], Bishop[1], gl, false);
                kingObject = ObjectModel.createInVBO(King[0], King[1], gl, false);
                knightObject = ObjectModel.createInVBO(Knight[0], Knight[1], gl, false);
            	pawnObject = ObjectModel.createInVBO(Pawn[0], Pawn[1], gl, false);
                queenObject = ObjectModel.createInVBO(Queen[0], Queen[1], gl, false);
                rookObject = ObjectModel.createInVBO(Rook[0], Rook[1], gl, false);
                casePlateauObject = ObjectModel.createInVBO(casePlateau[0], casePlateau[1], gl, false);
                plateauObject = ObjectModel.createInVBO(Plateau[0], Plateau[1], gl, false);
                tableObject = ObjectModel.createInVBO(Table[0], Table[1], gl, false);//*/
            	
            	/**
            	 * Mode multi thread (en construction)
            	 */
                
            	bishopObject = objectManager.importObjectVBO(Bishop[0], Bishop[1], gl);
                kingObject = objectManager.importObjectVBO(King[0], King[1], gl);
            	knightObject = objectManager.importObjectVBO(Knight[0], Knight[1], gl);
                pawnObject = objectManager.importObjectVBO(Pawn[0], Pawn[1], gl);
                queenObject = objectManager.importObjectVBO(Queen[0], Queen[1], gl);
                rookObject = objectManager.importObjectVBO(Rook[0], Rook[1], gl);
                casePlateauObject = objectManager.importObjectVBO(casePlateau[0], casePlateau[1], gl);
                plateauObject = objectManager.importObjectVBO(Plateau[0], Plateau[1], gl);
                tableObject = objectManager.importObjectVBO(Table[0], Table[1], gl);//*/
                
                //*/

            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    private void loadShader ()
    {
    	//if (!loaded)
    	//{
	        gl.getGLx().glDepthFunc(GL.GL_LEQUAL);
	        gl.getGLx().glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
	
	        logger.debug("Les shaders sont : " + ((shade) ? "ACTIVE !" : "DESACTIVE..."));
	
	        if (shade) {
	            // Les noms de fichiers sont sensible aux majuscules/minuscules sous les systèmes Unix !
	            // vertex != Vertex 
	            shaderBlack = new Shader(gl, "shaders/vertex.glsl", "shaders/fragmentChessBlack.glsl");
	            shaderWhite = new Shader(gl, "shaders/vertex.glsl", "shaders/fragmentChessWhite.glsl");
	            shaderBoard = new Shader(gl, "shaders/vertex.glsl", "shaders/fragmentChessBoard.glsl");
	            shaderPosCase = new Shader(gl, "shaders/vertex.glsl", "shaders/fragmentChessPossibleCase.glsl");
	            shaderSimple = new Shader(gl, "shaders/vertexSimple.glsl", "shaders/fragmentSimple.glsl");
	            shaderFBO = new Shader(gl, "shaders/vertexSimple.glsl", "shaders/fragmentFBO.glsl");
	            buf[0] = Buffers.newDirectFloatBuffer(2).put(new float[]{0f, 1f});
	            buf[0].rewind();
	            buf[1] = Buffers.newDirectFloatBuffer(3).put(new float[]{1f, 1f, 0f});
	            buf[1].rewind();
	            buf[2] = Buffers.newDirectFloatBuffer(4).put(new float[]{1.0f, 0.0f, 0.5f, 0.0f});
	            buf[2].rewind();
	
	
	            shaderBlack.initShaders();
	            shaderWhite.initShaders();
	            shaderBoard.initShaders();
	            shaderPosCase.initShaders();
	            shaderSimple.initShaders();
	            shaderFBO.initShaders();
	        }
	
	        gl.getGLx().glShadeModel(GLLightingFunc.GL_SMOOTH);
    	//}
    }
    
    /**
     * Charge les position 
     */
    private void loadPositionPieces ()
    {
    	try {
            GLException.getOpenGLError(gl);

            Model model = new Model();
            Echiquier echec = Echiquier.getInstance();
            Piece tmp = null;

            plateau = new Sprite3D(plateauObject, model, shaderBoard, mvp);
            table = new Sprite3D(tableObject, model, shaderBoard, mvp);

            for (int i = 0, max = 8; i < max; i++) {
                for (int j = 0; j < max; j++) {
                    caseBoard[i][7 - j] = new Sprite3D(casePlateauObject, Util.getInstance().newCasePosition(i, j), shaderPosCase, mvp);
                    caseBoard[i][7 - j].setBoundingShader(shaderSimple, casesBounding[i][j], mvp);
                }
            }

            Piece piece;
            int compteurNoir = 0, compteurBlanc = 0;          
            
            // Chargement des pièces de l'echequier
            for (int i = 0, max = 8; i < max; i++)
                for (int j = 0; j < max; j++)
                {
                	piece = echec.getPiece(i, j);
                	if (piece != null)
                	{
                		int indice;
                		Shader shader;
                		if (piece.getCouleurInt()==BLANC)
                		{
                			indice = compteurBlanc++;
                			shader = shaderWhite;
                		}
                		else
                		{
                			indice = compteurNoir++;
                			shader = shaderBlack;
                		}
                		int couleur = piece.getCouleurInt();
                		ObjectModel temp = null;
                		switch (piece.getNomCode()) 
                		{
							case Piece.CAVALIER:
								temp = knightObject;
							break;
							case Piece.FOU:
								temp = bishopObject;					
							break;
							case Piece.PION:
								temp = pawnObject;
							break;
							case Piece.REINE:
								temp = queenObject;
							break;
							case Piece.ROI:
								temp = kingObject;
							break;
							case Piece.TOUR:
								temp = rookObject;
							break;
	
							default:
								break;
						}
                		pieces[couleur][indice] = new Sprite3D(temp, Util.getInstance().newPiecePosition(i, 7-j, couleur), shader, mvp);
                		pieces[couleur][indice].setBoundingShader(shaderSimple, piecesBounding[couleur][indice], mvp);
			            tmp = echec.getPiece(i, j);
			            tmp.setSprite3D(pieces[couleur][indice]);
			            pieces[couleur][indice].setPieceAssociee(tmp);
                	}
                }

            GLException.getOpenGLError(gl);
        } catch (GLException e2) {
            e2.displayGLException();
            e2.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void dispose(GLAutoDrawable drawable) 
    {
    	logger.debug("disposed");
    }

    public GraphicLibrary getGL() {
        return gl;
    }
    
    Buffer[] buf = new Buffer[3];
    GraphicLibrary gl;
    
    private static int BLANC = Piece.BLANC, NOIR = Piece.NOIR;
    
    @Override
    public void init(GLAutoDrawable drawable) {

        logger.debug("Initialisation de l'animation...");

        gl = new GraphicLibrary(drawable.getGL());//.getGraphicLibrary();
        
        //spotLight = new Spot(new float[] {0.1f, 0.3f, 0}, new float[] {0, 0.2f, 0.4f}, new float[] {0, 0.5f, 0.1f}, 70f);
        posLight = new Punctual(new float[] {1,0,0}, new float[] {0,0,1});
        
        
        loadModels();

        loadShader();
        
        
        //spotLight.setIdLight(0);
        posLight.setIdLight(0);
        
        //loaded = true;

        gl.getGLx().glEnable(GL.GL_DEPTH_TEST);
        gl.getGLx().glEnable(GL.GL_TEXTURE_2D);
        gl.getGLx().glDepthFunc(GL.GL_LEQUAL);
        gl.getGLx().glHint(GL2ES1.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);

        mvp.getProjection().perspective(anglePerspective, (float) (fenetre_largeur) / fenetre_hauteur, 1, 100);

        mvp.loadIdentity();
        mvp.getView().lookAt(0, 40, 20, 0, 0, 0, 0, 1, 0);
        
        fbo = new FrameBufferObject(gl, fenetre_largeur, fenetre_hauteur);
        String[] NomObjet = Plateau;
        pieces = new Sprite3D[2][16];
        caseBoard = new Sprite3D[8][8];
        long a1 = System.currentTimeMillis();

        loadPositionPieces();

        logger.info("Temps d'execution (chargement) : " + (System.currentTimeMillis() - a1) + "ms");
        Echiquier partie = Echiquier.getInstance();
        Main.view.getEtatPartie().setText(
                "Partie creee. C'est au tour des " + partie.getTourJoueur().getCouleur() + "s");

        try {
            GLException.getOpenGLError(gl);
        } catch (GLException e) {
            e.displayGLException();
        }
        
        //spotLight.sendLight(shaderWhite);
        posLight.sendLight(shaderWhite);
        logger.debug(spotLight);
        
        mouse.setGraphicLibrary(gl);
        mouse.setObject(pieces, shaderSimple);
        try {
			objectManager.ImportAllObjectInGraphicCard();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        /*
         * Initialisation des animations
         */
        //lin = new Linear(new float[] {1,10});
    }
    Linear lin;
    
    FrameBufferObject fbo;
    TextureObject texture;
    IntBuffer cube;
    IntBuffer cubeLength;

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GraphicLibrary gl = new GraphicLibrary(drawable.getGL());

        if (height <= 0) // avoid a divide by zero error! 
        {
            height = 1;
        }
        final float h = (float) width / (float) height;
        fenetre_largeur = width;
        fenetre_hauteur = height;
        gl.getGLx().glViewport(0, 0, width, height);
        mvp.getProjection().perspective(anglePerspective, h, 1, 10000);
    }
    private float anglePerspective = 45;

    private void update() {
    	mouse.setGraphicLibrary(gl);
    	Echiquier plateau = Echiquier.getInstance();
    	/**
         * Collision mouse pointer
         */
        collisionMousePointer(plateau);
    }
    boolean affiche = false;

    private void render(GLAutoDrawable drawable) throws IOException 
    {
        GraphicLibrary gl = new GraphicLibrary(drawable.getGL());
        
        gl.getGLx().glEnable(GL.GL_DEPTH_TEST);

        gl.getGLx().glClear(GL.GL_DEPTH_BUFFER_BIT);
        gl.getGLx().glClear(GL.GL_COLOR_BUFFER_BIT);


        for (int i = 0, max = caseBoard.length; i < max; i++)
            for (int j = 0; j < max; j++)
                if (deplacementPossible[i][j])
                {
                    caseBoard[i][j].draw(gl, shaderPosCase.getIndexAttributes());
                }

        for (int i = 0, max = pieces.length; i < max; i++)
            for (int j = 0, max1 = pieces[0].length; j < max1; j++)
                if(pieces[i][j] != null)
                {
                	
                	
                    pieces[i][j].draw(gl, shaderBlack.getIndexAttributes());
                }
        
        
        table.draw(gl, shaderBoard.getIndexAttributes());
        plateau.draw(gl, shaderBoard.getIndexAttributes());

        gl.getGLx().glFlush();

        // Vérifie s'il y a une erreur ou non.
        try {
            GLException.getOpenGLError(gl);
        } catch (GLException e) {
            logger.fatal("Erreur durant le rendu 3D (render).", e);
        }
    }
    
    /**
     * Deplace la piece selectionne a la position (x, y) (reference coin bas gauche)
     * @param pieceSelectionnee
     * @param x
     * @param y
     */
    public void deplacement3D(Piece pieceSelectionnee, int x, int y)
    {
    	Echiquier plateau = Echiquier.getInstance();
    	logger.debug("Déplacement possible vérifié, déplacement en cours!");
        // Créer annimation de déplacement
        logger.debug("emplacement de la piece : " + (char) (pieceSelectionnee.getCaseX() + 97) + (pieceSelectionnee.getCaseY() + 1));
        pieceSelectionnee.getSprite3D().setModel(Util.getInstance().newPiecePosition(x, y, pieceSelectionnee.getCouleurInt()));
        try {
            plateau.deplacer(pieceSelectionnee, x, 7 - y);
        } catch (EchecsException ex) {
            java.util.logging.Logger.getLogger(Annim.class.getName()).log(Level.SEVERE, null, ex);
        }
        logger.debug("emplacement de la piece après le deplacement : " + (char) (pieceSelectionnee.getCaseX() + 97) + (pieceSelectionnee.getCaseY() + 1));
        if (plateau.getEtatPartie() == Echiquier.ECHEC) {
            String roiCouleur = (pieceSelectionnee.getCouleurInt() == Piece.BLANC) ? "Noir" : "Blanc";
            JOptionPane.showMessageDialog(
                    Main.view,
                    "Le Roi " + roiCouleur + " est en echec !",
                    "Information",
                    JOptionPane.WARNING_MESSAGE);
        } else {
            // Change de tour
            Main.view.getEtatPartie().setText(
                    "Blanc : " + plateau.getJoueur(Piece.BLANC).getCoups() + " coups | Noir : " + plateau.getJoueur(Piece.NOIR).getCoups()
                    + " | C'est au tour des " + plateau.getTourJoueur().getCouleur() + "s");
        }
    }
    
    private void collisionMousePointer (Echiquier plateau)
    {
    	if (mouse.leftClickPressed() && plateau.partieEnCours() && plateau.getTourJoueur().getDistant() == false) {
            mvp.getProjection().perspective(anglePerspective, (float) fenetre_largeur / (float) fenetre_hauteur, 1, 10000);
            IntBuffer color = IntBuffer.allocate(4);
            fbo.bindBuffer(0, 0);
            /**
             * Draw bounding here
             */
            //gl.getGLx().glViewport(mouse.getLastClickLeftPosition()[0], fenetre_hauteur - mouse.getLastClickLeftPosition()[1], 200, 200);
            //FrameBufferObject fbo = new FrameBufferObject(gl, texture, null, FileNameVertexShader, FileNameFragmentShader)
            try {
                shaderFBO.useProgram();
                mvp.sendMatrix(shaderFBO);
            } catch (GLException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // mvp.getProjection().pick(mouse.getLastClickLeftPosition()[0],
            //fenetre_hauteur - mouse.getLastClickLeftPosition()[1], 1, 1, fenetre_largeur, fenetre_hauteur);
            for (int i = 0, max = caseBoard.length; i < max; i++) {
                for (int j = 0; j < max; j++) {
                    caseBoard[i][j].drawBounding(gl, shaderFBO.getIndexAttributes());
                }
            }
            for (int i = 0, max = pieces.length; i < max; i++) {
                for (int j = 0, max1 = pieces[0].length; j < max1; j++) {
                    if (pieces[i][j] != null) {
                        pieces[i][j].drawBounding(gl, shaderFBO.getIndexAttributes());
                    }
                }
            }

            // Les corrdonnees y ne sont pas les mêmes pour opengl et pour java swing d'ou : fenetre_hauteur - mouse.getLastClickLeftPosition()[1]
            gl.getGLx().glReadPixels(mouse.getLastClickLeftPosition()[0], fenetre_hauteur - mouse.getLastClickLeftPosition()[1], 1, 1, IGL.GL_RGB, IGL.GL_UNSIGNED_BYTE, color);
            fbo.unbindBuffer();
            gl.getGLx().glClearColor(1.0f, 1.0f, 1.0f, 0.5f);


            int[] colorInt = new int[4];
            colorInt[0] = color.get(0) & 0x000000FF;
            colorInt[1] = color.get(0) >> 8 & 0x000000FF;
            colorInt[2] = color.get(0) >> 16 & 0x000000FF;
            colorInt[3] = color.get(0) >> 24 & 0x000000FF;

            logger.debug("r:" + colorInt[0] + ", g:" + colorInt[1] + ", b:" + colorInt[2] + ", a:" + colorInt[3]);

            boolean found = false;
            for (int i = 0, max = caseBoard.length; i < max; i++) {
                for (int j = 0; j < max; j++) {
                    if (casesBounding[i][j][0] == colorInt[0]
                            && casesBounding[i][j][1] == colorInt[1]
                            && casesBounding[i][j][2] == colorInt[2]) {
                        logger.debug("Case cliqué => " + (char) (i + 97) + "" + (j + 1));
                        if (deplacementPossible[i][7 - j]) {
                            deplacement3D(pieceSelectionnee, i, j);
                        }
                        // Remet le tableau des deplacements possibles à false
                        for (int k = 0, max2 = deplacementPossible.length; k < max; k++) {
                            for (int l = 0; l < max; l++) {
                                deplacementPossible[k][l] = false;
                            }
                        }
                        found = true;
                        pieceSelectionnee = null;
                        break;
                    }
                }
            }

            if (!found) {
                for (int i = 0, max = pieces.length; i < max; i++) {
                    for (int j = 0, max1 = pieces[0].length; j < max1; j++) {
                        if (piecesBounding[i][j][0] == colorInt[0]
                                && piecesBounding[i][j][1] == colorInt[1]
                                && piecesBounding[i][j][2] == colorInt[2]) {

                            //logger.debug("Piece cliqué => " + pieceSelectionnee.getNom() + " " + pieceSelectionnee.getCouleur() + " en " + (char) (pieceSelectionnee.getCaseX() + 97) + (8 - pieceSelectionnee.getCaseY()));
                            //pieces[i][j].
                            Piece pieceCase = pieces[i][j].getPieceAssociee();
                            logger.debug("Piece trouvee : " + pieceCase);
                            if (pieceSelectionnee != null && deplacementPossible[pieceCase.getCaseX()][pieceCase.getCaseY()]) {
                                /*logger.debug("Déplacement possible vérifié, déplacement en cours!");
                                // Créer annimation de déplacement
                                logger.debug("emplacement de la piece : " + (char) (pieceSelectionnee.getCaseX() + 97) + (pieceSelectionnee.getCaseY() + 1));
                                pieceSelectionnee.getSprite3D().setModel(Util.getInstance().newPiecePosition(pieceCase.getCaseX(), 7 - pieceCase.getCaseY(), pieceSelectionnee.getCouleurInt()));
                                plateau.deplacer(pieceSelectionnee, pieceCase.getCaseX(), pieceCase.getCaseY());
                                logger.debug("emplacement de la piece après le deplacement : " + (char) (pieceSelectionnee.getCaseX() + 97) + (pieceSelectionnee.getCaseY() + 1));
                                *///pieceSelectionnee.deplacer(i, 7 - j);
                            	deplacement3D(pieceSelectionnee, pieceCase.getCaseX(), 7 - pieceCase.getCaseY());
                                found = true;

                                pieceSelectionnee = null;
                                // Remet le tableau des deplacements possibles à false
                                for (int k = 0, max2 = deplacementPossible.length; k < max2; k++) {
                                    for (int l = 0; l < max2; l++) {
                                        deplacementPossible[k][l] = false;
                                    }
                                }

                                break;
                            } else {
                                // Remet le tableau des deplacements possibles à false
                                for (int k = 0, max2 = deplacementPossible.length; k < max2; k++) {
                                    for (int l = 0; l < max2; l++) {
                                        deplacementPossible[k][l] = false;
                                    }
                                }
                                pieceSelectionnee = pieceCase;
                                for (int k = 0, max2 = deplacementPossible.length; k < max2; k++) {
                                    for (int l = 0; l < max2; l++) {
                                        logger.debug("deplacementPossible[k=" + k + "][" + max2 + " -1-" + l + "] = test sur " + k + "|" + l);
                                        try {
                                            deplacementPossible[k][l] = plateau.deplacementPossible(pieceSelectionnee, k, l);
                                        } catch (EchecsException ex) {
                                        	
                                            //java.util.logging.Logger.getLogger(Annim.class.getName()).log(Level.SEVERE, null, ex.getMessage());
                                        }
                                    }
                                }
                            }
                            //deplacementPossible[k][max2 - 1 - l] = pieceSelectionnee.deplacementPossible(k, l);
                            found = true;
                            break;
                        }
                    }
                }
            }
            gl.getGLx().glClear(GL.GL_COLOR_BUFFER_BIT);
            fbo.undindTexture(fenetre_largeur, fenetre_hauteur);
        }
    }
} // fin class Annim