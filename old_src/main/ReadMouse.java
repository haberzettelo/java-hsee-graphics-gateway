package main;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import java_3d_engine.engine.light.Sprite3D;
import java_3d_engine.event.Observable;
import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.matrix.mvp.DataTransform;
import java_3d_engine.matrix.mvp.ModelViewProjectionMatrix;
import java_3d_engine.shader.Shader;

import javax.media.opengl.awt.GLCanvas;
import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

/**
 * Cette classe s'occupe de la souris. Clique gauche = mouvement autour d'un
 * point (celui au centre de l'ecran) Roulette (Wheel) = zoom/dézoom Clique
 * droit = mouvement autour d'un axe x/y
 *
 * @author jeanclement
 */
public class ReadMouse extends Observable<DataTransform> implements MouseListener, MouseMotionListener, ActionListener
{
    private final static Logger logger = Logger.getLogger(ReadMouse.class);
    private boolean sourisGauchePresse;
    private boolean sourisDroitPresse;
    private boolean molettePresse;
    private int pointXClique;
    private int pointYClique;
    private GraphicLibrary gl;
    private Sprite3D[][] pieces;
    private Shader shader;
    
    private GLCanvas canvas;
    private ModelViewProjectionMatrix mvp;
    
    public ReadMouse(GLCanvas canvas, ModelViewProjectionMatrix mvp) {
        this.canvas = canvas;
        this.mvp = mvp;
        
        this.canvas.addMouseListener(this);
        this.canvas.addMouseMotionListener(this);
        this.canvas.requestFocus(); // Perte du focus sous Gnome 3 (linux) !

        sourisGauchePresse = false;
        sourisDroitPresse = false;
        molettePresse = false;
    }

    public void setGraphicLibrary (GraphicLibrary gl)
    {
    	this.gl = gl;
    }
    
    public void setObject (Sprite3D[][] objet, Shader shade)
    {
    	pieces = objet;
    	shader = shade;
    }
    
    /**
     * Effet elastique lorsqu'on fait un mouvement de la souris
     *
     * @param x
     * @param y
     * @param duree
     */
    private void easeOutCubic(float x, float y, int duree) {
        //long t = // Temps de depart
        // return c*t*t*t + b ; avec (t,b,c,d)
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        // TODO Auto-generated method stub
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {

        PointerInfo ptInfo = MouseInfo.getPointerInfo();
        Point point = new Point(ptInfo.getLocation());
        SwingUtilities.convertPointFromScreen(point, e.getComponent());

        pointXClique = (int) point.getX();
        pointYClique = (int) point.getY();

        
        if (e.getButton() == MouseEvent.BUTTON1) 
        {
            sourisGauchePresse = true;
        } else if(e.getButton() == MouseEvent.BUTTON2) {
            molettePresse = true;
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            sourisDroitPresse = true;
        }

        logger.debug("Bouton enfoncé de la souris (Gauche=" + sourisGauchePresse + "/Droit=" + sourisDroitPresse + ")");
        logger.debug("Bouton enfoncé de la souris (X=" + pointXClique + " | y=" + pointYClique);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            sourisGauchePresse = false;
        } else if(e.getButton() == MouseEvent.BUTTON2) {
            molettePresse = false;
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            sourisDroitPresse = false;
        }
    }

    @Override
    public void mouseDragged(MouseEvent me) {
        int courantX = me.getX();
        int courantY = me.getY();
    
         int deltaX = courantX - pointXClique;
         int deltaY = courantY - pointYClique;
         
         
         
        
         // Si le bouton droit de la souris est enfoncé, alors on effectue une rotation autour d'un axe X/Y/Z
        if(sourisDroitPresse) {            
            /*
             * On se met en coordonnées sphériques
             * On considère deltaY comme l'angle theta et deltaX comme l'angle phi
             * 
             * Le passage des coordonnées sphérique à cartésienne se fait comme suit :
             * 		y = sin(theta)
             * 		x = r.cos(theta).sin(phi)
             *		z = r.cos(theta).cos(phi)
             */
        	/*
        	notifyAllIObservers(new DataTransform(DataTransform.ROTATE, true, new float[] {0f, (float)deltaX / 200, 0f}));
        	notifyAllIObservers(new DataTransform(DataTransform.ROTATE, true, new float[] {(float)deltaY / 200, 0f, 0}));*/
        	//double cosTheta = Math.cos(deltaX*Math.PI/180);
        	notifyAllIObservers(new DataTransform(DataTransform.ROTATE, true, new float[] {0, (float)deltaX/200, 0}));
        	//notifyAllIObservers(new DataTransform(DataTransform.ROTATE, true, new float[] {(float)deltaY * (deltaX)/200, 0, (float)deltaY * (5-deltaX)/200}));
        	//notifyAllIObservers(new DataTransform(DataTransform.TRANSLATE, true, new float[] {(float)deltaY/200,0,(float)deltaY/200}));
            
            pointXClique = courantX;
            pointYClique = courantY;
        
        // Si la molette est pressé (ou non), on effectue un zoom.
        } else if(molettePresse) {
            
        } else if(sourisGauchePresse) {
            
        }
    }
    
    public boolean leftClickPressed ()
    {
    	if (sourisGauchePresse)
    	{
    		sourisGauchePresse = false;
    		return true;
    	}
    	return false;
    }
    public int[] getLastClickLeftPosition ()
    {
    	return new int[] {pointXClique, pointYClique};
    }

    @Override
    public void mouseMoved(MouseEvent me) {

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
