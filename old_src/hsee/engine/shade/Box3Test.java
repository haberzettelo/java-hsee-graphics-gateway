package gl.hsee.engine.shade;

import static org.junit.Assert.*;
import gl.hsee.engine.math.primitive_shade.Vector3;

import org.junit.BeforeClass;
import org.junit.Test;

public class Box3Test
{
	private static Box3 _box1, _box2, _box3;
	@BeforeClass
	public static void setUpBeforeClass () throws Exception
	{
		_box1 = new Box3();
		_box2 = new Box3(new Vector3(16161.611f, -168461321.2154f, 0.645163f));
		_box3 = new Box3(new Vector3(68461.341645f, 1.543f, 354114.38464f), new Vector3());
	}

	@Test
	public void testBox3 ()
	{
		assertTrue(Vector3.equals(_box1.getV1(), new Vector3()));
		assertTrue(Vector3.equals(_box1.getV2(), new Vector3(1, 1, 1)));
	}

	@Test
	public void testBox3Vector3 ()
	{
		assertTrue(Vector3.equals(_box2.getV1(), new Vector3()));
		assertTrue(Vector3.equals(_box2.getV2(), new Vector3(16161.611f, -168461321.2154f, 0.645163f)));
	}

	@Test
	public void testBox3Vector3Vector3 ()
	{
		assertTrue(Vector3.equals(_box3.getV1(), new Vector3(68461.341645f, 1.543f, 354114.38464f)));
		assertTrue(Vector3.equals(_box3.getV2(), new Vector3()));
	}

	@Test
	public void testContain ()
	{
		assertTrue(_box1.contain(new Vector3(0.5f,  0.5f,  0.5f)));
		assertTrue(_box1.contain(new Vector3(0f,  0f,  0f)));
		assertTrue(_box1.contain(new Vector3(1f,  1f,  1f)));
		assertFalse(_box1.contain(new Vector3(1f,  1.1f,  1f)));
		assertFalse(_box1.contain(new Vector3(0f,  1.1f,  1f)));
		assertFalse(_box1.contain(new Vector3(0.5f,  1f,  -1f)));
		assertFalse(_box1.contain(new Vector3(-0.5f,  -1f,  -1f)));
	}

	@Test
	public void testGetMinX ()
	{
		assertEquals(0f, _box1.getMinX(), 0.0001f);
		assertEquals(0f, _box2.getMinX(), 0.0001f);
		assertEquals(0f, _box3.getMinX(), 0.0001f);
	}

	@Test
	public void testGetMinY ()
	{
		assertEquals(0f, _box1.getMinY(), 0.0001f);
		assertEquals(-168461321.2154f, _box2.getMinY(), 0.0001f);
		assertEquals(0f, _box3.getMinY(), 0.0001f);
	}

	@Test
	public void testGetMinZ ()
	{
		assertEquals(0f, _box1.getMinZ(), 0.0001f);
		assertEquals(0f, _box2.getMinZ(), 0.0001f);
		assertEquals(0f, _box3.getMinZ(), 0.0001f);
	}

	@Test
	public void testGetMaxX ()
	{
		assertEquals(1f, _box1.getMaxX(), 0.0001f);
		assertEquals(16161.611f, _box2.getMaxX(), 0.0001f);
		assertEquals(68461.341645f, _box3.getMaxX(), 0.0001f);
	}

	@Test
	public void testGetMaxY ()
	{
		assertEquals(1f, _box1.getMaxY(), 0.0001f);
		assertEquals(0f, _box2.getMaxY(), 0.0001f);
		assertEquals(1.543f, _box3.getMaxY(), 0.0001f);
	}

	@Test
	public void testGetMaxZ ()
	{
		assertEquals(1f, _box1.getMaxZ(), 0.0001f);
		assertEquals(0.645163f, _box2.getMaxZ(), 0.0001f);
		assertEquals(354114.38464f, _box3.getMaxZ(), 0.0001f);
	}

}
