package gl.hsee.engine.shade;

import gl.hsee.engine.math.primitive_shade.Vector3;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Box3
{
	private Vector3 _v1, _v2;
	
	/**
	 * Construct a box (0,0,0) to (1,1,1)
	 */
	public Box3 ()
	{
		_v1 = new Vector3();
		_v2 = new Vector3(1, 1, 1);
	}
	
	/**
	 * Construct a box (0,0,0) to vec
	 * @param vec Vector will be copied
	 */
	public Box3 (Vector3 vec)
	{
		_v1 = new Vector3();
		_v2 = new Vector3(vec);
	}
	
	/**
	 * Construct a box vec1 to vec2
	 * @param vec1
	 * @param vec2
	 */
	public Box3 (Vector3 vec1, Vector3 vec2)
	{
		_v1 = new Vector3(vec1);
		_v2 = new Vector3(vec2);
	}
	
	/**
	 * Return the first Vector
	 * @return
	 */
	public Vector3 getV1 ()
	{
		return _v1;
	}
	
	/**
	 * Return the second Vector
	 * @return
	 */
	public Vector3 getV2 ()
	{
		return _v2;
	}
	
	/**
	 * Return true if the vector you have specified is in the box
	 * @param vec
	 * @return
	 */
	public boolean contain (Vector3 vec)
	{
		return (vec.getX() <= getMaxX() && vec.getX() >= getMinX()) &&
				(vec.getY() <= getMaxY() && vec.getY() >= getMinY()) &&
				(vec.getZ() <= getMaxZ() && vec.getZ() >= getMinZ());
	}
	
	/**
	 * Return minimal value on X axis
	 * @return
	 */
	public float getMinX ()
	{
		if (_v1.getX() > _v2.getX())
			return _v2.getX();
		return _v1.getX();
	}
	
	/**
	 * Return minimal value on Y axis
	 * @return
	 */
	public float getMinY ()
	{
		if (_v1.getY() > _v2.getY())
			return _v2.getY();
		return _v1.getY();
	}
	
	/**
	 * Return minimal value on Z axis
	 * @return
	 */
	public float getMinZ ()
	{
		if (_v1.getZ() > _v2.getZ())
			return _v2.getZ();
		return _v1.getZ();
	}

	/**
	 * Return maximal value on X axis
	 * @return
	 */
	public float getMaxX ()
	{
		if (_v1.getX() > _v2.getX())
			return _v1.getX();
		return _v2.getX();
	}

	/**
	 * Return maximal value on Y axis
	 * @return
	 */
	public float getMaxY ()
	{
		if (_v1.getY() > _v2.getY())
			return _v1.getY();
		return _v2.getY();
	}

	/**
	 * Return maximal value on Z axis
	 * @return
	 */
	public float getMaxZ ()
	{
		if (_v1.getZ() > _v2.getZ())
			return _v1.getZ();
		return _v2.getZ();
	}
	
	public String toString ()
	{
		return "Box3 ("+_v1+", "+_v2+")";
	}
}
