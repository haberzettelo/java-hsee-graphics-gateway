package gl.hsee.engine.util;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;

import javax.media.opengl.GL;
import javax.media.opengl.GL4;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.sun.opengl.util.BufferUtil;

/**
 * Contient un ensemble de fonctions "utilitaire". C'est à dire des fonctions 
 * qui servent dans l'ensemble du programme.
 * @author Olivier HABERZETTEL, Jean-Clément NIGUES
 */
public class Util {

    // Pattern Singleton
    private static Util instance = null;
    
    /**
     * Logger qui sert à debugger l'application.
     */
    public final static Logger logger = Logger.getLogger(Util.class);

    public static Util getInstance() {
        if (instance == null) {
            instance = new Util();
        }
        return instance;
    }

    private Util() {
    }

    /**
     * Conversion d'une couleur hexadecimal en couleur OpenGL\n Par défaut,
     * alpha vaut 1 (opaque). (soit 00 en hexadecimal)
     *
     * @param hex nombre hexadecimal (ex: 0xFFFFFF ou 0xFFFFFF15) avec la valeur
     * Alpha.
     * @return un tableau de float de taille 4 qui contient les valeurs RGBA de
     * la couleur.
     */
    public float[] hexToGLColor(int hex) {
        float[] color = new float[4];

        color[0] = ((hex >> 16) & 0xFF) / 255.0f; // Red
        color[1] = ((hex >> 8) & 0xFF) / 255.0f; // Green
        color[2] = ((hex >> 0) & 0xFF) / 255.0f; // Blue !
        color[3] = 1.0f; // Alpha

        logger.debug("Conversation hex=>openGL : " + hex + " => " + color[0] + "/" + color[1] + "/" + color[2] + "/" + color[3]);

        return color;
    }

    /**
     *
     * @param _gl
     * @param type example : IGL.GL_ELEMENT_ARRAY_BUFFER or IGL.GL_ARRAY_BUFFER
     * @param nbDisplay nb of element to be displayed
     * @param nbElemtsByLine nb of element by line
     */
    @SuppressWarnings("unused")
    public void displayElementsOfVBO(GL4 _gl, int type, int nbDisplay, int nbElemtsByLine) {
        if (type != GL.GL_ELEMENT_ARRAY_BUFFER) {
            FloatBuffer a = Buffers.newDirectFloatBuffer(nbDisplay);

            _gl.glGetBufferSubData(type, 0, nbDisplay * BufferUtil.SIZEOF_FLOAT, a);


            if (a.capacity() > 0) {
                for (int i = 0; i < nbDisplay; i++) {
                    if (i % nbElemtsByLine == 0) {
                        System.out.print("\r	" + a.get());
                    } else {
                        System.out.print(", 	" + a.get());
                    }
                }
            }
        } else {
            IntBuffer a = Buffers.newDirectIntBuffer(nbDisplay);

            _gl.glGetBufferSubData(type, 0, nbDisplay * BufferUtil.SIZEOF_FLOAT, a);


            if (a.capacity() > 0) {
                for (int i = 0; i < nbDisplay; i++) {
                    if (i % nbElemtsByLine == 0) {
                        System.out.print("\r	" + a.get());
                    } else {
                        System.out.print(", 	" + a.get());
                    }
                }
            }
        }
        System.out.println();
    }

    public float max(float f1, float f2) {
        if (f1 > f2) {
            return f1;
        }
        return f2;
    }

}
