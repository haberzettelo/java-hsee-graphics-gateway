package gl.hsee.engine.object;

import java.util.ArrayList;

import javax.media.opengl.GL4;

import gl.hsee.engine.callback.IDisplayCallback;
import gl.hsee.engine.math.primitive_shade.Vector3;
import gl.hsee.engine.shade.Box3;

public class Cube extends AbstractObject
{
	private Vector3 _vec1, _vec2;
	public Cube (IDisplayCallback callback)
	{
		this(callback, new Vector3(), new Vector3(1, 1, 1));
	}
	
	public Cube (IDisplayCallback callback, Vector3 vec1, Vector3 vec2)
	{
		super(callback);
		super.setFilename("cube");
		_vec1 = vec1;
		_vec2 = vec2;
		store();
	}

	@Override
	protected void initShade ()
	{
		SubObject object = new SubObject();
		object.vertex = new ArrayList<Vector3>(10);
		object.normal = new ArrayList<Vector3>(6);
		object.texture = new ArrayList<Vector3>(1);
		object.color = new ArrayList<Vector3>(1);
		Box3 box = new Box3(_vec1, _vec2);

		object.vertex.add(new Vector3(box.getMinX(), box.getMinY(), box.getMinZ()));
		object.vertex.add(new Vector3(box.getMaxX(), box.getMinY(), box.getMinZ()));
		object.vertex.add(new Vector3(box.getMaxX(), box.getMinY(), box.getMaxZ()));
		object.vertex.add(new Vector3(box.getMinX(), box.getMinY(), box.getMaxZ()));
		object.vertex.add(new Vector3(box.getMinX(), box.getMaxY(), box.getMaxZ()));
		object.vertex.add(new Vector3(box.getMinX(), box.getMaxY(), box.getMinZ()));
		object.vertex.add(new Vector3(box.getMaxX(), box.getMaxY(), box.getMinZ()));
		object.vertex.add(new Vector3(box.getMaxX(), box.getMaxY(), box.getMaxZ()));
		System.out.println("Construction du cube");
		for(int i = 0; i < object.vertex.size(); i++)
			System.out.println(object.vertex.get(i));
		
		object.normal.add(new Vector3(0, 0, -1));
		object.normal.add(new Vector3(1, 0, 0));
		object.normal.add(new Vector3(0, 0, 1));
		object.normal.add(new Vector3(-1, 0, 0));
		object.normal.add(new Vector3(0, -1, 0));
		object.normal.add(new Vector3(0, 1, 0));
		
		object.texture.add(new Vector3());
		object.color.add(new Vector3());
		
		object.index = new ArrayList<int[]>(6);
		
		object.index.add(new int[] {
					0, 0, 0, 0, 
					5, 0, 0, 0, 
					1, 0, 0, 0, 
					6, 0, 0, 0
				});
		object.index.add(new int[] {
				1, 1, 0, 0, 
				6, 1, 0, 0, 
				2, 1, 0, 0,
				7, 1, 0, 0
		});
		object.index.add(new int[] {
				2, 2, 0, 0,
				7, 2, 0, 0, 
				3, 2, 0, 0, 
				4, 2, 0, 0
		});
		object.index.add(new int[] {
				7, 5, 0, 0, 
				6, 5, 0, 0, 
				4, 5, 0, 0, 
				5, 5, 0, 0
		});
		object.index.add(new int[] {
				4, 3, 0, 0, 
				5, 3, 0, 0, 
				3, 3, 0, 0, 
				0, 3, 0, 0
		});
		object.index.add(new int[] {
				3, 4, 0, 0, 
				0, 4, 0, 0, 
				2, 4, 0, 0, 
				1, 4, 0, 0
		});
		
		super.subObjects.add(object);
	}

	@Override
	public int getWayToDraw ()
	{
		// TODO Auto-generated method stub
		return GL4.GL_TRIANGLE_STRIP;
	}
	
}
