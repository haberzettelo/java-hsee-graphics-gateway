package gl.hsee.engine.object;

import gl.hsee.engine.callback.IDisplayCallback;
import gl.hsee.engine.callback.ObjectDisplayCallback;
import gl.hsee.engine.math.primitive_shade.Vector3;
import gl.hsee.engine.shader.Shader;

import java.nio.IntBuffer;
import java.util.ArrayList;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Define your filepath with setPath() method.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 17 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public abstract class AbstractObject
{

	/*public Vector3[] getVertex ()
	{
		return vertex;
	}

	public Vector3[] getNormal ()
	{
		return normal;
	}

	public Vector3[] getTexture ()
	{
		return texture;
	}
	
	public Vector3[] getColor ()
	{
		return color;
	}*/

	/**
	 * Index must be :
	 * <pre>
	 * int [][]{
	 * 	{
	 * 		idVertex, idNormal, idTexture, idColor,
	 * 		idVertex, idNormal, idTexture, idColor,
	 * 		idVertex, idNormal, idTexture, idColor,
	 * [...] (if it's not a triangle, add more vertex)
	 * 	},
	 * ...
	 * }</pre>
	 * @return
	 */
	/*public int[][] getIndex ()
	{
		return index;
	}*/


	/*protected Vector3[] vertexQuad;
	protected Vector3[] normalQuad;
	protected Vector3[] textureQuad;
	protected int[] indexVertexQuad;
	protected int[] indexNormalQuad;
	protected int[] indexTextureQuad;
*/
	/*protected Vector3[] vertex;
	protected Vector3[] normal;
	protected Vector3[] texture;
	protected Vector3[] color;
	protected int[][] index;*/
	
	protected ArrayList<SubObject> subObjects;
	
	public ArrayList<SubObject> getSubObjects ()
	{
		return subObjects;
	}
	
	private static String path = "";
	protected String _filename = "";
	
	public static String getPath ()
	{
		return path;
	}
	public static void setPath (String _path)
	{
		path = _path;
	}
	public String getFilename ()
	{
		return _filename;
	}
	public void setFilename (String _filename)
	{
		this._filename = _filename;
	}
	
	private IDisplayCallback _callback;
	private ObjectDisplayCallback _objectToCallback;
	
	/**
	 * Initialize callback
	 * @param callback
	 */
	protected AbstractObject (IDisplayCallback callback)
	{
	/*	vertexQuad = null;
		normalQuad = null;
		textureQuad = null;
		*//*vertex = null;
		normal = null;
		texture = null;
		color = null;
		
		//indexNormalQuad = null;
		//indexNormal = null;
		//indexTextureQuad = null;
		//indexTexture = null;
		//indexVertexQuad = null;
		//indexVertex = null;
		index = null;
		*/
		subObjects = new ArrayList<>();
		_callback = callback;
	}
	
	/**
	 * Call this method to generate all datas you need to store this OBJ
	 */
	public void init ()
	{
		initShade();
	}
	
	protected void store ()
	{
		_objectToCallback = _callback.store(this);
	}
	
	/**
	 * Store your data with "store" callback method<br>
	 * This method must fill datas contained in this object
	 */
	abstract protected void initShade(); 
	
	public final void display (Shader shader)
	{
		_callback.display(_objectToCallback, shader);
	}
	
	
	/**
	 * Get the way to draw this shape
	 * @return May be GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN or GL_TRIANGLE
	 */
	public abstract int getWayToDraw();
}
