package gl.hsee.engine.object;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.media.opengl.GL4;

import org.apache.log4j.Logger;

import java_3d_engine.models.object.ObjectModel;
import gl.hsee.engine.callback.IDisplayCallback;
import gl.hsee.engine.math.primitive_shade.Vector3;

public class ImportObj extends AbstractObject
{
    private final static Logger logger = Logger.getLogger(ImportObj.class);
    
	public ImportObj (IDisplayCallback callback, String filename)
	{
		super(callback);
		_filename = filename;
		store();
	}

	private ArrayList<SubObject> _objects = new ArrayList<SubObject>();

	@Override
	protected void initShade ()
	{
		//Vector3[] e = (Vector3[])vertex.toArray();
		
		/**
         * Opening object file (.obj)
         */
		BufferedReader reader = getReader(super.getPath() + super._filename);
		String line = "";
		// Partie lecture
		try
		{
			while ((line = reader.readLine()) != null)
			{
				treatLine(line);
			}
		}
		catch (IOException e)
		{
			logger.error(e.toString());
		}
		
		// Partie conversion
		storeSubObjects();
	}
	
	private void storeSubObjects ()
	{
		//SubObject regroupObject = getTotalCapacity(_objects);
		super.subObjects = _objects;
		ArrayList<Vector3> vec = new ArrayList<>();
		vec.add(new Vector3());
		for (int i = 0; i < subObjects.size(); i++)
		{
			if (subObjects.get(i).color == null || subObjects.get(i).color.size() < 1)
			{
				subObjects.get(i).color = vec;
			}
				
		}
		/*super.vertex = (Vector3[])regroupObject.vertex.toArray(new Vector3[1]);
		super.normal = (Vector3[])regroupObject.normal.toArray(new Vector3[1]);
		super.texture = (Vector3[])regroupObject.texture.toArray(new Vector3[1]);
		super.color = (Vector3[])regroupObject.color.toArray(new Vector3[1]);
		if (super.color == null)
			super.color = new Vector3[] {new Vector3()};
		super.index = (int[][])regroupObject.index.toArray(new int[1][1]);*/
	}
	
	
	
	private SubObject getTotalCapacity (ArrayList<SubObject> object)
	{
		SubObject regroupObject = new SubObject();
		if (object.size() < 1)
			return null;
		regroupObject.vertex = new ArrayList<>();
		regroupObject.normal = new ArrayList<>();
		regroupObject.texture = new ArrayList<>();
		regroupObject.color = new ArrayList<>();
		regroupObject.index = new ArrayList<>();
		for (SubObject objects : object)
		{
			regroupObject.vertex.addAll(objects.vertex);
			regroupObject.normal.addAll(objects.normal);
			regroupObject.texture.addAll(objects.texture);
			regroupObject.color.addAll(objects.color);
			regroupObject.index.addAll(objects.index);
		}
		return regroupObject;
	}
	
	private boolean _flagNewGroup = false;
	private void treatLine (String line)
	{
		String[] splitedLine = line.split(" ");
		if (splitedLine.length < 1 || (splitedLine.length == 1 && splitedLine[0] == ""))
			return;
		
		switch (splitedLine[0])
			{
			case "v":
				if (!_flagNewGroup)
				{
					_flagNewGroup = true;
					_objects.add(new SubObject());
				}
				_objects.get(_objects.size()-1).vertex.add(parseThreeFloat(splitedLine));
				break;
			case "vt":
				_objects.get(_objects.size()-1).texture.add(parseTwoFloat(splitedLine));
				
				break;
			case "vn":
				_objects.get(_objects.size()-1).normal.add(parseThreeFloat(splitedLine));
				
				break;
			case "vc":
				_objects.get(_objects.size()-1).color.add(parseThreeFloat(splitedLine));
				
				break;
			case "f":
				if (_flagNewGroup)
					_flagNewGroup = false;
				treatFace(splitedLine);
				break;

			default:
				break;
			}
	}
	
	private void treatFace(String[] splitedLine)
	{
		int[] face = new int[(splitedLine.length - 1) * 4];
		for (int i = 1, max = splitedLine.length; i < max; i++)
		{
			// Pour chaque point d'une face
			String[] splitedPoint = splitedLine[i].split("/");
			for (int j = 0, max1 = splitedPoint.length; j < max1; j++)
				// Pour chaque donnee d'un point
				face[(i - 1) * 4 + j] = new Integer(splitedPoint[j]) - 1;
			for (int j = splitedPoint.length, max1 = 4; j < max1; j++)
				// Pour chaque donnee d'un point
				face[(i - 1) * 4 + j] = 0;
			
			// On inverse l'index 2 avec le trois car les textures sont specifies avant les normale ce qui n'est pas le cas pour nous
			int tmp = face[(i - 1) * 4 + 1];
			face[(i - 1) * 4 + 1] = face[(i - 1) * 4 + 2];
			face[(i - 1) * 4 + 2] = tmp;
		}
		_objects.get(_objects.size()-1).index.add(face);
	}
	
	private BufferedReader getReader (String fileName)
	{
		FileInputStream fichierObj = null;
        try {
            fichierObj = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
        	logger.error("The file \"" + fileName + "\" is not found.");
        	System.exit(0);
        }

        return new BufferedReader(new InputStreamReader(fichierObj));
	}
	
    /**
     * Parse three float contained in string ignoring the first element
     * Typically the form of String is [String, Float in String, Float in
     * String, Float in String]
     *
     * @param threeFloats Table of String to parse
     * @return
     */
    private Vector3 parseThreeFloat(String[] threeFloats) {
        return new Vector3(Float.parseFloat(threeFloats[1]), Float.parseFloat(threeFloats[2]), Float.parseFloat(threeFloats[3]));
    }
    /**
     * Parse three float contained in string ignoring the first element
     * Typically the form of String is [String, Float in String, Float in
     * String, Float in String]
     *
     * @param threeFloats Table of String to parse
     * @return
     */
    private Vector3 parseTwoFloat(String[] threeFloats) {
    	return new Vector3(Float.parseFloat(threeFloats[1]), Float.parseFloat(threeFloats[2]), 0);
    }

	@Override
	public int getWayToDraw ()
	{
		// TODO Auto-generated method stub
		return GL4.GL_TRIANGLE_STRIP;
	}
}

