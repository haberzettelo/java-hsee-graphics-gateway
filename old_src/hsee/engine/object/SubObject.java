package gl.hsee.engine.object;

import gl.hsee.engine.math.primitive_shade.Vector3;

import java.util.ArrayList;

public class SubObject
{
	public ArrayList<Vector3> vertex = new ArrayList<>();
	public ArrayList<Vector3> normal = new ArrayList<>();
	public ArrayList<Vector3> texture = new ArrayList<>();
	public ArrayList<Vector3> color = new ArrayList<>();
	
	public ArrayList<int[]> index = new ArrayList<>();
}
