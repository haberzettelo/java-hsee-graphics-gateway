package gl.hsee.engine.shader;

import gl.hsee.engine.exceptions.GLException;
import gl.hsee.engine.math.matrix.Matrix;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Scanner;

import javax.media.opengl.GL;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GL4;




import org.apache.log4j.Logger;

/**
 *
 * Shader class, create vertex and fragment shader in opengl program
 *
 * glUniform1f : Set value uniform from your shader (float value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Float value
 *
 * glUniform2f : Set value uniform from your shader (vec2 value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Float value
 * @param y Float value
 *
 * glUniform3f : Set value uniform from your shader (vec3 value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Float value
 * @param y Float value
 * @param z Float value
 *
 * glUniform4f : Set value uniform from your shader (vec4 value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Float value
 * @param y Float value
 * @param z Float value
 * @param w Float value
 *
 * glUniform1i : Set value uniform from your shader (int value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Int value
 *
 * uniform2i : Set value uniform from your shader (ivec2 value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Int value
 * @param y Int value
 *
 * glUniform3i : Set value uniform from your shader (ivec3 value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Int value
 * @param y Int value
 * @param z Int value
 *
 * glUniform4i : Set value uniform from your shader (ivec4 value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param x Int value
 * @param y Int value
 * @param z Int value
 * @param w Int value
 *
 * glUniform1fv : Set value uniform from your shader (float
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniform2fv : Set value uniform from your shader (vec2 yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniform3fv : Set value uniform from your shader (vec3 yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniform4fv : Set value uniform from your shader (vec4 yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniform1fv : Set value uniform from your shader (float
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 *
 * glUniform2fv : Set value uniform from your shader (vec2 yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 *
 * glUniform3fv : Set value uniform from your shader (vec3 yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 *
 * glUniform4fv : Set value uniform from your shader (vec4 yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array float
 *
 * glUniform1iv : Set value uniform from your shader (int yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 * @param offset Offset in data (often 0)
 *
 * glUniform2iv : Set value uniform from your shader (ivec2
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 * @param offset Offset in data (often 0)
 *
 * glUniform3iv : Set value uniform from your shader (ivec3
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 * @param offset Offset in data (often 0)
 *
 * glUniform4iv : Set value uniform from your shader (ivec4
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 * @param offset Offset in data (often 0)
 *
 * glUniform1iv : Set value uniform from your shader (int yourUniformVar[count]
 * value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 *
 * glUniform2iv : Set value uniform from your shader (ivec2
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 *
 * glUniform3iv : Set value uniform from your shader (ivec3
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 *
 * glUniform4iv : Set value uniform from your shader (ivec4
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param data Array int
 *
 * glUniformMatrix2fv : Set value uniform from your shader (mat2
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param transpose If true, matrix will be transposed before send
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniformMatrix3fv : Set value uniform from your shader (mat3
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param transpose If true, matrix will be transposed before send
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniformMatrix4fv : Set value uniform from your shader (mat4
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param transpose If true, matrix will be transposed before send
 * @param data Array float
 * @param offset Offset in data (often 0)
 *
 * glUniformMatrix2fv : Set value uniform from your shader (mat2
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param transpose If true, matrix will be transposed before send
 * @param data Array float
 *
 * glUniformMatrix3fv : Set value uniform from your shader (mat3
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param transpose If true, matrix will be transposed before send
 * @param data Array float
 *
 * glUniformMatrix4fv : Set value uniform from your shader (mat4
 * yourUniformVar[count] value in shader)
 *
 * @param location ID in shader program of your uniform var
 * @param count Number of element of the array in GLSL
 * @param transpose If true, matrix will be transposed before send
 * @param data Array float
 *
 *
 * How to use (fr) : 1 - Instancier la classe dans un premier temps, avec de
 * préférence vos noms de shader Deux choix s'offrent à vous: Laisser la classe
 * détecter toutes les variables dans vos shader afin de récupérer leur id et
 * pouvoir les mettre à jour par la suite : Pour cela, utiliser dirrectement la
 * methode initShaders() Les id de chaque variables recuperees sont dans l'ordre
 * de declaration dans les shader, par exemple uniform vec3 color; uniform vec4
 * position; color aura comme indice uniform 0 et position 1 car c'est lordre de
 * déclaration. Choisir vous même toutes les variables de vos shader en faisant
 * appel aux methodes addXxxxx(). Une fois fait, vous devez faire appel à la
 * methode initShadersManualy().
 *
 * 2 - Il faut maintenant utiliser useProgram sans quoi l'étape suivante bugera.
 *
 * 3 - Pour mettre à jour vos variables (qui ne sont pas de type structure), il
 * faut faire appel aux methodes uniformxxx(). Pour les variables de type
 * structure, vous devez d'abord récupérer la structure avec la méthode
 * getUniformStructure() puis soit : Faire appel aux méthodes uniformxxx() pour
 * tous les membres de la structure Faire appel à la methode updateAttributes()
 * qui met à jours les donnees en un appel 4 - Répéter à chaque rafraichissement
 * les étapes 2 et 3.
 *
 * @author Olivier HABERZETTEL, Jean-Clément NIGUES
 *
 */
public class Shader {

    public final static Logger logger = Logger.getLogger(Shader.class);
    public final static byte dataFloat = (byte) 0x0;//0b0000000;
    public final static byte dataInt = (byte) 0x1;//0b0000001;
    public final static byte dataVec2 = (byte) 0x2;//0b0000010;
    public final static byte dataVec3 = (byte) 0x4;//0b0000100;
    public final static byte dataVec4 = (byte) 0x6;//0b0000110;
    public final static byte dataMat2x2 = (byte) 0x8;//0b0001000;
    public final static byte dataMat3x3 = (byte) 0xA;//0b0001010;
    public final static byte dataMat4x4 = (byte) 0xC;//0b0001100;
    public final static byte dataSampler2D = (byte) 0x11;//0b0010001;
    private final static String ModelViewMatrix = "ModelViewMatrix";
    private final static int MAX_ATTRIBUTES = 20, MAX_UNIFORM = 50, MAX_UNIFORM_STRUCT = 10, MAX_UNIFORM_BLOCK = 10, MAX_VAR_IN_UNIFORM_STRUCT = 30;
    private int programID;
    private String filenameVertexShader, filenameFragmentShader;
    private GL4 gl;
    private int vertexProgramID, fragmentProgramID;
    private int[] attributesID, uniformID, uniformBlockID;
    private String[] attributesName, uniformName, uniformBlockName;
    private int nbAttribute, nbUniform, nbUniformStruct, nbUniformBlock;
    private Membres[] uniformStructure;
    private boolean init;
    private int idModelViewMatrix;

    // Impossible d'instancier la classe sans argument
    private Shader() {
        
    }
    
    /**
     * Prépare un vertex shader nommé "Vertex.glsl" et
     * un fragment shader nommé "Fragment.glsl".
     * @param gl GL4
     */
    public Shader(GL4 gl) {
        this.gl = gl;
        programID = -1;
        vertexProgramID = -1;
        fragmentProgramID = -1;
        filenameVertexShader = "Vertex.glsl";
        filenameFragmentShader = "Fragment.glsl";

        attributesID = initIntArray(MAX_ATTRIBUTES);
        uniformID = initIntArray(MAX_UNIFORM);
        uniformBlockID = initIntArray(MAX_UNIFORM_BLOCK);

        attributesName = initStringArray(MAX_ATTRIBUTES);
        uniformName = initStringArray(MAX_UNIFORM);
        uniformBlockName = initStringArray(MAX_UNIFORM_BLOCK);

        uniformStructure = new Membres[MAX_UNIFORM_STRUCT];

        nbAttribute = 0;
        nbUniform = 0;
        nbUniformStruct = 0;
        nbUniformBlock = 0;
        idModelViewMatrix = -1;

        init = false;
    }

    /**
     * TODO a commenter
     * @param gl
     * @param FileNameVertexShader
     * @param FileNameFragmentShader
     */
    public Shader(GL4 gl, String FileNameVertexShader, String FileNameFragmentShader) {
        this.gl = gl;
        programID = -1;
        vertexProgramID = -1;
        fragmentProgramID = -1;
        filenameVertexShader = FileNameVertexShader;
        filenameFragmentShader = FileNameFragmentShader;

        attributesID = initIntArray(MAX_ATTRIBUTES);
        uniformID = initIntArray(MAX_UNIFORM);
        uniformBlockID = initIntArray(MAX_UNIFORM_BLOCK);

        attributesName = initStringArray(MAX_ATTRIBUTES);
        uniformName = initStringArray(MAX_UNIFORM);
        uniformBlockName = initStringArray(MAX_UNIFORM_BLOCK);

        uniformStructure = new Membres[MAX_UNIFORM_STRUCT];

        nbAttribute = 0;
        nbUniform = 0;
        nbUniformStruct = 0;
        nbUniformBlock = 0;
        idModelViewMatrix = -1;

        init = false;
    }

    @Override
    public void finalize() throws Throwable {
        super.finalize();
        gl.glUseProgram(0);
        gl.glDetachShader(programID, vertexProgramID);
        gl.glDetachShader(programID, fragmentProgramID);
        gl.glDeleteProgram(programID);
        gl.glDeleteShader(vertexProgramID);
        gl.glDeleteShader(fragmentProgramID);
    }

    /**
     * Initialisation of program
     */
    public void initShaders() {
        GLException.clearOpenGLError(gl);
        logger.debug("Initialisation d'un shader");

        vertexProgramID = gl.glCreateShader(GL2ES2.GL_VERTEX_SHADER);
        fragmentProgramID = gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER);

        // Lit et compile le shader 'Vertex.glsl' AVANT de l'attacher au 'Context OpenGL'

        String vshd = linkShaderWithFile(vertexProgramID, filenameVertexShader);
        String fshd = linkShaderWithFile(fragmentProgramID, filenameFragmentShader);
        filenameFragmentShader = null;
        filenameVertexShader = null;

        programID = gl.glCreateProgram();
        gl.glAttachShader(programID, vertexProgramID);
        gl.glAttachShader(programID, fragmentProgramID);

        gl.glLinkProgram(programID);

        //Verify the vertex shader
        gl.glValidateProgram(programID);

        verifyProgramAndShader();

        /**
         * Analyse des variables dans le vertex shader
         */
        analyzeVarInString(vshd, false);
        analyzeVarInString(fshd, true);

        try {
            GLException.getOpenGLError(gl);
        } catch (GLException e) {
            // TODO Auto-generated catch block
            logger.error("Erreur lors de la récupération des variables dans le Vertex shader.", e);
        }

        for (int i = 0; i < nbUniformStruct; i++) {
            uniformStructure[i].initIndexLocation(gl, programID);
        }

        //linkIDAttributesAndUniforms();

        attributesName = null;
        uniformName = null;
        uniformBlockName = null;
        init = true;
        logger.debug("Fin de la méthode 'initShader' de Shader.class");
    }

    /**
     * Initialisation of program
     */
    public void initShadersManualy() {
        logger.debug("Initialisation d'un shader");

        vertexProgramID = gl.glCreateShader(GL2ES2.GL_VERTEX_SHADER);
        fragmentProgramID = gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER);

        // Lit et compile le shader 'Vertex.glsl' AVANT de l'attacher au 'Context OpenGL'

        linkShaderWithFile(vertexProgramID, filenameVertexShader);
        linkShaderWithFile(fragmentProgramID, filenameFragmentShader);
        filenameFragmentShader = null;
        filenameVertexShader = null;

        programID = gl.glCreateProgram();
        gl.glAttachShader(programID, vertexProgramID);
        gl.glAttachShader(programID, fragmentProgramID);

        gl.glLinkProgram(programID);

        //Verify the vertex shader
        gl.glValidateProgram(programID);

        verifyProgramAndShader();

        linkIDAttributesAndUniforms();

        attributesName = null;
        uniformName = null;
        uniformBlockName = null;
        init = true;
        logger.debug("Fin de la méthode 'initShader' de Shader.class");
    }

    public void addAttribute(String AttribName) {
        if (init) {
            logger.error("The program has already initialized.");
        } else if (nbAttribute > MAX_ATTRIBUTES) {
            logger.error("Over attributes are specified.");
        } else {
            if (AttribName != null && !AttribName.equals("")) {
                attributesName[nbAttribute] = AttribName;
                nbAttribute++;
            } else {
                logger.error("Attrib Name is empty.");
            }
        }
    }

    public void addUniform(String UniformName) {
        if (init) {
            logger.error("The program has already initialized.");
        } else if (nbUniform > MAX_UNIFORM) {
            logger.error("Over uniform variables are specified.");
        } else {
            if (UniformName != null && !UniformName.equals("")) {
                uniformName[nbUniform] = UniformName;
                nbUniform++;
            } else {
                logger.error("Uniform Name is empty.");
            }
        }
    }

    public void addUniformStructure(String UniformStructureName, int NumberOfElement) {
        if (init) {
            logger.error("The program has already initialized.");
        } else if (nbUniformStruct > MAX_UNIFORM) {
            logger.error("Over uniform variables are specified.");
        } else {
            if (UniformStructureName != null && !UniformStructureName.equals("")) {
                uniformStructure[nbUniformStruct] = new Membres(UniformStructureName, NumberOfElement, this);
                nbUniformStruct++;
            } else {
                logger.error("Uniform Name is empty.");
            }
        }
    }

    public void addUniformBlock(String UniformBlockName) {
        if (init) {
            logger.error("The program has already initialized.");
        } else if (nbUniformBlock > MAX_UNIFORM) {
            logger.error("Over uniform variables are specified.");
        } else {
            if (UniformBlockName != null && !UniformBlockName.equals("")) {
                uniformBlockName[nbUniformBlock] = UniformBlockName;
                nbUniformBlock++;
            } else {
                logger.error("Uniform Name is empty.");
            }
        }
    }

    public Membres getUniformStructure(int ID) {
        if (ID > nbUniformStruct) {
            logger.error("ID out of bound.");
            (new Exception()).printStackTrace();
            return null;
        } else {
            return uniformStructure[ID];
        }
    }

    /**
     * Use your shader program with glUseProgram
     *
     * @throws GLException
     */
    public void useProgram() throws GLException {
        if (init) {
            gl.glUseProgram(programID);
        } else {
            errorInit();
        }
    }

    /**
     * Return ID of variables attributes
     *
     * @return {Vertex position ID, Vertex normal ID}
     */
    public int[] getIndexAttributes() {
        if (init) {
            return attributesID;
        } else {
            return null;
        }
    }

    /**
     * Return all ID in shader program in order of add (dans l'ordre d'ajout)
     * For use glUniformxx(index, data), use yourShader.useProgram() method
     * previously .
     *
     * @return
     */
    public int[] getIndexUniform() {
        if (init) {
            return uniformID;
        } else {
            return null;
        }
    }

    boolean isInit() {
        return init;
    }

    /**
     * Set float value uniform from your shader Matrix are available only with
     * FloatBuffer
     *
     * @param data Float data will be put in shader
     * @param location ID in shader program of your uniform var
     * @param type Represent vec2, vec3, vec4, mat2, mat3 or mat4 with symbolic
     * constant Shader.dataxxxx
     * @param count Number of elements in array
     * @param transpose If true, matrix will be transposed. If type isn't
     * matrix, this value will be ignored
     */
    public void uniformFloatArray(FloatBuffer data, int location, byte type, int count, boolean transpose) {
        data.rewind();
        switch (type & (byte) 0xFE)//0b11111110)
        {
	        case Shader.dataFloat:
	            gl.glUniform1fv(location, count, data);
	            break;
            case Shader.dataVec2:
                gl.glUniform2fv(location, count, data);
                break;
            case Shader.dataVec3:
                gl.glUniform3fv(location, count, data);
                break;
            case Shader.dataVec4:
                gl.glUniform4fv(location, count, data);
                break;
            case Shader.dataMat2x2:
                gl.glUniformMatrix2fv(location, count, transpose, data);
                break;
            case Shader.dataMat3x3:
                gl.glUniformMatrix3fv(location, count, transpose, data);
                break;
            case Shader.dataMat4x4:
                gl.glUniformMatrix4fv(location, count, transpose, data);
                break;
        }
    }
    
    /**
     * Set float value uniform from your shader Matrix are available only with
     * FloatBuffer
     *
     * @param data Float data will be put in shader. Values taken begin at the current 
     * position of you FloatBuffer. If you have a FloatBuffer who contain only data concerned
     * the values you want send, execute before this method data.rewend().
     * @param location ID in shader program of your uniform var
     * @param type Represent vec2, vec3 or vec4 with symbolic constant Shader.dataxxxx
     * @param count Number of elements in array
     * @param transpose If true, matrix will be transposed. If type isn't
     * matrix, this value will be ignored
     */
    public void uniformFloat(FloatBuffer data, int location, byte type, boolean transpose) {
        data.rewind();
        switch (type & (byte) 0xFE)//0b11111110)
        {
        	case Shader.dataFloat:
        		gl.glUniform1f(location, data.get());
        		break;
            case Shader.dataVec2:
                gl.glUniform2f(location, data.get(), data.get());
                break;
            case Shader.dataVec3:
                gl.glUniform3f(location, data.get(), data.get(), data.get());
                break;
            case Shader.dataVec4:
                gl.glUniform4f(location, data.get(), data.get(), data.get(), data.get());
                break;
        }
    }

    /**
     * Set int value uniform from your shader
     *
     * @param data Int data will be put in shader
     * @param location ID in shader program of your uniform var
     * @param type Represent vec2, vec3 vec4, mat2, mat3 or mat4 with symbolic
     * constant Shader.dataxxxx
     * @param count Number of elements in array
     */
    public void uniformIntArray(IntBuffer data, int location, byte type, byte count) {
        data.rewind();
        switch (type & (byte) 0xFE)//0b11111110)
        {
        	case Shader.dataInt:
        		gl.glUniform1iv(location, count, data);
        		break;
            case Shader.dataVec2:
                gl.glUniform2iv(location, count, data);
                break;
            case Shader.dataVec3:
                gl.glUniform3iv(location, count, data);
                break;
            case Shader.dataVec4:
                gl.glUniform4iv(location, count, data);
                break;
        }
    }

    public void sendModelViewMatrix(FloatBuffer data, boolean transpose) throws Exception {
        if (idModelViewMatrix != -1) {
            gl.glUniformMatrix4fv(idModelViewMatrix, 1, transpose, data);
        } else {
            throw new Exception("idModelViewMatrix = -1 in class Shader at function sendModelViewMatrix.");
        }
    }

    public void sendModelViewMatrix(float[] data, boolean transpose) throws Exception {
        if (idModelViewMatrix != -1) {
            gl.glUniformMatrix4fv(idModelViewMatrix, 1, transpose, data, 0);
        } else {
            throw new Exception("idModelViewMatrix = -1 in class Shader at function sendModelViewMatrix.");
        }
    }

    public void sendMatrix(Matrix data, boolean transpose, int idMatrix) throws Exception {
        if (idMatrix != -1) {
            gl.glUniformMatrix4fv(idMatrix, 1, transpose, data.getValues(), 0);
        } else {
            throw new Exception("idMatrix = -1 in class Shader at function sendMatrix.");
        }
    }

    public int getProgramId() {
        return programID;
    }

    /**
     * ------------------- Private function ----------------------
     */
    private int updateMembres(String type, String name) {
        int dataReturn = nbUniformStruct;
        for (int i = 0, max = nbUniformStruct; i < max; i++) {
            //TODO
            if (uniformStructure[i].getType().equals(type)) {
                if (uniformStructure[i].nameIsSet()) {
                    uniformStructure[nbUniformStruct] = new Membres(type, name, uniformStructure[i].getNumberOfElement(), this);
                    nbUniformStruct++;
                } else {
                    dataReturn = i;
                    uniformStructure[i].setName(name);
                }
            }
        }
        return dataReturn;
    }

    private byte getType(String type) {
        byte typeReturn = 0;
        char a = type.charAt(0);
        if (type.matches("^[vmfis][eoan][ctam][234tp]?[xl]?[234e]?(r2D)?$")) {
            if (a == 'v') {
                a = type.charAt(3);
                if (a == '2') {
                    typeReturn = Shader.dataVec2;
                } else if (a == '3') {
                    typeReturn = Shader.dataVec3;
                } else {
                    typeReturn = Shader.dataVec4;
                }

                typeReturn |= Shader.dataFloat;
            } else if (a == 'm') {
                a = type.charAt(3);
                if (a == '2') {
                    typeReturn = Shader.dataMat2x2;
                } else if (a == '3') {
                    typeReturn = Shader.dataMat3x3;
                } else {
                    typeReturn = Shader.dataMat4x4;
                }

                typeReturn |= Shader.dataFloat;
            } else if (a == 'f') {
                typeReturn = Shader.dataFloat;
            } else if (a == 's') {
                typeReturn = Shader.dataSampler2D;
            } else {
                a = type.charAt(1);
                if (a == 'n') {
                    typeReturn = Shader.dataInt;
                } else {
                    a = type.charAt(4);
                    if (a == '2') {
                        typeReturn = Shader.dataVec2;
                    } else if (a == '3') {
                        typeReturn = Shader.dataVec3;
                    } else {
                        typeReturn = Shader.dataVec4;
                    }

                    typeReturn |= Shader.dataInt;
                }
            }
        }

        return typeReturn;
    }

    private int[] initIntArray(int max) {
        int[] result = new int[max];
        for (int i = 0; i < max; i++) {
            result[i] = -1;
        }
        return result;
    }

    private void errorInit() throws GLException {
        logger.error("The program isn't initialized.");
        throw new GLException("The program isn't initialized.");
    }

    private String readFromStream(String str) throws IOException {
        FileReader ins = new FileReader(new File(str));
        StringBuffer buffer = new StringBuffer();
        Scanner scanner = new Scanner(ins);
        try {
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine() + "\n");
            }
        } finally {
            scanner.close();
        }
        return buffer.toString();
    }

    /**
     * Link your index shader with your file specified
     *
     * @param gl Opengl context
     * @param indexProgram Index of your shader
     * @param fileName File name of your shader (exemple : Vertex.glsl)
     */
    private String linkShaderWithFile(int indexProgram, String fileName) {
        String vsrc = null;
        try {
            logger.debug("Lecture du shader '" + fileName + "'.");
            vsrc = readFromStream(fileName);

            logger.debug("Compile le shader '" + fileName + "' ...");
            gl.glShaderSource(indexProgram, 1, new String[]{vsrc}, null, 0);
            gl.glCompileShader(indexProgram);

            GLException.getOpenGLError(gl);
        } catch (IOException e) {
            logger.error("Erreur lors de la lecture du shader '" + fileName + "' - stack:", e);
        } catch (GLException e) {
            logger.error("Exception indéterminé - stack:");
            e.displayGLException();
            e.printStackTrace();
        } catch (Exception e)
        {
        	logger.error("Erreur lors de la lecture du fichier");
        }
        return vsrc;
    }

    private void verifyShader(int shaderID, String addText) throws GLException {
        logger.debug("Debut de verifyShader");
        IntBuffer intBuffer = IntBuffer.allocate(1);
        String erreur = "";
        String add;
        if (!addText.equals("")) {
            add = addText + " s";
        } else {
            add = "S";
        }

        // 1 - Verifie si le shader compile
        gl.glGetShaderiv(shaderID, GL2ES2.GL_COMPILE_STATUS, intBuffer);
        if (intBuffer.get(0) != GL.GL_TRUE) {
            erreur = "Erreur lors de la compilation du shader !";
        }

        // 2 - Récupère des informations sur l'erreur (s'il y a une erreur)
        if (!erreur.equals("")) {
            gl.glGetShaderiv(shaderID, GL2ES2.GL_INFO_LOG_LENGTH, intBuffer);
            int size = intBuffer.get(0);
            String error = add + "hader error: \n";
            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetShaderInfoLog(shaderID, size, intBuffer, byteBuffer);
                for (int i = 0, max = intBuffer.get(0); i < max; i++) {
                    error += ((char) byteBuffer.get());
                }
            } else {
                logger.fatal("Erreur inconnue | intBuffer.get(0)=" + intBuffer.get(0) + " pour GL_INFO_LOG_LENGTH");
                error += ("Unknown");
            }
            if (!error.contains("No errors")) {
                logger.fatal(erreur);
                logger.fatal(error);
                GLException.getOpenGLError(gl);
            }
        }
        try {
            GLException.getOpenGLError(gl);
        } catch (GLException e) {
            logger.error("Erreur lors de la vérification de '" + addText + "'.", e);
            e.displayGLException();
        }
        logger.debug("Fin de verifyShader");
    }

    private void verifyProgram() throws GLException {
        IntBuffer validate_status = IntBuffer.allocate(1);
        IntBuffer attached_shaders = IntBuffer.allocate(1);
        IntBuffer link_status = IntBuffer.allocate(1);

        logger.info("programID = " + programID);

        gl.glGetProgramiv(programID, GL2ES2.GL_VALIDATE_STATUS, validate_status);
        gl.glGetProgramiv(programID, GL2ES2.GL_ATTACHED_SHADERS, attached_shaders);
        gl.glGetProgramiv(programID, GL2ES2.GL_LINK_STATUS, link_status);

        logger.info("Nombre de shaders attaché : " + attached_shaders.get(0));
        logger.info("Programme valide : " + validate_status.get(0));
        logger.info("Est ce que le shader est linké : " + link_status.get(0));

        if (validate_status.get(0) == GL.GL_FALSE) {
            IntBuffer intBuffer = IntBuffer.allocate(1);
            gl.glGetProgramiv(programID, GL2ES2.GL_INFO_LOG_LENGTH, intBuffer);
            int size = intBuffer.get(0);
            String error = "Program validate error: ";
            if (size > 0) {
                ByteBuffer byteBuffer = ByteBuffer.allocate(size);
                gl.glGetProgramInfoLog(programID, size, intBuffer, byteBuffer);
                for (byte b : byteBuffer.array()) {
                    error += ((char) b);
                }
            } else {
                logger.fatal("Erreur inconnue, intBuffer.get(0)=" + intBuffer.get(0));
                error += "Unknown";
            }
            logger.error(error);
            try {
                GLException.getOpenGLError(gl);
            } catch (GLException e) {
                logger.error("Erreur lors de la vérification du program. Erreur due à une mauvaise utilisation ", e);
                e.displayGLException();
            }
            System.exit(0);
        }
    }

    private void verifyProgramAndShader() {
        boolean error = false;
        try {
            verifyShader(vertexProgramID, "Vertex");
        } catch (GLException e) {
            logger.error("Exception GLException - verifyShader 'Vertex' fail", e);
            error = true;
        }

        try {
            verifyShader(fragmentProgramID, "Fragment");
        } catch (GLException e) {
            logger.error("Exception GLException - verifyShader 'Fragment' fail!", e);
            error = true;
        }


        try {
            verifyProgram();
        } catch (GLException e) {
            logger.error("Exception GLException - verifyProgram fail!", e);
            error = true;
        }

        if (error) {
            logger.fatal("Erreur dans la fonction Shader.verifyProgramAndShader. Arret du programme.", (new Exception()));
            System.exit(0);
        }
    }

    private void linkIDAttributesAndUniforms() {
        boolean error = false;
        for (int i = 0; i < nbAttribute; i++) {
            attributesID[i] = gl.glGetAttribLocation(programID, attributesName[i]);
            try {
                GLException.getOpenGLError(gl);
                if (attributesID[i] == -1) {
                    throw new Exception("Erreur lors de la récupération de l'id de l'attribute " + attributesName[i] + ", index = -1.");
                }
            } catch (GLException e) {
                logger.error("Erreur lors de la récupération de l'id de l'attribute " + attributesName[i] + ", index retourné : " + attributesID[i] + ".", e);
                e.displayGLException();
                error = true;
            } catch (Exception e) {
                logger.error(e);
                error = true;
            }
        }

        for (int i = 0; i < nbUniform; i++) {
            uniformID[i] = gl.glGetUniformLocation(programID, uniformName[i]);
            if (uniformName[i].equals(ModelViewMatrix)) {
                idModelViewMatrix = uniformID[i];
            }
            try {
                GLException.getOpenGLError(gl);
                if (uniformID[i] == -1) {
                    throw new Exception("Erreur lors de la récupération de l'id de la variable uniforme " + uniformName[i] + ", index = -1.");
                }
            } catch (GLException e) {
                logger.error("Erreur lors de la récupération de l'id de la variable uniforme " + uniformName[i] + ".", e);
                e.displayGLException();
                error = true;
            } catch (Exception e) {
                logger.error("Exception de type 'Exception'", e);
                error = true;
            }
        }

        for (int i = 0; i < nbUniformBlock; i++) {
            uniformBlockID[i] = gl.glGetUniformBlockIndex(programID, uniformBlockName[i]);
            try {
                GLException.getOpenGLError(gl);
                if (uniformBlockID[i] == -1) {
                    throw new Exception("Erreur lors de la récupération de l'id de la variable uniforme block " + uniformBlockName[i] + ", index = -1.");
                }
            } catch (GLException e) {
                logger.error("Erreur lors de la récupération de l'id de la variable uniforme block " + uniformBlockName[i] + ".");
                e.displayGLException();
                error = true;
            } catch (Exception e) {
                logger.error("Exception de type 'Exception'", e);
                error = true;
            }
        }

        for (int i = 0; i < nbUniformStruct; i++) {
            uniformStructure[i].initIndexLocation(gl, programID);
        }

        if (error) {
            logger.fatal("", (new Exception()));
            System.exit(0);
        }
    }

    /**
     * Init all element of array at null
     *
     * @param array
     */
    private String[] initStringArray(int nbOfElement) {
        String[] result = new String[nbOfElement];
        for (int i = 0, max = result.length; i < max; i++) {
            result[i] = null;
        }
        return result;
    }

    private String deleteComment(String vshd) {
        String replace = vshd.replaceAll("(?s)(.*)(/\\*.*?\\*/)(.*)", "$1$3");
        while (!vshd.equals(replace)) {
            vshd = replace;
            replace = vshd.replaceAll("(?s)(.*)(/\\*.*?\\*/)(.*)", "$1$3");
        }
        replace = vshd.replaceAll("(.*)(//.*?)([\r\n])", "$1$3");
        while (!vshd.equals(replace)) {
            vshd = replace;
            replace = vshd.replaceAll("(.*)(//.*?)([\r\n])", "$1$3");
        }
        return replace;
    }

    private void analyzeVarInString(String shader, boolean testUnicite) {
        String line = "", lineStruct = "";
        //Le (?s) permet à * d'avaler les \r et \n
        String vshd = shader;

        vshd = deleteComment(vshd);

        boolean struct = false;
        int beginIndex = -1, decalage = 0;

        for (int i = 0, max = vshd.length(); i < max; i++) {
            char a = vshd.charAt(i);
            if (a == '\n' || a == '\r') {
                line = vshd.substring(beginIndex + 1, i);
                beginIndex = i;

                if (struct) {
                    lineStruct += line + "\n";
                }
                String[] linesplit = line.split(" ");
                /**
                 * Test chaque élément de la ligne récupérée
                 */
                boolean Break = false;
                for (int j = 0, maxj = linesplit.length; j < maxj; j++) {
                    /**
                     * Test si c'est un attribute
                     */
                    if (linesplit[j].equals("in") || linesplit[j].equals("attribute")) {
                        String name = linesplit[linesplit.length - 1].substring(0, linesplit[linesplit.length - 1].length() - 1);
                        if (name.matches("\\[")) {
                            name = name.split("[")[0];
                        }
                        if (!testUnicite || !inArray(name, attributesName)) {
                            attributesName[nbAttribute] = name;
                            attributesID[nbAttribute] = gl.glGetAttribLocation(programID, name);
                            nbAttribute++;
                            Break = true;
                        }
                    } /**
                     * Test si c'est une var uniform
                     */
                    else if (linesplit[j].equals("uniform")) {
                        byte type = getType(linesplit[j + 1]);
                        String name = linesplit[linesplit.length - 1].substring(0, linesplit[linesplit.length - 1].length() - 1);
                        if (name.matches("\\[")) {
                            name = name.split("[")[0];
                        }


                        if (type == 0) {
                            if (!testUnicite || !nameInArray(name, uniformStructure)) {
                                updateMembres(linesplit[j + 1], name);
                            }
                        } else {
                            if (!testUnicite || !inArray(name, uniformBlockName)) {
                                uniformName[nbUniform] = name;
                                uniformID[nbUniform] = gl.glGetUniformLocation(programID, uniformName[nbUniform]);
                                if (uniformName[nbUniform].equals(ModelViewMatrix)) {
                                    idModelViewMatrix = uniformID[nbUniform];
                                    logger.debug(ModelViewMatrix + " found in your shader. ID : " + idModelViewMatrix);
                                }
                                nbUniform++;
                            }
                        }
                        Break = true;
                    } /**
                     * Si l'on trouve la declaration d'une structure
                     */
                    else if (linesplit[j].equals("struct") || struct) {
                        if (!struct) {
                            decalage = j;
                            lineStruct += line + "\n";
                        }
                        struct = true;
                        /**
                         * Si on a récupérer toutes les lignes concernant la
                         * déclaration de la structure
                         */
                        if (line.matches("};$")) {
                            String[] structLine = lineStruct.split("\n");
                            String[] names = new String[MAX_VAR_IN_UNIFORM_STRUCT];
                            byte[] varType = new byte[MAX_VAR_IN_UNIFORM_STRUCT];
                            String type = structLine[0].split(" ")[decalage + 1];
                            int count = 0;

                            if (!testUnicite || !typeInArray(type, uniformStructure)) {
                                for (int k = 0, maxk = structLine.length; k < maxk; k++) {
                                    if (structLine[k].matches(".*[vmfi][elan][ctoa][234at]?[tx]?[234]?.*")) {

                                        String[] newStructuLine = structLine[k].replaceFirst(".*([vmfi][elan][ctoa][234at]?[tx]?[234]?)(.*?);(\\n)?$", "$1$2").split(" ");
                                        names[count] = newStructuLine[1];
                                        varType[count] = getType(newStructuLine[0]);
                                        count++;
                                    }
                                }
                                uniformStructure[nbUniformStruct] = new Membres(type, count, this);
                                for (int k = 0; k < count; k++) {
                                    uniformStructure[nbUniformStruct].addAttribute(names[k], varType[k]);
                                }
                                struct = false;
                                lineStruct = "";
                                nbUniformStruct++;
                            }
                        }
                        Break = true;

                    }

                    if (Break) {
                        break;
                    }
                }



                line = "";
            }
        }
    }

    /**
     * Prototypes des fonctions inArray
     *
     * @return
     */
    private boolean nameInArray(String search, Membres[] base) {
        boolean result = false;
        for (int i = 0, max = base.length; i < max; i++) {
            //Si c'est null, on est a la fin du tableau
            if (base[i] != null) {
                //Si le name est identique
                if (search.equals(base[i].getName())) {
                    result = true;
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }

    private boolean typeInArray(String search, Membres[] base) {
        boolean result = false;
        for (int i = 0, max = base.length; i < max; i++) {
            //Si c'est null, on est a la fin du tableau
            if (base[i] != null) {
                //Si le name est identique
                if (search.equals(base[i].getType())) {
                    result = true;
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }

    private boolean inArray(String search, String[] base) {
        boolean result = false;
        for (int i = 0, max = base.length; i < max; i++) {
            //Si c'est null, on est a la fin du tableau
            if (base[i] != null) {
                //Si le name est identique
                if (search.equals(base[i])) {
                    result = true;
                    break;
                }
            } else {
                break;
            }
        }
        return result;
    }
}
