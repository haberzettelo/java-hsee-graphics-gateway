package gl.hsee.engine.jogl.display;

import java.nio.IntBuffer;

import com.jogamp.common.nio.Buffers;

public class AbstractObjectConversionIndex
{
	public static final int RESTART_INDEX = 0xFFFFFFFF;
	private static AbstractObjectConversionIndex _instance = null;
	public static AbstractObjectConversionIndex getInstance ()
	{
		if (_instance != null)
			return _instance;
		_instance = new AbstractObjectConversionIndex();
		return _instance;
	}
	private AbstractObjectConversionIndex ()
	{
		
	}
	
	/**
	 * Permet de convertir plusieurs faces pouvant etre mises bout a bout en une seule
	 * @param object
	 * @return
	 */
	public JoglObject optimizeToTrianglesStrip (JoglObject object)
	{
		return object;
	}
	
	/**
	 * Permet de fusioner toutes les faces en une seule en les separant par le RESTART_INDEX
	 * @param object
	 * @return
	 */
	public JoglObject mergeIndex (JoglObject object)
	{
		IntBuffer[] previousIndex = object.getIndex();
		IntBuffer resultIndex;
		
		int sizeOfBuffer = previousIndex.length - 1;
		for (IntBuffer intBuffer : previousIndex)
		{
			sizeOfBuffer += intBuffer.capacity();
		}
		
		resultIndex = Buffers.newDirectIntBuffer(sizeOfBuffer);
		for (int i = 0, max = previousIndex.length - 1; i < max; i++)
		{
			resultIndex.put(previousIndex[i]);
			resultIndex.put(RESTART_INDEX);
		}
		resultIndex.put(previousIndex[previousIndex.length - 1]);
		resultIndex.rewind();
		object.setIndex(new IntBuffer[]{resultIndex});
		
		return object;
	}
}
