package gl.hsee.engine.jogl.display;

import java.util.ArrayList;

public class NUplets
{
	private ArrayList<Integer> _datas;
	private int _size;
	private int _index;
	private int _indexTemp = 0;
	
	public NUplets (Integer... data)
	{
		_datas = new ArrayList<Integer>();
		for (Integer integer : data)
		{
			_datas.add(integer);
		}
		_size = data.length;
	}
	
	public NUplets (int[] data, int start, int count)
	{
		_datas = new ArrayList<Integer>();
		for (int i = start, end = start + count; i < end; i++)
			_datas.add(data[i]);
		_size = count;
	}
	
	public int getIndex ()
	{
		return _index;
	}
	public void setIndex (int index)
	{
		_index = index;
	}
	
	public Integer get (int index)
	{
		return _datas.get(index);
	}
	public Integer get ()
	{
		if (_indexTemp > _datas.size())
			_indexTemp = 0;
		int result = _datas.get(_indexTemp);
		_indexTemp++;
		return result;
	}
	public void rewind ()
	{
		_indexTemp = 0;
	}
	
	public boolean equals (NUplets object)
	{
		if (_size != object._size)
			return false;
		for(int i = 0; i < _size; i++)
		{
			if (get(i) != object.get(i))
				return false;
		}
		return true;
	}
	
	public String toString ()
	{
		String result = "";
		result += get(0);
		for(int i = 1; i < _size; i++)
			result += ", " + get(i);
		return "NUplet id=" + _index + " : (" + result + ")";
	}
	
}
