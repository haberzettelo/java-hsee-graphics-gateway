package gl.hsee.engine.jogl.display;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;

import gl.hsee.engine.math.primitive_shade.Vector3;
import gl.hsee.engine.object.AbstractObject;
import gl.hsee.engine.object.SubObject;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Permet de convertir chaque indice (Position, Normale, Texture, Couleur) en 1 seul
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Construisez l'objet avec votre objet � convertir en param�tre et r�cup�rez les 
 * valeurs des vertex et des indices.
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 16 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class AbstractObjectConversion
{
	public final static Logger logger = Logger.getLogger(AbstractObjectConversion.class);
	private JoglObject[] outObject;
	
	private SubObject object;
	

	private boolean _vertexPresent,
					_normalPresent,
					_texturePresent,
					_colorPresent;
	private int _numberOfDatas = 0;
	private ArrayList<NUplets> _allVertexIndex = new ArrayList<>();
	
	public AbstractObjectConversion (AbstractObject object)
	{
		logger.debug("Begining of the coversion");
		AbstractObjectConversionIndex conv = AbstractObjectConversionIndex.getInstance();
		if (object.getSubObjects().size() < 1)
			return ;
		
		outObject = new JoglObject[object.getSubObjects().size()];
		
		
		logger.debug("Begining of importation, " + object.getSubObjects().size() + " subobject to convert");
		for (int i = 0, max = object.getSubObjects().size(); i < max; i++)
		{
			logger.debug("Achievement to convert object " + (float)(i*100)/max + "%");
			_allVertexIndex = new ArrayList<>();
			this.object = object.getSubObjects().get(i);
			_numberOfDatas = 0;
			
			_vertexPresent = this.object.vertex.size() > 0;
			_normalPresent = this.object.normal.size() > 0;
			_texturePresent = this.object.texture.size() > 0;
			_colorPresent = this.object.color.size() > 0;
			
			if (_vertexPresent)
				_numberOfDatas++;
			if (_normalPresent)
				_numberOfDatas++;
			if (_texturePresent)
				_numberOfDatas++;
			if (_colorPresent)
				_numberOfDatas++;
			
			
			logger.debug("Now convert all index");
			int numberOfPoint = convertIndex();
			
			if (i == 1)
				System.out.println();;
			
			logger.debug("All datas collected, now convert");
			JoglObject out = convertDatas(numberOfPoint);
			
			out = conv.optimizeToTrianglesStrip(out);
			out = conv.mergeIndex(out);
			
			outObject[i] = out;
		}
	}
	
	public JoglObject[] getJoglObject ()
	{
		/*JoglObject object = new JoglObject();
		
		object.setColor(getColor());
		object.setIndex(getIndexIntBuffer());
		object.setNormal(getNormal());
		object.setVertex(getVertex());
		object.setTexture(getTexture());
		*/
		return outObject;
	}
	
	int[][] index;
	
	/**
	 * Convertit les donnees en Buffers adaptes a OpenGL
	 * @param points
	 * @return
	 */
	private JoglObject convertDatas (int points)
	{
		JoglObject result = new JoglObject();
		/**
		 * Il faut creer autant de donnees qu'il y a de points differents
		 */
		if (_vertexPresent)
			result.vertex = Buffers.newDirectFloatBuffer(points * 3);// new Vector3[points];
		if (_normalPresent)
			result.normal = Buffers.newDirectFloatBuffer(points * 3);//  = new Vector3[points];
		if (_texturePresent)
			result.texture = Buffers.newDirectFloatBuffer(points * 3);//  = new Vector3[points];
		if (_colorPresent)
			result.color = Buffers.newDirectFloatBuffer(points * 3);//  = new Vector3[points];

		
		index = new int[object.index.size()][];
		result.index = new IntBuffer[index.length];
		int iterator = 0;
		int counterToDislayAdvancement = 0;
		for (int faceIterator = 0, maxFace = index.length; faceIterator < maxFace; faceIterator++, counterToDislayAdvancement++)
		{
			if (counterToDislayAdvancement > 10000)
			{
				counterToDislayAdvancement = 0;
				logger.debug(" . . . Achievement to convert datas " + (float)(faceIterator*100)/maxFace + "%");
			}
			/**
			 * Pour chaque face
			 */
			int numberOfPoint = object.index.get(faceIterator).length / _numberOfDatas;
			index[faceIterator] = new int[numberOfPoint];
			result.index[faceIterator] = Buffers.newDirectIntBuffer(numberOfPoint);
			for (int point = 0, maxPoint = numberOfPoint; point < numberOfPoint; point++, iterator++)
			{
				/**
				 * Pour chaque point (chaque N-Uplet)
				 */
				// On ajoute l'indice au tableau d'indice
				index[faceIterator][point] = _allVertexIndex.get(iterator).getIndex();
				result.index[faceIterator].put(index[faceIterator][point]);
				// On reference nos points
				if (_vertexPresent)
				{
					result.vertex.position(index[faceIterator][point]*3);
					NUplets nuplet = _allVertexIndex.get(iterator);
					int indexToGetVertex = nuplet.get();
					Vector3 vertexSearched = object.vertex.get(indexToGetVertex);
					float[] vertexConverted = vertexSearched.getCoordinates();
					result.vertex.put(vertexConverted);
					//result.vertex.put(object.vertex.get(_allVertexIndex.get(iterator).get()).getCoordinates());
				}
				if (_normalPresent)
				{
					result.normal.position(index[faceIterator][point]*3);
					result.normal.put(object.normal.get(_allVertexIndex.get(iterator).get()).getCoordinates());
				}
					//					normal[index[faceIterator][point]] = object.getNormal()[_allVertexIndex.get(iterator).get()];
				if (_texturePresent)
				{
					result.texture.position(index[faceIterator][point]*3);
					result.texture.put(object.texture.get(_allVertexIndex.get(iterator).get()).getCoordinates());
				}
//					texture[index[faceIterator][point]] = object.getTexture()[_allVertexIndex.get(iterator).get()];
				if (_colorPresent)
				{
					result.color.position(index[faceIterator][point]*3);
					int index = _allVertexIndex.get(iterator).get();
					if (index < 0)
						index = 0;
					result.color.put(object.color.get(index).getCoordinates());
				}
//					color[index[faceIterator][point]] = object.getColor()[_allVertexIndex.get(iterator).get()];
			}
		}
		result.vertex.rewind();
		result.normal.rewind();
		result.texture.rewind();
		result.color.rewind();
		for (IntBuffer buf : result.index)
			buf.rewind();
		
		return result;
	}
	
	
	private int[] offset = new int[]{0, 0, 0, 0};
	/**
	 * Convertit les indices et retourne le nombre de points differents repertories
	 * @return Nombre de points differents
	 */
	private int convertIndex()
	{
		int indexPoint = 0;
		int[] newOffset = new int[] {offset[0], offset[1], offset[2], offset[3]};
		
		logger.debug("Begining of iteration, " + object.index.size() + " index to convert");
		int counterToDislayAdvancement = 0;
		long timeInAlgo = 0, timeOutAlgo = 0;
		for (int faceIterator = 0, maxFace = object.index.size(); faceIterator < maxFace; faceIterator++, counterToDislayAdvancement++)
		{
			long p1 = System.currentTimeMillis();
			if (counterToDislayAdvancement > 1000)
			{
				counterToDislayAdvancement = 0;
				logger.debug("Achievement to convert " + (float)(faceIterator*100)/maxFace + "%");
				logger.debug("	Time out algo :  " + (timeOutAlgo - timeInAlgo)/1000 + "s");
				logger.debug("	Time in algo :  " + (timeInAlgo)/1000 + "s");
			}
			/**
			 * Pour chaque face
			 */
			int[] face = object.index.get(faceIterator);
			for (int vertexData = 0, maxVertex = face.length; vertexData < maxVertex; vertexData += _numberOfDatas)
			{
				/**
				 * Pour chaque point
				 */
				if (face[vertexData] + 1 > newOffset[0])
					newOffset[0] = face[vertexData] + 1;
				if (face[vertexData + 1] + 1 > newOffset[1])
					newOffset[1] = face[vertexData + 1] + 1;
				if (face[vertexData + 2] + 1 > newOffset[2])
					newOffset[2] = face[vertexData + 2] + 1;
				if (face[vertexData + 3] + 1 > newOffset[3])
					newOffset[3] = face[vertexData + 3] + 1;
					
				face[vertexData] 		-= (offset[0]);
				face[vertexData + 1] 	-= (offset[1]);
				face[vertexData + 2] 	-= (offset[2]);
				face[vertexData + 3] 	-= (offset[3]);
				
				NUplets nouveauPoint = new NUplets(face, vertexData, _numberOfDatas);
				
				boolean flagAlreadyExist = false;
				/**
				 * On verifie que ce point n'existe pas deja
				 */
				long p2 = System.currentTimeMillis();
				for (int counterNUplets = 0, max = _allVertexIndex.size(); 
						counterNUplets < max; 
						counterNUplets++)//NUplets nuplet : _allVertexIndex)
				{
					if (nouveauPoint.equals(_allVertexIndex.get(counterNUplets)))
					{
						nouveauPoint.setIndex(_allVertexIndex.get(counterNUplets).getIndex());
						flagAlreadyExist = true;
						break;
					}
				}
				nouveauPoint.rewind();
				long p3 = System.currentTimeMillis();
				timeInAlgo += p3 - p2;
				if (!flagAlreadyExist)
				{
					nouveauPoint.setIndex(indexPoint);
					indexPoint++;
				}
				
				_allVertexIndex.add(nouveauPoint);
			}
			long p4 = System.currentTimeMillis();
			timeOutAlgo += p4 - p1;
		}
		logger.debug("End of iteration index");
		
		offset = newOffset;
		
		return indexPoint;
	}
}
