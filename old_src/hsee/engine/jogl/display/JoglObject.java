package gl.hsee.engine.jogl.display;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Represent an object to store in VBO
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 17 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class JoglObject
{
	FloatBuffer vertex, normal, texture, color;
	IntBuffer[] index;
	public JoglObject ()
	{
		vertex = null;
		normal = null;
		texture = null;
		color = null;
		index = null;
	}
	public FloatBuffer getVertex ()
	{
		return vertex;
	}
	public void setVertex (FloatBuffer vertex)
	{
		this.vertex = vertex;
	}
	public FloatBuffer getNormal ()
	{
		return normal;
	}
	public void setNormal (FloatBuffer normal)
	{
		this.normal = normal;
	}
	public FloatBuffer getTexture ()
	{
		return texture;
	}
	public void setTexture (FloatBuffer texture)
	{
		this.texture = texture;
	}
	public FloatBuffer getColor ()
	{
		return color;
	}
	public void setColor (FloatBuffer color)
	{
		this.color = color;
	}
	public IntBuffer[] getIndex ()
	{
		return index;
	}
	public void setIndex (IntBuffer[] index)
	{
		this.index = index;
	}
	
	
}
