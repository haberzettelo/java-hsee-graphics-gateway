package gl.hsee.engine.jogl.display;

import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;

import javax.media.opengl.GL4;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.sun.opengl.util.BufferUtil;

import gl.hsee.engine.callback.IDisplayCallback;
import gl.hsee.engine.callback.ObjectDisplayCallback;
import gl.hsee.engine.callback.ObjectDisplayCallback.Index;
import gl.hsee.engine.callback.ObjectDisplayCallback.ObjectGroup;
import gl.hsee.engine.jogl.io.JoglExportObject;
import gl.hsee.engine.jogl.io.JoglImportObject;
import gl.hsee.engine.object.AbstractObject;
import gl.hsee.engine.shader.Shader;
import gl.hsee.engine.util.Util;
import static javax.media.opengl.GL4.*;

/**
 *TODO A tester avec es tests unitaires pour la methode store
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 12 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class DisplayJoglVbo implements IDisplayCallback
{
	public final static Logger logger = Logger.getLogger(DisplayJoglVbo.class);

	private GL4 _gl;
	
	public DisplayJoglVbo (GL4 gl)
	{
		_gl = gl;
		//_gl.glgetuniformi
		_gl.glPrimitiveRestartIndex(AbstractObjectConversionIndex.RESTART_INDEX);
	}
	
	/**
	 * Methode testee en visuel
	 */
	@Override
	public ObjectDisplayCallback store (AbstractObject implentedObject)
	{
		logger.debug("Determination of the way to get object");
		String filePath = AbstractObject.getPath() + implentedObject.getFilename();
		JoglObject[] objectToStore = null;
		try
		{//TODO
			objectToStore = JoglImportObject.importObject(filePath);
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (objectToStore == null || objectToStore.length == 0 || objectToStore[0] == null)
		{
			logger.debug("Normal, we will compile object");
			// Dans ce cas, l'objet n'est pas compile
			logger.debug("Read object");
			implentedObject.init();
			logger.debug("Convert object");
			AbstractObjectConversion convertedObject = new AbstractObjectConversion(implentedObject);
			objectToStore = convertedObject.getJoglObject();
			try
			{
				logger.debug("Export object");
				JoglExportObject.exportObject(filePath, objectToStore);
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
			logger.debug("Fast, read from compiled file");
		
		ObjectDisplayCallback result = new ObjectDisplayCallback();
		
		logger.debug("Store an object in VBO");
		for (JoglObject joglObject : objectToStore)
		{
			IntBuffer _buf;
			int sizeOfAllocate = 2 + joglObject.getIndex().length;
			_buf = IntBuffer.allocate(sizeOfAllocate);
			// We must generate n buffers 
			// n = 1 (vertex) + 1 (normal) + 1 (texture) + nb_index_vertex + nb_index_normal + nb_index_texture
			_gl.glGenBuffers(sizeOfAllocate, _buf);
			
			int indexVertex = _buf.get();
			_gl.glBindBuffer(GL_ARRAY_BUFFER, indexVertex);
			_gl.glBufferData(GL_ARRAY_BUFFER, joglObject.getVertex().capacity() * BufferUtil.SIZEOF_FLOAT, joglObject.getVertex(), GL_STREAM_DRAW);
			Util.getInstance().displayElementsOfVBO(_gl, GL_ARRAY_BUFFER, 72, 3);
			
			int indexNormal = _buf.get();
			_gl.glBindBuffer(GL_ARRAY_BUFFER, indexNormal);
			_gl.glBufferData(GL_ARRAY_BUFFER, joglObject.getNormal().capacity() * BufferUtil.SIZEOF_FLOAT, joglObject.getNormal(), GL_STREAM_DRAW);
			Util.getInstance().displayElementsOfVBO(_gl, GL_ARRAY_BUFFER, 72, 3);
			
			ArrayList<Index> allIndex = new ArrayList<>();
			for (int i = 0, max = joglObject.getIndex().length; i < max; i++)
			{
				//_gl.glpr
				int indexIbo = _buf.get();
				int size = BufferUtil.SIZEOF_INT * (joglObject.getIndex()[i].capacity());
				_gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexIbo);
				_gl.glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, joglObject.getIndex()[i], GL_STREAM_DRAW);
				//Util.getInstance().displayElementsOfVBO(_gl, GL_ELEMENT_ARRAY_BUFFER, 8, 4);
				allIndex.add(result.createIndex(indexIbo, implentedObject.getWayToDraw(), joglObject.getIndex()[i].capacity()));
			}
			result.createObjectGroup(allIndex, indexVertex, indexNormal, 0, 0);
		}
		return result;
	}

	@Override
	public void display (ObjectDisplayCallback object, Shader shader)
	{
		int indexVertexDansShader = shader.getIndexAttributes()[0];
		int indexNormalDansShader = shader.getIndexAttributes()[1];
		
		_gl.glEnableVertexAttribArray(indexVertexDansShader);
		_gl.glEnableVertexAttribArray(indexNormalDansShader);
		_gl.glEnable(GL_PRIMITIVE_RESTART);
		for (ObjectGroup subObject : object.getIbos())
		{
			_gl.glBindBuffer(GL_ARRAY_BUFFER, subObject.indexVertex);
			_gl.glVertexAttribPointer(indexVertexDansShader, 3, GL_FLOAT, false, 0, 0);
			_gl.glBindBuffer(GL_ARRAY_BUFFER, subObject.indexNormal);
			_gl.glVertexAttribPointer(indexNormalDansShader, 3, GL_FLOAT, false, 0, 0);
			
			// Il faut ne faire qu'un appel et pas une boucle, sinon performance = mediocres
			for (Index index : subObject.object)
			{
				_gl.glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index.indexIbo);
				_gl.glDrawElements(index.wayToDraw, index.nbFaces, GL_UNSIGNED_INT, 0);	
			}
		}
		_gl.glDisable(GL_PRIMITIVE_RESTART);
		_gl.glDisableVertexAttribArray(indexVertexDansShader);
		_gl.glDisableVertexAttribArray(indexNormalDansShader);
	}

}
