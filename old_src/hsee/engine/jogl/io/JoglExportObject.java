package gl.hsee.engine.jogl.io;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Vector;

import com.jogamp.common.nio.Buffers;

import gl.hsee.engine.jogl.display.JoglObject;

public class JoglExportObject extends JoglImportExport
{
	public static void exportObject(String fileName, JoglObject[] objects) throws IOException
	{
		//TODO
		FloatBuffer[] vertex = new FloatBuffer[objects.length],
					normal = new FloatBuffer[objects.length],
					texture = new FloatBuffer[objects.length],
					color = new FloatBuffer[objects.length];
		IntBuffer[][] index = new IntBuffer[objects.length][];
		int counter = 0;
		for (JoglObject joglObject : objects)
		{
			vertex[counter] = joglObject.getVertex();
			normal[counter] = joglObject.getNormal();
			texture[counter] = joglObject.getTexture();
			color[counter] = joglObject.getColor();
			index[counter] = joglObject.getIndex();
			counter++;
		}
		
		DataOutputStream file = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fileName + extensionCompileFile)));
		
		byte dataInfo = (byte) (typeFloat | dataVertex);
		writeTableBuffer(file, vertex, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataNormal);
		writeTableBuffer(file, normal, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataTexCoord);
		writeTableBuffer(file, texture, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataColor);
		writeTableBuffer(file, color, dataInfo);
		
		dataInfo = (byte) (typeInt | dataFace);
		writeTableOfTableIntBuffer(file, index, dataInfo);
		
		file.close();
		
	}
	
	//---
	//----------
	//	Privates Methods
	//--------------------------
	//------------------------------------
	
		//---------------------
		//	Functions writer
		//---------------------
	private static void writeBoolean (DataOutputStream file, boolean data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);

		file.writeBoolean(data);
	}
	
	/**
	 * Write a table of Buffer whatever the type
	 * @param file The file where the data will be written
	 * @param data The table of buffer to write in file
	 * @param infoTypeAndData 
	 * @throws IOException
	 */
	private static void writeTableBuffer (DataOutputStream file, Buffer[] data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);

		writeTableBufferUnlessInfo(file, data, infoTypeAndData);
	}
	
	private static void writeTableBufferUnlessInfo (DataOutputStream file, Buffer[] data, byte infoTypeAndData) throws IOException
	{
		file.writeInt(data.length);
		
		for (int i = 0, max = data.length; i < max; i++)
		{
			if (data[i] != null)
			{
				data[i].rewind();
				writeBufferUnlessInfo(file, data[i]);
			}
			else
				file.writeInt(0);
		}
	}

	private static void writeVector (DataOutputStream file, Vector<Integer> data, byte infoTypeAndData) throws IOException
	{
		int length = data.size();
		
		file.write(infoTypeAndData);
		file.writeInt(length);
		
		for (int i = 0; i < length; i++)
		{
			file.writeInt(data.get(i));
		}
	}
	
	private static void writeInt (DataOutputStream file, int data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);
		file.writeInt(data);
	}
	
	private static void writeBuffer (DataOutputStream file, Buffer data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);
		
		writeBufferUnlessInfo(file, data);
	}
	private static void writeBufferUnlessInfo (DataOutputStream file, Buffer data) throws IOException
	{
		IntBuffer intBufDirect = Buffers.newDirectIntBuffer(0);
		FloatBuffer floatBufDirect = Buffers.newDirectFloatBuffer(0);
		ShortBuffer shortBufDirect = Buffers.newDirectShortBuffer(0);
		IntBuffer intBuf = IntBuffer.allocate(0);
		FloatBuffer floatBuf = FloatBuffer.allocate(0);
		ShortBuffer shortBuf = ShortBuffer.allocate(0);
		
		file.writeInt(data.capacity());
		
		if (data.getClass().equals(intBuf.getClass()) || data.getClass().equals(intBufDirect.getClass()))
		{
			IntBuffer dataTypeKnown = (IntBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeInt(dataTypeKnown.get(i));
		}
		else if (data.getClass().equals(floatBuf.getClass()) || data.getClass().equals(floatBufDirect.getClass()))
		{
			FloatBuffer dataTypeKnown = (FloatBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeFloat(dataTypeKnown.get(i));
		}
		else if (data.getClass().equals(shortBuf.getClass()) || data.getClass().equals(shortBufDirect.getClass()))
		{
			ShortBuffer dataTypeKnown = (ShortBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeShort(dataTypeKnown.get(i));
		}
		else
		{
			System.err.println("Problem when compilation file.");
		}		
	}

	private static void writeTableOfTableIntBuffer (DataOutputStream file, Buffer[][] data, byte infoTypeAndData) throws IOException
	{
		int length = data.length;
		
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		
		for (int i = 0; i < length; i++)
		{
			writeTableBufferUnlessInfo(file, data[i], infoTypeAndData);
		}
	}
	
	private static void writeTableInt (DataOutputStream file, int[] data, byte infoTypeAndData) throws IOException
	{
		int length = data.length;
		
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		
		for (int i = 0; i < length; i++)
		{
			file.writeInt(data[i]);
		}
	}
	
	private static void writeString (DataOutputStream file, String data, byte infoTypeAndData) throws IOException
	{
		file.write(infoTypeAndData);
		writeStringUnlessInfo(file, data, infoTypeAndData);
	}
	
	private static void writeStringUnlessInfo (DataOutputStream file, String data, byte infoTypeAndData) throws IOException
	{
		if (data == null)
			file.writeShort(0);
		else
		{
			file.writeShort(data.length());
			file.writeChars(data);
		}
	}
	
	private static void writeTableString (DataOutputStream file, String[] data, byte infoTypeAndData) throws IOException
	{
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		for (int i = 0, max = data.length; i < max; i++)
		{
			writeStringUnlessInfo(file, data[i], infoTypeAndData);
		}
	}

}
