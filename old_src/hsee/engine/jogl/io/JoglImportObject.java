package gl.hsee.engine.jogl.io;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Vector;

import com.jogamp.common.nio.Buffers;

import gl.hsee.engine.jogl.display.JoglObject;
import gl.hsee.ext.importobj.ResultObjMtlFileIndexed;

public class JoglImportObject extends JoglImportExport
{
	/**
	 * @throws IOException 
	 * Importe l'objet s'il existe, null sinon
	 * @param fileName pour le fichier "3dmodel/cube.obj", indiquez "3dmodels/cube"
	 * @return
	 * @throws  
	 */
	public static JoglObject[] importObject (String fileName) throws IOException
	{
		if (!isCompiled(fileName))
			return null;
	
		DataInputStream file = new DataInputStream(new BufferedInputStream(new FileInputStream(fileName + extensionCompileFile)));
		JoglObject object = new JoglObject();
		
		FloatBuffer[] vertex = null, normal = null, texture = null, color = null;
		IntBuffer[][] index = null;
		
		//mask = 0b 0111 1000
		byte mask = (byte)0x78;//0b1111000;
		
		while (file.available() > 0)
		{
			byte typeOfData = file.readByte();
			
			switch (typeOfData & mask) 
			{
				case dataVertex:
					vertex = readTableFloatBuffer(file); break;
					
				case dataTexCoord:
					texture = readTableFloatBuffer(file); break;
					
				case dataNormal:
					normal = readTableFloatBuffer(file); break;
					
				case dataColor:
					color = readTableFloatBuffer(file); break;
					
				case dataFace:
					index = readTableOfTableIntBuffer(file); break;
					
				case dataFileMTLName:
					/*TODO dataResult.fileMTLName = readString(file);*/ break;
					
				case dataFileMtlExist:
					/*TODO dataResult.fileMTLExist = readBoolean(file);*/ break;
					
				default:
					System.err.println("Nothing"); break;
				
			}
		}
		
		file.close();
		
		JoglObject[] result = new JoglObject[vertex.length];
		for (int counter = 0, max = vertex.length; counter < max; counter++)
		{
			result[counter] = new JoglObject();
			result[counter].setVertex(vertex[counter]);
			result[counter].setNormal(normal[counter]);
			result[counter].setTexture(texture[counter]);
			result[counter].setColor(color[counter]);
			result[counter].setIndex(index[counter]);
		}
		
		return result;
	}
	
	public static boolean isCompiled (String filename)
	{
		return new File(filename + extensionCompileFile).exists();
	}
	
	//---------------------
	//	Functions reader
	//---------------------	

	
	private static boolean readBoolean (DataInputStream file) throws IOException
	{
		return file.readBoolean();
	}
	
	private static int[] readTableInt (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		int[] dataResult = new int[max];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = file.readInt();
		}
		
		return dataResult;
	}
	
	private static IntBuffer[] readTableIntBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		IntBuffer[] dataResult = new IntBuffer[max];
		
		for (int i = 0; i < max; i++)
		{
			int length = file.readInt();
			dataResult[i] = Buffers.newDirectIntBuffer(length);
			for (int j = 0; j < length; j++)
			{
				dataResult[i].put(file.readInt());
			}
			dataResult[i].rewind();
		}
		return dataResult;
	}
	
	private static IntBuffer[][] readTableOfTableIntBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		IntBuffer[][] dataResult = new IntBuffer[max][];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = readTableIntBuffer(file);
		}
		
		return dataResult;
	}
	
	private static FloatBuffer[] readTableFloatBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		FloatBuffer[] dataResult = new FloatBuffer[max];
		
		for (int i = 0; i < max; i++)
		{
			int length = file.readInt();
			dataResult[i] = Buffers.newDirectFloatBuffer(length);
			for (int j = 0; j < length; j++)
			{
				dataResult[i].put(file.readFloat());
			}
			dataResult[i].rewind();
		}
		
		return dataResult;
	}
	private static FloatBuffer readFloatBuffer (DataInputStream file) throws IOException
	{
		FloatBuffer dataResult;
		
		int length = file.readInt();
		dataResult = Buffers.newDirectFloatBuffer(length);
		for (int j = 0; j < length; j++)
		{
			dataResult.put(file.readFloat());
		}
		dataResult.rewind();
		
		return dataResult;
	}
	
	private static Vector<Integer> readVector (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		Vector<Integer> dataResult = new Vector<Integer>(max);
		
		for (int i = 0; i < max; i++)
		{
			dataResult.add(file.readInt());
		}
		
		return dataResult;
	}
	
	private static String readString (DataInputStream file) throws IOException
	{
		short length = file.readShort();
		String dataResult = "";
		for (int i = 0; i < length; i++)
		{
			dataResult += file.readChar();
		}
		return dataResult;
	}
	
	private static String[] readTableString (DataInputStream file) throws IOException
	{
		String dataResult[] = new String[file.readInt()];
		for (int i = 0, max = dataResult.length; i < max; i++)
		{
			dataResult[i] = readString(file);
		}
		return dataResult;
	}
}
