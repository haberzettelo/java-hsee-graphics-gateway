package gl.hsee.engine.jogl.io;

public abstract class JoglImportExport
{
	protected static String extensionCompileFile = ".hseeobject";
	
	//Type of variable
	protected static final byte typeFloat		= (byte)0x1; //0b00000001;
	protected static final byte typeInt 		= (byte)0x2; //0b00000010;
	//private static final byte typeShort 		= 0b00000011;
	protected static final byte typeBoolean 		= (byte)0x4; //0b00000100;
	//private static final byte typeLong 		= 0b00000100;
	//private static final byte typeChar 		= 0b00000101;
	protected static final byte typeString 		= (byte)0x6; //0b00000110;
	//private static final byte typeDouble	= 0b00000111;
	
	//Type of data
	//protected static final byte dataBufOrderedQuads		= (byte)0x8; //0b00001000;
	protected static final byte dataColor			= (byte)0x10; //0b00010000;
	protected static final byte dataVertex			= (byte)0x18; //0b00011000;
	protected static final byte dataNormal 			= (byte)0x20; //0b00100000;
	protected static final byte dataTexCoord			= (byte)0x28; //0b00101000;
	//protected static final byte dataNbFace			= (byte)0x30; //0b00110000;
	//protected static final byte dataNbTriangle		= (byte)0x38; //0b00111000;
	//protected static final byte dataNbQuad			= (byte)0x40; //0b01000000;
	protected static final byte dataFileMTLName		= (byte)0x48; //0b01001000;
	protected static final byte dataFace			= (byte)0x50; //0b01010000;
	//protected static final byte dataNbFaces			= (byte)0x58; //0b01011000;
	//protected static final byte dataMaterialGroup		= (byte)0x60; //0b01100000;
	protected static final byte dataFileMtlExist		= (byte)0x68; //0b01101000;
	//private static final byte dataNum3		= 0b01110000;
	//private static final byte dataNum4		= 0b01111000;
}
