package gl.hsee.engine.callback;

import gl.hsee.engine.object.AbstractObject;
import gl.hsee.engine.shader.Shader;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Callback interface to display each object and store this vertex properties.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public interface IDisplayCallback
{
	/**
	 * Put all vertex, normal and texture coordinates in memory
	 * @param vertexQuad
	 * @param normalQuad
	 * @param textureQuad
	 * @param vertexTriangle
	 * @param normalTriangle
	 * @param textureTriangle
	 */
	public ObjectDisplayCallback store (AbstractObject implentedObject);
	
	/**
	 * Display your object
	 */
	public void display (ObjectDisplayCallback object, Shader shader);
}
