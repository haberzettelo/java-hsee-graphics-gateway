package gl.hsee.engine.callback;

import java.util.ArrayList;
import java.util.Hashtable;

public class ObjectDisplayCallback
{
	public class Index 
	{
		public int indexIbo;
		public int wayToDraw;
		public int nbFaces;
	}
	
	public class ObjectGroup
	{
		public ArrayList<Index> object = new ArrayList<>();
		public int indexVertex;
		public int indexNormal;
		public int indexColor;
		public int indexTexture;
		//public Material m;
	}
	private ArrayList<ObjectGroup> _indexs;
	
	public ObjectDisplayCallback ()
	{
		_indexs = new ArrayList<>();
	}
	
	private int _currentIndex = 0;
	
	public void createObjectGroup (ArrayList<Index> indexArray, int indexVertex, int indexNormal, int indexTexture, int indexColor)
	{
		ObjectGroup result = new ObjectGroup();
		result.object = indexArray;
		result.indexColor = indexColor;
		result.indexNormal = indexNormal;
		result.indexTexture = indexTexture;
		result.indexVertex = indexVertex;

		_indexs.add(result);
	}
	
	public Index createIndex(int indexIbo, int wayToDraw, int nbFaces)
	{
		Index result = new Index();
		result.indexIbo = indexIbo;
		result.nbFaces = nbFaces;
		result.wayToDraw = wayToDraw;
		return result;
	}
	
	public ArrayList<ObjectGroup> getIbos ()
	{
		return _indexs;
	}
}
