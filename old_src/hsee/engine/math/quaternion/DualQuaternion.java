package gl.hsee.engine.math.quaternion;

import gl.hsee.engine.math.matrix.Matrix;
import gl.hsee.engine.math.primitive_shade.Vector3;


public class DualQuaternion
{
    private Quaternion _q1, _q2;
    
    public DualQuaternion()
    {
		_q1 = new Quaternion();
		_q2 = new Quaternion();
    }
    
    public DualQuaternion(Quaternion q1, Vector3 translation)
    {
    	fromRotationTranslation(q1, translation);
    }
    
    public DualQuaternion(float x1, float y1, float z1, float w1, float x2, float y2, float z2, float w2)
    {
		_q1 = new Quaternion(x1, y1, z1, w1);
		_q2 = new Quaternion(x2, y2, z2, w2);
    }
    
    /**
     * Translate quaternion q by the vector tranlation you have specified
     * @param q Quaternion to translate
     * @param translation Translation vector
     */
    public void fromRotationTranslation(Quaternion q, Vector3 translation)
    {
		_q1 = q;
		float half = 0.5f;
		_q2 = new Quaternion(
			half * (translation.getX() * _q1.getW() + translation.getY() * _q1.getZ() - translation.getZ() * _q1.getY()), 
			half * (translation.getY() * _q1.getW() - translation.getX() * _q1.getZ() + translation.getZ() * _q1.getX()), 
			half * (translation.getX() * _q1.getY() - translation.getY() * _q1.getX() + translation.getZ() * _q1.getW()), 
			-half * (translation.getX() * _q1.getX() + translation.getY() * _q1.getY() + translation.getZ() * _q1.getZ()));
    }
    
    /**
     * Translate quaternion q by the vector tranlation you have specified
     * @param q Quaternion to translate
     * @param translation Translation vector
     */
    public void fromTranslationRotation(Quaternion q, Vector3 translation)
    {
		_q1 = q;
		float half = 0.5f;
		_q2 = new Quaternion(
			half * (_q1.getW() * translation.getX() + _q1.getY() * translation.getZ() - _q1.getZ() * translation.getY()), 
			half * (_q1.getW() * translation.getY() - _q1.getX() * translation.getZ() + _q1.getZ() * translation.getX()), 
			half * (_q1.getX() * translation.getY() - _q1.getY() * translation.getX() + _q1.getW() * translation.getZ()), 
			-half * (_q1.getX() * translation.getX() + _q1.getY() * translation.getY() + _q1.getZ() * translation.getZ()));
    }
    
    /**
     * Get the quaternion and the translation vector correspond to this dual quaternion
     * @param qResult Quaternion result
     * @return Translation vector
     */
    public final Vector3 toRotationTranslation (Quaternion qResult)
    {
		qResult.setX(_q1.getX());
		qResult.setY(_q1.getY());
		qResult.setZ(_q1.getZ());
		qResult.setW(_q1.getW());
		
		Vector3 result = new Vector3(
			2 * (_q2.getW()*_q1.getX() + _q2.getX()*_q1.getW() - _q2.getY()*_q1.getZ() + _q2.getZ()*_q1.getY()),
			2 * (_q2.getW()*_q1.getY() + _q2.getX()*_q1.getZ() + _q2.getY()*_q1.getW() - _q2.getW()*_q1.getX()),
			2 * (_q2.getW()*_q1.getZ() - _q2.getX()*_q1.getY() + _q2.getY()*_q1.getX() + _q2.getX()*_q1.getW()));
		
		return result;
    }
    
    public Matrix toTransformationMatrix()
    {
		Vector3 pos = new Vector3();
		Quaternion rot = new Quaternion();
		pos = toRotationTranslation(rot);
		
		Vector3 scale = new Vector3(1, 1, 1);
		
		return makeTransform(pos, scale, rot);
    }
    
    public Matrix makeTransform(Vector3 position, Vector3 scale, Quaternion orientation)
    {
		Matrix rotation = orientation.toRotationMatrix();
		return new Matrix (new float[]{
			scale.getX()*rotation.getValues()[0], scale.getY()*rotation.getValues()[1], scale.getZ()*rotation.getValues()[2], position.getX(),
			scale.getX()*rotation.getValues()[4], scale.getY()*rotation.getValues()[5], scale.getZ()*rotation.getValues()[6], position.getY(),
			scale.getX()*rotation.getValues()[5], scale.getY()*rotation.getValues()[9], scale.getZ()*rotation.getValues()[10], position.getZ(),
			0, 0, 0, 1 }
			);
    }
    
    /*public Matrix makeInverseTransform (Vector3 position, Vector3 scale, Quaternion orientation)
    {
		Vector3 oppositePosition = new Vector3(-position.getX(), -position.getY(), -position.getZ());
		Vector3 inverseScale = new Vector3(1 / scale.getX(), 1 / scale.getY(), 1 / scale.getZ());
		Quaternion inverseOrientation = Quaternion.inverse(orientation);
		
		oppositePosition = rotate(inverseOrientation, oppositePosition);
		oppositePosition.mult(inverseScale);
		Matrix rotation = inverseOrientation.toRotationMatrix();
		
		return new Matrix( new float[]{
			inverseScale.getX()*rotation.getValues()[0], inverseScale.getX()*rotation.getValues()[1],
				inverseScale.getX()*rotation.getValues()[2], oppositePosition.getX(),
			inverseScale.getY()*rotation.getValues()[4], inverseScale.getY()*rotation.getValues()[5],
				inverseScale.getY()*rotation.getValues()[6], oppositePosition.getY(),
			inverseScale.getZ()*rotation.getValues()[8], inverseScale.getZ()*rotation.getValues()[9],
				inverseScale.getZ()*rotation.getValues()[10], oppositePosition.getZ(),
			0, 0, 0, 1}
			);
    }*/
    
    private Vector3 rotate (Quaternion q, Vector3 c)
    {
		float twoX = 2*q.getX(), twoY = 2*q.getY(), twoZ = 2*q.getZ(),
			wVx = q.getW() * c.getX(), wVy = q.getW() * c.getY(), wVz = q.getW()*c.getZ(),
			zVx = q.getZ() * c.getX(), zVy = q.getZ() * c.getY(),
			yVx = q.getY() * c.getX(), yVz = q.getY() * c.getZ(),
			xVy = q.getX() * c.getY(), xVz = q.getX() * c.getZ();
		return new Vector3(
			q.getX() + twoY*(wVz + xVy - yVx) + twoZ*(xVz - zVx - wVy),
			q.getY() + twoZ*(wVx + yVz - zVy) + twoX*(yVx - xVy - wVz),
			q.getZ() + twoX*(wVy + zVx - xVz) + twoY*(zVy - yVz - wVx));
    }
    
    public String toString()
    {
    	return "DualQuaternion :\n" + _q1 + "\n" + _q2;
    }
}
