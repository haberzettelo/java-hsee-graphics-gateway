package gl.hsee.engine.math.quaternion;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

import gl.hsee.engine.math.matrix.Matrix;
import gl.hsee.engine.math.primitive_shade.Vector3;
import gl.hsee.engine.math.primitive_shade.Vector4;

/**
 * Class <br>
 *     - - - - - - - - - - - -<br>
 * 	    DESCRIPTION<br>
 * - - - - - - - - - - - - - - - -<br>
 * Quaternions are useful to rotate around a vector. You can rotate easily an object.<br>
 * A quaternion is represented by a vector, this vector contain 3 coordinates, it represents<br>
 * the rotation-vector, and with a real number which represent the angle of rotation.<br>
 * See these link to understand better : <br>
 * <a href="https://bitbucket.org/sinbad/ogre/src/691073040b54d7b55f7d310bf0e9738bde9f40ce/OgreMain/src/OgreQuaternion.cpp?at=default">Source of Ogre in c++</a><br>
 * <a href="https://github.com/LWJGL/lwjgl/blob/master/src/java/org/lwjgl/util/vector/Quaternion.java">Source of Lwjgl in c++</a><br>
 * <a href="http://content.gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation">Anabstract of quaternions</a><br>
 * <a href="http://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/">An other abstract of quaternions</a><br>
 *     - - - - - - - - - - - -<br>
 * 	    HOW TO USE<br>
 * - - - - - - - - - - - - - - - -<br>
 * To use a quaternion, you must use one of the three constructor<br>
 * Several methods are available to transform your quaternion:<br>
 * 	- If you want rotate a point to an other point, use rotate method. <br>
 * 	- If you want rotate axis, use multiplications methods<br>
 * You can copy your quaternion to duplicate instance with copy method.<br>
 * To apply the quaternion (rotation) to your model, you must get the matrix of your<br>
 * quaternion and put this in this matrix, such as RotationMatrix, i.e. follow this step :
 * <pre>	ModelMatrix = TranslationMatrix * QuaternionMatrix * ScaleMatrix</pre>
 *
 * @author Olivier Haberzettel
 * @version 23 f�vr. 2015
 */
class Quaternion extends Vector4
{
    private static float epsilon = 0.0001f;
    
    /**
     * Construct a quaternion with identity, i.e. vector null and 1 for rotation-angle
     */
    public Quaternion()
    {
    	super (0, 0, 0, 1);
    }
    /**
     * Constructor a quaternion from explicit values
     * @param xRotationVector _coordinate[0] coordinate of rotation-vector
     * @param yRotationVector _coordinate[1] coordinate of rotation-vector
     * @param zRotationVector _coordinate[2] coordinate of rotation-vector
     * @param rotationAngle Rotation angle to turn around rotation vector in radian
     */
    public Quaternion(float xRotationVector, float yRotationVector, float zRotationVector, float rotationAngle)
    {
    	super(xRotationVector, yRotationVector, zRotationVector, rotationAngle);
    }
    
    /**
     * Constructor a quaternion from rotation-vector and rotation-angle
     * @param rotationVector Rotation vector
     * @param rotationAngle Rotation angle to turn around rotation vector in radian
     */
    public Quaternion(Vector3 rotationVector, float rotationAngle)
    {
    	super(0, 0, 0, 0);
    	fromRotationVector(rotationVector, rotationAngle);
    }
    
    /**
     * @return _coordinate[0] component of rotaton-vector
     */
    public float getX() {return _coordinate[0];}
    /**
     * @return _coordinate[1] component of rotaton-vector
     */
    public float getY() {return _coordinate[1];}
    /**
     * @return _coordinate[2] component of rotaton-vector
     */
    public float getZ() {return _coordinate[2];}
    /**
     * @return Rotation-vector of quaternion
     */
    public Vector3 getRotationVector() {return new Vector3(_coordinate[0], _coordinate[1], _coordinate[2]);}
    /**
     * @return Rotation-angle of quaternion
     */
    public float getW() {return _coordinate[3];}
    
    /**
     * Set _coordinate[0] component of rotaton-vector
     */
    void setX(float newValue) {_coordinate[0] = newValue;}
    /**
     * Set _coordinate[1] component of rotaton-vector
     */
    void setY(float newValue) {_coordinate[1] = newValue;}
    /**
     * Set _coordinate[2] component of rotaton-vector
     */
    void setZ(float newValue) {_coordinate[2] = newValue;}
    /**
     * Set Rotation-angle of quaternion
     */
    void setW(float newValue) {_coordinate[3] = newValue;}
    
    /**
     * Get quaternion to rotation matrix, useful to inject in model matrix
     * @return Matrix represent quaternion
     */
    public Matrix toRotationMatrix()
    {
		// Use of addition to improve speed, 2 * _coordinate[0] (slow) = _coordinate[0] + _coordinate[0] (fast)
		float twoX = _coordinate[0] + _coordinate[0], twoY = _coordinate[1] + _coordinate[1], twoZ = _coordinate[2] + _coordinate[2];
		float twoXX = twoX * _coordinate[0], twoYY = twoY * _coordinate[1], twoZZ = twoZ * _coordinate[2],
			twoXW = twoX * _coordinate[3], twoYW = twoY * _coordinate[3], twoZW = twoZ * _coordinate[3],
			twoXY = twoX * _coordinate[1], twoXZ = twoX * _coordinate[2],
			twoYZ = twoY * _coordinate[2];
		return new Matrix(new float[] {
			1-(twoYY+twoZZ), twoXY - twoZW, twoXZ + twoYW, 	0, 
			twoXY+twoZW, 	1-(twoXX+twoZZ), twoYZ-twoXW, 	0, 
			twoXZ-twoYW, 	twoYZ+twoXW, 	1-(twoXX+twoYY), 0, 
			0, 		0, 		0, 		1});
    }
    
    /**
     * Compute quaternion from rotation-axis and rotation-angle
     * @param rotationVector Rotation vector
     * @param rotationAngle Rotation angle to turn around rotation vector in radian
     * @return Chaining
     */
    public Quaternion fromRotationVector(Vector3 rotationVector, float rotationAngle)
    {
		float angleOverTwo = rotationAngle * 0.5f;
		float sinAngle = (float) sin(angleOverTwo);
		_coordinate[3] = (float) cos(angleOverTwo);
		_coordinate[0] = rotationVector.getX() * sinAngle;
		_coordinate[1] = rotationVector.getY() * sinAngle;
		_coordinate[2] = rotationVector.getZ() * sinAngle;
		return this;
    }
    
    /**
     * Get a quaternion from your rotation matrix
     * @param rot Rotation matrix
     * @return Chaining
     */
    public Quaternion fromRotationMatrix(Matrix rot)
    {
    	float[] m = rot.getValues();
		float trace = m[0] + m[5] + m[10] + m[15],
			root;
		
		if (trace > 0f)
		{
		    root = (float)sqrt(trace);
		    _coordinate[3] = 0.5f * root;
		    root = 0.5f/root;
		    _coordinate[0] = (m[10] - m[6]) * root;
		    _coordinate[1] = (m[2] - m[8]) * root;
		    _coordinate[2] = (m[4] - m[1]) * root;
		} else {
		    if (m[0] > m[5] &&
			    m[0] > m[10])
		    {
				// m00 max
				root = (float)sqrt(m[0] -(m[5] + m[10]) + 1);
				_coordinate[0] = root * 0.5f;
				root = 0.5f / root;
				_coordinate[1] = (m[1] + m[4]) * root;
				_coordinate[2] = (m[2] + m[8]) * root;
				_coordinate[3] = (m[6] + m[9]) * root;
			} else if (m[5] > m[10]) {
				// m11 max
				root = (float)sqrt(m[5] -(m[0] + m[10]) + 1);
				_coordinate[1] = root * 0.5f;
				root = 0.5f / root;
				_coordinate[0] = (m[1] + m[4]) * root;
				_coordinate[3] = (m[2] + m[8]) * root;
				_coordinate[2] = (m[6] + m[9]) * root;
			} else {
				// m22 max
				root = (float)sqrt(m[10] -(m[0] + m[5]) + 1);
				_coordinate[2] = root * 0.5f;
				root = 0.5f / root;
				_coordinate[3] = (m[1] + m[4]) * root;
				_coordinate[0] = (m[2] + m[8]) * root;
				_coordinate[1] = (m[6] + m[9]) * root;
		    }
		}
		return this;
    }
    
    /**
     * Normalize your quaternion "q"
     * @param q Quaternion to normalize
     * @return New instance of quaternion fill by the result of the operation
     */
    public static Quaternion normalize (Quaternion q)
    {
    	float[] v = q.getCoordinates();
		float root = (float)sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2] + v[3]*v[3]);
		return new Quaternion(v[0] / root, v[1] / root, v[2] / root, v[3] / root);
    }
    
    /**
     * Normalize your quaternion<br>
     * This quaternion will be replaced by the result of the normalization, use static method if you want keep your quaternion.
     * @return This instance to chain
     */
    public Quaternion normalize()
    {
		float root = (float)sqrt(_coordinate[0]*_coordinate[0] + _coordinate[1]*_coordinate[1] + _coordinate[2]*_coordinate[2] + _coordinate[3]*_coordinate[3]);
		_coordinate[0] = _coordinate[0]/root;
		_coordinate[1] = _coordinate[1]/root;
		_coordinate[2] = _coordinate[2]/root;
		_coordinate[3] = _coordinate[3]/root;
		return this;
    }
    
    /**
     * Conjugate the quaternion "q"
     * @param q Quaternion to conjugate
     * @return New instance of quaternion fill by the result of the operation
     */
    public static Quaternion conjugate(Quaternion q)
    {
    	float[] v = q.getCoordinates();
    	return new Quaternion(-v[0], -v[1], -v[2], v[3]);
    }
    
    /**
     * Conjugate your quaternion<br>
     * This quaternion will be replaced by the result of the conjugation, use static method if you want keep your quaternion.
     * @return This instance to chain
     */
    public Quaternion conjugate()
    {
    	_coordinate[0] = -_coordinate[0];
    	_coordinate[1] = -_coordinate[1];
    	_coordinate[2] = -_coordinate[2];
		return this;
    }
    
    /**
     * Get new instance with values of this quaternion
     * @return New instance
     */
    public Quaternion copy ()
    {
    	return new Quaternion(_coordinate[0], _coordinate[1], _coordinate[2], _coordinate[3]);
    }
    
    /**
     * Multiply two quaternions together 
     * @param left Left operand
     * @param right Right operand
     * @return New instance of quaternion, fill by the result of the operation
     */
    public static Quaternion mult(Quaternion left, Quaternion right)
    {
    	float[] l = left._coordinate, r = right._coordinate;
		return new Quaternion(
			l[0]*r[3] + l[3]*r[0] + l[1]*r[2] - l[2]*r[1], 
			l[1]*r[3] + l[3]*r[1] + l[2]*r[0] - l[0]*r[2], 
			l[2]*r[3] + l[3]*r[2] + l[0]*r[1] - l[1]*r[0],
			l[3]*r[3] - l[0]*r[0] - l[1]*r[1] - l[2]*r[2]);
    }
    
    /**
     * Multiply this quaternion by another quaternion, this quaternion is the left operand.<br>
     * This quaternion will be replaced by the result of multiplication, use static method if you want keep your quaternion.
     * @param right Right operand
     * @return This instance to chain
     */
    public Quaternion mult(Quaternion right)
    {
    	float[] q = right._coordinate;
		float xTemp, yTemp, zTemp;
		xTemp = _coordinate[0]*q[3] + _coordinate[3]*q[0] + _coordinate[1]*q[2] - _coordinate[2]*q[1];
		yTemp = _coordinate[1]*q[3] + _coordinate[3]*q[1] + _coordinate[2]*q[0] - _coordinate[0]*q[2];
		zTemp = _coordinate[2]*q[3] + _coordinate[3]*q[2] + _coordinate[0]*q[1] - _coordinate[1]*q[0];
		_coordinate[3] = _coordinate[3]*q[3] - _coordinate[0]*q[0] - _coordinate[1]*q[1] - _coordinate[2]*q[2];
		_coordinate[0] = xTemp;
		_coordinate[1] = yTemp;
		_coordinate[2] = zTemp;
		return this;
    }
    
    /**
     * Multiply a quaternion with a vector
     * @param left Left operand
     * @param right Right operand
     * @return New instance of quaternion, fill by the result of the operation
     */
    public static Quaternion mult (Quaternion left, Vector3 right)
    {
    	float[] l = left._coordinate, r = right.getCoordinates();
		return new Quaternion(
			l[3]*r[0] + l[1]*r[2] - l[2]*r[1], 
			l[3]*r[1] + l[2]*r[0] - l[0]*r[2], 
			l[3]*r[2] + l[0]*r[1] - l[1]*r[0], 
			-l[0]*r[0] - l[1]*r[1] - l[2]*r[2]);
    }
    
    /**
     * Multiply this quaternion by vector, this quaternion is the left operand<br>
     * This quaternion will be replaced by the result of multiplication, use static method if you want keep your quaternion.
     * @param vec Right operand
     * @return This instance to chain
     */
    public Quaternion mult(Vector3 vec)
    {
		float xTemp, yTemp, zTemp;
		float[] v = vec.getCoordinates();
		xTemp = _coordinate[3]*v[0] + _coordinate[1]*v[2] - _coordinate[2]*v[1];
		yTemp = _coordinate[3]*v[1] + _coordinate[2]*v[0] - _coordinate[0]*v[2];
		zTemp = _coordinate[3]*v[2] + _coordinate[0]*v[1] - _coordinate[1]*v[0];
		_coordinate[3] = -_coordinate[0]*v[0] - _coordinate[1]*v[1] - _coordinate[2]*v[2];
		_coordinate[0] = xTemp;
		_coordinate[1] = yTemp;
		_coordinate[2] = zTemp;
		return this;
    }
    
    /**
     * Rotate your vector "v" by your quaternion "q"
     * @param q Quaternion
     * @param v Vector
     * @return The vector rotated
     */
    public static Vector3 rotate(Quaternion q, Vector3 v)
    {
		Quaternion conjugate = q.copy().conjugate();
		return Quaternion.mult(Quaternion.mult(q, v), conjugate).getRotationVector();
    }
    
    /**
     * Rotate your vector "v" by this quaternion
     * @param v Vector
     * @return The vector rotated
     */
    public Vector3 rotate(Vector3 v)
    {
		Quaternion conjugate = copy().conjugate();
		return Quaternion.mult(Quaternion.mult(this, v), conjugate).getRotationVector();
    }
    
    /**
     * Compute the inverse of your quaternion q
     * @param q Quaternion to compute inverse
     * @return New instance filled by the result of the operation
     */
    public static Quaternion inverse (Quaternion q)
    {
    	float[] qv = q._coordinate;
		float norm = qv[0]*qv[0] + qv[1]*qv[1] + qv[2]*qv[2] + qv[3]*qv[3];
		if (norm > 0) {
		    float inverseNorm = 1 / norm;
		    return new Quaternion(-qv[0] * inverseNorm, -qv[1]*inverseNorm, -qv[2]*inverseNorm, qv[3]*inverseNorm);
		} else {
		    return null;
		}
    }
    
    /**
     * Compute inverse of this quaternion
     * @return This instance to chain
     */
    public Quaternion inverse()
    {
		float norm = _coordinate[0]*_coordinate[0] + _coordinate[1]*_coordinate[1] + _coordinate[2]*_coordinate[2] + _coordinate[3]*_coordinate[3];
		if (norm > 0) {
		    float inverseNorm = 1 / norm;
		    _coordinate[0] *= -inverseNorm;
		    _coordinate[1] *= -inverseNorm;
		    _coordinate[2] *= -inverseNorm;
		    _coordinate[3] *= inverseNorm;
		    return this;
		} else {
		    return null;
		}
    }
    
    public boolean equals(Object o)
    {
		if (!(o instanceof Quaternion))
		    return false;
		float[] qat = ((Quaternion)o)._coordinate;
		float xMinusX = qat[0] - _coordinate[0],
			yMinusY = qat[1] - _coordinate[1],
			zMinusZ = qat[2] - _coordinate[2],
			wMinusW = qat[3] - _coordinate[3], 
			minusEpsilon = -epsilon;
		if (xMinusX < epsilon && xMinusX > minusEpsilon &&
			yMinusY < epsilon && yMinusY > minusEpsilon &&
			zMinusZ < epsilon && zMinusZ > minusEpsilon &&
			wMinusW < epsilon && wMinusW > minusEpsilon)
		    return true;
		return false;
    }
    
    public String toString()
    {
    	return "Quaternion \n" + super.toString();
    }
}