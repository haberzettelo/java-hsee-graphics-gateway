package gl.hsee.engine.math.matrix.mvp;

import java_3d_engine.matrix.Matrix;
import java_3d_engine.matrix.MatrixOperation;

import org.apache.log4j.Logger;

public class Model extends Matrix
{
	private final static Logger logger = Logger.getLogger(Model.class);
	private int currentMatrix;
	private final static int MAX_STACK_MATRIX = 20;
	private Matrix[] matrixModel;
	
	public Model ()
	{
		super();
		matrixModel = new Matrix[MAX_STACK_MATRIX];
		for (int i = 0; i < MAX_STACK_MATRIX; i++)
			matrixModel[i] = new Matrix();
	}
	
	public void resetStack ()
	{
		currentMatrix = 0;
		matrixModel[currentMatrix].identity();
	}
	
	/**
	 * Save context
	 * Add current Model View matrix on stack
	 */
	public void push ()
	{
		if (currentMatrix < MAX_STACK_MATRIX)
		{
			matrixModel[currentMatrix + 1] = matrixModel[currentMatrix].clone();
			currentMatrix++;
		}
		else
			logger.error("Stack matrix overflow.");
	}
	
	/**
	 * Restore context
	 */
	public void pop ()
	{
		if (currentMatrix > 0)
		{
			currentMatrix--;
		}
		else
			logger.error("ViewProjectionMatrix.pop method can't load -1 id. Maybe pop method was called more than push.");
	}
	
	/**
	 * Rotate at angle theta on specified axis
	 * @param theta Angle rotate
	 * @param x float value
	 * @param y float value
	 * @param z float value
	 */
	public void rotate (float theta, float x, float y, float z)
	{
		matrixModel[currentMatrix].setValues(super.rotate(theta, x, y, z, matrixModel[currentMatrix]));
	}
	
	/**
	 * Translate at specified value 
	 * @param x Translate on x axe 
	 * @param y Translate on y axe
	 * @param z Translate on z axe
	 */
	public void translate (float x, float y, float z)
	{
		matrixModel[currentMatrix].setValues(super.translate(x, y, z, matrixModel[currentMatrix]));
	}
	
	/**
	 * Modify the scale at specified values
	 * @param x Resize on x axe
	 * @param y Resize on y axe
	 * @param z Resize on z axe
	 */
	public void scale (float x, float y, float z)
	{
		matrixModel[currentMatrix].setValues(super.scale(x, y, z, matrixModel[currentMatrix]));
	}
	
	
	/**
	 * Get ModelView matrix
	 * @return Return the last (current) ModelView matrix
	 */
	public Matrix getLastMatrix ()
	{
		return matrixModel[currentMatrix];
	}
	
	public String toString ()
	{
		return "Model matrix :\n" + MatrixOperation.matrix4x4ToString(super.getValues());
	}
}
