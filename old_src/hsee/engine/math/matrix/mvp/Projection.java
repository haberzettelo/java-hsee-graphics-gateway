package gl.hsee.engine.math.matrix.mvp;

import java_3d_engine.matrix.Matrix;
import java_3d_engine.matrix.MatrixOperation;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * This matrix represent the perspective of your scene. By default, in 
 * OpenGL, an object will appear to have the same size no matter where 
 * the camera is positioned. This is against our day to day experience,
 * where an object closer to us (to the camera) looks larger than an 
 * object that is at a greater distance. Picture the way a ball 
 * approaches you, the ball will appear bigger and bigger as it is 
 * closer to your eyes.
 * <a href="http://solarianprogrammer.com/2013/05/22/opengl-101-matrices-projection-view-model/">Source</a>
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * To use this matrix, you need to construct your matrix with 
 * Projetction() constructor, then you must use perspective() 
 * operation, orthogonal() operation or orthogonal2D()
 * operation only if you want work on 2D environment to 
 * initialize your camera.
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 13 mai 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Projection extends Matrix
{
	//private Matrix	projectionSave;

	public Projection ()
	{
		super();
	}

	/**
	 * Set the way to view the scene to perspective (most real)
	 * 
	 * @param angle
	 *            Angle display by camera
	 * @param ratio
	 *            Ratio of windows (often 4/3 ou 16/9) <br>
	 *            !! Caution, if you insert 4/3 = 1 because int/int => int, you
	 *            must cast one of value to float or double !!
	 * @param near
	 *            Value represent the part of 3D scene will be not display near
	 *            the camera
	 * @param far
	 *            Value represent the part of 3D scene will be not display far
	 *            the camera {@link source 
	 *            http://www.opengl.org/sdk/docs/man2/xhtml/gluPerspective.xml}
	 */
	public void perspective (float angle, float ratio, float near, float far)
	{
		double angleRadians = angle * Math.PI / 180;
		float f = (float) (1 / (Math.tan(angleRadians / 2)));

		this.setValues(new float[] {f / ratio, 0, 0, 0, 0, f, 0, 0, 0, 0,
				(near + far) / (near - far), (2 * near * far) / (near - far),
				0, 0, -1, 0});
		//projectionSave = new Matrix(this.getValues());
		// logger.debug("Projection matrix : " +
		// MatrixOperation.matrix4x4ToString(matrixProjection) );
	}

	/**
	 * Set the way to view the scene to orthogonal (all coordinates paralels)
	 * 
	 * @param right
	 *            Specify the coordinates for the right vertical clipping planes
	 * @param left
	 *            Specify the coordinates for the left vertical clipping planes
	 * @param top
	 *            Specify the coordinates for the top horizontal clipping planes
	 * @param bottom
	 *            Specify the coordinates for the bottom horizontal clipping
	 *            planes
	 * @param farVal
	 *            Specify the distances to the farther depth clipping planes.
	 * @param nearVal
	 *            Specify the distances to the nearer depth clipping planes.
	 *            These values (farVal and nearVal) are negative if the plane is
	 *            to be behind the viewer
	 */
	public void orthogonal (float right, float left, float top, float bottom,
			float farVal, float nearVal)
	{
		float rightLessLeft = right - left, topLessBottom = top - bottom, farValLessNearVal = farVal
				- nearVal;

		this.setValues(new float[] {2 / rightLessLeft, 0, 0,
				(-right - left) / rightLessLeft, 0, 2 / topLessBottom, 0,
				(-top - bottom) / topLessBottom, 0, 0, -2 / farValLessNearVal,
				(-farVal - nearVal) / farValLessNearVal, 0, 0, 0, 1});

		//projectionSave = new Matrix(this.getValues());
	}

	/**
	 * Set the way to view the scene to orthogonal in plane (all coordinates
	 * paralels)
	 * 
	 * @param right
	 *            Specify the coordinates for the right vertical clipping planes
	 * @param left
	 *            Specify the coordinates for the left vertical clipping planes
	 * @param top
	 *            Specify the coordinates for the top horizontal clipping planes
	 * @param bottom
	 *            Specify the coordinates for the bottom horizontal clipping
	 *            planes
	 */
	public void orthogonal2D (float right, float left, float top, float bottom)
	{
		orthogonal(right, left, top, bottom, 1, -1);
	}

	/*
	public Matrix frustrum (float left, float right, float bottom, float top,
			float near, float far)
	{
		float x, y, a, b, c, d;
		Matrix m = new Matrix();

		x = 2 * near / (right - left);
		y = 2 * near / (top - bottom);

		a = (right + left) / (right - left);
		b = (top + bottom) / (top - bottom);
		c = -(far + near) / (far - near);
		d = -2 * far * near / (far - near);

		m.setValues(new float[] {x, 0f, a, 0f, 0f, y, b, 0f, 0f, 0f, c, d, 0f,
				0f, -1f, 0f});

		return m;
	}*/

	/**
	 * Pick method permit to improve your performance if you use another
	 * Projections matrix, this operation cut your display to show only the part
	 * of the screen interesting. !! This method doesn't work, this is not yet
	 * implemented, this method do nothing for the moment !!
	 * 
	 * @param x
	 *            Position x
	 * @param y
	 *            Position y
	 * @param deltaX
	 *            Width of the part to cut
	 * @param deltaY
	 *            Height of the part to cut
	 * @param viewportWidth
	 *            Width of the original screen
	 * @param viewportHeight
	 *            Height of the original screen {@link source
	 *            http://www.opengl.org/sdk/docs/man2/xhtml/gluPickMatrix.xml}
	 */
	public void pick (float x, float y, float deltaX, float deltaY,
			float viewportWidth, float viewportHeight)
	{
		/**
		 * A lire pour implementer
		 * http://www.opengl.org/archives/resources/faq/technical/selection.htm
		 */
		/*
		if (deltaX <= 0 || deltaY <= 0)
		{
			return;
		}

		setValues(projectionSave.getValues());

		/* Translate and scale the picked region to the entire window *//*
																		System.out.println("Deplacement pick : x:"
																		+ ((viewportWidth - 2 * x) / deltaX) + ", y:"
																		+ ((viewportHeight - 2 * y) / deltaY));
																		System.out.println("viewPortWidth:" + viewportWidth + ", x:" + x
																		+ ", dX:" + deltaX);
																		// setValues(translate(viewportWidth/2 -x - (deltaX-x)/2,
																		// (viewportHeight - 2*y)/deltaY, 0, this));
																		int value = 40;
																		// setValues(translate((viewportWidth - value*(x))/deltaX,
																		// (viewportHeight - value*(y))/deltaY, 0, this));
																		setValues(translate((viewportWidth - value * x) / deltaX,
																		(viewportHeight - value * y) / deltaY, 0, this));

																		setValues(scale((viewportWidth) / deltaX, viewportHeight / deltaY, 1,
																		this));
																		/*glTranslatef((viewport[2] - 2 * (x - viewport[0])) / deltax,
																		(viewport[3] - 2 * (y - viewport[1])) / deltay, 0);*/

		// glScalef(viewport[2] / deltax, viewport[3] / deltay, 1.0);
		// this.setValues(MatrixOperation.multiply4x4(projectionSave,
		// this.getValues(), result));
		/*m = identity;
		m[0,0] = vw / (r-l);
		m[1,1] = vh / (b-t);
		m[3,0] = (vw-l-r) / (r-l);
		m[3,1] = (vh-b-t) / (t-b); */
	}

	public String toString ()
	{
		return "Projection matrix :\n"
				+ MatrixOperation.matrix4x4ToString(super.getValues());
	}
}
