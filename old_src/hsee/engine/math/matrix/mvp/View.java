package gl.hsee.engine.math.matrix.mvp;

import java_3d_engine.matrix.Matrix;
import java_3d_engine.matrix.MatrixOperation;

public class View extends Matrix
{
	public View ()
	{
		super();
	}
	
	/**
	 * Initialize camera
	 * @param eyeX Position of camera on x axis
	 * @param eyeY Position of camera on y axis
	 * @param eyeZ Position of camera on z axis
	 * @param centerX Point look by camera
	 * @param centerY Point look by camera
	 * @param centerZ Point look by camera
	 * @param upX Vertical axis for camera
	 * @param upY Vertical axis for camera
	 * @param upZ Vertical axis for camera
	 */
	public void lookAt (float eyeX, float eyeY, float eyeZ, float centerX, float centerY, float centerZ, float upX, float upY, float upZ)
	{
		float[] regard = new float[3], normal = new float[3], newAxe = new float[3];
		//Calcul de regard : center - eye
		regard = new float[]{centerX - eyeX, centerY - eyeY, centerZ - eyeZ};
		//Calcul de normal : regard /\ up
		MatrixOperation.normaliserVec3(regard);
		MatrixOperation.vectorialProductVec3(regard, new float[] {upX ,upY, upZ}, normal);
		//Calcul de newAxe : normal /\ regard
		MatrixOperation.normaliserVec3(normal);
		MatrixOperation.vectorialProductVec3(normal, regard, newAxe);
		
		
		//Met à jour la matrice
		this.setValues(new float[] {normal[0], 	normal[1], 	normal[2], 	0,
									newAxe[0], 	newAxe[1], 	newAxe[2], 	0,
									-regard[0], -regard[1], -regard[2], 0,
									0, 				0, 				0, 	1});
		
		//matrixModel[currentMatrix] = MatrixOperation.multiply4x4(matrixCamera, matrixModel[currentMatrix]);
		translate(-eyeX, -eyeY, -eyeZ);//this.setValues(MatrixOperation.multiply4x4(this.getValues(), new float[]{1,0,0,-eyeX,  0,1,0,-eyeY,  0,0,1,-eyeZ,  0,0,0,1})); //translateView(-eyeX, -eyeY, -eyeZ);
		//logger.debug("View matrix : " + MatrixOperation.matrix4x4ToString(matrixView));
	}
	
	/**
	 * Rotate at angle theta on specified axis
	 * @param theta Angle rotate
	 * @param x float value in radian
	 * @param y float value in radian
	 * @param z float value in radian
	 */
	public void rotate (float theta, float x, float y, float z)
	{
		this.setValues(this.rotate(theta, x, y, z, this));
	}
	
	/**
	 * Translate at specified value 
	 * @param x Translate on x axe 
	 * @param y Translate on y axe
	 * @param z Translate on z axe
	 */
	public void translate (float x, float y, float z)
	{
		setValues(translate(x, y, z, this));
	}
	
	/**
	 * Modify the scale at specified values
	 * @param x Resize on x axe
	 * @param y Resize on y axe
	 * @param z Resize on z axe
	 */
	public void scale (float x, float y, float z)
	{
		this.setValues(this.scale(x, y, z, this));
	}
	
	public String toString ()
	{
		return "View matrix :\n" + MatrixOperation.matrix4x4ToString(super.getValues());
	}
	

}
