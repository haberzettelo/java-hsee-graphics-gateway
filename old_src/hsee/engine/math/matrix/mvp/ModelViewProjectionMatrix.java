package gl.hsee.engine.math.matrix.mvp;

import java_3d_engine.event.IObserver;
import gl.hsee.engine.math.matrix.Matrix;
import gl.hsee.engine.math.matrix.MatrixOperation;
import gl.hsee.engine.shader.Shader;

public class ModelViewProjectionMatrix extends Matrix implements IObserver<DataTransform> {
    //private Logger logger = Logger.getLogger(ModelViewProjectionMatrix.class);

    private Projection matrixProjection;
    private View matrixView;
    private Model matrixModel;

    public ModelViewProjectionMatrix() {
        matrixProjection = new Projection();
        matrixView = new View();
        matrixModel = new Model();
    }

    /**
     * Clone la matrice de projection et garde les adresses des autres matrices
     *
     * @param mvp
     */
    public ModelViewProjectionMatrix(ModelViewProjectionMatrix mvp) {
        matrixProjection = new Projection();
        matrixView = mvp.matrixView;
        matrixModel = mvp.matrixModel;
    }

    /**
     * Load identity matrixModel. Call this method at the begin of your infinite
     * while
     */
    public void loadIdentity() {
        //Load Identity matrix
        matrixModel.resetStack();
        //matrixProjection.identity();
        matrixView.identity();
    }
    /**
     * Send matrix ModelViewProjection at specified shader
     *
     * @param shader Shader which will gone ModelViewProjection matrix
     * @throws Exception If ModelViewMatrix isn't identified in Shader
     */
    long time = 0;

    /**
     *
     * @param shader
     * @throws Exception if ModelViewMatrix wasn't been found in shader
     */
    public void sendMVPMatrix(Shader shader) throws Exception {
        float[] matrix = new float[16];
        MatrixOperation.multiply4x4(matrixProjection.getValues(), matrixView.getValues(), matrixModel.getLastMatrix().getValues(), matrix);
        shader.sendModelViewMatrix(matrix, true);
    }

    /**
     *
     * @param shader
     * @throws Exception if ModelViewMatrix wasn't been found in shader
     */
    public void sendMVPMatrix(Shader shader, Model matrixModel) throws Exception {
        float[] matrix = new float[16];
        MatrixOperation.multiply4x4(matrixProjection.getValues(), matrixView.getValues(), matrixModel.getLastMatrix().getValues(), matrix);
        shader.sendModelViewMatrix(matrix, true);
    }

    /**
     *
     * @param shader
     * @throws Exception if ModelViewMatrix wasn't been found in shader
     */
    public void sendMVPMatrix(Shader shader, View matrixView, Model matrixModel) throws Exception {
        float[] matrix = new float[16];
        MatrixOperation.multiply4x4(matrixProjection.getValues(), matrixView.getValues(), matrixModel.getLastMatrix().getValues(), matrix);
        shader.sendModelViewMatrix(matrix, true);
    }

    public View getView() {
        return matrixView;
    }

    public Projection getProjection() {
        return matrixProjection;
    }

    public Model getModel() {
        return matrixModel;
    }

    @Override
    public void update(DataTransform data) {

        if (data.isViewMatrixModified()) {
            updateMatrixModelView(matrixView, data.getTypeTransform(), data.getData());
        } else {
            updateMatrixModelView(matrixModel, data.getTypeTransform(), data.getData());
        }
    }

    private void updateMatrixModelView(Model matrix, byte typeTransform, float[] data) {
        switch (typeTransform) {
            case DataTransform.ROTATE:
                if (data[0] != 0) {
                    matrix.rotate(data[0], 1, 0, 0);
                }
                if (data[1] != 0) {
                    matrix.rotate(data[1], 0, 1, 0);
                }
                if (data[2] != 0) {
                    matrix.rotate(data[2], 0, 0, 1);
                }
                break;
            case DataTransform.TRANSLATE:
                matrix.translate(data[0], data[1], data[2]);
                break;
            case DataTransform.SCALE:
                matrix.scale(data[0], data[1], data[2]);
                break;
        }
    }

    private void updateMatrixModelView(View matrix, byte typeTransform, float[] data) {
        switch (typeTransform) {
            case DataTransform.ROTATE:
                if (data[0] != 0) {
                    matrix.rotate(data[0], 1, 0, 0);
                }
                if (data[1] != 0) {
                    matrix.rotate(data[1], 0, 1, 0);
                }
                if (data[2] != 0) {
                    matrix.rotate(data[2], 0, 0, 1);
                }
                break;
            case DataTransform.TRANSLATE:
                matrix.translate(data[0], data[1], data[2]);
                break;
            case DataTransform.SCALE:
                matrix.scale(data[0], data[1], data[2]);
                break;
        }
    }
    
	public String toString () {
		float[] matrix = new float[16];
        MatrixOperation.multiply4x4(matrixProjection.getValues(), matrixView.getValues(), matrixModel.getLastMatrix().getValues(), matrix);
        Matrix result = new Matrix(matrix);
		return matrixProjection.toString() + "\n" + matrixView + "\n" + matrixModel + "\n" + result;
	}
}
