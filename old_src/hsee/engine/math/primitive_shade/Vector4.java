package gl.hsee.engine.math.primitive_shade;

public class Vector4 {
	protected float[] _coordinate;
	
	/**
	 * Contruct a vector, (0, 0, 0, 0)
	 */
	public Vector4 ()
	{
		_coordinate = new float[4];
		_coordinate[0] = 0;
		_coordinate[1] = 0;
		_coordinate[2] = 0;
		_coordinate[3] = 0;
	}
	
	/**
	 * Construct a vector (x, y, z, w)
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param z Z coordinate
	 * @param w W coordinate
	 */
	public Vector4 (float x, float y, float z, float w)
	{
		_coordinate = new float[4];
		_coordinate[0] = x;
		_coordinate[1] = y;
		_coordinate[2] = z;
		_coordinate[3] = w;
	}

	/**
	 * Construct a vector by copy of you specified vector
	 * @param vec
	 */
	public Vector4 (Vector4 vec)
	{
		_coordinate = new float[4];
		_coordinate[0] = vec._coordinate[0];
		_coordinate[1] = vec._coordinate[1];
		_coordinate[2] = vec._coordinate[2];
		_coordinate[3] = vec._coordinate[3];
	}

	/**
	 * Return the coordinates of vector
	 * @return
	 */
	public float[] getCoordinates ()
	{
		return _coordinate;
	}
	
	/**
	 * Return the x coordinate
	 * @return
	 */
	public float getX()
	{
		return _coordinate[0];
	}
	
	/**
	 * Return the y coordinate
	 * @return
	 */
	public float getY()
	{
		return _coordinate[1];
	}
	
	/**
	 * Return the z coordinate
	 * @return
	 */
	public float getZ()
	{
		return _coordinate[2];
	}
	
	/**
	 * Return the w coordinate
	 * @return
	 */
	public float getW()
	{
		return _coordinate[3];
	}
	
	public String toString ()
	{
		return "Vector4 (" + _coordinate[0] + ", " + _coordinate[1] + ", " + _coordinate[2] + ", " + _coordinate[3] + ")";
	}
	
	public static boolean equals(Vector4 vec1, Vector4 vec2)
	{
		return Vector4.equals(vec1, vec2, 0.0001f);
	}
	
	public static boolean equals(Vector4 vec1, Vector4 vec2, float delta)
	{
		return (Math.abs(vec1.getX() - vec2.getX()) <= delta) &&
				(Math.abs(vec1.getY() - vec2.getY()) <= delta) &&
				(Math.abs(vec1.getZ() - vec2.getZ()) <= delta) &&
				(Math.abs(vec1.getW() - vec2.getW()) <= delta);
	}
}
