package gl.hsee.engine.math.primitive_shade;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Reprensent 3 coordinates on space, (x, y, z)
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 11 sept. 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Vector3
{
	private float[] _coordinate;
	
	/**
	 * Contruct a vector, (0, 0, 0)
	 */
	public Vector3 ()
	{
		_coordinate = new float[3];
		_coordinate[0] = 0;
		_coordinate[1] = 0;
		_coordinate[2] = 0;
	}
	
	/**
	 * Construct a vector (x, y, z)
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param z Z coordinate
	 */
	public Vector3 (float x, float y, float z)
	{
		_coordinate = new float[3];
		_coordinate[0] = x;
		_coordinate[1] = y;
		_coordinate[2] = z;
	}

	/**
	 * Construct a vector by copy of you specified vector
	 * @param vec
	 */
	public Vector3 (Vector3 vec)
	{
		_coordinate = new float[3];
		_coordinate[0] = vec.getX();
		_coordinate[1] = vec.getY();
		_coordinate[2] = vec.getZ();
	}

	/**
	 * Return the coordinates of vector
	 * @return
	 */
	public float[] getCoordinates ()
	{
		return _coordinate;
	}
	
	/**
	 * Return the x coordinate
	 * @return
	 */
	public float getX()
	{
		return _coordinate[0];
	}
	
	/**
	 * Return the y coordinate
	 * @return
	 */
	public float getY()
	{
		return _coordinate[1];
	}
	
	/**
	 * Return the z coordinate
	 * @return
	 */
	public float getZ()
	{
		return _coordinate[2];
	}
	
	public String toString ()
	{
		return "Vector3 (" + _coordinate[0] + ", " + _coordinate[1] + ", " + _coordinate[2] + ")";
	}
	
	public static boolean equals(Vector3 vec1, Vector3 vec2)
	{
		return Vector3.equals(vec1, vec2, 0.0001f);
	}
	
	public static boolean equals(Vector3 vec1, Vector3 vec2, float delta)
	{
		return (Math.abs(vec1.getX() - vec2.getX()) <= delta) &&
				(Math.abs(vec1.getY() - vec2.getY()) <= delta) &&
				(Math.abs(vec1.getZ() - vec2.getZ()) <= delta);
	}
}
