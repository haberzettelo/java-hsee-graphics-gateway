package gl.hsee.ext.importobj;

/**
 * 
 * @author Olivier
 *
 */
class FaceTriangle extends Face
{
	public FaceTriangle (int[] p1, int[] p2, int[] p3)
	{
		super (p1, p2, p3);
	}
	
	public FaceTriangle (int[][] points)
	{
		super (points);
	}
}