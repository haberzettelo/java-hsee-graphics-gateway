package gl.hsee.ext.importobj;

import java.io.BufferedReader;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Vector;

/**
 * 
 * @author Olivier
 *
 */
public class ResultObjMtlFileIndexed 
{
	public FloatBuffer[] vertex;
	public FloatBuffer[] normal;
	public FloatBuffer[] texture;
	//public Face[][] face;
	
	public int nbFaces;
	public int[] nbTriangle;
	public int[] nbQuad;
	public Vector<Integer> nbFace;
	public BufferedReader inputFileObj;
	
	public String[] materialGroup;
	public String fileMTLName;
	
	/*
	 * Indices of each Vertex, Normal and CoordTextures grouped by material
	 */
	public IntBuffer[] bufOrderedQuads;
	public IntBuffer[] bufOrderedTriangles;
	public IntBuffer[][] face;
	
	public boolean fileMTLExist;
	
	public ResultObjMtlFileIndexed() {
		// TODO Auto-generated constructor stub
	}

}
