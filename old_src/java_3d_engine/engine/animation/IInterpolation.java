package java_3d_engine.engine.animation;

public interface IInterpolation 
{
	/**
	 * Interpole une courbe temporelle avec les points passés en paramètres.
	 * @param points Sous la forme [t1,  t2,   ...,   tf]. 
	 * 				 Au premier point, t doit être égal à 0. Les autres points doivent
	 * 				 êtres rangés dans l'ordre croissant suivant la coordonnée t.
	 */
    public void interpolation(float[] points);
    
    /**
     * Use only after interpolation() method
     * @return All points interpolated follow method you have used
     */
    public float[] getInterpolation();
    
    /**
     * Use only after interpolation() method
     * @return Followed point interpolated follow method you have used
     */
    public float getFollowPoint ();
    
    public int getFpsSet ();
}
