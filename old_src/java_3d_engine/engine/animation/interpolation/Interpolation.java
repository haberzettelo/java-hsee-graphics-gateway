package java_3d_engine.engine.animation.interpolation;

import java_3d_engine.engine.animation.IInterpolation;

import org.apache.log4j.Logger;

abstract class Interpolation implements IInterpolation
{
	private static final Logger logger = Logger.getLogger(Interpolation.class); 
	private int _follow = 0;
	protected float[] _interpolation;
	protected int _offsetInterpolation = 0;
	protected int _fps = 60;
	
	/**
	 * 
	 * @param points
	 */
	public void interpolation(float[] points) 
	{
		/**
		 * Le but est de créer des points d'interpolations correspondant
		 * à la fréquence d'affichage.*/
		
		// On initialise d'abord le tableau d'interpolation
		int lengthArray;
		lengthArray = (int)(_fps * points[points.length - 1] + 1);
		_interpolation = new float[lengthArray];
		int count = 0;
		int max = points.length - 1;
		while (count < max)
		{
			interpolationBetweenTwoPoints(points, count);
			count++;
		}
	}
	
	void interpolationBetweenTwoPoints(float[] points, int count) {
		logger.error("interpolationBetweenTwoPoints method is not override.");
	}

	public float[] getInterpolation() 
	{
		return _interpolation;
	}
	
	public float getFollowPoint ()
	{
		return _interpolation[_follow++];
	}
	
	public int getFpsSet ()
	{
		return _fps;
	}
}
