package java_3d_engine.engine.animation.interpolation;

import java_3d_engine.engine.animation.IInterpolation;

import org.apache.log4j.Logger;


/**
 * This annimation is very basic, one degree on polynomial equation.
 * @author Olivier
 *
 */
public class Linear extends Interpolation implements IInterpolation
{
	private static final Logger logger = Logger.getLogger(Linear.class); 
	/**
	 * Create a basic animation
	 * @param points
	 */
	public Linear (float[] points)
	{
		interpolation(points);
	}
	
	void interpolationBetweenTwoPoints (float[] points, int offset)
	{
		float tempValue = points[offset];
		float pointDebut = points[offset];
		int compteur = 0;
		
		
		while (tempValue != points[offset + 1])
		{
			compteur++;
			_interpolation[_offsetInterpolation] = tempValue;
			_offsetInterpolation++;
			tempValue = pointDebut + compteur/_fps;
		}
		logger.debug("method is override. Great !! Interpolation sucess.");
	}
}
