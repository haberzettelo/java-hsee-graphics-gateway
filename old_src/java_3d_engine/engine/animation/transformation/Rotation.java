package java_3d_engine.engine.animation.transformation;

import java_3d_engine.engine.animation.IAnimation;
import java_3d_engine.engine.animation.IInterpolation;

public class Rotation extends Animation implements IAnimation
{
	public Rotation (IInterpolation inter)
	{
		_curve = inter.getInterpolation();
	}
	
    /**
     * 
     * Exemple: Roue d'une voiture
     * @param vitesseAngulaire 
     */
    public void rotation(float[] centre, float vitesseAngulaire)
    {
    }
}
