package java_3d_engine.engine.animation.transformation;

import java_3d_engine.engine.animation.IAnimation;
import java_3d_engine.engine.animation.IInterpolation;
import java_3d_engine.engine.light.Sprite3D;

import org.apache.log4j.Logger;

/**
 * This abstract class serves to overwrite annimate method and declare curve member.
 * @author Olivier
 *
 */
abstract class Animation implements IAnimation
{
	private final static Logger logger = Logger.getLogger(Animation.class);
	protected float[] _curve;
	protected Sprite3D _objectToMove;
	
	public void createAnimation (IInterpolation inter, float[] points)
	{
		/**
		 * Le but est de créer des points d'interpolations correspondant
		 * à la fréquence d'affichage.*/
		
		// On initialise d'abord le tableau d'interpolation
		int lengthArray;
		lengthArray = (int)(inter.getFpsSet() * points[points.length - 1] + 1);
		_curve = new float[lengthArray];
		int count = 0;
		int max = _curve.length - 3;
		while (count < max)
		{
			animationBetweenTwoPoints(points, count);
			count += 3;
		}
	}
	
	public void annimate(int FPS) 
	{
		new Thread(new Runnable() 
		{
			public void run() 
			{
				/**
				 * Animation here ! with while or for loop
				 */
			}
		}).start();
	}

	public void annimate() 
	{
		new Thread(new Runnable() 
		{
			public void run() 
			{
				/**
				 * Animation here ! with while or for loop
				 */
			}
		}).start();
	}
	
	protected void animationBetweenTwoPoints (float[] points, int offset)
	{
		logger.error("animationBetweenTwoPoints method is not override.");
	}
}
