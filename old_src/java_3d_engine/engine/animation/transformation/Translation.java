package java_3d_engine.engine.animation.transformation;

import java_3d_engine.engine.animation.IAnimation;
import java_3d_engine.engine.animation.IInterpolation;

import org.apache.log4j.Logger;

public class Translation extends Animation implements IAnimation
{
	private static Logger logger = Logger.getLogger(Translation.class);
	
	public Translation (IInterpolation inter, float[] points)
	{
		createAnimation(inter, points);
	}
	
	protected void animationBetweenTwoPoints (float[] points, int offset)
	{
		float tempValue = points[offset];
		float pointDebut = points[offset];
		int compteur = 0;
		
		
		while (tempValue != points[offset + 1])
		{
			compteur++;
			//_interpolation[_offsetInterpolation] = tempValue;
			//_offsetInterpolation++;
			//tempValue = pointDebut + compteur/_fps;
		}
		logger.debug("method is override. Great !! Animation initialization sucess.");
	}
}
