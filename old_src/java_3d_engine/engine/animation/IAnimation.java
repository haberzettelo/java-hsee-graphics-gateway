package java_3d_engine.engine.animation;

/**
 * Interface annimation
 * 
 * How to use (fr) :
 * 	Pour créer une animation, vous devez d'abord créer votre courbe d'interpolation à l'aide du package engine.animation.interpolation
 * 	et des méthodes interpolation(...).
 * 	Ensuite, vous devrez séléctionner un type de transformation avec le package engine.animation.transformation et appliquer à cette 
 * 	transformation l'interpolation précédente.
 * 
 * 	Pour lancer votre animation, utilisez la méthode animate([...]).
 * 
 * 	Votre animation se lance dans un nouveau thread et se termine au dernier point d'interpolation.
 * 
 *
 * @author Olivier
 *
 */
public interface IAnimation {
    /**
     * Lance l'animation configurée à la vitesse de "FPS" (Frame per Seconds) dans un nouveau thread
     * @param FPS 
     */
    public void annimate(int FPS);
    
    /**
     * Lance l'animation configurée dans un nouveau thread
     */
    public void annimate();    
}
