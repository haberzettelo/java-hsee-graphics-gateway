package java_3d_engine.engine.light;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IDrawable;
import java_3d_engine.interfaces.IDrawableBounding;
import java_3d_engine.matrix.mvp.Model;
import java_3d_engine.matrix.mvp.ModelViewProjectionMatrix;
import java_3d_engine.models.object.ObjectModel;
import java_3d_engine.shader.Shader;
import echiquier.Piece;
import main.GLException;

/**
 * Sprite3D class represent a concrete object. That is an object to draw with
 * its position in space and with shader to define the way of draw this Sprite3D
 *
 * @author Olivier
 *
 */
public class Sprite3D implements IDrawable, IDrawableBounding {

    private ObjectModel obj;
    private Model model;
    private Shader shader, shaderBounding = null;
    private float[] colorBounding = null;
    private float[] colorBoundingRecup = null;
    private ModelViewProjectionMatrix mvp;
    private ModelViewProjectionMatrix mvpBounding;
    private Piece pieceAssociee;

    /**
     * Create an object Sprite 3D
     *
     * @param objectLoaded Must be loaded with ObjectModel.createXXX() method
     * @param initalPosition Initial position in the 3D world
     * @param shader Associated shader
     * @throws Exception Throw Exception if objectLoaded is null
     */
    public Sprite3D(ObjectModel objectLoaded, Model initalPosition, Shader shader, ModelViewProjectionMatrix mvp) throws Exception {
        if (objectLoaded != null) {
            obj = objectLoaded;
            model = initalPosition;
            this.shader = shader;
            this.mvp = mvp;
        } else {
            throw new Exception("Your ObjectModel isn't loaded.");
        }
    }

    public void setBoundingShader(Shader bounding, int[] colorBounding, ModelViewProjectionMatrix mvp) {
        shaderBounding = bounding;
        this.colorBounding = new float[3];
        this.colorBoundingRecup = new float[3];
        this.colorBounding[0] = colorBounding[0] / 255f;
        this.colorBounding[1] = colorBounding[1] / 255f;
        this.colorBounding[2] = colorBounding[2] / 255f;
        mvpBounding = mvp;
    }
    
    public void setCliquable (boolean cliquable)
    {
    	if (cliquable)
    	{
    		this.colorBoundingRecup[0] = 0;
	        this.colorBoundingRecup[1] = 0;
	        this.colorBoundingRecup[2] = 0;
	    	this.colorBounding[0] = colorBounding[0] / 255f;
	        this.colorBounding[1] = colorBounding[1] / 255f;
	        this.colorBounding[2] = colorBounding[2] / 255f;
    	}
    	else
    	{
	    	this.colorBoundingRecup[0] = colorBounding[0] / 255f;
	        this.colorBoundingRecup[1] = colorBounding[1] / 255f;
	        this.colorBoundingRecup[2] = colorBounding[2] / 255f;
	    	this.colorBounding[0] = 0;
	        this.colorBounding[1] = 0;
	        this.colorBounding[2] = 0;
    	}
    }

    public void setNewShader(Shader newShader) {
        shader = newShader;
    }

    public void setPieceAssociee(Piece piece) {
        pieceAssociee = piece;
    }

    public Piece getPieceAssociee() {
        return pieceAssociee;
    }

    public void setModel(Model newModel) {
        model = newModel;
    }

    public Model getModel() {
        return model;
    }

    public void draw(GraphicLibrary gl, int[] indexShader) {
        try {
            shader.useProgram();
        } catch (GLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            mvp.sendMVPMatrix(shader, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        obj.draw(gl, indexShader);
    }

    public void drawBounding(GraphicLibrary gl, int[] indexShader) {
        if (shaderBounding != null) {
            try {
                shaderBounding.useProgram();
            } catch (GLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            gl.getGLx().glUniform3f(shaderBounding.getIndexUniform()[1], colorBounding[0], colorBounding[1], colorBounding[2]);
            try {
                mvpBounding.sendMVPMatrix(shaderBounding, model);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            obj.draw(gl, shaderBounding.getIndexAttributes());
        }
    }
}