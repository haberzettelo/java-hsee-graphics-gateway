package java_3d_engine.engine.light;

import java.nio.FloatBuffer;

import java_3d_engine.shader.Membres;
import java_3d_engine.shader.Shader;

import org.apache.log4j.Logger;



/**
 * Spot light represent a positional and directional light with a light cone.
 * This type of light can be every light on this world.
 * @author Olivier
 *
 */
public class Spot extends Light
{
	private final static Logger logger = Logger.getLogger(Spot.class);
	private float[] _position;
	private float[] _direction;
	private float _cutOff;
	private int _indexStructure;
	
	private FloatBuffer buffer1 = FloatBuffer.allocate(3), 
						buffer2 = FloatBuffer.allocate(3), 
						buffer3 = FloatBuffer.allocate(3),
						buffer4 = FloatBuffer.allocate(1);
	
	/**
	 * Initialize a spot light with this properties
	 * @param Color RGB color
	 * @param Position (x, y, z) position
	 * @param Direction (x, y, z) direction
	 * @param CutOff Angle of light cone (in degrees)
	 */
	public Spot (float[] Color, float[] Position, float[] Direction, float CutOff)
	{
		super(Color);
		_position = Position;
		_direction = Direction;
		_cutOff = CutOff;
	}
	
	public void setIdLight(int index)
	{
		_indexStructure = index;
	}
	
	public void sendLight (Shader shader)
	{
		try {
			shader.useProgram();
		} catch (main.GLException e1) {
			// TODO Auto-generated catch block
			e1.displayGLException();
			e1.printStackTrace();
		}
		Membres struct = shader.getUniformStructure(_indexStructure);
		int[] index = struct.getIndexAttributes();
		
		if (index.length > 0)
		{
			buffer1.put(_color);
			buffer1.rewind();
			shader.uniformFloat(buffer1, index[0], Shader.dataVec3, false);
			if (index.length > 1)
			{
				buffer2.put(_position);
				buffer2.rewind();
				shader.uniformFloat(buffer2, index[1], Shader.dataVec3, false);
				if (index.length > 2)
				{
					buffer3.put(_direction);
					buffer3.rewind();
					shader.uniformFloat(buffer3, index[2], Shader.dataVec3, false);
					if (index.length > 3)
					{
						buffer4.put(_cutOff);
						buffer4.rewind();
						shader.uniformFloat(buffer4, index[3], Shader.dataFloat, false);
						logger.debug("cufOff send.");
					}
				}
			}
		}
	}
	
	public String toString ()
	{
		return "SpotLight : \n\t"
							+ "color = \t(" + _color[0] + ", " + _color[1] + ", " + _color[2] + ")\n\t"
							+ "position =\t(" + _position[0] + ", " + _position[1] + ", " + _position[2] + ")\n\t"
							+ "direction =\t(" + _direction[0] + ", " + _direction[1] + ", " + _direction[2] + ")\n\t"
							+ "cutOff =\t" + _cutOff;
	}
}
