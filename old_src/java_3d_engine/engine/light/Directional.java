package java_3d_engine.engine.light;

import java.nio.FloatBuffer;

import java_3d_engine.shader.Membres;
import java_3d_engine.shader.Shader;

import org.apache.log4j.Logger;

/**
 * Directional light represent light with color and direction attribute. This light send all of its ray in the same direction. 
 * An example of this type is the sun light on the earth. It is too far to consider this ray not in same direction.
 * @author Olivier
 *
 */
public class Directional extends Light
{

	private final static Logger logger = Logger.getLogger(Directional.class);
	private float[] _direction;
	private int _indexStructure;
	
	private FloatBuffer buffer1 = FloatBuffer.allocate(3), 
						buffer2 = FloatBuffer.allocate(3);
	
	/**
	 * Initialize a spot light with this properties
	 * @param Color RGB color
	 * @param Position (x, y, z) position
	 * @param Direction (x, y, z) direction
	 * @param CutOff Angle of light cone (in degrees)
	 */
	public Directional (float[] Color, float[] Direction)
	{
		super(Color);
		_direction = Direction;
	}
	
	public void setIdLight(int index)
	{
		_indexStructure = index;
	}
	
	public void sendLight (Shader shader)
	{
		try {
			shader.useProgram();
		} catch (main.GLException e1) {
			// TODO Auto-generated catch block
			e1.displayGLException();
			e1.printStackTrace();
		}
		Membres struct = shader.getUniformStructure(_indexStructure);
		int[] index = struct.getIndexAttributes();
		
		if (index.length > 0)
		{
			buffer1.put(_color);
			buffer1.rewind();
			shader.uniformFloat(buffer1, index[0], Shader.dataVec3, false);
			if (index.length > 1)
			{
				buffer2.put(_direction);
				buffer2.rewind();
				shader.uniformFloat(buffer2, index[1], Shader.dataVec3, false);
			}
		}
	}
	
	public String toString ()
	{
		return "DirectionalLight : \n\t"
							+ "color = \t(" + _color[0] + ", " + _color[1] + ", " + _color[2] + ")\n\t"
							+ "direction =\t(" + _direction[0] + ", " + _direction[1] + ", " + _direction[2] + ")\n\t";
	}
}
