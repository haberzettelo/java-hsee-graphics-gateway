package java_3d_engine.engine.light;

import java.nio.FloatBuffer;

import java_3d_engine.shader.Membres;
import java_3d_engine.shader.Shader;

import org.apache.log4j.Logger;

/**
 * Punctual light represent a light with color and position attribute. This
 * light send ray in all direction at specified point. An example of this light
 * is the sun light on every planets.
 *
 * @author Olivier
 *
 */
public class Punctual extends Light {

    private final static Logger logger = Logger.getLogger(Punctual.class);
    private float[] _position;
    private int _indexStructure;
    private FloatBuffer buffer1 = FloatBuffer.allocate(3),
            buffer2 = FloatBuffer.allocate(3);

    /**
     * Initialize a spot light with this properties
     *
     * @param Color RGB color
     * @param Position (x, y, z) position
     */
    public Punctual(float[] Color, float[] Position) {
        super(Color);
        _position = Position;
    }

    public void setIdLight(int index) {
        _indexStructure = index;
    }

    public void sendLight(Shader shader) {
        try {
            shader.useProgram();
        } catch (main.GLException e1) {
            // TODO Auto-generated catch block
            e1.displayGLException();
            logger.debug(e1);
        }
        Membres struct = shader.getUniformStructure(_indexStructure);
        int[] index = struct.getIndexAttributes();

        if (index.length > 0) {
            buffer1.put(_color);
            buffer1.rewind();
            shader.uniformFloat(buffer1, index[0], Shader.dataVec3, false);
            if (index.length > 1) {
                buffer2.put(_position);
                buffer2.rewind();
                shader.uniformFloat(buffer2, index[1], Shader.dataVec3, false);
            }
        }
    }

    public String toString() {
        return "SpotLight : \n\t"
                + "color = \t(" + _color[0] + ", " + _color[1] + ", " + _color[2] + ")\n\t"
                + "position =\t(" + _position[0] + ", " + _position[1] + ", " + _position[2] + ")\n\t";
    }
}
