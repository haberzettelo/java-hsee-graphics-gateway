package java_3d_engine.engine.light;

/**
 * The light class represent a light with few attributes (only color for this abstract class)
 * 
 * To use a light, create your light with Spot, Directional or Positional class. 
 * When you've created your light with constructor, use setIdLight() to define the index of your 
 * structure variable (0 if it's the first structure variable, 1 if it's second, ...).
 * 
 * When your light is ready, use sendLight method to send your light at your shader.
 * 
 * If you modify your light, you should re-use sendLight method to update values in your shader 
 *  
 * @author Olivier
 *
 */
public abstract class Light 
{
	protected float[] _color; 
	
	/**
	 * Create very simple light 
	 * @param Color vec3 in RGB
	 */
	protected Light(float[] Color) 
	{
		_color = Color;
	}
	
	public String toString ()
	{
		return "Light : color(" + _color[0] + ", " + _color[1] + ", " + _color[2] + ")";
	}
}
