package java_3d_engine.util;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.matrix.mvp.Model;
import java_3d_engine.models.object.IntegerByReference;

import javax.media.opengl.GL;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.sun.opengl.util.BufferUtil;

import echiquier.Piece;

/**
 * Contient un ensemble de fonctions "utilitaire". C'est à dire des fonctions 
 * qui servent dans l'ensemble du programme.
 * @author Olivier HABERZETTEL, Jean-Clément NIGUES
 */
public class Util {

    // Pattern Singleton
    private static Util instance = null;
    
    /**
     * Logger qui sert à debugger l'application.
     */
    public final static Logger logger = Logger.getLogger(Util.class);

    public static Util getInstance() {
        if (instance == null) {
            instance = new Util();
        }
        return instance;
    }

    private Util() {
    }

    /**
     * Conversion d'une couleur hexadecimal en couleur OpenGL\n Par défaut,
     * alpha vaut 1 (opaque). (soit 00 en hexadecimal)
     *
     * @param hex nombre hexadecimal (ex: 0xFFFFFF ou 0xFFFFFF15) avec la valeur
     * Alpha.
     * @return un tableau de float de taille 4 qui contient les valeurs RGBA de
     * la couleur.
     */
    public float[] hexToGLColor(int hex) {
        float[] color = new float[4];

        color[0] = ((hex >> 16) & 0xFF) / 255.0f; // Red
        color[1] = ((hex >> 8) & 0xFF) / 255.0f; // Green
        color[2] = ((hex >> 0) & 0xFF) / 255.0f; // Blue !
        color[3] = 1.0f; // Alpha

        logger.debug("Conversation hex=>openGL : " + hex + " => " + color[0] + "/" + color[1] + "/" + color[2] + "/" + color[3]);

        return color;
    }

    /**
     *
     * @param gl
     * @param type example : IGL.GL_ELEMENT_ARRAY_BUFFER or IGL.GL_ARRAY_BUFFER
     * @param nbDisplay nb of element to be displayed
     * @param nbElemtsByLine nb of element by line
     */
    @SuppressWarnings("unused")
    public void displayElementsOfVBO(GraphicLibrary gl, int type, int nbDisplay, int nbElemtsByLine) {
        if (type != GL.GL_ELEMENT_ARRAY_BUFFER) {
            FloatBuffer a = Buffers.newDirectFloatBuffer(nbDisplay);

            gl.getGLx().glGetBufferSubData(type, 0, nbDisplay * BufferUtil.SIZEOF_FLOAT, a);


            if (a.capacity() > 0) {
                for (int i = 0; i < nbDisplay; i++) {
                    if (i % nbElemtsByLine == 0) {
                        System.out.print("\r	" + a.get());
                    } else {
                        System.out.print(", 	" + a.get());
                    }
                }
            }
        } else {
            IntBuffer a = Buffers.newDirectIntBuffer(nbDisplay);

            gl.getGLx().glGetBufferSubData(type, 0, nbDisplay * BufferUtil.SIZEOF_FLOAT, a);


            if (a.capacity() > 0) {
                for (int i = 0; i < nbDisplay; i++) {
                    if (i % nbElemtsByLine == 0) {
                        System.out.print("\r	" + a.get());
                    } else {
                        System.out.print(", 	" + a.get());
                    }
                }
            }
        }
        System.out.println();
    }

    public float max(float f1, float f2) {
        if (f1 > f2) {
            return f1;
        }
        return f2;
    }

    /**
     * Return the same array with 0 values at the end of the array
     *
     * @param obj Your array to be increase
     * @param newLength The new size
     * @post If the new length is < at obj.length, obj won't been modify
     */
    public Object[] increaseSize(Object[] obj, int newLength) {
        if (newLength > obj.length) {
            Object[] arrayIncreased = new IntegerByReference[newLength];
            for (int y = 0, maxY = obj.length; y < maxY; y++) {
                arrayIncreased[y] = obj[y];
            }
            for (int y = obj.length, maxY = newLength; y < maxY; y++) {
                arrayIncreased[y] = null;
            }
            return arrayIncreased;
        }
        return obj;
    }

    /**
     * Return the same array with 0 values at the end of the array
     *
     * @param obj Your array to be increase
     * @param newLength The new size
     * @post If the new length is < at obj.length, obj won't been modify
     */
    public FloatBuffer increaseSize(FloatBuffer obj, int newLength) {
        if (newLength > obj.capacity()) {
            FloatBuffer arrayIncreased = FloatBuffer.allocate(newLength);
            obj.rewind();
            for (int y = 0, maxY = obj.capacity(); y < maxY; y++) {
                arrayIncreased.put(obj.get());
            }
            for (int y = obj.capacity(), maxY = newLength; y < maxY; y++) {
                arrayIncreased.put(0);
            }
            return arrayIncreased;
        }
        return obj;
    }

    public void display(FloatBuffer buf, byte nbElementByLine, String name) {
        System.out.println();
        buf.rewind();
        System.out.println(name + ", capacity:" + buf.capacity() + " :");
        int j = 1;
        for (int i = 0, max = buf.capacity() - nbElementByLine; i < max; i += nbElementByLine) {
            System.out.print("	" + (j++) + ":	" + buf.get());

            for (int k = 1; k < nbElementByLine; k++) {
                System.out.print(", " + buf.get());
            }
            System.out.println();
        }
        if (buf.capacity() > buf.position()) {
            System.out.print("	" + j + ":	" + buf.get());
            for (int i = 0, max = buf.capacity() - buf.position(); i < max; i++) {
                System.out.print(", " + buf.get());
            }
        }
    }

    public void display(IntegerByReference[] buf, byte nbElementByLine, String name) {
        if (buf == null) {
            return;
        }
        System.out.println();
        System.out.println(name + ", capacity:" + buf.length + " :");
        int position = 0;
        for (int i = 0, max = buf.length - nbElementByLine; i < max; i += nbElementByLine) {
            System.out.print("	" + (buf[i].getInt() + 1));
            position++;
            for (int k = 1; k < nbElementByLine; k++) {
                System.out.print(", " + (buf[i + k].getInt() + 1));
                position++;
            }
            System.out.println();
        }
        if (buf.length > position) {
            System.out.print("	" + (buf[position++].getInt() + 1));
            for (int i = 0, max = buf.length - position; i < max; i++) {
                System.out.print(", " + (buf[position++].getInt() + 1));
            }
        }
    }

    public void display(Object[] buf, byte nbElementByLine, String name) {
        if (buf == null) {
            return;
        }
        System.out.println();
        System.out.println(name + ", capacity:" + buf.length + " :");
        int position = 0;
        for (int i = 0, max = buf.length - nbElementByLine; i < max; i += nbElementByLine) {
            System.out.print("	" + buf[i]);
            position++;
            for (int k = 1; k < nbElementByLine; k++) {
                System.out.print(", " + buf[i + k]);
                position++;
            }
            System.out.println();
        }
        if (buf.length > position) {
            System.out.print("	" + buf[position++]);
            for (int i = 0, max = buf.length - position; i < max; i++) {
                System.out.print(", " + buf[position++]);
            }
        }
    }
    // This value are found experimentally
    private float scale = 0.015f;//0.02
    private float pasScale = 3.8f * 1 / scale;
    private float offsetScale = 1.9f * 1 / scale;//1.99
    private float offsetScaleX = offsetScale - 0f * 1 / scale;//0.1
    private static int BLANC = Piece.BLANC, NOIR = Piece.NOIR;

    /**
     * _________________________________________ | | | | | | | | | 7
     * _________________________________________ | | | | | | | | | 6
     * _________________________________________ ^ | | | | | | | | | 5 /|\
     * _________________________________________ | | | | | | | | | | 4 |
     * _________________________________________ | + positionZ | | | | | | | | |
     * 3 | _________________________________________ | | | | | | | | | | 2 |
     * _________________________________________ | | | | | | | | | | 1 |
     * _________________________________________ | | Pi | | | | | | | | 0
     * _________________________________________ 0 1 2 3 4 5 6 7
     * -----------------------------> + positionX Pi is the initial position at
     * (0,0) coordinates
     *
     * @param positionX 0 to 7
     * @param positionZ 0 to 7
     * @return Model matrix associate with the transformation needed
     */
    public Model newPiecePosition(int positionX, int positionZ, int color) {
        /*
         * 	Board initial to explain the following moves 
         * _________________________________
         * |   |   |   |   |   |   |   |   | 7
         * _________________________________
         * |   |   |   |   |   |   |   |   | 6  
         * _________________________________    ^
         * |   |   |   |   |   |   |   |   | 5 /|\
         * _________________________________    |
         * |   |   |   |   |   |   |   |   | 4  |
         * _________________________________    |  + z
         * |   |   |   |   | Pi|   |   |   | 3  |
         * _________________________________    |
         * |   |   |   |   |   |   |   |   | 2  |
         * _________________________________
         * |   |   |   |   |   |   |   |   | 1
         * _________________________________
         * | Z |   |   |   |   |   |   |   | 0
         * _________________________________
         *   0   1   2   3   4   5   6   7
         * 		-------------------->        
         * 				+ x
         * 
         * Pi = position initial of all piece
         * 
         * n = noir (black) in pieces[0]
         * b = blanc (white) in pieces[1]
         */
        Model result = new Model();
        int coef = 1;
        if (color == BLANC) {
            result.rotate((float) Math.PI, 0f, 1f, 0f);
            coef = -1;
        }
        result.scale(scale, scale, scale);
        result.translate(coef * (offsetScaleX + (positionX - 4) * pasScale), 0f, coef * (-pasScale * (positionZ - 3) + offsetScale));
        return result;
    }

    public Model newCasePosition(int positionX, int positionZ) {
        /*
         * 	Board initial to explain the following moves 
         * _________________________________
         * |   |   |   |   |   |   |   |   | 7
         * _________________________________
         * |   |   |   |   |   |   |   |   | 6  
         * _________________________________    ^
         * |   |   |   |   |   |   |   |   | 5 /|\
         * _________________________________    |
         * |   |   |   |   |   |   |   |   | 4  |
         * _________________________________    |  + z
         * |   |   |   |   | Pi|   |   |   | 3  |
         * _________________________________    |
         * |   |   |   |   |   |   |   |   | 2  |
         * _________________________________
         * |   |   |   |   |   |   |   |   | 1
         * _________________________________
         * | Z |   |   |   |   |   |   |   | 0
         * _________________________________
         *   0   1   2   3   4   5   6   7
         * 		-------------------->        
         * 				+ x
         * 
         * Pi = position initial of all piece
         * 
         * n = noir (black) in pieces[0]
         * b = blanc (white) in pieces[1]
         */
        Model result = new Model();
        result.translate((positionX - 4) * pasScale * scale + offsetScale * scale, 0f, -(positionZ - 3) * pasScale * scale + offsetScale * scale);
        return result;
    }
}
