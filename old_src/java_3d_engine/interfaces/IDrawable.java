
package java_3d_engine.interfaces;

/**
 * 
 * @author Olivier
 * Interface for draw your object in the gl Context
 */
public interface IDrawable 
{
	/**
	 * Draw your object
	 * @param gl Context opengl
	 * @param indexShader {id shader program, id shader position, id shader normal}
	 */
	public void draw (GraphicLibrary gl, int[] indexShader);
}
