package java_3d_engine.interfaces;

import javax.media.opengl.GL2;
import javax.media.opengl.GLES2;

public interface IGL extends GL2 {

    final public String NAME_V_POSITION = "V_Position";
    final public String NAME_V_NORMAL = "V_Normal";
    final public String NAME_V_TEXCOORD = "Texcoord";
    //final public GLES2 OPEN_GL_ES_2 = null;

    
}
