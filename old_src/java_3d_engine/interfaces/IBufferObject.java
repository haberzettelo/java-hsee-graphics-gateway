package java_3d_engine.interfaces;

import java.nio.Buffer;

public interface IBufferObject 
{
	//public void bufferData (Buffer data);
	
	public void bindBuffer ();
	
	public void unbindBuffer ();
}
