package java_3d_engine.interfaces;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL2ES1;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GL2GL3;
import javax.media.opengl.GL3;
import javax.media.opengl.GL3bc;
import javax.media.opengl.GL4;
import javax.media.opengl.GL4bc;
import javax.media.opengl.GLArrayData;
import javax.media.opengl.GLContext;
import javax.media.opengl.GLES1;
import javax.media.opengl.GLES2;
import javax.media.opengl.GLException;
import javax.media.opengl.GLProfile;
import javax.media.opengl.GLUniformData;

import com.jogamp.common.nio.PointerBuffer;

public class GraphicLibrary implements IGL {

	private GL2 gl;
	
	public GraphicLibrary (GL glContext)
	{
		gl = glContext.getGL2();
	}
	
	public synchronized GL2 getGLx()
	{
		return gl;
	}
	public void setGL (GL2 glContext)
	{
		gl = glContext;
	}
	
	@Override
	public void glAccum(int op, float value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glActiveStencilFaceEXT(int face) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glApplyTextureEXT(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glAreTexturesResident(int n, IntBuffer textures,
			ByteBuffer residences) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glAreTexturesResident(int n, int[] textures,
			int textures_offset, byte[] residences, int residences_offset) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glArrayElement(int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glAttachObjectARB(int containerObj, int obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBegin(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginOcclusionQueryNV(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginPerfMonitorAMD(int monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginVertexShaderEXT() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginVideoCaptureNV(int video_capture_slot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindBufferOffset(int target, int index, int buffer,
			long offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glBindLightParameterEXT(int light, int value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glBindMaterialParameterEXT(int face, int value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glBindMultiTextureEXT(int texunit, int target, int texture) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glBindParameterEXT(int value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glBindProgramARB(int target, int program) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glBindTexGenParameterEXT(int unit, int coord, int value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glBindTextureUnitParameterEXT(int unit, int value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glBindTransformFeedbackNV(int target, int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindVertexShaderEXT(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindVideoCaptureStreamBufferNV(int video_capture_slot,
			int stream, int frame_region, long offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindVideoCaptureStreamTextureNV(int video_capture_slot,
			int stream, int frame_region, int target, int texture) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBitmap(int width, int height, float xorig, float yorig,
			float xmove, float ymove, ByteBuffer bitmap) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBitmap(int width, int height, float xorig, float yorig,
			float xmove, float ymove, long bitmap_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBitmap(int width, int height, float xorig, float yorig,
			float xmove, float ymove, byte[] bitmap, int bitmap_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendEquationIndexedAMD(int buf, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendEquationSeparateIndexedAMD(int buf, int modeRGB,
			int modeAlpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFuncIndexedAMD(int buf, int src, int dst) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFuncSeparateINGR(int sfactorRGB, int dfactorRGB,
			int sfactorAlpha, int dfactorAlpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFuncSeparateIndexedAMD(int buf, int srcRGB, int dstRGB,
			int srcAlpha, int dstAlpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBufferParameteri(int target, int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCallList(int list) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCallLists(int n, int type, Buffer lists) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glCheckNamedFramebufferStatusEXT(int framebuffer, int target) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glClearAccum(float red, float green, float blue, float alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearColorIi(int red, int green, int blue, int alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearColorIui(int red, int green, int blue, int alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearIndex(float c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClientAttribDefaultEXT(int mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClipPlane(int plane, DoubleBuffer equation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClipPlane(int plane, double[] equation, int equation_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3b(byte red, byte green, byte blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3bv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3bv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3d(double red, double green, double blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3f(float red, float green, float blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3h(short red, short green, short blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3i(int red, int green, int blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3s(short red, short green, short blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3ub(byte red, byte green, byte blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3ubv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3ubv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3ui(int red, int green, int blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3uiv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3uiv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3us(short red, short green, short blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3usv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor3usv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4b(byte red, byte green, byte blue, byte alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4bv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4bv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4d(double red, double green, double blue, double alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4h(short red, short green, short blue, short alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4i(int red, int green, int blue, int alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4s(short red, short green, short blue, short alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4ubv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4ubv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4ui(int red, int green, int blue, int alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4uiv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4uiv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4us(short red, short green, short blue, short alpha) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4usv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4usv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorMaskIndexed(int index, boolean r, boolean g, boolean b,
			boolean a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorMaterial(int face, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorSubTable(int target, int start, int count, int format,
			int type, Buffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorSubTable(int target, int start, int count, int format,
			int type, long data_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorTable(int target, int internalformat, int width,
			int format, int type, Buffer table) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorTable(int target, int internalformat, int width,
			int format, int type, long table_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorTableParameterfv(int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorTableParameterfv(int target, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorTableParameteriv(int target, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorTableParameteriv(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompileShaderARB(int shaderObj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedMultiTexImage1DEXT(int texunit, int target,
			int level, int internalformat, int width, int border,
			int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedMultiTexImage2DEXT(int texunit, int target,
			int level, int internalformat, int width, int height, int border,
			int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedMultiTexImage3DEXT(int texunit, int target,
			int level, int internalformat, int width, int height, int depth,
			int border, int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedMultiTexSubImage1DEXT(int texunit, int target,
			int level, int xoffset, int width, int format, int imageSize,
			Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedMultiTexSubImage2DEXT(int texunit, int target,
			int level, int xoffset, int yoffset, int width, int height,
			int format, int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedMultiTexSubImage3DEXT(int texunit, int target,
			int level, int xoffset, int yoffset, int zoffset, int width,
			int height, int depth, int format, int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTextureImage1DEXT(int texture, int target,
			int level, int internalformat, int width, int border,
			int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTextureImage2DEXT(int texture, int target,
			int level, int internalformat, int width, int height, int border,
			int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTextureImage3DEXT(int texture, int target,
			int level, int internalformat, int width, int height, int depth,
			int border, int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTextureSubImage1DEXT(int texture, int target,
			int level, int xoffset, int width, int format, int imageSize,
			Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTextureSubImage2DEXT(int texture, int target,
			int level, int xoffset, int yoffset, int width, int height,
			int format, int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTextureSubImage3DEXT(int texture, int target,
			int level, int xoffset, int yoffset, int zoffset, int width,
			int height, int depth, int format, int imageSize, Buffer bits) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionFilter1D(int target, int internalformat,
			int width, int format, int type, Buffer image) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionFilter1D(int target, int internalformat,
			int width, int format, int type, long image_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionFilter2D(int target, int internalformat,
			int width, int height, int format, int type, Buffer image) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionFilter2D(int target, int internalformat,
			int width, int height, int format, int type,
			long image_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionParameterf(int target, int pname, float params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionParameterfv(int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionParameterfv(int target, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionParameteri(int target, int pname, int params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionParameteriv(int target, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glConvolutionParameteriv(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyColorSubTable(int target, int start, int x, int y,
			int width) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyColorTable(int target, int internalformat, int x, int y,
			int width) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyConvolutionFilter1D(int target, int internalformat,
			int x, int y, int width) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyConvolutionFilter2D(int target, int internalformat,
			int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyImageSubDataNV(int srcName, int srcTarget, int srcLevel,
			int srcX, int srcY, int srcZ, int dstName, int dstTarget,
			int dstLevel, int dstX, int dstY, int dstZ, int width, int height,
			int depth) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyMultiTexImage1DEXT(int texunit, int target, int level,
			int internalformat, int x, int y, int width, int border) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyMultiTexImage2DEXT(int texunit, int target, int level,
			int internalformat, int x, int y, int width, int height, int border) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyMultiTexSubImage1DEXT(int texunit, int target, int level,
			int xoffset, int x, int y, int width) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyMultiTexSubImage2DEXT(int texunit, int target, int level,
			int xoffset, int yoffset, int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyMultiTexSubImage3DEXT(int texunit, int target, int level,
			int xoffset, int yoffset, int zoffset, int x, int y, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyPixels(int x, int y, int width, int height, int type) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTextureImage1DEXT(int texture, int target, int level,
			int internalformat, int x, int y, int width, int border) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTextureImage2DEXT(int texture, int target, int level,
			int internalformat, int x, int y, int width, int height, int border) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTextureSubImage1DEXT(int texture, int target, int level,
			int xoffset, int x, int y, int width) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTextureSubImage2DEXT(int texture, int target, int level,
			int xoffset, int yoffset, int x, int y, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTextureSubImage3DEXT(int texture, int target, int level,
			int xoffset, int yoffset, int zoffset, int x, int y, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glCreateProgramObjectARB() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glCreateShaderObjectARB(int shaderType) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glCullParameterdvEXT(int pname, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCullParameterdvEXT(int pname, double[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCullParameterfvEXT(int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCullParameterfvEXT(int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteFencesAPPLE(int n, IntBuffer fences) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteFencesAPPLE(int n, int[] fences, int fences_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteFencesNV(int n, IntBuffer fences) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteFencesNV(int n, int[] fences, int fences_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteLists(int list, int range) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteNamesAMD(int identifier, int num, IntBuffer names) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteNamesAMD(int identifier, int num, int[] names,
			int names_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteObjectARB(int obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteOcclusionQueriesNV(int n, IntBuffer ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteOcclusionQueriesNV(int n, int[] ids, int ids_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeletePerfMonitorsAMD(int n, IntBuffer monitors) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeletePerfMonitorsAMD(int n, int[] monitors,
			int monitors_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteProgramsARB(int n, IntBuffer programs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteProgramsARB(int n, int[] programs, int programs_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteTransformFeedbacksNV(int n, IntBuffer ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteTransformFeedbacksNV(int n, int[] ids, int ids_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteVertexShaderEXT(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthBoundsEXT(double zmin, double zmax) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDetachObjectARB(int containerObj, int attachedObj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableClientStateIndexedEXT(int array, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableIndexed(int target, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableVariantClientStateEXT(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableVertexAttribAPPLE(int index, int pname) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableVertexAttribArrayARB(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawBuffersATI(int n, IntBuffer bufs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawBuffersATI(int n, int[] bufs, int bufs_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawPixels(int width, int height, int format, int type,
			Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawPixels(int width, int height, int format, int type,
			long pixels_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawTransformFeedbackNV(int mode, int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEdgeFlag(boolean flag) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEdgeFlagPointer(int stride, Buffer ptr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEdgeFlagPointer(int stride, long ptr_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEdgeFlagv(ByteBuffer flag) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEdgeFlagv(byte[] flag, int flag_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableClientStateIndexedEXT(int array, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableIndexed(int target, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableVariantClientStateEXT(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableVertexAttribAPPLE(int index, int pname) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableVertexAttribArrayARB(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnd() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndList() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndOcclusionQueryNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndPerfMonitorAMD(int monitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndVertexShaderEXT() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndVideoCaptureNV(int video_capture_slot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord1d(double u) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord1dv(DoubleBuffer u) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord1dv(double[] u, int u_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord1f(float u) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord1fv(FloatBuffer u) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord1fv(float[] u, int u_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord2d(double u, double v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord2dv(DoubleBuffer u) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord2dv(double[] u, int u_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord2f(float u, float v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord2fv(FloatBuffer u) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalCoord2fv(float[] u, int u_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalMapsNV(int target, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalMesh1(int mode, int i1, int i2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalMesh2(int mode, int i1, int i2, int j1, int j2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalPoint1(int i) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEvalPoint2(int i, int j) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glExtractComponentEXT(int res, int src, int num) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFeedbackBuffer(int size, int type, FloatBuffer buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFinishFenceAPPLE(int fence) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFinishFenceNV(int fence) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFinishObjectAPPLE(int object, int name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFinishRenderAPPLE() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFinishTextureSUNX() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlushMappedNamedBufferRangeEXT(int buffer, long offset,
			long length) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlushPixelDataRangeNV(int target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlushRenderAPPLE() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlushVertexArrayRangeAPPLE(int length, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlushVertexArrayRangeNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordPointer(int type, int stride, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordPointer(int type, int stride,
			long pointer_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordd(double coord) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoorddv(DoubleBuffer coord) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoorddv(double[] coord, int coord_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordf(float coord) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordfv(FloatBuffer coord) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordfv(float[] coord, int coord_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordh(short fog) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordhv(ShortBuffer fog) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordhv(short[] fog, int fog_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogi(int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogiv(int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogiv(int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFrameTerminatorGREMEDY() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferDrawBufferEXT(int framebuffer, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferDrawBuffersEXT(int framebuffer, int n,
			IntBuffer bufs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferDrawBuffersEXT(int framebuffer, int n, int[] bufs,
			int bufs_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferReadBufferEXT(int framebuffer, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureEXT(int target, int attachment,
			int texture, int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureFaceEXT(int target, int attachment,
			int texture, int level, int face) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureLayerEXT(int target, int attachment,
			int texture, int level, int layer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenFencesAPPLE(int n, IntBuffer fences) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenFencesAPPLE(int n, int[] fences, int fences_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenFencesNV(int n, IntBuffer fences) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenFencesNV(int n, int[] fences, int fences_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGenLists(int range) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGenNamesAMD(int identifier, int num, IntBuffer names) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenNamesAMD(int identifier, int num, int[] names,
			int names_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenOcclusionQueriesNV(int n, IntBuffer ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenOcclusionQueriesNV(int n, int[] ids, int ids_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenPerfMonitorsAMD(int n, IntBuffer monitors) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenPerfMonitorsAMD(int n, int[] monitors, int monitors_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenProgramsARB(int n, IntBuffer programs) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenProgramsARB(int n, int[] programs, int programs_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGenSymbolsEXT(int datatype, int storagetype, int range,
			int components) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGenTransformFeedbacksNV(int n, IntBuffer ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenTransformFeedbacksNV(int n, int[] ids, int ids_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGenVertexShadersEXT(int range) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGenerateMultiTexMipmapEXT(int texunit, int target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenerateTextureMipmapEXT(int texture, int target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformARB(int programObj, int index, int maxLength,
			IntBuffer length, IntBuffer size, IntBuffer type, ByteBuffer name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformARB(int programObj, int index, int maxLength,
			int[] length, int length_offset, int[] size, int size_offset,
			int[] type, int type_offset, byte[] name, int name_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetAttachedObjectsARB(int containerObj, int maxCount,
			IntBuffer count, IntBuffer obj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetAttachedObjectsARB(int containerObj, int maxCount,
			int[] count, int count_offset, int[] obj, int obj_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBooleanIndexedv(int target, int index, ByteBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBooleanIndexedv(int target, int index, byte[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetClipPlane(int plane, DoubleBuffer equation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetClipPlane(int plane, double[] equation, int equation_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetColorTable(int target, int format, int type, Buffer table) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetColorTable(int target, int format, int type,
			long table_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetColorTableParameterfv(int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetColorTableParameterfv(int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetColorTableParameteriv(int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetColorTableParameteriv(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetCompressedMultiTexImageEXT(int texunit, int target,
			int lod, Buffer img) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetCompressedTextureImageEXT(int texture, int target,
			int lod, Buffer img) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetConvolutionFilter(int target, int format, int type,
			Buffer image) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetConvolutionFilter(int target, int format, int type,
			long image_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetConvolutionParameterfv(int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetConvolutionParameterfv(int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetConvolutionParameteriv(int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetConvolutionParameteriv(int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetDoubleIndexedvEXT(int target, int index, DoubleBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetDoubleIndexedvEXT(int target, int index, double[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFenceivNV(int fence, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFenceivNV(int fence, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFloatIndexedvEXT(int target, int index, FloatBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFloatIndexedvEXT(int target, int index, float[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFramebufferParameterivEXT(int framebuffer, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFramebufferParameterivEXT(int framebuffer, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetHandleARB(int pname) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetHistogram(int target, boolean reset, int format, int type,
			Buffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetHistogram(int target, boolean reset, int format, int type,
			long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetHistogramParameterfv(int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetHistogramParameterfv(int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetHistogramParameteriv(int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetHistogramParameteriv(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInfoLogARB(int obj, int maxLength, IntBuffer length,
			ByteBuffer infoLog) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInfoLogARB(int obj, int maxLength, int[] length,
			int length_offset, byte[] infoLog, int infoLog_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerIndexedv(int target, int index, IntBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerIndexedv(int target, int index, int[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInvariantBooleanvEXT(int id, int value, ByteBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInvariantBooleanvEXT(int id, int value, byte[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInvariantFloatvEXT(int id, int value, FloatBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInvariantFloatvEXT(int id, int value, float[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInvariantIntegervEXT(int id, int value, IntBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInvariantIntegervEXT(int id, int value, int[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLightiv(int light, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLightiv(int light, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLocalConstantBooleanvEXT(int id, int value, ByteBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLocalConstantBooleanvEXT(int id, int value, byte[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLocalConstantFloatvEXT(int id, int value, FloatBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLocalConstantFloatvEXT(int id, int value, float[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLocalConstantIntegervEXT(int id, int value, IntBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLocalConstantIntegervEXT(int id, int value, int[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapAttribParameterfvNV(int target, int index, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapAttribParameterfvNV(int target, int index, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapAttribParameterivNV(int target, int index, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapAttribParameterivNV(int target, int index, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapControlPointsNV(int target, int index, int type,
			int ustride, int vstride, boolean packed, Buffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapParameterfvNV(int target, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapParameterfvNV(int target, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapParameterivNV(int target, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapParameterivNV(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapdv(int target, int query, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapdv(int target, int query, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapfv(int target, int query, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapfv(int target, int query, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapiv(int target, int query, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMapiv(int target, int query, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMaterialiv(int face, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMaterialiv(int face, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMinmax(int target, boolean reset, int format, int type,
			Buffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMinmax(int target, boolean reset, int format, int type,
			long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMinmaxParameterfv(int target, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMinmaxParameterfv(int target, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMinmaxParameteriv(int target, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMinmaxParameteriv(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexEnvfvEXT(int texunit, int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexEnvfvEXT(int texunit, int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexEnvivEXT(int texunit, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexEnvivEXT(int texunit, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexGendvEXT(int texunit, int coord, int pname,
			DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexGendvEXT(int texunit, int coord, int pname,
			double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexGenfvEXT(int texunit, int coord, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexGenfvEXT(int texunit, int coord, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexGenivEXT(int texunit, int coord, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexGenivEXT(int texunit, int coord, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexImageEXT(int texunit, int target, int level,
			int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexLevelParameterfvEXT(int texunit, int target,
			int level, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexLevelParameterfvEXT(int texunit, int target,
			int level, int pname, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexLevelParameterivEXT(int texunit, int target,
			int level, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexLevelParameterivEXT(int texunit, int target,
			int level, int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterIivEXT(int texunit, int target,
			int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterIivEXT(int texunit, int target,
			int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterIuivEXT(int texunit, int target,
			int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterIuivEXT(int texunit, int target,
			int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterfvEXT(int texunit, int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterfvEXT(int texunit, int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterivEXT(int texunit, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultiTexParameterivEXT(int texunit, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultisamplefvNV(int pname, int index, FloatBuffer val) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultisamplefvNV(int pname, int index, float[] val,
			int val_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedBufferParameterivEXT(int buffer, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedBufferParameterivEXT(int buffer, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedBufferSubDataEXT(int buffer, long offset, long size,
			Buffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedFramebufferAttachmentParameterivEXT(int framebuffer,
			int attachment, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedFramebufferAttachmentParameterivEXT(int framebuffer,
			int attachment, int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterIivEXT(int program, int target,
			int index, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterIivEXT(int program, int target,
			int index, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterIuivEXT(int program, int target,
			int index, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterIuivEXT(int program, int target,
			int index, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterdvEXT(int program, int target,
			int index, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterdvEXT(int program, int target,
			int index, double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterfvEXT(int program, int target,
			int index, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramLocalParameterfvEXT(int program, int target,
			int index, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramStringEXT(int program, int target, int pname,
			Buffer string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramivEXT(int program, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedProgramivEXT(int program, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedRenderbufferParameterivEXT(int renderbuffer,
			int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedRenderbufferParameterivEXT(int renderbuffer,
			int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetObjectParameterfvARB(int obj, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetObjectParameterfvARB(int obj, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetObjectParameterivAPPLE(int objectType, int name,
			int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetObjectParameterivAPPLE(int objectType, int name,
			int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetObjectParameterivARB(int obj, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetObjectParameterivARB(int obj, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetOcclusionQueryivNV(int id, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetOcclusionQueryivNV(int id, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetOcclusionQueryuivNV(int id, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetOcclusionQueryuivNV(int id, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCounterDataAMD(int monitor, int pname,
			int dataSize, IntBuffer data, IntBuffer bytesWritten) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCounterDataAMD(int monitor, int pname,
			int dataSize, int[] data, int data_offset, int[] bytesWritten,
			int bytesWritten_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCounterInfoAMD(int group, int counter,
			int pname, Buffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCounterStringAMD(int group, int counter,
			int bufSize, IntBuffer length, ByteBuffer counterString) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCounterStringAMD(int group, int counter,
			int bufSize, int[] length, int length_offset, byte[] counterString,
			int counterString_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCountersAMD(int group, IntBuffer numCounters,
			IntBuffer maxActiveCounters, int counterSize, IntBuffer counters) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorCountersAMD(int group, int[] numCounters,
			int numCounters_offset, int[] maxActiveCounters,
			int maxActiveCounters_offset, int counterSize, int[] counters,
			int counters_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorGroupStringAMD(int group, int bufSize,
			IntBuffer length, ByteBuffer groupString) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorGroupStringAMD(int group, int bufSize,
			int[] length, int length_offset, byte[] groupString,
			int groupString_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorGroupsAMD(IntBuffer numGroups, int groupsSize,
			IntBuffer groups) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPerfMonitorGroupsAMD(int[] numGroups,
			int numGroups_offset, int groupsSize, int[] groups,
			int groups_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapfv(int map, FloatBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapfv(int map, long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapfv(int map, float[] values, int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapuiv(int map, IntBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapuiv(int map, long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapuiv(int map, int[] values, int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapusv(int map, ShortBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapusv(int map, long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPixelMapusv(int map, short[] values, int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPolygonStipple(ByteBuffer mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPolygonStipple(long mask_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetPolygonStipple(byte[] mask, int mask_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterIivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterIivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterIuivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterIuivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterdvARB(int target, int index,
			DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterdvARB(int target, int index,
			double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterfvARB(int target, int index,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramEnvParameterfvARB(int target, int index,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterIivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterIivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterIuivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterIuivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterdvARB(int target, int index,
			DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterdvARB(int target, int index,
			double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterfvARB(int target, int index,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramLocalParameterfvARB(int target, int index,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramStringARB(int target, int pname, Buffer string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramSubroutineParameteruivNV(int target, int index,
			IntBuffer param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramSubroutineParameteruivNV(int target, int index,
			int[] param, int param_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramivARB(int target, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramivARB(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjecti64vEXT(int id, int pname, LongBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjecti64vEXT(int id, int pname, long[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectui64vEXT(int id, int pname, LongBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectui64vEXT(int id, int pname, long[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSeparableFilter(int target, int format, int type,
			Buffer row, Buffer column, Buffer span) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSeparableFilter(int target, int format, int type,
			long row_buffer_offset, long column_buffer_offset,
			long span_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderSourceARB(int obj, int maxLength, IntBuffer length,
			ByteBuffer source) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderSourceARB(int obj, int maxLength, int[] length,
			int length_offset, byte[] source, int source_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexGendv(int coord, int pname, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexGendv(int coord, int pname, double[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureImageEXT(int texture, int target, int level,
			int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureLevelParameterfvEXT(int texture, int target,
			int level, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureLevelParameterfvEXT(int texture, int target,
			int level, int pname, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureLevelParameterivEXT(int texture, int target,
			int level, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureLevelParameterivEXT(int texture, int target,
			int level, int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterIivEXT(int texture, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterIivEXT(int texture, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterIuivEXT(int texture, int target,
			int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterIuivEXT(int texture, int target,
			int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterfvEXT(int texture, int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterfvEXT(int texture, int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterivEXT(int texture, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTextureParameterivEXT(int texture, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetUniformBufferSizeEXT(int program, int location) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glGetUniformLocationARB(int programObj, String name) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long glGetUniformOffsetEXT(int program, int location) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetUniformfvARB(int programObj, int location,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformfvARB(int programObj, int location, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformivARB(int programObj, int location, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformivARB(int programObj, int location, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVariantBooleanvEXT(int id, int value, ByteBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVariantBooleanvEXT(int id, int value, byte[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVariantFloatvEXT(int id, int value, FloatBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVariantFloatvEXT(int id, int value, float[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVariantIntegervEXT(int id, int value, IntBuffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVariantIntegervEXT(int id, int value, int[] data,
			int data_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIivEXT(int index, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIivEXT(int index, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIuivEXT(int index, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIuivEXT(int index, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribLi64vNV(int index, int pname, LongBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribLi64vNV(int index, int pname, long[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribLui64vNV(int index, int pname,
			LongBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribLui64vNV(int index, int pname, long[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribdvARB(int index, int pname, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribdvARB(int index, int pname, double[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribfvARB(int index, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribfvARB(int index, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribivARB(int index, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribivARB(int index, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureStreamdvNV(int video_capture_slot, int stream,
			int pname, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureStreamdvNV(int video_capture_slot, int stream,
			int pname, double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureStreamfvNV(int video_capture_slot, int stream,
			int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureStreamfvNV(int video_capture_slot, int stream,
			int pname, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureStreamivNV(int video_capture_slot, int stream,
			int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureStreamivNV(int video_capture_slot, int stream,
			int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureivNV(int video_capture_slot, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVideoCaptureivNV(int video_capture_slot, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glHintPGI(int target, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glHistogram(int target, int width, int internalformat,
			boolean sink) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexFuncEXT(int func, float ref) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexMask(int mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexMaterialEXT(int face, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexPointer(int type, int stride, Buffer ptr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexd(double c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexdv(DoubleBuffer c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexdv(double[] c, int c_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexf(float c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexfv(FloatBuffer c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexfv(float[] c, int c_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexi(int c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexiv(IntBuffer c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexiv(int[] c, int c_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexs(short c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexsv(ShortBuffer c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexsv(short[] c, int c_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexub(byte c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexubv(ByteBuffer c) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glIndexubv(byte[] c, int c_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glInitNames() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glInsertComponentEXT(int res, int src, int num) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glInterleavedArrays(int format, int stride, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glInterleavedArrays(int format, int stride,
			long pointer_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glIsEnabledIndexed(int target, int index) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsFenceAPPLE(int fence) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsFenceNV(int fence) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsList(int list) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsNameAMD(int identifier, int name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsOcclusionQueryNV(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsPBOPackEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsPBOUnpackEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsProgramARB(int program) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsTransformFeedbackNV(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsVariantEnabledEXT(int id, int cap) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsVertexAttribEnabledAPPLE(int index, int pname) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glLightModeli(int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightModeliv(int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightModeliv(int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLighti(int light, int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightiv(int light, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightiv(int light, int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLineStipple(int factor, short pattern) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLinkProgramARB(int programObj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glListBase(int base) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadMatrixd(DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadMatrixd(double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadName(int name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadTransposeMatrixd(DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadTransposeMatrixd(double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadTransposeMatrixf(FloatBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadTransposeMatrixf(float[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLockArraysEXT(int first, int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap1d(int target, double u1, double u2, int stride,
			int order, DoubleBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap1d(int target, double u1, double u2, int stride,
			int order, double[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap1f(int target, float u1, float u2, int stride, int order,
			FloatBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap1f(int target, float u1, float u2, int stride, int order,
			float[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap2d(int target, double u1, double u2, int ustride,
			int uorder, double v1, double v2, int vstride, int vorder,
			DoubleBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap2d(int target, double u1, double u2, int ustride,
			int uorder, double v1, double v2, int vstride, int vorder,
			double[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap2f(int target, float u1, float u2, int ustride,
			int uorder, float v1, float v2, int vstride, int vorder,
			FloatBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMap2f(int target, float u1, float u2, int ustride,
			int uorder, float v1, float v2, int vstride, int vorder,
			float[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapControlPointsNV(int target, int index, int type,
			int ustride, int vstride, int uorder, int vorder, boolean packed,
			Buffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapGrid1d(int un, double u1, double u2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapGrid1f(int un, float u1, float u2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapGrid2d(int un, double u1, double u2, int vn, double v1,
			double v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapGrid2f(int un, float u1, float u2, int vn, float v1,
			float v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ByteBuffer glMapNamedBufferEXT(int buffer, int access) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ByteBuffer glMapNamedBufferRangeEXT(int buffer, long offset,
			long length, int access) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void glMapParameterfvNV(int target, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapParameterfvNV(int target, int pname, float[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapParameterivNV(int target, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapParameterivNV(int target, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib1dAPPLE(int index, int size, double u1,
			double u2, int stride, int order, DoubleBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib1dAPPLE(int index, int size, double u1,
			double u2, int stride, int order, double[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib1fAPPLE(int index, int size, float u1,
			float u2, int stride, int order, FloatBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib1fAPPLE(int index, int size, float u1,
			float u2, int stride, int order, float[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib2dAPPLE(int index, int size, double u1,
			double u2, int ustride, int uorder, double v1, double v2,
			int vstride, int vorder, DoubleBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib2dAPPLE(int index, int size, double u1,
			double u2, int ustride, int uorder, double v1, double v2,
			int vstride, int vorder, double[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib2fAPPLE(int index, int size, float u1,
			float u2, int ustride, int uorder, float v1, float v2, int vstride,
			int vorder, FloatBuffer points) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMapVertexAttrib2fAPPLE(int index, int size, float u1,
			float u2, int ustride, int uorder, float v1, float v2, int vstride,
			int vorder, float[] points, int points_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMateriali(int face, int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMaterialiv(int face, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMaterialiv(int face, int pname, int[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixFrustumEXT(int mode, double left, double right,
			double bottom, double top, double zNear, double zFar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexubvARB(int size, ByteBuffer indices) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexubvARB(int size, byte[] indices, int indices_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexuivARB(int size, IntBuffer indices) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexuivARB(int size, int[] indices, int indices_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexusvARB(int size, ShortBuffer indices) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexusvARB(int size, short[] indices,
			int indices_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadIdentityEXT(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadTransposedEXT(int mode, DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadTransposedEXT(int mode, double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadTransposefEXT(int mode, FloatBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadTransposefEXT(int mode, float[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoaddEXT(int mode, DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoaddEXT(int mode, double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadfEXT(int mode, FloatBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixLoadfEXT(int mode, float[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultTransposedEXT(int mode, DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultTransposedEXT(int mode, double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultTransposefEXT(int mode, FloatBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultTransposefEXT(int mode, float[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultdEXT(int mode, DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultdEXT(int mode, double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultfEXT(int mode, FloatBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMultfEXT(int mode, float[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixOrthoEXT(int mode, double left, double right,
			double bottom, double top, double zNear, double zFar) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixPopEXT(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixPushEXT(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixRotatedEXT(int mode, double angle, double x, double y,
			double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixRotatefEXT(int mode, float angle, float x, float y,
			float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixScaledEXT(int mode, double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixScalefEXT(int mode, float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixTranslatedEXT(int mode, double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixTranslatefEXT(int mode, float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMinmax(int target, int internalformat, boolean sink) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultMatrixd(DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultMatrixd(double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultTransposeMatrixd(DoubleBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultTransposeMatrixd(double[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultTransposeMatrixf(FloatBuffer m) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultTransposeMatrixf(float[] m, int m_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexBufferEXT(int texunit, int target,
			int internalformat, int buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1d(int target, double s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1dv(int target, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1dv(int target, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1f(int target, float s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1fv(int target, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1fv(int target, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1h(int target, short s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1hv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1hv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1i(int target, int s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1iv(int target, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1iv(int target, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1s(int target, short s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1sv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord1sv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2d(int target, double s, double t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2dv(int target, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2dv(int target, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2f(int target, float s, float t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2fv(int target, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2fv(int target, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2h(int target, short s, short t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2hv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2hv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2i(int target, int s, int t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2iv(int target, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2iv(int target, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2s(int target, short s, short t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2sv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord2sv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3d(int target, double s, double t, double r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3dv(int target, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3dv(int target, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3f(int target, float s, float t, float r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3fv(int target, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3fv(int target, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3h(int target, short s, short t, short r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3hv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3hv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3i(int target, int s, int t, int r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3iv(int target, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3iv(int target, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3s(int target, short s, short t, short r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3sv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord3sv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4d(int target, double s, double t, double r,
			double q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4dv(int target, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4dv(int target, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4fv(int target, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4fv(int target, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4h(int target, short s, short t, short r, short q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4hv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4hv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4i(int target, int s, int t, int r, int q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4iv(int target, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4iv(int target, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4s(int target, short s, short t, short r, short q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4sv(int target, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4sv(int target, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordPointerEXT(int texunit, int size, int type,
			int stride, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexEnvfEXT(int texunit, int target, int pname,
			float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexEnvfvEXT(int texunit, int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexEnvfvEXT(int texunit, int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexEnviEXT(int texunit, int target, int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexEnvivEXT(int texunit, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexEnvivEXT(int texunit, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGendEXT(int texunit, int coord, int pname,
			double param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGendvEXT(int texunit, int coord, int pname,
			DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGendvEXT(int texunit, int coord, int pname,
			double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGenfEXT(int texunit, int coord, int pname, float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGenfvEXT(int texunit, int coord, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGenfvEXT(int texunit, int coord, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGeniEXT(int texunit, int coord, int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGenivEXT(int texunit, int coord, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexGenivEXT(int texunit, int coord, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexImage1DEXT(int texunit, int target, int level,
			int internalformat, int width, int border, int format, int type,
			Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexImage2DEXT(int texunit, int target, int level,
			int internalformat, int width, int height, int border, int format,
			int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexImage3DEXT(int texunit, int target, int level,
			int internalformat, int width, int height, int depth, int border,
			int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterIivEXT(int texunit, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterIivEXT(int texunit, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterIuivEXT(int texunit, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterIuivEXT(int texunit, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterfEXT(int texunit, int target, int pname,
			float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterfvEXT(int texunit, int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterfvEXT(int texunit, int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameteriEXT(int texunit, int target, int pname,
			int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterivEXT(int texunit, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexParameterivEXT(int texunit, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexRenderbufferEXT(int texunit, int target,
			int renderbuffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexSubImage1DEXT(int texunit, int target, int level,
			int xoffset, int width, int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexSubImage2DEXT(int texunit, int target, int level,
			int xoffset, int yoffset, int width, int height, int format,
			int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexSubImage3DEXT(int texunit, int target, int level,
			int xoffset, int yoffset, int zoffset, int width, int height,
			int depth, int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedBufferDataEXT(int buffer, long size, Buffer data,
			int usage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedBufferSubDataEXT(int buffer, long offset, long size,
			Buffer data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedCopyBufferSubDataEXT(int readBuffer, int writeBuffer,
			long readOffset, long writeOffset, long size) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferRenderbufferEXT(int framebuffer,
			int attachment, int renderbuffertarget, int renderbuffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferTexture1DEXT(int framebuffer, int attachment,
			int textarget, int texture, int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferTexture2DEXT(int framebuffer, int attachment,
			int textarget, int texture, int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferTexture3DEXT(int framebuffer, int attachment,
			int textarget, int texture, int level, int zoffset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferTextureEXT(int framebuffer, int attachment,
			int texture, int level) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferTextureFaceEXT(int framebuffer,
			int attachment, int texture, int level, int face) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedFramebufferTextureLayerEXT(int framebuffer,
			int attachment, int texture, int level, int layer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameter4dEXT(int program, int target,
			int index, double x, double y, double z, double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameter4dvEXT(int program, int target,
			int index, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameter4dvEXT(int program, int target,
			int index, double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameter4fEXT(int program, int target,
			int index, float x, float y, float z, float w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameter4fvEXT(int program, int target,
			int index, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameter4fvEXT(int program, int target,
			int index, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameterI4iEXT(int program, int target,
			int index, int x, int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameterI4ivEXT(int program, int target,
			int index, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameterI4ivEXT(int program, int target,
			int index, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameterI4uiEXT(int program, int target,
			int index, int x, int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameterI4uivEXT(int program, int target,
			int index, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameterI4uivEXT(int program, int target,
			int index, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameters4fvEXT(int program, int target,
			int index, int count, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParameters4fvEXT(int program, int target,
			int index, int count, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParametersI4ivEXT(int program, int target,
			int index, int count, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParametersI4ivEXT(int program, int target,
			int index, int count, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParametersI4uivEXT(int program, int target,
			int index, int count, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramLocalParametersI4uivEXT(int program, int target,
			int index, int count, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedProgramStringEXT(int program, int target, int format,
			int len, Buffer string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedRenderbufferStorageEXT(int renderbuffer,
			int internalformat, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedRenderbufferStorageMultisampleCoverageEXT(
			int renderbuffer, int coverageSamples, int colorSamples,
			int internalformat, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedRenderbufferStorageMultisampleEXT(int renderbuffer,
			int samples, int internalformat, int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNewList(int list, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3b(byte nx, byte ny, byte nz) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3bv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3bv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3d(double nx, double ny, double nz) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3h(short nx, short ny, short nz) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3i(int nx, int ny, int nz) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3s(short nx, short ny, short nz) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glObjectPurgeableAPPLE(int objectType, int name, int option) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glObjectUnpurgeableAPPLE(int objectType, int name, int option) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glPNTrianglesfATI(int pname, float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPNTrianglesiATI(int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPassThrough(float token) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPauseTransformFeedbackNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelDataRangeNV(int target, int length, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapfv(int map, int mapsize, FloatBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapfv(int map, int mapsize, long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapfv(int map, int mapsize, float[] values,
			int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapuiv(int map, int mapsize, IntBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapuiv(int map, int mapsize, long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapuiv(int map, int mapsize, int[] values,
			int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapusv(int map, int mapsize, ShortBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapusv(int map, int mapsize, long values_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelMapusv(int map, int mapsize, short[] values,
			int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransferf(int pname, float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransferi(int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransformParameterfEXT(int target, int pname, float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransformParameterfvEXT(int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransformParameterfvEXT(int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransformParameteriEXT(int target, int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransformParameterivEXT(int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelTransformParameterivEXT(int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelZoom(float xfactor, float yfactor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPolygonStipple(ByteBuffer mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPolygonStipple(long mask_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPolygonStipple(byte[] mask, int mask_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPopAttrib() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPopClientAttrib() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPopName() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPrimitiveRestartIndexNV(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPrimitiveRestartNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPrioritizeTextures(int n, IntBuffer textures,
			FloatBuffer priorities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPrioritizeTextures(int n, int[] textures,
			int textures_offset, float[] priorities, int priorities_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBufferParametersIivNV(int target, int buffer,
			int index, int count, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBufferParametersIivNV(int target, int buffer,
			int index, int count, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBufferParametersIuivNV(int target, int buffer,
			int index, int count, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBufferParametersIuivNV(int target, int buffer,
			int index, int count, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBufferParametersfvNV(int target, int buffer,
			int index, int count, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBufferParametersfvNV(int target, int buffer,
			int index, int count, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameter4dARB(int target, int index, double x,
			double y, double z, double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameter4dvARB(int target, int index,
			DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameter4dvARB(int target, int index,
			double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameter4fARB(int target, int index, float x,
			float y, float z, float w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameter4fvARB(int target, int index,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameter4fvARB(int target, int index,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameterI4iNV(int target, int index, int x, int y,
			int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameterI4ivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameterI4ivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameterI4uiNV(int target, int index, int x,
			int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameterI4uivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameterI4uivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameters4fvEXT(int target, int index, int count,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParameters4fvEXT(int target, int index, int count,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParametersI4ivNV(int target, int index, int count,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParametersI4ivNV(int target, int index, int count,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParametersI4uivNV(int target, int index, int count,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramEnvParametersI4uivNV(int target, int index, int count,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameter4dARB(int target, int index, double x,
			double y, double z, double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameter4dvARB(int target, int index,
			DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameter4dvARB(int target, int index,
			double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameter4fARB(int target, int index, float x,
			float y, float z, float w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameter4fvARB(int target, int index,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameter4fvARB(int target, int index,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameterI4iNV(int target, int index, int x,
			int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameterI4ivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameterI4ivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameterI4uiNV(int target, int index, int x,
			int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameterI4uivNV(int target, int index,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameterI4uivNV(int target, int index,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameters4fvEXT(int target, int index,
			int count, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParameters4fvEXT(int target, int index,
			int count, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParametersI4ivNV(int target, int index,
			int count, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParametersI4ivNV(int target, int index,
			int count, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParametersI4uivNV(int target, int index,
			int count, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramLocalParametersI4uivNV(int target, int index,
			int count, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramStringARB(int target, int format, int len,
			String string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramSubroutineParametersuivNV(int target, int count,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramSubroutineParametersuivNV(int target, int count,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1dEXT(int program, int location, double x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1dvEXT(int program, int location, int count,
			DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1dvEXT(int program, int location, int count,
			double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1fEXT(int program, int location, float v0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1fvEXT(int program, int location, int count,
			FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1fvEXT(int program, int location, int count,
			float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1iEXT(int program, int location, int v0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1ivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1ivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1uiEXT(int program, int location, int v0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1uivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1uivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2dEXT(int program, int location, double x,
			double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2dvEXT(int program, int location, int count,
			DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2dvEXT(int program, int location, int count,
			double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2fEXT(int program, int location, float v0,
			float v1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2fvEXT(int program, int location, int count,
			FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2fvEXT(int program, int location, int count,
			float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2iEXT(int program, int location, int v0, int v1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2ivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2ivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2uiEXT(int program, int location, int v0, int v1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2uivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2uivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3dEXT(int program, int location, double x,
			double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3dvEXT(int program, int location, int count,
			DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3dvEXT(int program, int location, int count,
			double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3fEXT(int program, int location, float v0,
			float v1, float v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3fvEXT(int program, int location, int count,
			FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3fvEXT(int program, int location, int count,
			float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3iEXT(int program, int location, int v0,
			int v1, int v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3ivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3ivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3uiEXT(int program, int location, int v0,
			int v1, int v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3uivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3uivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4dEXT(int program, int location, double x,
			double y, double z, double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4dvEXT(int program, int location, int count,
			DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4dvEXT(int program, int location, int count,
			double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4fEXT(int program, int location, float v0,
			float v1, float v2, float v3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4fvEXT(int program, int location, int count,
			FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4fvEXT(int program, int location, int count,
			float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4iEXT(int program, int location, int v0,
			int v1, int v2, int v3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4ivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4ivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4uiEXT(int program, int location, int v0,
			int v1, int v2, int v3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4uivEXT(int program, int location, int count,
			IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4uivEXT(int program, int location, int count,
			int[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3dvEXT(int program, int location,
			int count, boolean transpose, DoubleBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3dvEXT(int program, int location,
			int count, boolean transpose, double[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3fvEXT(int program, int location,
			int count, boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3fvEXT(int program, int location,
			int count, boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramVertexLimitNV(int target, int limit) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProvokingVertexEXT(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPushAttrib(int mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPushClientAttrib(int mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPushClientAttribDefaultEXT(int mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPushName(int name) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2d(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2f(float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2i(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2s(short x, short y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos2sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3d(double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3f(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3i(int x, int y, int z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3s(short x, short y, short z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4d(double x, double y, double z, double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4f(float x, float y, float z, float w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4i(int x, int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4s(short x, short y, short z, short w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRasterPos4sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectd(double x1, double y1, double x2, double y2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectdv(DoubleBuffer v1, DoubleBuffer v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectdv(double[] v1, int v1_offset, double[] v2, int v2_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectf(float x1, float y1, float x2, float y2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectfv(FloatBuffer v1, FloatBuffer v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectfv(float[] v1, int v1_offset, float[] v2, int v2_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRecti(int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectiv(IntBuffer v1, IntBuffer v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectiv(int[] v1, int v1_offset, int[] v2, int v2_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRects(short x1, short y1, short x2, short y2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectsv(ShortBuffer v1, ShortBuffer v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRectsv(short[] v1, int v1_offset, short[] v2, int v2_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glRenderMode(int mode) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glRenderbufferStorageMultisampleCoverageNV(int target,
			int coverageSamples, int colorSamples, int internalformat,
			int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glResetHistogram(int target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glResetMinmax(int target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glResumeTransformFeedbackNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRotated(double angle, double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSampleMaskIndexedNV(int index, int mask) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScaled(double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3b(byte red, byte green, byte blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3bv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3bv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3d(double red, double green, double blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3f(float red, float green, float blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3h(short red, short green, short blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3i(int red, int green, int blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3s(short red, short green, short blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3ub(byte red, byte green, byte blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3ubv(ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3ubv(byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3ui(int red, int green, int blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3uiv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3uiv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3us(short red, short green, short blue) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3usv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColor3usv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColorPointer(int size, int type, int stride,
			Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColorPointer(int size, int type, int stride,
			long pointer_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSelectBuffer(int size, IntBuffer buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSelectPerfMonitorCountersAMD(int monitor, boolean enable,
			int group, int numCounters, IntBuffer counterList) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSelectPerfMonitorCountersAMD(int monitor, boolean enable,
			int group, int numCounters, int[] counterList,
			int counterList_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSeparableFilter2D(int target, int internalformat, int width,
			int height, int format, int type, Buffer row, Buffer column) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSeparableFilter2D(int target, int internalformat, int width,
			int height, int format, int type, long row_buffer_offset,
			long column_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSetFenceAPPLE(int fence) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSetFenceNV(int fence, int condition) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSetInvariantEXT(int id, int type, Buffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSetLocalConstantEXT(int id, int type, Buffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderOp1EXT(int op, int res, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderOp2EXT(int op, int res, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderOp3EXT(int op, int res, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderSourceARB(int shaderObj, int count, String[] string,
			IntBuffer length) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderSourceARB(int shaderObj, int count, String[] string,
			int[] length, int length_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilClearTagEXT(int stencilTagBits, int stencilClearTag) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStringMarkerGREMEDY(int len, Buffer string) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSwapAPPLE() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSwizzleEXT(int res, int in, int outX, int outY, int outZ,
			int outW) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glTestFenceAPPLE(int fence) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glTestFenceNV(int fence) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glTestObjectAPPLE(int object, int name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glTexCoord1d(double s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1f(float s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1h(short s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1i(int s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1s(short s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord1sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2d(double s, double t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2f(float s, float t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2h(short s, short t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2i(int s, int t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2s(short s, short t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord2sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3d(double s, double t, double r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3f(float s, float t, float r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3h(short s, short t, short r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3i(int s, int t, int r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3s(short s, short t, short r) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4d(double s, double t, double r, double q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4f(float s, float t, float r, float q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4h(short s, short t, short r, short q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4i(int s, int t, int r, int q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4s(short s, short t, short r, short q) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoord4sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGend(int coord, int pname, double param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGendv(int coord, int pname, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGendv(int coord, int pname, double[] params,
			int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexRenderbufferNV(int target, int renderbuffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureBarrierNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureBufferEXT(int texture, int target, int internalformat,
			int buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage1DEXT(int texture, int target, int level,
			int internalformat, int width, int border, int format, int type,
			Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage2DEXT(int texture, int target, int level,
			int internalformat, int width, int height, int border, int format,
			int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage3DEXT(int texture, int target, int level,
			int internalformat, int width, int height, int depth, int border,
			int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureLightEXT(int pname) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureMaterialEXT(int face, int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureNormalEXT(int mode) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterIivEXT(int texture, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterIivEXT(int texture, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterIuivEXT(int texture, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterIuivEXT(int texture, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterfEXT(int texture, int target, int pname,
			float param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterfvEXT(int texture, int target, int pname,
			FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterfvEXT(int texture, int target, int pname,
			float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameteriEXT(int texture, int target, int pname,
			int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterivEXT(int texture, int target, int pname,
			IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureParameterivEXT(int texture, int target, int pname,
			int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureRangeAPPLE(int target, int length, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureRenderbufferEXT(int texture, int target,
			int renderbuffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureSubImage1DEXT(int texture, int target, int level,
			int xoffset, int width, int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureSubImage2DEXT(int texture, int target, int level,
			int xoffset, int yoffset, int width, int height, int format,
			int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureSubImage3DEXT(int texture, int target, int level,
			int xoffset, int yoffset, int zoffset, int width, int height,
			int depth, int format, int type, Buffer pixels) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTranslated(double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1fARB(int location, float v0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1fvARB(int location, int count, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1fvARB(int location, int count, float[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1iARB(int location, int v0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1ivARB(int location, int count, IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1ivARB(int location, int count, int[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2fARB(int location, float v0, float v1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2fvARB(int location, int count, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2fvARB(int location, int count, float[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2iARB(int location, int v0, int v1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2ivARB(int location, int count, IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2ivARB(int location, int count, int[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3fARB(int location, float v0, float v1, float v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3fvARB(int location, int count, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3fvARB(int location, int count, float[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3iARB(int location, int v0, int v1, int v2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3ivARB(int location, int count, IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3ivARB(int location, int count, int[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4fARB(int location, float v0, float v1, float v2,
			float v3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4fvARB(int location, int count, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4fvARB(int location, int count, float[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4iARB(int location, int v0, int v1, int v2, int v3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4ivARB(int location, int count, IntBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4ivARB(int location, int count, int[] value,
			int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformBufferEXT(int program, int location, int buffer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2fvARB(int location, int count,
			boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2fvARB(int location, int count,
			boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3fvARB(int location, int count,
			boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3fvARB(int location, int count,
			boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4fvARB(int location, int count,
			boolean transpose, FloatBuffer value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4fvARB(int location, int count,
			boolean transpose, float[] value, int value_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUnlockArraysEXT() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glUnmapNamedBufferEXT(int buffer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glUseProgramObjectARB(int programObj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUFiniNV() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUGetSurfaceivNV(long surface, int pname, int bufSize,
			IntBuffer length, IntBuffer values) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUGetSurfaceivNV(long surface, int pname, int bufSize,
			int[] length, int length_offset, int[] values, int values_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUInitNV(Buffer vdpDevice, Buffer getProcAddress) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUIsSurfaceNV(long surface) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUMapSurfacesNV(int numSurfaces, LongBuffer surfaces) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUMapSurfacesNV(int numSurfaces, long[] surfaces,
			int surfaces_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long glVDPAURegisterOutputSurfaceNV(Buffer vdpSurface, int target,
			int numTextureNames, IntBuffer textureNames) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long glVDPAURegisterOutputSurfaceNV(Buffer vdpSurface, int target,
			int numTextureNames, int[] textureNames, int textureNames_offset) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long glVDPAURegisterVideoSurfaceNV(Buffer vdpSurface, int target,
			int numTextureNames, IntBuffer textureNames) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long glVDPAURegisterVideoSurfaceNV(Buffer vdpSurface, int target,
			int numTextureNames, int[] textureNames, int textureNames_offset) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glVDPAUSurfaceAccessNV(long surface, int access) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUUnmapSurfacesNV(int numSurface, LongBuffer surfaces) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUUnmapSurfacesNV(int numSurface, long[] surfaces,
			int surfaces_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVDPAUUnregisterSurfaceNV(long surface) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glValidateProgramARB(int programObj) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantPointerEXT(int id, int type, int stride, Buffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantPointerEXT(int id, int type, int stride,
			long addr_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantbvEXT(int id, ByteBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantbvEXT(int id, byte[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantdvEXT(int id, DoubleBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantdvEXT(int id, double[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantfvEXT(int id, FloatBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantfvEXT(int id, float[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantivEXT(int id, IntBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantivEXT(int id, int[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantsvEXT(int id, ShortBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantsvEXT(int id, short[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantubvEXT(int id, ByteBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantubvEXT(int id, byte[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantuivEXT(int id, IntBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantuivEXT(int id, int[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantusvEXT(int id, ShortBuffer addr) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVariantusvEXT(int id, short[] addr, int addr_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2d(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2f(float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2h(short x, short y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2i(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2s(short x, short y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex2sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3d(double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3f(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3h(short x, short y, short z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3i(int x, int y, int z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3s(short x, short y, short z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4d(double x, double y, double z, double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4f(float x, float y, float z, float w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4h(short x, short y, short z, short w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4hv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4hv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4i(int x, int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4s(short x, short y, short z, short w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertex4sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexArrayParameteriAPPLE(int pname, int param) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexArrayRangeAPPLE(int length, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexArrayRangeNV(int length, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1dARB(int index, double x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1dvARB(int index, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1dvARB(int index, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1fARB(int index, float x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1fvARB(int index, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1fvARB(int index, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1h(int index, short x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1hv(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1hv(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1sARB(int index, short x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1svARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1svARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2dARB(int index, double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2dvARB(int index, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2dvARB(int index, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2fARB(int index, float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2fvARB(int index, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2fvARB(int index, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2h(int index, short x, short y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2hv(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2hv(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2sARB(int index, short x, short y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2svARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2svARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3dARB(int index, double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3dvARB(int index, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3dvARB(int index, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3fARB(int index, float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3fvARB(int index, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3fvARB(int index, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3h(int index, short x, short y, short z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3hv(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3hv(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3sARB(int index, short x, short y, short z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3svARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3svARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NbvARB(int index, ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NbvARB(int index, byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NivARB(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NivARB(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NsvARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NsvARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NubARB(int index, byte x, byte y, byte z, byte w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NubvARB(int index, ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NubvARB(int index, byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NuivARB(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NuivARB(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NusvARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4NusvARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4bvARB(int index, ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4bvARB(int index, byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4dARB(int index, double x, double y, double z,
			double w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4dvARB(int index, DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4dvARB(int index, double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4fARB(int index, float x, float y, float z,
			float w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4fvARB(int index, FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4fvARB(int index, float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4h(int index, short x, short y, short z, short w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4hv(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4hv(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4ivARB(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4ivARB(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4sARB(int index, short x, short y, short z,
			short w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4svARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4svARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4ubvARB(int index, ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4ubvARB(int index, byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4uivARB(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4uivARB(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4usvARB(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4usvARB(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1iEXT(int index, int x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1ivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1ivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1uiEXT(int index, int x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1uivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1uivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2iEXT(int index, int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2ivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2ivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2uiEXT(int index, int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2uivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2uivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3iEXT(int index, int x, int y, int z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3ivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3ivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3uiEXT(int index, int x, int y, int z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3uivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3uivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4bvEXT(int index, ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4bvEXT(int index, byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4iEXT(int index, int x, int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4svEXT(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4svEXT(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ubvEXT(int index, ByteBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ubvEXT(int index, byte[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4uiEXT(int index, int x, int y, int z, int w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4uivEXT(int index, IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4uivEXT(int index, int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4usvEXT(int index, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4usvEXT(int index, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribIPointerEXT(int index, int size, int type,
			int stride, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1i64NV(int index, long x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1i64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1i64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1ui64NV(int index, long x) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1ui64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1ui64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2i64NV(int index, long x, long y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2i64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2i64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2ui64NV(int index, long x, long y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2ui64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2ui64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3i64NV(int index, long x, long y, long z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3i64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3i64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3ui64NV(int index, long x, long y, long z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3ui64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3ui64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4i64NV(int index, long x, long y, long z, long w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4i64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4i64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4ui64NV(int index, long x, long y, long z, long w) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4ui64vNV(int index, LongBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4ui64vNV(int index, long[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribLFormatNV(int index, int size, int type,
			int stride) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribPointerARB(int index, int size, int type,
			boolean normalized, int stride, Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribPointerARB(int index, int size, int type,
			boolean normalized, int stride, long pointer_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs1hv(int index, int n, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs1hv(int index, int n, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs2hv(int index, int n, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs2hv(int index, int n, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs3hv(int index, int n, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs3hv(int index, int n, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs4hv(int index, int n, ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribs4hv(int index, int n, short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexBlendARB(int count) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeightPointerEXT(int size, int type, int stride,
			Buffer pointer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeightPointerEXT(int size, int type, int stride,
			long pointer_buffer_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeightfEXT(float weight) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeightfvEXT(FloatBuffer weight) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeightfvEXT(float[] weight, int weight_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeighth(short weight) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeighthv(ShortBuffer weight) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexWeighthv(short[] weight, int weight_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glVideoCaptureNV(int video_capture_slot, IntBuffer sequence_num,
			LongBuffer capture_time) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glVideoCaptureNV(int video_capture_slot, int[] sequence_num,
			int sequence_num_offset, long[] capture_time,
			int capture_time_offset) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glVideoCaptureStreamParameterdvNV(int video_capture_slot,
			int stream, int pname, DoubleBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVideoCaptureStreamParameterdvNV(int video_capture_slot,
			int stream, int pname, double[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVideoCaptureStreamParameterfvNV(int video_capture_slot,
			int stream, int pname, FloatBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVideoCaptureStreamParameterfvNV(int video_capture_slot,
			int stream, int pname, float[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVideoCaptureStreamParameterivNV(int video_capture_slot,
			int stream, int pname, IntBuffer params) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVideoCaptureStreamParameterivNV(int video_capture_slot,
			int stream, int pname, int[] params, int params_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightbvARB(int size, ByteBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightbvARB(int size, byte[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightdvARB(int size, DoubleBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightdvARB(int size, double[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightfvARB(int size, FloatBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightfvARB(int size, float[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightivARB(int size, IntBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightivARB(int size, int[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightsvARB(int size, ShortBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightsvARB(int size, short[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightubvARB(int size, ByteBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightubvARB(int size, byte[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightuivARB(int size, IntBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightuivARB(int size, int[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightusvARB(int size, ShortBuffer weights) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightusvARB(int size, short[] weights, int weights_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2d(double x, double y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2f(float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2i(int x, int y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2s(short x, short y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos2sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3d(double x, double y, double z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3dv(DoubleBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3dv(double[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3f(float x, float y, float z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3fv(FloatBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3fv(float[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3i(int x, int y, int z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3iv(IntBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3iv(int[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3s(short x, short y, short z) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3sv(ShortBuffer v) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWindowPos3sv(short[] v, int v_offset) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWriteMaskEXT(int res, int in, int outX, int outY, int outZ,
			int outW) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glAlphaFunc(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClientActiveTexture(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4ub(byte arg0, byte arg1, byte arg2, byte arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCurrentPaletteMatrix(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogf(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogfv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogfv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFrustum(double arg0, double arg1, double arg2, double arg3,
			double arg4, double arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLightfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetLightfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMaterialfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMaterialfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexEnvfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexEnvfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexEnviv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexEnviv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexGenfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexGenfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexGeniv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexGeniv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightModelf(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightModelfv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightModelfv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightf(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLogicOp(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixIndexPointer(int arg0, int arg1, int arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoord4f(int arg0, float arg1, float arg2, float arg3,
			float arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormal3f(float arg0, float arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glOrtho(double arg0, double arg1, double arg2, double arg3,
			double arg4, double arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointParameterf(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointParameterfv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointParameterfv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointSize(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexEnvf(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexEnvfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexEnvfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexEnvi(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexEnviv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexEnviv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGenf(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGenfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGenfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGeni(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGeniv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexGeniv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glWeightPointer(int arg0, int arg1, int arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glActiveTexture(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindBuffer(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindFramebuffer(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindRenderbuffer(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindTexture(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendEquation(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendEquationSeparate(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFunc(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFuncSeparate(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBufferData(int arg0, long arg1, Buffer arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBufferSubData(int arg0, long arg1, long arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glCheckFramebufferStatus(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glClear(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearColor(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearDepthf(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearStencil(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorMask(boolean arg0, boolean arg1, boolean arg2,
			boolean arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexImage2D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, Buffer arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexImage2D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, long arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexSubImage2D(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, int arg6, int arg7, Buffer arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexSubImage2D(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, int arg6, int arg7, long arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTexImage2D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTexSubImage2D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCullFace(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteBuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteBuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteFramebuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteFramebuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteRenderbuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteRenderbuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteTextures(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteTextures(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthFunc(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthMask(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthRangef(float arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisable(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawArrays(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElements(int arg0, int arg1, int arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElements(int arg0, int arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnable(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFinish() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferRenderbuffer(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTexture2D(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFrontFace(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenBuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenBuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenFramebuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenFramebuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenRenderbuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenRenderbuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenTextures(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenTextures(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenerateMipmap(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBooleanv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBooleanv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBufferParameteriv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBufferParameteriv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetError() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetFloatv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFloatv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFramebufferAttachmentParameteriv(int arg0, int arg1,
			int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFramebufferAttachmentParameteriv(int arg0, int arg1,
			int arg2, int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetGraphicsResetStatus() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetIntegerv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetRenderbufferParameteriv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetRenderbufferParameteriv(int arg0, int arg1, int[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String glGetString(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void glGetTexParameterfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameterfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameteriv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameteriv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformfv(int arg0, int arg1, int arg2, FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformfv(int arg0, int arg1, int arg2, float[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformiv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glHint(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glIsBuffer(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsEnabled(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsFramebuffer(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsRenderbuffer(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsTexture(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glLineWidth(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ByteBuffer glMapBuffer(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void glPixelStorei(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPolygonOffset(float arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glReadPixels(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, Buffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glReadPixels(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, long arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glReadnPixels(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, Buffer arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRenderbufferStorage(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSampleCoverage(float arg0, boolean arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScissor(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilFunc(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilMask(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilOp(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage2D(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, int arg7, Buffer arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage2D(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, int arg7, long arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterf(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameteri(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameteriv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameteriv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexStorage1D(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexStorage2D(int arg0, int arg1, int arg2, int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexStorage3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexSubImage2D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, Buffer arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexSubImage2D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, long arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureStorage1DEXT(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureStorage2DEXT(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureStorage3DEXT(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glUnmapBuffer(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glViewport(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int getBoundFramebuffer(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public GLContext getContext() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getDefaultDrawFramebuffer() {
		// TODO Auto-generated method stub
		return 0;
        }

	@Override
	public int getDefaultReadFramebuffer() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getExtension(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL getGL() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL2 getGL2() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL2ES1 getGL2ES1() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL2ES2 getGL2ES2() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL2GL3 getGL2GL3() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL3 getGL3() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL3bc getGL3bc() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL4 getGL4() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GL4bc getGL4bc() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GLES1 getGLES1() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GLES2 getGLES2() throws GLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GLProfile getGLProfile() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMaxRenderbufferSamples() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object getPlatformGLExtensions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSwapInterval() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glClearDepth(double arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthRange(double arg0, double arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetBoundBuffer(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long glGetBufferSize(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean glIsVBOArrayEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsVBOElementArrayEnabled() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasBasicFBOSupport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasFullFBOSupport() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasGLSL() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isExtensionAvailable(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isFunctionAvailable(String arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL2ES1() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL2ES2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL2GL3() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL3() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL3bc() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL4() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGL4bc() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGLES() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGLES1() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGLES2() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isGLES2Compatible() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isNPOTTextureAvailable() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isTextureFormatBGRA8888Available() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setSwapInterval(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFrustumf(float arg0, float arg1, float arg2, float arg3,
			float arg4, float arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadIdentity() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadMatrixf(FloatBuffer arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLoadMatrixf(float[] arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMatrixMode(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultMatrixf(FloatBuffer arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultMatrixf(float[] arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glOrthof(float arg0, float arg1, float arg2, float arg3,
			float arg4, float arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPopMatrix() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPushMatrix() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRotatef(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScalef(float arg0, float arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTranslatef(float arg0, float arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColor4f(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorPointer(GLArrayData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorPointer(int arg0, int arg1, int arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorPointer(int arg0, int arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableClientState(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableClientState(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalPointer(GLArrayData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalPointer(int arg0, int arg1, Buffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalPointer(int arg0, int arg1, long arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordPointer(GLArrayData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordPointer(int arg0, int arg1, int arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordPointer(int arg0, int arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexPointer(GLArrayData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexPointer(int arg0, int arg1, int arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexPointer(int arg0, int arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glLightfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMaterialf(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMaterialfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMaterialfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShadeModel(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glActiveShaderProgram(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ByteBuffer glAllocateMemoryNV(int arg0, float arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void glBeginConditionalRender(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginQueryIndexed(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginTransformFeedback(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindBufferBase(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindBufferRange(int arg0, int arg1, int arg2, long arg3,
			long arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindFragDataLocation(int arg0, int arg1, String arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindFragDataLocationIndexed(int arg0, int arg1, int arg2,
			String arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindImageTexture(int arg0, int arg1, int arg2, boolean arg3,
			int arg4, int arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindProgramPipeline(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindSampler(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindTransformFeedback(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindVertexArray(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendEquationSeparatei(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendEquationi(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFuncSeparatei(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendFunci(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlitFramebuffer(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, int arg8, int arg9) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBufferAddressRangeNV(int arg0, int arg1, long arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClampColor(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferfi(int arg0, int arg1, float arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glClearBufferuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorFormatNV(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorMaski(int arg0, boolean arg1, boolean arg2,
			boolean arg3, boolean arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorP3ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorP3uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorP3uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorP4ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorP4uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glColorP4uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompileShaderIncludeARB(int arg0, int arg1, String[] arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompileShaderIncludeARB(int arg0, int arg1, String[] arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexImage1D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, Buffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexImage1D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, long arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexSubImage1D(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, Buffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexSubImage1D(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, long arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyBufferSubData(int arg0, int arg1, long arg2, long arg3,
			long arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTexImage1D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTexSubImage1D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glCreateShaderProgramv(int arg0, int arg1, PointerBuffer arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long glCreateSyncFromCLeventARB(Buffer arg0, Buffer arg1, int arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glDebugMessageControlARB(int arg0, int arg1, int arg2,
			int arg3, IntBuffer arg4, boolean arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDebugMessageControlARB(int arg0, int arg1, int arg2,
			int arg3, int[] arg4, int arg5, boolean arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDebugMessageEnableAMD(int arg0, int arg1, int arg2,
			IntBuffer arg3, boolean arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDebugMessageEnableAMD(int arg0, int arg1, int arg2,
			int[] arg3, int arg4, boolean arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDebugMessageInsertAMD(int arg0, int arg1, int arg2, int arg3,
			String arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDebugMessageInsertARB(int arg0, int arg1, int arg2, int arg3,
			int arg4, String arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteNamedStringARB(int arg0, String arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteProgramPipelines(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteProgramPipelines(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteSamplers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteSamplers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteTransformFeedbacks(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteTransformFeedbacks(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteVertexArrays(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteVertexArrays(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthRangeArrayv(int arg0, int arg1, DoubleBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthRangeArrayv(int arg0, int arg1, double[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDepthRangeIndexed(int arg0, double arg1, double arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisablei(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawArraysInstanced(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawArraysInstancedBaseInstance(int arg0, int arg1, int arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawBuffer(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawBuffers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawBuffers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElementsBaseVertex(int arg0, int arg1, int arg2,
			Buffer arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElementsInstanced(int arg0, int arg1, int arg2,
			Buffer arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElementsInstancedBaseInstance(int arg0, int arg1,
			int arg2, Buffer arg3, int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElementsInstancedBaseVertex(int arg0, int arg1, int arg2,
			Buffer arg3, int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawElementsInstancedBaseVertexBaseInstance(int arg0,
			int arg1, int arg2, Buffer arg3, int arg4, int arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawRangeElements(int arg0, int arg1, int arg2, int arg3,
			int arg4, Buffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawRangeElements(int arg0, int arg1, int arg2, int arg3,
			int arg4, long arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawRangeElementsBaseVertex(int arg0, int arg1, int arg2,
			int arg3, int arg4, Buffer arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawTransformFeedback(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawTransformFeedbackInstanced(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawTransformFeedbackStream(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDrawTransformFeedbackStreamInstanced(int arg0, int arg1,
			int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEdgeFlagFormatNV(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnablei(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndConditionalRender() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndQueryIndexed(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndTransformFeedback() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFlushMappedBufferRange(int arg0, long arg1, long arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFogCoordFormatNV(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTexture1D(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureARB(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureFaceARB(int arg0, int arg1, int arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureLayer(int arg0, int arg1, int arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTextureLayerARB(int arg0, int arg1, int arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenProgramPipelines(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenProgramPipelines(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenSamplers(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenSamplers(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenTransformFeedbacks(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenTransformFeedbacks(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenVertexArrays(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenVertexArrays(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveAtomicCounterBufferiv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveAtomicCounterBufferiv(int arg0, int arg1, int arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveSubroutineName(int arg0, int arg1, int arg2,
			int arg3, IntBuffer arg4, ByteBuffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveSubroutineName(int arg0, int arg1, int arg2,
			int arg3, int[] arg4, int arg5, byte[] arg6, int arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveSubroutineUniformName(int arg0, int arg1, int arg2,
			int arg3, IntBuffer arg4, ByteBuffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveSubroutineUniformName(int arg0, int arg1, int arg2,
			int arg3, int[] arg4, int arg5, byte[] arg6, int arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveSubroutineUniformiv(int arg0, int arg1, int arg2,
			int arg3, IntBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveSubroutineUniformiv(int arg0, int arg1, int arg2,
			int arg3, int[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformBlockName(int arg0, int arg1, int arg2,
			IntBuffer arg3, ByteBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformBlockName(int arg0, int arg1, int arg2,
			int[] arg3, int arg4, byte[] arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformBlockiv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformBlockiv(int arg0, int arg1, int arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformName(int arg0, int arg1, int arg2,
			IntBuffer arg3, ByteBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformName(int arg0, int arg1, int arg2,
			int[] arg3, int arg4, byte[] arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformsiv(int arg0, int arg1, IntBuffer arg2,
			int arg3, IntBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniformsiv(int arg0, int arg1, int[] arg2, int arg3,
			int arg4, int[] arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBooleani_v(int arg0, int arg1, ByteBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBooleani_v(int arg0, int arg1, byte[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBufferParameterui64vNV(int arg0, int arg1, LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBufferParameterui64vNV(int arg0, int arg1, long[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetBufferSubData(int arg0, long arg1, long arg2, Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetCompressedTexImage(int arg0, int arg1, Buffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetCompressedTexImage(int arg0, int arg1, long arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetDebugMessageLogAMD(int arg0, int arg1, IntBuffer arg2,
			IntBuffer arg3, IntBuffer arg4, IntBuffer arg5, ByteBuffer arg6) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glGetDebugMessageLogAMD(int arg0, int arg1, int[] arg2,
			int arg3, int[] arg4, int arg5, int[] arg6, int arg7, int[] arg8,
			int arg9, byte[] arg10, int arg11) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glGetDebugMessageLogARB(int arg0, int arg1, IntBuffer arg2,
			IntBuffer arg3, IntBuffer arg4, IntBuffer arg5, IntBuffer arg6,
			ByteBuffer arg7) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glGetDebugMessageLogARB(int arg0, int arg1, int[] arg2,
			int arg3, int[] arg4, int arg5, int[] arg6, int arg7, int[] arg8,
			int arg9, int[] arg10, int arg11, byte[] arg12, int arg13) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetDoublei_v(int arg0, int arg1, DoubleBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetDoublei_v(int arg0, int arg1, double[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetDoublev(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetDoublev(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFloati_v(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetFloati_v(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetFragDataIndex(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glGetFragDataLocation(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetIntegeri_v(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegeri_v(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerui64i_vNV(int arg0, int arg1, LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerui64i_vNV(int arg0, int arg1, long[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerui64vNV(int arg0, LongBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetIntegerui64vNV(int arg0, long[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInternalformativ(int arg0, int arg1, int arg2, int arg3,
			IntBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetInternalformativ(int arg0, int arg1, int arg2, int arg3,
			int[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultisamplefv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetMultisamplefv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedBufferParameterui64vNV(int arg0, int arg1,
			LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedBufferParameterui64vNV(int arg0, int arg1,
			long[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedStringARB(int arg0, String arg1, int arg2,
			IntBuffer arg3, ByteBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedStringARB(int arg0, String arg1, int arg2,
			int[] arg3, int arg4, byte[] arg5, int arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedStringivARB(int arg0, String arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetNamedStringivARB(int arg0, String arg1, int arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramPipelineInfoLog(int arg0, int arg1, IntBuffer arg2,
			ByteBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramPipelineInfoLog(int arg0, int arg1, int[] arg2,
			int arg3, byte[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramPipelineiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramPipelineiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramStageiv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramStageiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryIndexediv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryIndexediv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjecti64v(int arg0, int arg1, LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjecti64v(int arg0, int arg1, long[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectui64v(int arg0, int arg1, LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectui64v(int arg0, int arg1, long[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameterIiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameterIiv(int arg0, int arg1, int[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameterIuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameterIuiv(int arg0, int arg1, int[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameterfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameterfv(int arg0, int arg1, float[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameteriv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetSamplerParameteriv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String glGetStringi(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int glGetSubroutineIndex(int arg0, int arg1, String arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glGetSubroutineUniformLocation(int arg0, int arg1, String arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetTexImage(int arg0, int arg1, int arg2, int arg3,
			Buffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexImage(int arg0, int arg1, int arg2, int arg3, long arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexLevelParameterfv(int arg0, int arg1, int arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexLevelParameterfv(int arg0, int arg1, int arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexLevelParameteriv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexLevelParameteriv(int arg0, int arg1, int arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameterIiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameterIiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameterIuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTexParameterIuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTransformFeedbackVarying(int arg0, int arg1, int arg2,
			IntBuffer arg3, IntBuffer arg4, IntBuffer arg5, ByteBuffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetTransformFeedbackVarying(int arg0, int arg1, int arg2,
			int[] arg3, int arg4, int[] arg5, int arg6, int[] arg7, int arg8,
			byte[] arg9, int arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetUniformBlockIndex(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetUniformIndices(int arg0, int arg1, String[] arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformIndices(int arg0, int arg1, String[] arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformSubroutineuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformSubroutineuiv(int arg0, int arg1, int[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformui64vNV(int arg0, int arg1, LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformui64vNV(int arg0, int arg1, long[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribIuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribLdv(int arg0, int arg1, DoubleBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribLdv(int arg0, int arg1, double[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribdv(int arg0, int arg1, DoubleBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribdv(int arg0, int arg1, double[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnColorTable(int arg0, int arg1, int arg2, int arg3,
			Buffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnCompressedTexImage(int arg0, int arg1, int arg2,
			Buffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnConvolutionFilter(int arg0, int arg1, int arg2, int arg3,
			Buffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnHistogram(int arg0, boolean arg1, int arg2, int arg3,
			int arg4, Buffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMapdv(int arg0, int arg1, int arg2, DoubleBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMapdv(int arg0, int arg1, int arg2, double[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMapfv(int arg0, int arg1, int arg2, FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMapfv(int arg0, int arg1, int arg2, float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMapiv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMapiv(int arg0, int arg1, int arg2, int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnMinmax(int arg0, boolean arg1, int arg2, int arg3,
			int arg4, Buffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPixelMapfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPixelMapfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPixelMapuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPixelMapuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPixelMapusv(int arg0, int arg1, ShortBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPixelMapusv(int arg0, int arg1, short[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPolygonStipple(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnPolygonStipple(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnSeparableFilter(int arg0, int arg1, int arg2, int arg3,
			Buffer arg4, int arg5, Buffer arg6, Buffer arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnTexImage(int arg0, int arg1, int arg2, int arg3,
			int arg4, Buffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformdv(int arg0, int arg1, int arg2, DoubleBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformdv(int arg0, int arg1, int arg2, double[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformuiv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetnUniformuiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public long glImportSyncEXT(int arg0, long arg1, int arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glIndexFormatNV(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glIsBufferResidentNV(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsEnabledi(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsNamedBufferResidentNV(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsNamedStringARB(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsProgramPipeline(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsSampler(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsTransformFeedback(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsVertexArray(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glMakeBufferNonResidentNV(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMakeBufferResidentNV(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMakeNamedBufferNonResidentNV(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMakeNamedBufferResidentNV(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ByteBuffer glMapBufferRange(int arg0, long arg1, long arg2, int arg3) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void glMemoryBarrier(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMinSampleShading(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiDrawArrays(int arg0, IntBuffer arg1, IntBuffer arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiDrawArrays(int arg0, int[] arg1, int arg2, int[] arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiDrawArraysIndirectAMD(int arg0, Buffer arg1, int arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiDrawElements(int arg0, IntBuffer arg1, int arg2,
			PointerBuffer arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiDrawElements(int arg0, int[] arg1, int arg2, int arg3,
			PointerBuffer arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiDrawElementsIndirectAMD(int arg0, int arg1, Buffer arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP1ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP1uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP1uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP2ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP2uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP2uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP3ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP3uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP3uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP4ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP4uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glMultiTexCoordP4uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNamedStringARB(int arg0, int arg1, String arg2, int arg3,
			String arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalFormatNV(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalP3ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalP3uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glNormalP3uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPauseTransformFeedback() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPixelStoref(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointParameteri(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointParameteriv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPointParameteriv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPolygonMode(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glPrimitiveRestartIndex(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramParameteri(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramParameteriARB(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1d(int arg0, int arg1, double arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1dv(int arg0, int arg1, int arg2,
			DoubleBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1dv(int arg0, int arg1, int arg2,
			double[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1f(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1fv(int arg0, int arg1, int arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1fv(int arg0, int arg1, int arg2, float[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1i(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1iv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1iv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1uiv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform1uiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2d(int arg0, int arg1, double arg2, double arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2dv(int arg0, int arg1, int arg2,
			DoubleBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2dv(int arg0, int arg1, int arg2,
			double[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2f(int arg0, int arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2fv(int arg0, int arg1, int arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2fv(int arg0, int arg1, int arg2, float[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2i(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2iv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2iv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2ui(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2uiv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform2uiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3d(int arg0, int arg1, double arg2,
			double arg3, double arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3dv(int arg0, int arg1, int arg2,
			DoubleBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3dv(int arg0, int arg1, int arg2,
			double[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3f(int arg0, int arg1, float arg2, float arg3,
			float arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3fv(int arg0, int arg1, int arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3fv(int arg0, int arg1, int arg2, float[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3i(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3iv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3iv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3ui(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3uiv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform3uiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4d(int arg0, int arg1, double arg2,
			double arg3, double arg4, double arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4dv(int arg0, int arg1, int arg2,
			DoubleBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4dv(int arg0, int arg1, int arg2,
			double[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4f(int arg0, int arg1, float arg2, float arg3,
			float arg4, float arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4fv(int arg0, int arg1, int arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4fv(int arg0, int arg1, int arg2, float[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4i(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4iv(int arg0, int arg1, int arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4iv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4ui(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4uiv(int arg0, int arg1, int arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniform4uiv(int arg0, int arg1, int arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x3fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix2x4fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x2fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix3x4fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x2fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3dv(int arg0, int arg1, int arg2,
			boolean arg3, DoubleBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3dv(int arg0, int arg1, int arg2,
			boolean arg3, double[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3fv(int arg0, int arg1, int arg2,
			boolean arg3, FloatBuffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformMatrix4x3fv(int arg0, int arg1, int arg2,
			boolean arg3, float[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformui64NV(int arg0, int arg1, long arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformui64vNV(int arg0, int arg1, int arg2,
			LongBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramUniformui64vNV(int arg0, int arg1, int arg2,
			long[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProvokingVertex(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glQueryCounter(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glReadBuffer(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glRenderbufferStorageMultisample(int arg0, int arg1, int arg2,
			int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glResumeTransformFeedback() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSampleMaski(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterIiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterIiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterIuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterIuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterf(int arg0, int arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameterfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameteri(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameteriv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSamplerParameteriv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScissorArrayv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScissorArrayv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScissorIndexed(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScissorIndexedv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glScissorIndexedv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColorFormatNV(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColorP3ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColorP3uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSecondaryColorP3uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSetMultisamplefvAMD(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glSetMultisamplefvAMD(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilOpValueAMD(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTessellationFactorAMD(float arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTessellationModeAMD(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexBuffer(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordFormatNV(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP1ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP1uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP1uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP2ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP2uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP2uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP3ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP3uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP3uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP4ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP4uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexCoordP4uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage1D(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, Buffer arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage1D(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, long arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage2DMultisample(int arg0, int arg1, int arg2, int arg3,
			int arg4, boolean arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage2DMultisampleCoverageNV(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, boolean arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage3DMultisample(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, boolean arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage3DMultisampleCoverageNV(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, int arg6, boolean arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterIiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterIiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterIuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexParameterIuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexSubImage1D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, Buffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexSubImage1D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, long arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage2DMultisampleCoverageNV(int arg0, int arg1,
			int arg2, int arg3, int arg4, int arg5, int arg6, boolean arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage2DMultisampleNV(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, boolean arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage3DMultisampleCoverageNV(int arg0, int arg1,
			int arg2, int arg3, int arg4, int arg5, int arg6, int arg7,
			boolean arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTextureImage3DMultisampleNV(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, int arg6, boolean arg7) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTransformFeedbackVaryings(int arg0, int arg1, String[] arg2,
			int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3ui(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4ui(int arg0, int arg1, int arg2, int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4uiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4uiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformBlockBinding(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2x3fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2x3fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2x4fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2x4fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3x2fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3x2fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3x4fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3x4fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4x2fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4x2fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4x3fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4x3fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformSubroutinesuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformSubroutinesuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformui64NV(int arg0, long arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformui64vNV(int arg0, int arg1, LongBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformui64vNV(int arg0, int arg1, long[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUseProgramStages(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glValidateProgramPipeline(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1d(int arg0, double arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1s(int arg0, short arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1sv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1sv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2d(int arg0, double arg1, double arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2s(int arg0, short arg1, short arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2sv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2sv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3d(int arg0, double arg1, double arg2, double arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3s(int arg0, short arg1, short arg2, short arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3sv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3sv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nbv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nbv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Niv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Niv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nsv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nsv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nub(int arg0, byte arg1, byte arg2, byte arg3,
			byte arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nubv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nubv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nuiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nuiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nusv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4Nusv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4bv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4bv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4d(int arg0, double arg1, double arg2,
			double arg3, double arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4iv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4iv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4s(int arg0, short arg1, short arg2, short arg3,
			short arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4sv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4sv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4ubv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4ubv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4usv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4usv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribFormatNV(int arg0, int arg1, int arg2,
			boolean arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1i(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1iv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1iv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI1uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2i(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2iv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2iv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2ui(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI2uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3i(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3iv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3iv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3ui(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI3uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4bv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4bv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4i(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4iv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4iv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4sv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4sv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ubv(int arg0, ByteBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ubv(int arg0, byte[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4ui(int arg0, int arg1, int arg2, int arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4usv(int arg0, ShortBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribI4usv(int arg0, short[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribIFormatNV(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribIPointer(int arg0, int arg1, int arg2, int arg3,
			Buffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribIPointer(int arg0, int arg1, int arg2, int arg3,
			long arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1d(int arg0, double arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL1dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2d(int arg0, double arg1, double arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL2dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3d(int arg0, double arg1, double arg2,
			double arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL3dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4d(int arg0, double arg1, double arg2,
			double arg3, double arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4dv(int arg0, DoubleBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribL4dv(int arg0, double[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribLPointer(int arg0, int arg1, int arg2, int arg3,
			Buffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribLPointer(int arg0, int arg1, int arg2, int arg3,
			long arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP1ui(int arg0, int arg1, boolean arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP1uiv(int arg0, int arg1, boolean arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP1uiv(int arg0, int arg1, boolean arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP2ui(int arg0, int arg1, boolean arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP2uiv(int arg0, int arg1, boolean arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP2uiv(int arg0, int arg1, boolean arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP3ui(int arg0, int arg1, boolean arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP3uiv(int arg0, int arg1, boolean arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP3uiv(int arg0, int arg1, boolean arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP4ui(int arg0, int arg1, boolean arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP4uiv(int arg0, int arg1, boolean arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribP4uiv(int arg0, int arg1, boolean arg2,
			int[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexFormatNV(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP2ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP2uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP2uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP3ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP3uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP3uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP4ui(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP4uiv(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexP4uiv(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glViewportArrayv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glViewportArrayv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glViewportIndexedf(int arg0, float arg1, float arg2,
			float arg3, float arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glViewportIndexedfv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glViewportIndexedfv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glAttachShader(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBeginQuery(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBindAttribLocation(int arg0, int arg1, String arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glBlendColor(float arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompileShader(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexImage3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, Buffer arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexImage3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, long arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexSubImage3D(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, int arg6, int arg7, int arg8,
			int arg9, Buffer arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCompressedTexSubImage3D(int arg0, int arg1, int arg2,
			int arg3, int arg4, int arg5, int arg6, int arg7, int arg8,
			int arg9, long arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glCopyTexSubImage3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, int arg8) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glCreateProgram() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int glCreateShader(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glDeleteProgram(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteQueries(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteQueries(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDeleteShader(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDetachShader(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glDisableVertexAttribArray(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEnableVertexAttribArray(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glEndQuery(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glFramebufferTexture3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenQueries(int arg0, IntBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGenQueries(int arg0, int[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveAttrib(int arg0, int arg1, int arg2, IntBuffer arg3,
			IntBuffer arg4, IntBuffer arg5, ByteBuffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveAttrib(int arg0, int arg1, int arg2, int[] arg3,
			int arg4, int[] arg5, int arg6, int[] arg7, int arg8, byte[] arg9,
			int arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniform(int arg0, int arg1, int arg2,
			IntBuffer arg3, IntBuffer arg4, IntBuffer arg5, ByteBuffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetActiveUniform(int arg0, int arg1, int arg2, int[] arg3,
			int arg4, int[] arg5, int arg6, int[] arg7, int arg8, byte[] arg9,
			int arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetAttachedShaders(int arg0, int arg1, IntBuffer arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetAttachedShaders(int arg0, int arg1, int[] arg2, int arg3,
			int[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetAttribLocation(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetProgramBinary(int arg0, int arg1, IntBuffer arg2,
			IntBuffer arg3, Buffer arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramBinary(int arg0, int arg1, int[] arg2, int arg3,
			int[] arg4, int arg5, Buffer arg6) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramInfoLog(int arg0, int arg1, IntBuffer arg2,
			ByteBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramInfoLog(int arg0, int arg1, int[] arg2, int arg3,
			byte[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetProgramiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectuiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryObjectuiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetQueryiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderInfoLog(int arg0, int arg1, IntBuffer arg2,
			ByteBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderInfoLog(int arg0, int arg1, int[] arg2, int arg3,
			byte[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderPrecisionFormat(int arg0, int arg1, IntBuffer arg2,
			IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderPrecisionFormat(int arg0, int arg1, int[] arg2,
			int arg3, int[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderSource(int arg0, int arg1, IntBuffer arg2,
			ByteBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderSource(int arg0, int arg1, int[] arg2, int arg3,
			byte[] arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetShaderiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int glGetUniformLocation(int arg0, String arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void glGetUniformfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetUniformiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribfv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribfv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribiv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glGetVertexAttribiv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean glIsProgram(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsQuery(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean glIsShader(int arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void glLinkProgram(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glProgramBinary(int arg0, int arg1, Buffer arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glReleaseShaderCompiler() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderBinary(int arg0, IntBuffer arg1, int arg2, Buffer arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderBinary(int arg0, int[] arg1, int arg2, int arg3,
			Buffer arg4, int arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderSource(int arg0, int arg1, String[] arg2, IntBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glShaderSource(int arg0, int arg1, String[] arg2, int[] arg3,
			int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilFuncSeparate(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilMaskSeparate(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glStencilOpSeparate(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage3D(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, int arg7, int arg8, Buffer arg9) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexImage3D(int arg0, int arg1, int arg2, int arg3, int arg4,
			int arg5, int arg6, int arg7, int arg8, long arg9) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexSubImage3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, int arg8, int arg9,
			Buffer arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glTexSubImage3D(int arg0, int arg1, int arg2, int arg3,
			int arg4, int arg5, int arg6, int arg7, int arg8, int arg9,
			long arg10) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform(GLUniformData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1f(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1fv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1fv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1i(int arg0, int arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1iv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform1iv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2f(int arg0, float arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2fv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2fv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2i(int arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2iv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform2iv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3f(int arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3fv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3fv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3i(int arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3iv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform3iv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4f(int arg0, float arg1, float arg2, float arg3,
			float arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4fv(int arg0, int arg1, FloatBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4fv(int arg0, int arg1, float[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4i(int arg0, int arg1, int arg2, int arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4iv(int arg0, int arg1, IntBuffer arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniform4iv(int arg0, int arg1, int[] arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix2fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix3fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4fv(int arg0, int arg1, boolean arg2,
			FloatBuffer arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUniformMatrix4fv(int arg0, int arg1, boolean arg2,
			float[] arg3, int arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glUseProgram(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glValidateProgram(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1f(int arg0, float arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1fv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib1fv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2f(int arg0, float arg1, float arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2fv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib2fv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3f(int arg0, float arg1, float arg2, float arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3fv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib3fv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4f(int arg0, float arg1, float arg2, float arg3,
			float arg4) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4fv(int arg0, FloatBuffer arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttrib4fv(int arg0, float[] arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribPointer(GLArrayData arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribPointer(int arg0, int arg1, int arg2,
			boolean arg3, int arg4, Buffer arg5) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void glVertexAttribPointer(int arg0, int arg1, int arg2,
			boolean arg3, int arg4, long arg5) {
		// TODO Auto-generated method stub
		
	}

	public int getDefaultReadBuffer() {
		// TODO Auto-generated method stub
		return 0;
	}

}
