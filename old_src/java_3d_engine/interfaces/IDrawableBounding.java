package java_3d_engine.interfaces;

public interface IDrawableBounding 
{
	/**
	 * Draw your object with uniform color to pick this by mouse
	 * @param gl Context opengl
	 * @param indexShader {id shader program, id shader position, id shader normal}
	 */
	public void drawBounding (GraphicLibrary gl, int[] indexShader);
}
