package java_3d_engine.test.matrix;

import static org.junit.Assert.*;

import java.util.Arrays;

import java_3d_engine.matrix.Matrix;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 13 mai 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class MatrixTest
{
	private Matrix _matrix;
	private float[] _identity = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1}, 
					_matrix1 = {1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4},
					_matrix2 = {1,2,3,4, 5,6,7,8, 9,10,11,12, 13,14,15,16};
	@Before
	public void setUp () throws Exception
	{
		_matrix = new Matrix();
	}

	@After
	public void tearDown () throws Exception
	{
	}

	@Test
	public void testMatrix ()
	{
		_matrix = new Matrix();
		assertArrayEquals(_matrix.getValues(), _identity, 0.0000001f);
	}

	@Test
	public void testMatrixFloatArray ()
	{
		_matrix = new Matrix(_matrix1);
		assertArrayEquals(_matrix.getValues(), _matrix1, 0.0000001f);
	}

	@Test
	public void testIdentity ()
	{
		_matrix = new Matrix(_matrix1);
		assertArrayEquals(_matrix.getValues(), _matrix1, 0.0000001f);
		_matrix.identity();
		assertArrayEquals(_matrix.getValues(), _identity, 0.0000001f);
	}

	@Test
	public void testClone ()
	{
		Matrix matrixClone = _matrix.clone();
		assertFalse(matrixClone == _matrix);
		assertArrayEquals(matrixClone.getValues(), _matrix.getValues(), 0.0001f);
		_matrix.setValues(_matrix1);
		assertFalse(Arrays.equals(matrixClone.getValues(), _matrix.getValues()));
	}

	@Test
	public void testSetIndexMatrixInShader ()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testSendMatrix ()
	{
		fail("Not yet implemented");
	}

}
