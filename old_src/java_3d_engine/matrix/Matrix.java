package java_3d_engine.matrix;

import java_3d_engine.shader.Shader;

import org.apache.log4j.Logger;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * Permit to create matrix 4x4. A matrix is represented by array of float with
 * 4*4=16 elements.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * Create matrix with constructor and use this with another matrix to perform
 * your operation. A matrix can be sended at a shader if you set its id with the
 * method setIndexMatrixInShader().
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 13 mai 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class Matrix
{

	private final static Logger	logger	= Logger.getLogger(Matrix.class);
	private float[]				matrix;
	private int					idMatrix;
	protected float[]			operation;
	protected float[]			tempResult;

	/**
	 * Instantiate identity matrix
	 */
	public Matrix ()
	{
		identity();
		idMatrix = -1;
	}

	/**
	 * Instanciate new matrix with your data
	 * 
	 * @param data
	 *            Size must be 16, be caution
	 */
	public Matrix (float[] data)
	{
		matrix = data;
		idMatrix = -1;
	}

	/**
	 * Define this matrix to identity
	 */
	public void identity ()
	{
		float[] result = {1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,};
		matrix = result;
	}

	@Override
	public Matrix clone ()
	{
		return new Matrix(this.matrix.clone());
	}

	/**
	 * Return the real value in float[16] array
	 * 
	 * @return
	 */
	public float[] getValues ()
	{
		return matrix;
	}

	/**
	 * Set value directly with array
	 * 
	 * @param data
	 *            Size must be 16, be caution
	 */
	public void setValues (float[] data)
	{
		matrix = data;
	}

	/**
	 * Set id matrix collected with Shader correspond at this matrix
	 * 
	 * @param index
	 */
	public void setIndexMatrixInShader (int index)
	{
		idMatrix = index;
	}

	long	time	= 0;

	/**
	 * Send matrix ModelViewProjection at specified shader
	 * 
	 * @param shader
	 *            Shader which will gone ModelViewProjection matrix
	 * @throws Exception
	 *             If ModelViewMatrix isn't identified in Shader
	 */
	public void sendMatrix (Shader shader) throws Exception
	{
		if (idMatrix != -1)
		{
			shader.sendMatrix(this, true, idMatrix);
		}
		else
		{
			logger.error("IdMatrix = -1, use Matrix.setIndexMatrixInShader(int) method before Matrix.sendMatrix(Shader).");
		}
	}

	/**
	 * Not yet implemented, This operation is discouraged, it is best to use
	 * this operation in shader.
	 */
	public void inverseMatrix ()
	{
	}

	/**
	 * Rotate at angle theta on specified axis
	 * 
	 * @param theta
	 *            Angle rotate
	 * @param x
	 *            float value
	 * @param y
	 *            float value
	 * @param z
	 *            float value
	 * @param mat
	 *            matrix to be rotated
	 * @return Matrix rotated
	 */
	protected float[] rotate (float theta, float x, float y, float z, Matrix mat)
	{
		float cosTheta = (float) Math.cos(theta);
		float unMoinsCosTheta = 1 - cosTheta;
		float sinTheta = (float) Math.sin(theta);
		operation = new float[] {x * x * (unMoinsCosTheta) + cosTheta,
				x * y * (unMoinsCosTheta) - z * sinTheta,
				x * z * (unMoinsCosTheta) + y * sinTheta, 0f,
				y * x * (unMoinsCosTheta) + z * sinTheta,
				y * y * (unMoinsCosTheta) + cosTheta,
				y * z * (unMoinsCosTheta) - x * sinTheta, 0f,
				x * z * (unMoinsCosTheta) - y * sinTheta,
				y * z * (unMoinsCosTheta) + x * sinTheta,
				z * z * (unMoinsCosTheta) + cosTheta, 0f, 0f, 0f, 0f, 1f};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MatrixOperation.multiply4x4(mat.getValues(), operation, tempResult);
		return tempResult;
	}

	/**
	 * Translate at specified value
	 * 
	 * @param x
	 *            Translate on x axe
	 * @param y
	 *            Translate on y axe
	 * @param z
	 *            Translate on z axe
	 */
	protected float[] translate (float x, float y, float z, Matrix mat)
	{
		operation = new float[] {1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MatrixOperation.multiply4x4(mat.getValues(), operation, tempResult);
		return tempResult;
	}

	/**
	 * Modify the scale at specified values
	 * 
	 * @param x
	 *            Resize on x axe
	 * @param y
	 *            Resize on y axe
	 * @param z
	 *            Resize on z axe
	 */
	protected float[] scale (float x, float y, float z, Matrix mat)
	{
		operation = new float[] {x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1};
		tempResult = new float[] {1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0,
				1};
		MatrixOperation.multiply4x4(mat.getValues(), operation, tempResult);
		return tempResult;
	}
}
