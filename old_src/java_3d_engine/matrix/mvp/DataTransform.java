package java_3d_engine.matrix.mvp;

public class DataTransform {

    public final static byte ROTATE = (byte) 0x0; //0b0; 
    public final static byte TRANSLATE = (byte) 0x1; //0b1;
    public final static byte SCALE = (byte) 0x2; //0b10;
    private float[] _data;
    private byte _typeTransform;
    private boolean _isViewMatrixModified;

    public DataTransform(byte typeTransform, boolean isViewMatrixModified, float[] data) {
        _typeTransform = typeTransform;
        _data = data;
        _isViewMatrixModified = isViewMatrixModified;
    }

    public byte getTypeTransform() {
        return _typeTransform;
    }

    public float[] getData() {
        return _data;
    }

    public boolean isViewMatrixModified() {
        return _isViewMatrixModified;
    }

    public String toString() {
        String type = "";
        switch (_typeTransform) {
            case ROTATE:
                type = "rotation";
                break;
            case TRANSLATE:
                type = "translation";
                break;
            case SCALE:
                type = "resize";
                break;
        }
        return type + " : x=" + _data[0] + ", y=" + _data[1] + ", z=" + _data[2];
    }
}
