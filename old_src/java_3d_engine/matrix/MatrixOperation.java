package java_3d_engine.matrix;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.GLBuffers;

/**
 * 
 * ----------------------------DESCRIPTION-------------------------------------<br>
 * 
 * <pre>
 * The class matrix operation permit to do all operation usefull on your matrix.
 * It's preferable it's possible to do your operation on your matrix in your shader
 * to improve your performance.
 * </pre>
 * 
 * ----------------------------HOW-TO-USE--------------------------------------<br>
 * 
 * <pre>
 * To use theses operations, you must call MatrixOperation with the static way.
 * By exemple, to multiply 2 matrix4x4, I call the operation
 * MatrixOperation.multiply4x4(firstMarix, secondMatrix, resultMatrix);
 * </pre>
 * 
 * ---------------------------INFORMATIONS-------------------------------------<br>
 * 
 * @version 14 mai 2014
 * 
 * @author Olivier, contact: olivier.haberzettel@hotmail.fr
 */
public class MatrixOperation {

    private final static Logger logger = Logger.getLogger(MatrixOperation.class);
    private static FloatBuffer temp = GLBuffers.newDirectFloatBuffer(16);

    /**
     * Multiply 2 matrix 4x4 and store result in result matrix
     * @param m1 FloatBuffer with 16 size represent matrix
     * @param m2 FloatBuffer with 16 size represent matrix
     * @param result FloatBuffer result of the operation
     * @return The same object result
     */
    public static FloatBuffer multiply4x4(FloatBuffer m1, FloatBuffer m2, FloatBuffer result) {
        result.rewind();

        result.put(m1.get(0) * m2.get(0) + m1.get(1) * m2.get(4) + m1.get(2) * m2.get(8) + m1.get(3) * m2.get(12));
        result.put(m1.get(0) * m2.get(1) + m1.get(1) * m2.get(5) + m1.get(2) * m2.get(9) + m1.get(3) * m2.get(13));
        result.put(m1.get(0) * m2.get(2) + m1.get(1) * m2.get(6) + m1.get(2) * m2.get(10) + m1.get(3) * m2.get(14));
        result.put(m1.get(0) * m2.get(3) + m1.get(1) * m2.get(7) + m1.get(2) * m2.get(11) + m1.get(3) * m2.get(15));

        result.put(m1.get(4) * m2.get(0) + m1.get(5) * m2.get(4) + m1.get(6) * m2.get(8) + m1.get(7) * m2.get(12));
        result.put(m1.get(4) * m2.get(1) + m1.get(5) * m2.get(5) + m1.get(6) * m2.get(9) + m1.get(7) * m2.get(13));
        result.put(m1.get(4) * m2.get(2) + m1.get(5) * m2.get(6) + m1.get(6) * m2.get(10) + m1.get(7) * m2.get(14));
        result.put(m1.get(4) * m2.get(3) + m1.get(5) * m2.get(7) + m1.get(6) * m2.get(11) + m1.get(7) * m2.get(15));

        result.put(m1.get(8) * m2.get(0) + m1.get(9) * m2.get(4) + m1.get(10) * m2.get(8) + m1.get(11) * m2.get(12));
        result.put(m1.get(8) * m2.get(1) + m1.get(9) * m2.get(5) + m1.get(10) * m2.get(9) + m1.get(11) * m2.get(13));
        result.put(m1.get(8) * m2.get(2) + m1.get(9) * m2.get(6) + m1.get(10) * m2.get(10) + m1.get(11) * m2.get(14));
        result.put(m1.get(8) * m2.get(3) + m1.get(9) * m2.get(7) + m1.get(10) * m2.get(11) + m1.get(11) * m2.get(15));

        result.put(m1.get(12) * m2.get(0) + m1.get(13) * m2.get(4) + m1.get(14) * m2.get(8) + m1.get(15) * m2.get(12));
        result.put(m1.get(12) * m2.get(1) + m1.get(13) * m2.get(5) + m1.get(14) * m2.get(9) + m1.get(15) * m2.get(13));
        result.put(m1.get(12) * m2.get(2) + m1.get(13) * m2.get(6) + m1.get(14) * m2.get(10) + m1.get(15) * m2.get(14));
        result.put(m1.get(12) * m2.get(3) + m1.get(13) * m2.get(7) + m1.get(14) * m2.get(11) + m1.get(15) * m2.get(15));

        result.rewind();

        return result;
    }

    /**
     * Multiply 2 matrix 4x4 and store result in result matrix
     * @param m1 IntBuffer with 16 size represent matrix
     * @param m2 IntBuffer with 16 size represent matrix
     * @param result IntBuffer result of the operation
     * @return The same object result
     */
    public static IntBuffer multiply4x4(IntBuffer m1, IntBuffer m2, IntBuffer result) {
        result.rewind();

        result.put(m1.get(0) * m2.get(0) + m1.get(1) * m2.get(4) + m1.get(2) * m2.get(8) + m1.get(3) * m2.get(12));
        result.put(m1.get(0) * m2.get(1) + m1.get(1) * m2.get(5) + m1.get(2) * m2.get(9) + m1.get(3) * m2.get(13));
        result.put(m1.get(0) * m2.get(2) + m1.get(1) * m2.get(6) + m1.get(2) * m2.get(10) + m1.get(3) * m2.get(14));
        result.put(m1.get(0) * m2.get(3) + m1.get(1) * m2.get(7) + m1.get(2) * m2.get(11) + m1.get(3) * m2.get(15));

        result.put(m1.get(4) * m2.get(0) + m1.get(5) * m2.get(4) + m1.get(6) * m2.get(8) + m1.get(7) * m2.get(12));
        result.put(m1.get(4) * m2.get(1) + m1.get(5) * m2.get(5) + m1.get(6) * m2.get(9) + m1.get(7) * m2.get(13));
        result.put(m1.get(4) * m2.get(2) + m1.get(5) * m2.get(6) + m1.get(6) * m2.get(10) + m1.get(7) * m2.get(14));
        result.put(m1.get(4) * m2.get(3) + m1.get(5) * m2.get(7) + m1.get(6) * m2.get(11) + m1.get(7) * m2.get(15));

        result.put(m1.get(8) * m2.get(0) + m1.get(9) * m2.get(4) + m1.get(10) * m2.get(8) + m1.get(11) * m2.get(12));
        result.put(m1.get(8) * m2.get(1) + m1.get(9) * m2.get(5) + m1.get(10) * m2.get(9) + m1.get(11) * m2.get(13));
        result.put(m1.get(8) * m2.get(2) + m1.get(9) * m2.get(6) + m1.get(10) * m2.get(10) + m1.get(11) * m2.get(14));
        result.put(m1.get(8) * m2.get(3) + m1.get(9) * m2.get(7) + m1.get(10) * m2.get(11) + m1.get(11) * m2.get(15));

        result.put(m1.get(12) * m2.get(0) + m1.get(13) * m2.get(4) + m1.get(14) * m2.get(8) + m1.get(15) * m2.get(12));
        result.put(m1.get(12) * m2.get(1) + m1.get(13) * m2.get(5) + m1.get(14) * m2.get(9) + m1.get(15) * m2.get(13));
        result.put(m1.get(12) * m2.get(2) + m1.get(13) * m2.get(6) + m1.get(14) * m2.get(10) + m1.get(15) * m2.get(14));
        result.put(m1.get(12) * m2.get(3) + m1.get(13) * m2.get(7) + m1.get(14) * m2.get(11) + m1.get(15) * m2.get(15));

        result.rewind();

        return result;
    }

    /**
     * Multiply 2 matrix 4x4 and store result in result matrix
     * @param m1 float[] with 16 size represent matrix
     * @param m2 float[] with 16 size represent matrix
     * @param result float[] result of the operation
     * @return The same object result
     */
    public static float[] multiply4x4(float[] m1, float[] m2, float[] result) {
        result[0] = m1[0] * m2[0] + m1[1] * m2[4] + m1[2] * m2[8] + m1[3] * m2[12];
        result[1] = m1[0] * m2[1] + m1[1] * m2[5] + m1[2] * m2[9] + m1[3] * m2[13];
        result[2] = m1[0] * m2[2] + m1[1] * m2[6] + m1[2] * m2[10] + m1[3] * m2[14];
        result[3] = m1[0] * m2[3] + m1[1] * m2[7] + m1[2] * m2[11] + m1[3] * m2[15];

        result[4] = m1[4] * m2[0] + m1[5] * m2[4] + m1[6] * m2[8] + m1[7] * m2[12];
        result[5] = m1[4] * m2[1] + m1[5] * m2[5] + m1[6] * m2[9] + m1[7] * m2[13];
        result[6] = m1[4] * m2[2] + m1[5] * m2[6] + m1[6] * m2[10] + m1[7] * m2[14];
        result[7] = m1[4] * m2[3] + m1[5] * m2[7] + m1[6] * m2[11] + m1[7] * m2[15];

        result[8] = m1[8] * m2[0] + m1[9] * m2[4] + m1[10] * m2[8] + m1[11] * m2[12];
        result[9] = m1[8] * m2[1] + m1[9] * m2[5] + m1[10] * m2[9] + m1[11] * m2[13];
        result[10] = m1[8] * m2[2] + m1[9] * m2[6] + m1[10] * m2[10] + m1[11] * m2[14];
        result[11] = m1[8] * m2[3] + m1[9] * m2[7] + m1[10] * m2[11] + m1[11] * m2[15];

        result[12] = m1[12] * m2[0] + m1[13] * m2[4] + m1[14] * m2[8] + m1[15] * m2[12];
        result[13] = m1[12] * m2[1] + m1[13] * m2[5] + m1[14] * m2[9] + m1[15] * m2[13];
        result[14] = m1[12] * m2[2] + m1[13] * m2[6] + m1[14] * m2[10] + m1[15] * m2[14];
        result[15] = m1[12] * m2[3] + m1[13] * m2[7] + m1[14] * m2[11] + m1[15] * m2[15];

        return result;
    }

    /**
     * Multiply 2 matrix 4x4 and store result in result matrix
     * @param m1 float[] with 16 size represent matrix
     * @param m2 float[] with 16 size represent matrix
     * @param result FloatBuffer result of the operation
     * @return The same object result
     */
    public static FloatBuffer multiply4x4(float[] m1, float[] m2, FloatBuffer result) {
        result.rewind();

        result.put(m1[0] * m2[0] + m1[1] * m2[4] + m1[2] * m2[8] + m1[3] * m2[12]);
        result.put(m1[0] * m2[1] + m1[1] * m2[5] + m1[2] * m2[9] + m1[3] * m2[13]);
        result.put(m1[0] * m2[2] + m1[1] * m2[6] + m1[2] * m2[10] + m1[3] * m2[14]);
        result.put(m1[0] * m2[3] + m1[1] * m2[7] + m1[2] * m2[11] + m1[3] * m2[15]);

        result.put(m1[4] * m2[0] + m1[5] * m2[4] + m1[6] * m2[8] + m1[7] * m2[12]);
        result.put(m1[4] * m2[1] + m1[5] * m2[5] + m1[6] * m2[9] + m1[7] * m2[13]);
        result.put(m1[4] * m2[2] + m1[5] * m2[6] + m1[6] * m2[10] + m1[7] * m2[14]);
        result.put(m1[4] * m2[3] + m1[5] * m2[7] + m1[6] * m2[11] + m1[7] * m2[15]);

        result.put(m1[8] * m2[0] + m1[9] * m2[4] + m1[10] * m2[8] + m1[11] * m2[12]);
        result.put(m1[8] * m2[1] + m1[9] * m2[5] + m1[10] * m2[9] + m1[11] * m2[13]);
        result.put(m1[8] * m2[2] + m1[9] * m2[6] + m1[10] * m2[10] + m1[11] * m2[14]);
        result.put(m1[8] * m2[3] + m1[9] * m2[7] + m1[10] * m2[11] + m1[11] * m2[15]);

        result.put(m1[12] * m2[0] + m1[13] * m2[4] + m1[14] * m2[8] + m1[15] * m2[12]);
        result.put(m1[12] * m2[1] + m1[13] * m2[5] + m1[14] * m2[9] + m1[15] * m2[13]);
        result.put(m1[12] * m2[2] + m1[13] * m2[6] + m1[14] * m2[10] + m1[15] * m2[14]);
        result.put(m1[12] * m2[3] + m1[13] * m2[7] + m1[14] * m2[11] + m1[15] * m2[15]);

        return result;
    }

    /**
     * Multiply 2 matrix 4x4 and store result in result matrix
     * @param matrix1 Matrix object
     * @param matrix2 Matrix object
     * @param result Matrix result of the operation
     * @return The same object result
     */
    public static Matrix multiply4x4(Matrix matrix1, Matrix matrix2, Matrix result) {
        float[] m1 = matrix1.getValues();
        float[] m2 = matrix2.getValues();
        multiply4x4(m1, m2, result.getValues());
        return result;
    }

    /**
     * Multiply 3 matrix 4x4 and store result in result matrix in this order : m1 * m2 * m3
     * @param m1 FloatBuffer with 16 size represent matrix
     * @param m2 FloatBuffer with 16 size represent matrix
     * @param m3 FloatBuffer with 16 size represent matrix
     * @param resultBuffer FloatBuffer result of the operation
     * @return The same object result
     */
    public static FloatBuffer multiply4x4(FloatBuffer m1, FloatBuffer m2, FloatBuffer m3, FloatBuffer resultBuffer) {
        //Temp definit en statique une fois pour eviter de reallouer la zone memoire directe
        multiply4x4(m1, m2, temp);
        multiply4x4(temp, m3, resultBuffer);

        return resultBuffer;
    }

    /**
     * Multiply 3 matrix 4x4 and store result in result matrix in this order : m1 * m2 * m3
     * @param m1 float[] with 16 size represent matrix
     * @param m2 float[] with 16 size represent matrix
     * @param m3 float[] with 16 size represent matrix
     * @param resultBuffer float[] result of the operation
     * @return The same object result
     */
    public static float[] multiply4x4(float[] m1, float[] m2, float[] m3, float[] result) {
        float[] temp = new float[16];
        multiply4x4(m1, m2, temp);
        multiply4x4(temp, m3, result);

        return result;
    }

    public static Matrix multiply4x4(Matrix m1, Matrix m2, Matrix m3, Matrix result) {
        Matrix temp = new Matrix();

        multiply4x4(m1, m2, temp);
        multiply4x4(temp, m3, result);

        return result;
    }

    public static FloatBuffer vectorialProductVec3(FloatBuffer v1, FloatBuffer v2, FloatBuffer result) {
        result.rewind();
        result.put(v1.get(1) * v2.get(2) - v1.get(2) * v2.get(1));
        result.put(v1.get(2) * v2.get(0) - v1.get(0) * v2.get(2));
        result.put(v1.get(0) * v2.get(1) - v1.get(1) * v2.get(0));

        return result;
    }

    public static float[] vectorialProductVec3(float[] v1, float[] v2, float[] result) {
        result[0] = v1[1] * v2[2] - v1[2] * v2[1];
        result[1] = v1[2] * v2[0] - v1[0] * v2[2];
        result[2] = v1[0] * v2[1] - v1[1] * v2[0];

        return result;
    }

    public static FloatBuffer normaliserVec3(FloatBuffer vec) {
        vec.rewind();

        float norme = (float) Math.sqrt(vec.get(0) * vec.get(0) + vec.get(1) * vec.get(1) + vec.get(2) * vec.get(2));

        if (norme != 0) {
            vec.put(vec.get(0) / norme);
            vec.put(vec.get(1) / norme);
            vec.put(vec.get(2) / norme);
        } else {
            logger.error("Norme equal 0");
        }
        return vec;
    }

    public static float[] normaliserVec3(float[] vec) {
        float norme = (float) Math.sqrt(vec[0] * vec[0] + vec[1] * vec[1] + vec[2] * vec[2]);

        if (norme != 0) {
            vec[0] = vec[0] / norme;
            vec[1] = vec[1] / norme;
            vec[2] = vec[2] / norme;
        } else {
            logger.error("Norme equal 0");
        }
        return vec;
    }

    public static String matrix4x4ToString(FloatBuffer m) {
        String result;

        result = "[\n";
        result += "\t" + m.get(0) + " \t" + m.get(1) + " \t" + m.get(2) + " \t" + m.get(3) + "\n";
        result += "\t" + m.get(4) + " \t" + m.get(5) + " \t" + m.get(6) + " \t" + m.get(7) + "\n";
        result += "\t" + m.get(8) + " \t" + m.get(9) + " \t" + m.get(10) + " \t" + m.get(11) + "\n";
        result += "\t" + m.get(12) + " \t" + m.get(13) + " \t" + m.get(14) + " \t" + m.get(15) + "\n";
        result += "]";

        return result;
    }

    public static String matrix4x4ToString(float[] m) {
        String result;

        result = "[\n";
        result += "\t" + m[0] + " \t" + m[1] + " \t" + m[2] + " \t" + m[3] + "\n";
        result += "\t" + m[4] + " \t" + m[5] + " \t" + m[6] + " \t" + m[7] + "\n";
        result += "\t" + m[8] + " \t" + m[9] + " \t" + m[10] + " \t" + m[11] + "\n";
        result += "\t" + m[12] + " \t" + m[13] + " \t" + m[14] + " \t" + m[15] + "\n";
        result += "]";

        return result;
    }

    public static String matrix4x4ToString(Matrix matrix) {
        float[] m = matrix.getValues();
        String result;

        result = "[\n";
        result += "\t" + m[0] + " \t" + m[1] + " \t" + m[2] + " \t" + m[3] + "\n";
        result += "\t" + m[4] + " \t" + m[5] + " \t" + m[6] + " \t" + m[7] + "\n";
        result += "\t" + m[8] + " \t" + m[9] + " \t" + m[10] + " \t" + m[11] + "\n";
        result += "\t" + m[12] + " \t" + m[13] + " \t" + m[14] + " \t" + m[15] + "\n";
        result += "]";

        return result;
    }

    public static void unitTestMatrixMultiplication() {
        FloatBuffer m1 = Buffers.newDirectFloatBuffer(16);
        FloatBuffer m2 = Buffers.newDirectFloatBuffer(16);
        FloatBuffer resultatCalc = Buffers.newDirectFloatBuffer(16);
        FloatBuffer resultat = Buffers.newDirectFloatBuffer(16);
        m1.put(1).put(2).put(3).put(4)
                .put(5).put(6).put(7).put(8)
                .put(9).put(10).put(11).put(12)
                .put(13).put(14).put(15).put(16);
        resultatCalc.put(0).put(0).put(0).put(0).
                put(0).put(0).put(0).put(0).
                put(0).put(0).put(0).put(0).
                put(0).put(0).put(0).put(0);
        /*m2	.put(17).put(18).put(19).put(20)
         .put(21).put(22).put(23).put(24)
         .put(25).put(26).put(27).put(28)
         .put(29).put(30).put(31).put(32);
         resultat.put(250).put(260).put(270).put(280)
         .put(618).put(644).put(670).put(696)
         .put(986).put(1028).put(1070).put(1112)
         .put(1354).put(1412).put(1470).put(1528);
         multiply4x4(m1, m2, resultatCalc);*/

        float[] m3 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};
        float[] m4 = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        long time = System.currentTimeMillis();
        int max = 10000000;
        for (int i = 0; i < max; i++) {
            multiply4x4(m3, m3, m4);
        }
        logger.debug("Temps exec " + max + " multiplications de matrices de type float[] : " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        for (int i = 0; i < max; i++) {
            multiply4x4(m1, m1, resultatCalc);
        }
        logger.debug("Temps exec " + max + " multiplications de matrices de type FloatBuffer : " + (System.currentTimeMillis() - time));

        time = System.currentTimeMillis();
        for (int i = 0; i < max; i++) {
            multiply4x4(m3, m3, resultatCalc);
        }
        logger.debug("Temps exec " + max + " multiplications de matrices de type float[] return FloatBuffer : " + (System.currentTimeMillis() - time));

        logger.debug("Test multiplication de deux matrices 4x4 : " + (testEqual(resultatCalc, resultat) ? "OK" : "Erreur"));
    }

    public static void unitTestVectorialProduct() {
        FloatBuffer v1 = FloatBuffer.allocate(3);
        FloatBuffer v2 = FloatBuffer.allocate(3);

        v1.put(1).put(2).put(3);

        v2.put(-5).put(-10).put(17);
        FloatBuffer result = FloatBuffer.allocate(3).put(64).put(-32).put(0);
        FloatBuffer res = FloatBuffer.allocate(3);
        vectorialProductVec3(v1, v2, res);
        logger.debug("Test produit vectoriel de deux vecteur 3 : " + (testEqual(res, result) ? "OK" : "Erreur"));
    }

    public static void unitTestNormalize() {
        FloatBuffer v1 = Buffers.newDirectFloatBuffer(3);

        v1.put(1).put(2).put(3);
        FloatBuffer result = FloatBuffer.allocate(3).put(0.26726124f).put(0.5345225f).put(0.8017837f);
        normaliserVec3(v1);
        logger.debug("Test produit vectoriel de deux vecteur 3 : " + (testEqual(v1, result) ? "OK" : "Erreur"));
    }

    public static boolean testEqual(FloatBuffer m1, FloatBuffer m2) {
        boolean result = true;

        if (m1.capacity() == m2.capacity()) {
            for (int i = 0, max = m1.capacity(); i < max; i++) {
                if (m1.get(i) != m2.get(i)) {
                    result = false;
                    break;
                }
            }
        } else {
            result = false;
        }

        return result;
    }

    public static String vec3ToString(FloatBuffer v) {
        String result;
        result = "(" + v.get(0) + ", " + v.get(1) + ", " + v.get(2) + ")";
        return result;
    }
}
