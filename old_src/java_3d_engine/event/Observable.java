package java_3d_engine.event;

import java.util.ArrayList;

/**
 * Classe abstraite qui va pouvoir être observée via des Classes
 * qui implémentent IObserver.
 * 
 * Chaque Objet qui veut être attentif à cette classe doit être ajouté
 * via la méthode addObserveur()
 * 
 * @author Olivier
 *
 * @param <O>
 */
public abstract class Observable<O>
{
	ArrayList<IObserver<O>> _observers;
	public Observable ()
	{
		_observers = new ArrayList<IObserver<O>>();
	}
	
	public void addObsever (IObserver<O> observer)
	{
		_observers.add(observer);
	}
	
	public void removeObsever (IObserver<O> observer)
	{
		_observers.remove(observer);
	}
	
	public void notifyAllIObservers (O data)
	{
		for (IObserver<O> iterator : _observers) 
		{
			iterator.update(data);
		}
	}
}
