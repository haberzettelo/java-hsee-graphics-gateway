package java_3d_engine.event;

public interface IObserver<A>
{
	public void update (A data);
}
