package java_3d_engine.shader;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;
import main.GLException;

import org.apache.log4j.Logger;

public class Membres {

    final public static Logger logger = Logger.getLogger(Membres.class);
    private String name;
    private String type;
    private String[] attr;
    private int[] attrID;
    private byte[] attrType;
    private int nb;
    private Shader shader;
    private GraphicLibrary gl;

    public Membres(String type, String Name, int numberOfElement, Shader shader) {
        this.type = type;
        name = Name;
        attr = new String[numberOfElement];
        attrID = new int[numberOfElement];
        attrType = new byte[numberOfElement];
        nb = 0;
        this.shader = shader;
    }

    public Membres(String type, int numberOfElement, Shader shader) {
        this.type = type;
        name = null;
        attr = new String[numberOfElement];
        attrID = new int[numberOfElement];
        attrType = new byte[numberOfElement];
        nb = 0;
        this.shader = shader;
    }

    public void setName(String newName) {
        name = newName;
    }

    public int getNumberOfElement() {
        return attr.length;
    }

    public boolean nameIsSet() {
        return name != null;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void addAttribute(String nameOfAttribute, byte dataType) {
        attr[nb] = nameOfAttribute;
        attrType[nb] = dataType;
        nb++;
    }

    public int[] getIndexAttributes() {
        return attrID;
    }

    public int getIndexAttribute(int index) {
        if (index > nb) {
            logger.error("ID '" + index + "' out of bound. Max ID acceptable is '" + nb + "'");
            (new Exception()).printStackTrace();
            return -1;
        } else {
            return attrID[index];
        }
    }

    void initIndexLocation(GraphicLibrary gl, int IDProgram) {
        this.gl = gl;
        try {
            GLException.getOpenGLError(gl);
        } catch (GLException e1) {
            e1.displayGLException();
        }
        for (int i = 0; i < nb; i++) {
            attrID[i] = gl.getGLx().glGetUniformLocation(IDProgram, name + '.' + attr[i]);
            try {
                if (attrID[i] == -1) {
                    throw new Exception("Erreur lors de la récupération de l'id de la variable uniforme '" + name + '.' + attr[i] + "', index = -1.");
                }
            } catch (Exception e) {
                logger.error(e);
            }
            try {
                GLException.getOpenGLError(gl);
            } catch (GLException e) {
                logger.error("Erreur lors de la récupération de l'id de la variable uniforme '" + name + '.' + attr[i] + "'.");
                e.displayGLException();
            }
        }
    }

    /**
     * Update all attributes of your structure
     *
     * @param data Buffer[0] -> data at index 0,
     */
    public void updateAttributes(Buffer[] data) {
        for (int i = 0; i < nb; i++) {
            try {
                GLException.getOpenGLError(gl);
            } catch (GLException e) {
                // TODO Auto-generated catch block
                logger.error("Erreur lors de la mise à jours de la var uniform '" + name + '.' + attr[i] + "' de type structure.");
                e.displayGLException();
            }
            if (data[i] instanceof FloatBuffer && ((attrType[i] & Shader.dataFloat) == Shader.dataFloat)) {
                FloatBuffer dataFloat = (FloatBuffer) data[i];
                shader.uniformFloatArray(dataFloat, attrID[i], attrType[i], 1, false);
            } else if (data[i] instanceof IntBuffer && ((attrType[i] & Shader.dataInt) == Shader.dataInt)) {
                IntBuffer dataInt = (IntBuffer) data[i];
                shader.uniformIntArray(dataInt, attrID[i], attrType[i], (byte) 1);
            } else {
                logger.fatal("Le type " + data[i].getClass() + " n'est pas géré. Le type " + getBufferType(attrType[i]) + " est attendu.");
            }

        }
    }

    public String getBufferType(byte dataType) {
        if (((dataType & Shader.dataFloat) == Shader.dataFloat)) {
            return "FloatBuffer";
        } else {
            return "IntBuffer";
        }
    }
}
