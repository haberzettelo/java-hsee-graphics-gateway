package java_3d_engine.models.object;

import java.nio.FloatBuffer;

public class Attribut 
{
	public FloatBuffer attr;
	public IntegerByReference[] attrIndex;
	
	public Attribut (FloatBuffer attribut, IntegerByReference[] attributIndex)
	{
		attr = attribut;
		attrIndex = attributIndex;
	}
}
