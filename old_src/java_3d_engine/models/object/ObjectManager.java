package java_3d_engine.models.object;

import java.util.ArrayList;

import java_3d_engine.interfaces.GraphicLibrary;

/**
 * Managed the multi-thread import of obj files
 * 
 * How to use :
 * 	1 - Import your object with importObjectXxxx(path, filename, gl)
 * 		Each import will be executed with new thread
 * 		We recommend to import the heaviest object in first and the lighter in last
 * 	2 - Use ImportAllObjectInGraphicCard() method to put all data imported in graphic card memory
 * @author Olivier
 *
 */
public class ObjectManager
{
	private ObjectManager instance = null;
	private ArrayList<ObjectModel> models = new ArrayList<ObjectModel>();
	private GraphicLibrary _gl = null;
	
	public ObjectManager getInstance ()
	{
		return instance;
	}
	
	/**
     * This constructor use VBO technology The memory of graphic card will be
     * used
     *
     * @param filename
     * @param gl
     * @throws Exception
     */
    public ObjectModel importObjectVBO(String path, String filename, GraphicLibrary gl) throws Exception 
    {
    	ObjectModel objecttemp = new ObjectVBO();
    	models.add(objecttemp);
    	
    	_gl = gl;
    	ThreadImport ti = new ThreadImport(objecttemp, path, filename);
    	new Thread(ti).start();
    	
    	return objecttemp;
    }
    
    /**
     * Get busy main thread while all object are not imported
     * @throws Exception
     */
    public void ImportAllObjectInGraphicCard () throws Exception
    {
    	while (models.size() > 0)
    	{
	    	for (int i = 0, max = models.size(); i < max; i++)
	    	{
	    		ObjectModel element = models.get(i);
	    		if (element.Imported())
	    		{
	    			element.importObject(_gl);
	    			models.remove(i);
	    			i--;
	    			max--;
	    		}
	    	}
    	}
    }
}
