package java_3d_engine.models.object;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Vector;

import java_3d_engine.interfaces.GraphicLibrary;

import javax.media.opengl.GL;
import javax.media.opengl.fixedfunc.GLLightingFunc;
import javax.media.opengl.fixedfunc.GLPointerFunc;

import com.sun.opengl.util.BufferUtil;

/**
 * Classe laissé à l'abandon car remplacée par la classe ObjectVBO
 * @author Olivier
 *
 */
public class ObjectWithoutVBO  extends ObjectModel
{
	private FloatBuffer[] vertexQuad;
	private FloatBuffer[] normalQuad;
	private FloatBuffer[] textureQuad;
	
	private FloatBuffer[] vertexTriangle;
	private FloatBuffer[] normalTriangle;
	private FloatBuffer[] textureTriangle;
	
	/**
	 * Construct the variables and read files
	 * @param filename Name of Object file (for object.obj, enter "object")
	 * @param gl The GraphicLibrary object
	 * @param stripWay True for IIGL.GL_TRIANGLE/QUAD_STRIP and False for IIGL.GL_TRIANGLES/QUADS
	 * @throws Exception 
	 */
	public ObjectWithoutVBO (String path, String filename, GraphicLibrary gl, boolean stripWay) throws Exception
	{
		
			
		ResultObjMtlFileIndexed result = readObjMtlFilesIndexed(path, filename, false);
		
		
		if (result == null)
			throw new Exception("Files obj or/and mtl encoutered problem when write poperties.");
		
		/**
		 * Take the variable modified by readObjMtlFiles
		 */
		
		
		FloatBuffer[] vertex = result.vertex;
		FloatBuffer[] normal = result.normal;
		FloatBuffer[] texture = result.texture;
		IntBuffer[][] face = result.face;
		
		Vector<Integer> nbFace = result.nbFace;
		//boolean fileMTLExist = result.fileMTLExist;
		int nbMaterial = result.nbFaces;
		
		/**
		 * Put all vertex, normals and coordTextures from file object in buffer
		 */
		/*if (fileMTLExist)
			indiceFaceMaterial = -1;*/
		
		/**
		 * Start of put in memory ordered of vertex, normals and coordTextures for create faces
		 */
		System.out.println("\tEnd of files importation.\r\tStart of put in memory ordered of vertex, normals and coordTextures for create faces.");
		
		gl.getGLx().glColor3f(1, 0.5f, 1);
			/**
			 * Allocation of memory for buffer contains vertex ordered, normals ordered ...
			 */
		vertexQuad = new FloatBuffer[nbMaterial];
		normalQuad = new FloatBuffer[nbMaterial];
		textureQuad = new FloatBuffer[nbMaterial];
		//nbTriangle*3*3 => number of triangle * 3 vertex by triangle * 3 coordinates by vertex
		vertexTriangle = new FloatBuffer[nbMaterial];
		normalTriangle = new FloatBuffer[nbMaterial];
		textureTriangle =  new FloatBuffer[nbMaterial];
		for (int i = 0; i < nbMaterial; i++)
		{
			//nbQuad*4*3 => number of quad * 4 vertex by quad * 3 coordinates by vertex
			vertexQuad[i] = BufferUtil.newFloatBuffer(nbQuad[i]*4*3);
			normalQuad[i] = BufferUtil.newFloatBuffer(nbQuad[i]*4*3);
			textureQuad[i] = BufferUtil.newFloatBuffer(nbQuad[i]*4*2);
			//nbTriangle*3*3 => number of triangle * 3 vertex by triangle * 3 coordinates by vertex
			vertexTriangle[i] = BufferUtil.newFloatBuffer(nbTriangle[i]*3*3);
			normalTriangle[i] = BufferUtil.newFloatBuffer(nbTriangle[i]*3*3);
			textureTriangle[i] = BufferUtil.newFloatBuffer(nbTriangle[i]*3*2);
		}
		
			/**
			 * Put all vertex in order, normals in order and coordTextures in order from buffer
			 */
		for (int i = 0; i < nbMaterial; i++)
		{
			for (int k = 0, numberOfFace = nbFace.get(i); k < numberOfFace; k++)
			{
				/**
				 * Getting the face informations
				 */
				
				if (face[i][k].capacity() > 10)
				{		
					//Put in memory the quad vertex, normals ans texCoord
					vertexQuad[i].put(vertex[i].get(face[i][k].get(0)*3));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(0)*3+1));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(0)*3+2));
					
					vertexQuad[i].put(vertex[i].get(face[i][k].get(1)*3));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(1)*3+1));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(1)*3+2));
					
					vertexQuad[i].put(vertex[i].get(face[i][k].get(2)*3));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(2)*3+1));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(2)*3+2));
					
					vertexQuad[i].put(vertex[i].get(face[i][k].get(3)*3));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(3)*3+1));
					vertexQuad[i].put(vertex[i].get(face[i][k].get(3)*3+2));
					
					
					//indice = f.getP1Texture() * 2;
					textureQuad[i].put(texture[i].get(face[i][k].get(4)*2));
					textureQuad[i].put(texture[i].get(face[i][k].get(4)*2+1));
					//indice = f.getP2Texture() * 2;
					textureQuad[i].put(texture[i].get(face[i][k].get(5)*2));
					textureQuad[i].put(texture[i].get(face[i][k].get(5)*2+1));
					//indice = f.getP3Texture() * 2;
					textureQuad[i].put(texture[i].get(face[i][k].get(6)*2));
					textureQuad[i].put(texture[i].get(face[i][k].get(6)*2+1));
					
					textureQuad[i].put(texture[i].get(face[i][k].get(7)*2));
					textureQuad[i].put(texture[i].get(face[i][k].get(7)*2+1));
					
					
					
					//indice = f.getP1Normal() * 3;
					normalQuad[i].put(normal[i].get(face[i][k].get(8)*3));
					normalQuad[i].put(normal[i].get(face[i][k].get(8)*3+1));
					normalQuad[i].put(normal[i].get(face[i][k].get(8)*3+2));
					
					//indice = f.getP2Normal() * 3;
					normalQuad[i].put(normal[i].get(face[i][k].get(9)*3));
					normalQuad[i].put(normal[i].get(face[i][k].get(9)*3+1));
					normalQuad[i].put(normal[i].get(face[i][k].get(9)*3+2));
					
					//indice = f.getP3Normal() * 3;
					normalQuad[i].put(normal[i].get(face[i][k].get(10)*3));
					normalQuad[i].put(normal[i].get(face[i][k].get(10)*3+1));
					normalQuad[i].put(normal[i].get(face[i][k].get(10)*3+2));
					
					normalQuad[i].put(normal[i].get(face[i][k].get(11)*3));
					normalQuad[i].put(normal[i].get(face[i][k].get(11)*3+1));
					normalQuad[i].put(normal[i].get(face[i][k].get(11)*3+2));
				}
				else
				{
					//Put in memory the triangle vertex, normals ans texCoord
					//indice = f.getP1Vertex() * 3;
					
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(0)*3));
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(0)*3+1));
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(0)*3+2));
					
					
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(1)*3));
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(1)*3+1));
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(1)*3+2));
					
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(2)*3));
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(2)*3+1));
					vertexTriangle[i].put(vertex[i].get(face[i][k].get(2)*3+2));
					
					
					
					//indice = f.getP1Texture() * 2;
					textureTriangle[i].put(texture[i].get(face[i][k].get(3)*2));
					textureTriangle[i].put(texture[i].get(face[i][k].get(3)*2+1));
					//indice = f.getP2Texture() * 2;
					textureTriangle[i].put(texture[i].get(face[i][k].get(4)*2));
					textureTriangle[i].put(texture[i].get(face[i][k].get(4)*2+1));
					//indice = f.getP3Texture() * 2;
					textureTriangle[i].put(texture[i].get(face[i][k].get(5)*2));
					textureTriangle[i].put(texture[i].get(face[i][k].get(5)*2+1));
					
					
					
					//indice = f.getP1Normal() * 3;
					normalTriangle[i].put(normal[i].get(face[i][k].get(6)*3));
					normalTriangle[i].put(normal[i].get(face[i][k].get(6)*3+1));
					normalTriangle[i].put(normal[i].get(face[i][k].get(6)*3+2));
					
					//indice = f.getP2Normal() * 3;
					normalTriangle[i].put(normal[i].get(face[i][k].get(7)*3));
					normalTriangle[i].put(normal[i].get(face[i][k].get(7)*3+1));
					normalTriangle[i].put(normal[i].get(face[i][k].get(7)*3+2));
					
					//indice = f.getP3Normal() * 3;
					normalTriangle[i].put(normal[i].get(face[i][k].get(8)*3));
					normalTriangle[i].put(normal[i].get(face[i][k].get(8)*3+1));
					normalTriangle[i].put(normal[i].get(face[i][k].get(8)*3+2));
					
					
				}
		
			}
			/**
			 * Reset the cursor to begin
			 */
			normalQuad[i].rewind();
			textureQuad[i].rewind();
			vertexQuad[i].rewind();
			normalTriangle[i].rewind();
			textureTriangle[i].rewind();
			vertexTriangle[i].rewind();
		}
		System.out.println("\tEnd of put in memory.\rImportation has succeeded.");
		
		//inputFileOBJ.close();
	}
	
	@Override
	public void draw(GraphicLibrary gl, int[] indexShader) 
	{
		boolean textureEnable = false;
		boolean transparance = false;
		int /*indexNormal = indexShader[1], indexPosition = indexShader[0], */indexTex = indexShader[2];
		
		gl.getGLx().glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		gl.getGLx().glEnableClientState(GLPointerFunc.GL_NORMAL_ARRAY);
		/*if (textureEnable)
			gl.getGLx().glEnableClientState(IIGL.GL_TEXTURE_COORD_ARRAY);*/
		
		for (int i = 0; i < nbQuad.length; i++)
		{
			if (material.get(materialGroup[i]) != null)
			{
				transparance = material.get(materialGroup[i]).isTransparent();
				if (transparance)
				{
					gl.getGLx().glEnable(GL.GL_BLEND);
					gl.getGLx().glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
				}
				gl.getGLx().glEnable(GLLightingFunc.GL_COLOR_MATERIAL);
				material.get(materialGroup[i]).enableMaterial(gl, indexTex);
			}
			if (nbQuad[i] > 0)
			{
				if (textureEnable)
					gl.getGLx().glTexCoordPointer(2, GL.GL_FLOAT, 0, textureQuad[i]);
				gl.getGLx().glVertexPointer(3, GL.GL_FLOAT, 0, vertexQuad[i]);
				gl.getGLx().glNormalPointer(GL.GL_FLOAT, 0, normalQuad[i]);
				gl.getGLx().glDrawArrays(wayDrawQuad, 0, nbQuad[i]*4);
			}
			
			if (nbTriangle[i] > 0)
			{
				if (textureEnable)
					gl.getGLx().glTexCoordPointer(2, GL.GL_FLOAT, 0, textureTriangle[i]);
				gl.getGLx().glVertexPointer(3, GL.GL_FLOAT, 0, vertexTriangle[i]);
				gl.getGLx().glNormalPointer(GL.GL_FLOAT, 0, normalTriangle[i]);
				//gl.getGLx().glDrawElements(IIGL.GL_TRIANGLES, nbTriangle[i]*3, IIGL.GL_UNSIGNED_BYTE, vertexTriangle[i])
				gl.getGLx().glDrawArrays(wayDrawTriangle, 0, nbTriangle[i]*3);
			}
			
			if (material.get(materialGroup[i]) != null)
			{
				if (transparance)
					gl.getGLx().glDisable(GL.GL_BLEND);
				gl.getGLx().glDisable(GLLightingFunc.GL_COLOR_MATERIAL);
			}
		
		}
		/*if (textureEnable)
			gl.getGLx().glDisableClientState(IIGL.GL_TEXTURE_COORD_ARRAY);*/
		gl.getGLx().glDisableClientState(GLPointerFunc.GL_NORMAL_ARRAY);
		gl.getGLx().glDisableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
		
	}
}
