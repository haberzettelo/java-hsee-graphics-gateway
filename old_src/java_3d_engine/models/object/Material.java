package java_3d_engine.models.object;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.models.buffers.TextureObject;

import javax.media.opengl.GL;
import javax.media.opengl.fixedfunc.GLLightingFunc;

/**
 *
 * @author Olivier
 *
 */
class Material {

    private TextureObject texture;
    private float[] ambiantReflexivity = null;
    private float[] diffuseReflexivity = null;
    private float[] specularReflexivity = null;
    private float[] transmissionFilter = null;
    @SuppressWarnings("unused")
    private float opticalDensity = -1;
    @SuppressWarnings("unused")
    private float transparency = -1;
    private float alpha = 0;
    @SuppressWarnings("unused")
    private int illuminationType = -1;
    private int specularExponent = -1;
    private Boolean[] type;

    public Material() {
        int nbElement = 15;
        type = new Boolean[nbElement];
        for (int i = 0; i < type.length; i++) {
            type[i] = false;
        }

        texture = null;
    }

    public TextureObject getTexture ()
    {
    	return texture;
    }
    
    public void setTexture(TextureObject tex) {
        texture = tex;
    }

    public void setAmbiantReflexivity(float[] rgb) {
        type[0] = true;
        ambiantReflexivity = rgb;
    }

    public void setDiffuseReflexivity(float[] rgb) {
        type[1] = true;
        diffuseReflexivity = rgb;
    }

    public void setSpecularReflexivity(float[] rgb) {
        type[2] = true;
        specularReflexivity = rgb;
    }

    public void setTransmissionFilter(float[] rgb) {
        type[3] = true;
        transmissionFilter = rgb;
    }

    public boolean isTransparent() {
        return type[3];
    }

    public void setIlluminationType(int type) {
        this.type[4] = true;
        illuminationType = type;
    }

    public void setSpecularExponent(int exponent) {
        type[5] = true;
        specularExponent = exponent;
    }

    public void setOpticalDensity(float density) {
        type[6] = true;
        opticalDensity = density;
    }

    public void setTransparency(float dissolve) {
        type[7] = true;
        transparency = dissolve;
    }

    public void enableMaterial(GraphicLibrary gl, int indexTextureInShader) {
        if (type[3]) {
            alpha = (transmissionFilter[0] + transmissionFilter[1] + transmissionFilter[2]) / 3;
        }
        if (type[0]) {
            gl.getGLx().glColorMaterial(GL.GL_FRONT, GLLightingFunc.GL_AMBIENT);
            gl.getGLx().glColor4f(ambiantReflexivity[0], ambiantReflexivity[1], ambiantReflexivity[2], alpha);
        }
        if (type[1]) {
            gl.getGLx().glColorMaterial(GL.GL_FRONT, GLLightingFunc.GL_DIFFUSE);
            gl.getGLx().glColor4f(diffuseReflexivity[0], diffuseReflexivity[1], diffuseReflexivity[2], alpha);
        }
        if (type[2]) {
            gl.getGLx().glColorMaterial(GL.GL_FRONT, GLLightingFunc.GL_SPECULAR);
            gl.getGLx().glColor4f(specularReflexivity[0], specularReflexivity[1], specularReflexivity[2], alpha);
        }
        /*
         if (type[4])
         {
         gl.getGLx().glColorMaterial(GL2.GL_FRONT, GL2.GL_EMISSION);
         gl.getGLx().glColor4f(ambiantReflexivity[0], ambiantReflexivity[1]);
         }*/
        if (type[5]) {
            gl.getGLx().glMaterialf(GL.GL_FRONT, GLLightingFunc.GL_SHININESS, specularExponent * 128 / 1000);
        }
        /*if (type[6])
         gl.getGLx().glMaterialf(GL2.GL_FRONT, GL2.GL_o, opticalDensity);*/
        /*if (type[7])
         gl.getGLx().glbl*/

        if (texture != null) {
            texture.draw();
            //gl.getGLx().glUniform1i(indexTextureInShader, 0);
            //texture.unbindBuffer();
        }
    }

    public void disableMaterial(GraphicLibrary gl, int indexTextureInShader) {
        if (texture != null) {
            texture.stopDraw();
        }
        gl.getGLx().glUniform1i(indexTextureInShader, 0);
    }

    public void enableMaterialWithMaterialfv(GraphicLibrary gl) {
        if (type[3]) {
            alpha = (transmissionFilter[0] + transmissionFilter[1] + transmissionFilter[2]) / 3;
        }
        if (type[0]) {
            gl.getGLx().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_AMBIENT, ambiantReflexivity, 0);
        }
        if (type[1]) {
            gl.getGLx().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_DIFFUSE, diffuseReflexivity, 0);
        }
        if (type[2]) {
            gl.getGLx().glMaterialfv(GL.GL_FRONT, GLLightingFunc.GL_SPECULAR, specularReflexivity, 0);
        }
        /*
         if (type[4])
         {
         gl.getGLx().glColorMaterial(GL2.GL_FRONT, GL2.GL_EMISSION);
         gl.getGLx().glColor4f(ambiantReflexivity[0], ambiantReflexivity[1]);
         }*/
        if (type[5]) {
            gl.getGLx().glMaterialf(GL.GL_FRONT, GLLightingFunc.GL_SHININESS, specularExponent * 128 / 1000);
        }
        /*if (type[6])
         gl.getGLx().glMaterialf(GL2.GL_FRONT, GL2.GL_o, opticalDensity);*/
        /*if (type[7])
         gl.getGLx().glbl*/
    }
}
