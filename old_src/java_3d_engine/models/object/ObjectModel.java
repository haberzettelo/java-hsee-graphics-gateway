package java_3d_engine.models.object;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Hashtable;
import java.util.Vector;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IDrawable;
import java_3d_engine.models.buffers.TextureObject;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.sun.opengl.util.BufferUtil;

/**
 *
 * @author Olivier
 *
 */
public abstract class ObjectModel implements IDrawable {

    /**
     * Logger qui sert à debugger l'application.
     */
    private final static Logger logger = Logger.getLogger(ObjectModel.class);
    protected int[] nbTriangle, nbQuad;
    protected byte wayDrawTriangle, wayDrawQuad;
    protected String[] materialGroup;
    protected Hashtable<String, Material> material;
    protected boolean imported = false;

    /*
     * -
     * ---------
     * -- Factory pattern --
     * -------------------------------
     * --------------------------------------------
     */
    final public static ObjectModel createInVBO(String path, String filename, GraphicLibrary gl, boolean stripWay) throws Exception {
        ObjectVBO instance = new ObjectVBO();
        instance.allImport(path, filename, gl);
    	return instance;
    }

    final public static ObjectModel createInRAM(String path, String filename, GraphicLibrary gl, boolean stripWay) throws Exception {
        return new ObjectWithoutVBO(path, filename, gl, stripWay);
    }
    
    /*
     * -
     * ---------
     * -- Public Method --
     * -------------------------------
     * --------------------------------------------
     */
    /**
     * Return true if object is imported (not necessarily in Graphic Card)
     * @return
     */
    public boolean Imported ()
    {
    	return imported;
    }
    
    public void importObject (GraphicLibrary gl) throws Exception 
    {	logger.debug("importObject method : Object not import !!!!!!!!!!! override method has don't been realized");}
    
    public void initImport (String path, String filename) throws Exception 
    {	logger.debug("initImport method : Object not import !!!!!!!!!!! override method has don't been realized");}
    
    public void allImport (String path, String filename, GraphicLibrary gl) throws Exception
    {	logger.debug("allImport method : Object not import !!!!!!!!!!! override method has don't been realized");}

    /*
     * -
     * ---------
     * -- Protected Methods --
     * -------------------------------
     * --------------------------------------------
     */
    /**
     * Generate table that will contain the values below
     *
     * @param size Size of table
     * @return [1, 2, 3, ..., size] in buffer type
     */
    protected IntBuffer genIntBufferOfIndex(int size) {
        IntBuffer table = IntBuffer.allocate(size);
        for (int i = 0; i < size; i++) {
            table.put(i);
        }
        table.rewind();
        return table;
    }

    /**
     * Read the OBJ and MTL file
     *
     * @param path Path of the Object file
     * @param filename Name of the Object file
     * @param stripWay true for triangle/quad strip and false for normal method
     * @return Caution, inputFileObj must be closed after your operations
     * @throws IOException
     */
    @SuppressWarnings("resource")
    protected ResultObjMtlFile readObjMtlFiles(String path, String filename, boolean stripWay) throws Exception {
        /**
         * Way of draw the triangles and the quads
         */
        if (stripWay) {
            wayDrawTriangle = GL.GL_TRIANGLE_STRIP;
            wayDrawQuad = GL2.GL_QUAD_STRIP;
        } else {
            wayDrawTriangle = GL.GL_TRIANGLES;
            wayDrawQuad = GL2.GL_QUADS;
        }

        /**
         * Creation of temporary variables for create the buffer contains the
         * vertex, the normals and coord of textures
         */
        String fileLocation = path + filename + ".obj";
        String fileLocationMtl = null;
        String ligne;
        String[] ligneParse = new String[4];
        /*Vector<FloatBuffer> vertex;
         Vector<FloatBuffer> normal;
         Vector<FloatBuffer> texture;*/
        FloatBuffer vertex;
        FloatBuffer normal;
        FloatBuffer texture;

        Face[][] face = null;
        BufferedReader inputFileOBJ;
        FileInputStream fichierObj;

        /**
         * Initialize variables
         */
        Vector<Integer> nbVert = new Vector<Integer>(), nbText = new Vector<Integer>(), nbNorm = new Vector<Integer>(), nbFace = new Vector<Integer>();
        int nbVertTot = 0, nbTextTot = 0, nbMaterial = 0, nbNormTot = 0;
        nbTriangle = null;
        nbQuad = null;

        /**
         * Opening object file (.obj)
         */
        try {
            fichierObj = new FileInputStream(fileLocation);
        } catch (FileNotFoundException e) {
            System.out.println("The object file has encountered a problem during opening.");
            throw e;
        }

        inputFileOBJ = new BufferedReader(new InputStreamReader(fichierObj));

        try {
            /**
             * Searching the mtl reference file in object file
             */
            boolean fileMTLExist = false;
            while ((ligne = inputFileOBJ.readLine()) != null) {
                ligneParse = ligne.split(" ");
                if (ligneParse[0].equals("mtllib")) {
                    fileMTLExist = true;
                    break;
                }
            }
            if (fileMTLExist) {
                String[] fileName = filename.split("/");
                if (fileName.length > 1) {
                    fileLocationMtl = path;
                    for (int i = 0; i < fileName.length - 1; i++) {
                        fileLocationMtl += fileName[i] + "/";
                    }
                    fileLocationMtl += ligneParse[1];
                } else {
                    fileLocationMtl = path + ligneParse[1];
                }
            }


            /**
             * Opening material file (.mtl)
             */
            if (fileMTLExist) {
                try {
                    readMTLFile(fileLocationMtl);
                } catch (Exception e) {
                    fichierObj.close();
                    throw e;
                }
            }

            /**
             * Treatment of object file
             */
            logger.debug("2 - Traitement du fichier " + fileLocation);
            /**
             * Counting the number of vertex, normals and coordTextures
             */
            int i1 = 0, i2 = 0, i3 = 0, i4 = 0;

            /**
             * Return to begin of object file
             */
            fichierObj.getChannel().position(0);
            inputFileOBJ = new BufferedReader(new InputStreamReader(fichierObj));

            while ((ligne = inputFileOBJ.readLine()) != null) {
                ligneParse = ligne.split(" ");
                String type = ligneParse[0];


                if (type.equals("v")) {
                    i1++;
                    nbVertTot++;
                } else if (type.equals("vt")) {
                    i2++;
                    nbTextTot++;
                } else if (type.equals("vn")) {
                    i3++;
                    nbNormTot++;
                } else if (type.equals("f")) {
                    i4++;
                } /*else if (type.equals("usemtl"))
                 {
                 if (nbMaterial > 0)
                 {
                 nbVert.add(i1); i1 = 0;
                 nbText.add(i2); i2 = 0;
                 nbNorm.add(i3); i3 = 0;
                 nbFace.add(i4); i4 = 0;
                 }
                 nbMaterial++;
                 }*/ 
                else if (type.equals("g")) 
                {
                    if (ligneParse.length > 1)
                        type = ligneParse[1];
                    else 
                        type = "";

                    if (!type.equals("default") && !type.equals("Defaut") && !type.equals("")) 
                    {
                        if (nbMaterial > 0) 
                        {
                            nbFace.add(i4);

                            i4 = 0;
                        }
                        nbMaterial++;
                    } 
                    else 
                    {
	                    if (nbMaterial > 0) 
	                    {
	                        nbVert.add(i1);
	                        nbText.add(i2);
	                        nbNorm.add(i3);
	
	                        i1 = 0;
	                        i2 = 0;
	                        i3 = 0;
	                    }
                    }
                }
            }
            nbVert.add(i1);
            i1 = 0;
            nbText.add(i2);
            i2 = 0;
            nbNorm.add(i3);
            i3 = 0;
            nbFace.add(i4);
            i4 = 0;

            /**
             * Return to begin of object file
             */
            fichierObj.getChannel().position(0);
            inputFileOBJ = new BufferedReader(new InputStreamReader(fichierObj));

            /**
             * Allocation of memory for buffer contains vertex, ...
             */
            /*vertex = new Vector<FloatBuffer>(nbMaterial);
             normal = new Vector<FloatBuffer>(nbMaterial);
             texture = new Vector<FloatBuffer>(nbMaterial);*/
            face = new Face[nbMaterial][];
            for (int i = 0; i < nbMaterial; i++) {
                face[i] = new Face[nbFace.get(i)];
            }
            nbQuad = new int[nbMaterial];
            nbTriangle = new int[nbMaterial];
            materialGroup = new String[nbMaterial];
            vertex = BufferUtil.newFloatBuffer(nbVertTot * 3);
            normal = BufferUtil.newFloatBuffer(nbNormTot * 3);
            texture = BufferUtil.newFloatBuffer(nbTextTot * 3);


            ResultObjMtlFile result = new ResultObjMtlFile();
            result.face = face;
            result.nbFace = nbFace;
            result.nbMaterial = nbMaterial;
            result.nbNorm = nbNorm;
            result.nbNormTot = nbNormTot;
            result.nbText = nbText;
            result.nbTextTot = nbTextTot;
            result.nbVert = nbVert;
            result.nbVertTot = nbVertTot;
            result.normal = normal;
            result.texture = texture;
            result.vertex = vertex;
            result.fileMTLExist = fileMTLExist;
            result.inputFileObj = inputFileOBJ;
            result.fileLocationMtl = fileLocationMtl;
            return result;
        } catch (IOException e) {

            throw e;
        }


    }

    protected ResultObjMtlFileIndexed readObjMtlFilesIndexed(String path, String filename, boolean stripWay) throws Exception {
        CompiledFile fileCompile = new CompiledFile(path, filename);
        ResultObjMtlFile result = null;

        FloatBuffer[] vertex = null;
        FloatBuffer[] normal = null;
        FloatBuffer[] texture = null;
        IntBuffer[] bufOrderedQuads = null;
        IntBuffer[] bufOrderedTriangles = null;
        Vector<Integer> nbFace = null;
        Vector<Integer> nbVert = null;
        Vector<Integer> nbNorm = null;
        Vector<Integer> nbText = null;
        IntBuffer[][] faceResult = null;
        IntegerByReference[][][] face = null;
        IntegerByReference[][] referenceVert = null;
        IntegerByReference[][] referenceText = null;
        IntegerByReference[][] referenceNorm = null;
        ResultObjMtlFileIndexed resultReturn;
        boolean fileMTLExist = false;
        int nbMaterial = -1;
        int nbFaces = -1;

        logger.debug("1 - Importation du fichier " + path + filename);

        if (!fileCompile.fileCompiled()) 
        {
            result = readObjMtlFiles(path, filename, stripWay);

            if (result == null) {
                throw new Exception("Files obj or/and mtl encoutered problem when write poperties.");
            }

            /**
             * Take the variable modified by readObjMtlFiles
             */
            fileMTLExist = result.fileMTLExist;
            BufferedReader inputFileOBJ = result.inputFileObj;


            nbVert = result.nbVert;
            nbNorm = result.nbNorm;
            nbText = result.nbText;
            nbFace = result.nbFace;

            vertex = new FloatBuffer[nbVert.capacity()];
            normal = new FloatBuffer[nbNorm.capacity()];
            texture = new FloatBuffer[nbText.capacity()];
            referenceVert = new IntegerByReference[nbVert.capacity()][];
            referenceText = new IntegerByReference[nbText.capacity()][];
            referenceNorm = new IntegerByReference[nbNorm.capacity()][];


            nbMaterial = result.nbMaterial;
            nbFaces = nbMaterial;

            face = new IntegerByReference[nbFaces][][];
            for (int i = 0, n = nbFaces; i < n; i++) {
                vertex[i] = BufferUtil.newFloatBuffer(nbVert.get(i) * 3);
                normal[i] = BufferUtil.newFloatBuffer(nbNorm.get(i) * 3);
                texture[i] = BufferUtil.newFloatBuffer(nbText.get(i) * 2);

                referenceVert[i] = new IntegerByReference[nbVert.get(i)];
                for (int j = 0, max = referenceVert[i].length; j < max; j++) {
                    referenceVert[i][j] = new IntegerByReference(j);
                }

                referenceText[i] = new IntegerByReference[nbText.get(i)];
                for (int j = 0, max = referenceText[i].length; j < max; j++) {
                    referenceText[i][j] = new IntegerByReference(j);
                }

                referenceNorm[i] = new IntegerByReference[nbNorm.get(i)];
                for (int j = 0, max = referenceNorm[i].length; j < max; j++) {
                    referenceNorm[i][j] = new IntegerByReference(j);
                }
            }
            for (int i = 0, n = nbFaces; i < n; i++) {
                face[i] = new IntegerByReference[nbFace.get(i)][];
            }

            /**
             * Put all vertex, normals and coordTextures from file object in
             * buffer
             */
            int indiceFaceMaterial = 0;
            int indiceFace = 0;
            /*if (fileMTLExist)
             indiceFaceMaterial = -1;*/
            String ligne;
            /*if (fileMTLExist)
             indiceFaceMaterial = -1;*/
            boolean differentFace = false;

            int offsetVert = 0;
            int offsetText = 0;
            int offsetNorm = 0;
            while ((ligne = inputFileOBJ.readLine()) != null) {
                String[] ligneParse = ligne.split(" +");
                String type = ligneParse[0];
                if (type.equals("v")) {
                    if (differentFace) {
                        indiceFaceMaterial++;
                        differentFace = false;
                        indiceFace = 0;

                        materialGroup[indiceFaceMaterial] = materialGroup[indiceFaceMaterial - 1];
                        offsetVert += nbVert.get(indiceFaceMaterial - 1);
                        offsetText += nbText.get(indiceFaceMaterial - 1);
                        offsetNorm += nbNorm.get(indiceFaceMaterial - 1);
                    }
                    vertex[indiceFaceMaterial].put(Buffers.newDirectFloatBuffer(parseThreeFloat(ligneParse)));
                } else if (type.equals("vt")) {
                    texture[indiceFaceMaterial].put(Buffers.newDirectFloatBuffer(parseTwoFloat(ligneParse)));
                } else if (type.equals("vn")) {
                    normal[indiceFaceMaterial].put(Buffers.newDirectFloatBuffer(parseThreeFloat(ligneParse)));
                } else if (type.equals("f")) {


                    String[][] tripleNum = null;
                    int[][] tripleInt = null;

                    if (!differentFace) {
                        differentFace = true;
                    }

                    if (ligneParse.length == 5) {
                        //We have a quad
                        tripleNum = new String[4][3];
                        tripleInt = new int[4][3];
                        tripleNum[0] = ligneParse[1].split("/");
                        tripleInt[0] = parseThreeInt(tripleNum[0]);
                        tripleNum[1] = ligneParse[2].split("/");
                        tripleInt[1] = parseThreeInt(tripleNum[1]);
                        tripleNum[2] = ligneParse[3].split("/");
                        tripleInt[2] = parseThreeInt(tripleNum[2]);
                        tripleNum[3] = ligneParse[4].split("/");
                        tripleInt[3] = parseThreeInt(tripleNum[3]);
                        face[indiceFaceMaterial][indiceFace] = new IntegerByReference[12];//IntBuffer.allocate(12);
                        face[indiceFaceMaterial][indiceFace][0] = referenceVert[indiceFaceMaterial][tripleInt[0][0] - 1 - offsetVert];
                        face[indiceFaceMaterial][indiceFace][1] = referenceVert[indiceFaceMaterial][tripleInt[1][0] - 1 - offsetVert];
                        face[indiceFaceMaterial][indiceFace][2] = referenceVert[indiceFaceMaterial][tripleInt[2][0] - 1 - offsetVert];
                        face[indiceFaceMaterial][indiceFace][3] = referenceVert[indiceFaceMaterial][tripleInt[3][0] - 1 - offsetVert];

                        face[indiceFaceMaterial][indiceFace][4] = referenceText[indiceFaceMaterial][tripleInt[0][1] - 1 - offsetText];
                        face[indiceFaceMaterial][indiceFace][5] = referenceText[indiceFaceMaterial][tripleInt[1][1] - 1 - offsetText];
                        face[indiceFaceMaterial][indiceFace][6] = referenceText[indiceFaceMaterial][tripleInt[2][1] - 1 - offsetText];
                        face[indiceFaceMaterial][indiceFace][7] = referenceText[indiceFaceMaterial][tripleInt[3][1] - 1 - offsetText];

                        face[indiceFaceMaterial][indiceFace][8] = referenceNorm[indiceFaceMaterial][tripleInt[0][2] - 1 - offsetNorm];
                        face[indiceFaceMaterial][indiceFace][9] = referenceNorm[indiceFaceMaterial][tripleInt[1][2] - 1 - offsetNorm];
                        face[indiceFaceMaterial][indiceFace][10] = referenceNorm[indiceFaceMaterial][tripleInt[2][2] - 1 - offsetNorm];
                        face[indiceFaceMaterial][indiceFace][11] = referenceNorm[indiceFaceMaterial][tripleInt[3][2] - 1 - offsetNorm];
                        if (tripleInt[0][0] - 1 - offsetVert < 0) {
                            System.err.println(indiceFaceMaterial + "Coucou:" + (tripleInt[0][0] - 1 - offsetVert));
                        }
                        nbQuad[indiceFaceMaterial]++;
                    } else {
                        //We have a triangle
                        tripleNum = new String[3][3];
                        tripleInt = new int[3][3];
                        tripleNum[0] = ligneParse[1].split("/");
                        tripleInt[0] = parseThreeInt(tripleNum[0]);
                        tripleNum[1] = ligneParse[2].split("/");
                        tripleInt[1] = parseThreeInt(tripleNum[1]);
                        tripleNum[2] = ligneParse[3].split("/");
                        tripleInt[2] = parseThreeInt(tripleNum[2]);
                        face[indiceFaceMaterial][indiceFace] = new IntegerByReference[9];// = IntBuffer.allocate(9);
                        face[indiceFaceMaterial][indiceFace][0] = referenceVert[indiceFaceMaterial][tripleInt[0][0] - 1 - offsetVert];
                        face[indiceFaceMaterial][indiceFace][1] = referenceVert[indiceFaceMaterial][tripleInt[1][0] - 1 - offsetVert];
                        face[indiceFaceMaterial][indiceFace][2] = referenceVert[indiceFaceMaterial][tripleInt[2][0] - 1 - offsetVert];

                        face[indiceFaceMaterial][indiceFace][3] = referenceText[indiceFaceMaterial][tripleInt[0][1] - 1 - offsetText];
                        face[indiceFaceMaterial][indiceFace][4] = referenceText[indiceFaceMaterial][tripleInt[1][1] - 1 - offsetText];
                        face[indiceFaceMaterial][indiceFace][5] = referenceText[indiceFaceMaterial][tripleInt[2][1] - 1 - offsetText];

                        face[indiceFaceMaterial][indiceFace][6] = referenceNorm[indiceFaceMaterial][tripleInt[0][2] - 1 - offsetNorm];
                        face[indiceFaceMaterial][indiceFace][7] = referenceNorm[indiceFaceMaterial][tripleInt[1][2] - 1 - offsetNorm];
                        face[indiceFaceMaterial][indiceFace][8] = referenceNorm[indiceFaceMaterial][tripleInt[2][2] - 1 - offsetNorm];
                        nbTriangle[indiceFaceMaterial]++;
                    }

                    indiceFace++;
                } else if (type.equals("usemtl")) {
                    materialGroup[indiceFaceMaterial] = ligneParse[1];
                    /*vertex.add(BufferUtil.newFloatBuffer(nbVect*3));
                     normal.add(BufferUtil.newFloatBuffer(nbNorm*3));
                     texture.add(BufferUtil.newFloatBuffer(nbText*2));*/
                } else if (type.equals("g")) {
                }

            }
            
            /**
             * -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- 
             * This part give same problems, if the vertex has more of one coordinate of texture or normal, 
             * you should be expect bug
             * -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!- -!-!-!-
             */
            
            
            try{
            /**
             * Reorganize the index of normal to obtain the same index of
             * vertex and normal
             */
            for (int i = 0, max = face.length; i < max; i++) 
            {
                boolean[] test = new boolean[vertex[i].capacity()];
                int l = 0;
                for (int j = 0, maxFace = face[i].length; j < maxFace; j++) 
                {
                    if (face[i][j] != null) 
                    {
                    	// TODO
                    	reorganizeIndex(face[i], j, texture[i], referenceText[i], test, OFFSET_TEXTURE, vertex[i],  referenceVert[i]);
                    	l++;
                    } else {
                        System.err.println("Error, j:" + j);
                    }
                }
                logger.debug("Nb tour tex : " + l);
                l = 0;
                test = new boolean[vertex[i].capacity()];
                for (int j = 0, maxFace = face[i].length; j < maxFace; j++) 
                {
                    if (face[i][j] != null) {
                        reorganizeIndex(face[i], j, normal[i],  referenceNorm[i], test, OFFSET_NORMAL, vertex[i],  referenceVert[i]);
                        /*vertex[i] = res[0].attr;
                        referenceVert[i] = res[0].attrIndex;
                        normal[i] = res[1].attr;
                        referenceNorm[i] = res[1].attrIndex;
                        texture[i] = res[2].attr;
                        referenceText[i] = res[2].attrIndex;*
                        
                        Util.getInstance().display(vertex[i], (byte)3, "Vertex position " + j);
                        for (IntegerByReference[] oneFace : face[i]) {
							Util.getInstance().display(oneFace, (byte)3, "Index vertex");
						}*/
                        
                        
                        l++;
                    } else {
                        System.err.println("Error, j:" + j);
                    }
                }
                logger.debug("Nb tour tex : " + l);
                /*int x = 0;
                for (int k = 0; k < test.length; k++) 
                {
                    if (test[k]) 
                    {
                        x++;
                    }
                }
                System.out.println("Nombre de vertex remplacés : " + x);*/
            }
            }
            catch (Exception e)
            {
            	e.printStackTrace();
            	System.exit(2);
            }
            

            faceResult = convertTableTableTableIntegerByRef(face);

            //createObjFile(path + "nouveau.obj", vertex, normal, texture, faceResult);		


            /**
             * Order all vertex for the index for IBO We want this model :
             * V1,V2,V3,V4,V5,V6,...,T1,T2,T3,...,N1,... and we have :
             * V1,V2,V3,T1,T2,T3,N1,N2,N3,V4,...
             */
            bufOrderedQuads = new IntBuffer[nbFaces];
            bufOrderedTriangles = new IntBuffer[nbFaces];
            for (int i = 0; i < nbFaces; i++) {
                bufOrderedQuads[i] = IntBuffer.allocate(nbQuad[i] * 4 * 3);//-1
                bufOrderedTriangles[i] = IntBuffer.allocate(nbTriangle[i] * 3 * 3);
                /**
                 * Put the vertex
                 */
                for (int j = 0, n = face[i].length; j < n; j++) {
                    if (faceResult[i][j].capacity() > 9) {
                        //We have a quad
                        bufOrderedQuads[i].put(faceResult[i][j].get(0));
                        bufOrderedQuads[i].put(faceResult[i][j].get(1));
                        bufOrderedQuads[i].put(faceResult[i][j].get(2));
                        bufOrderedQuads[i].put(faceResult[i][j].get(3));
                    } else {
                        //We have a triangle
                        bufOrderedTriangles[i].put(faceResult[i][j].get(0));
                        bufOrderedTriangles[i].put(faceResult[i][j].get(1));
                        bufOrderedTriangles[i].put(faceResult[i][j].get(2));
                    }
                }

                /**
                 * Put the textures coordinates
                 */
                for (int j = 0, n = faceResult[i].length; j < n; j++) {

                    if (faceResult[i][j].capacity() > 9) {
                        //We have a quad
                        bufOrderedQuads[i].put(faceResult[i][j].get(4));
                        bufOrderedQuads[i].put(faceResult[i][j].get(5));
                        bufOrderedQuads[i].put(faceResult[i][j].get(6));
                        bufOrderedQuads[i].put(faceResult[i][j].get(7));
                    } else {
                        // We have a triangle
                        bufOrderedTriangles[i].put(faceResult[i][j].get(3));
                        bufOrderedTriangles[i].put(faceResult[i][j].get(4));
                        bufOrderedTriangles[i].put(faceResult[i][j].get(5));
                    }
                }

                /**
                 * Put the normals
                 */
                for (int j = 0, n = faceResult[i].length; j < n; j++) {

                    if (faceResult[i][j].capacity() > 9) {
                        //We have a quad
                        bufOrderedQuads[i].put(faceResult[i][j].get(8));
                        bufOrderedQuads[i].put(faceResult[i][j].get(9));
                        bufOrderedQuads[i].put(faceResult[i][j].get(10));
                        bufOrderedQuads[i].put(faceResult[i][j].get(11));
                    } else {
                        //We have a triangle
                        bufOrderedTriangles[i].put(faceResult[i][j].get(6));
                        bufOrderedTriangles[i].put(faceResult[i][j].get(7));
                        bufOrderedTriangles[i].put(faceResult[i][j].get(8));
                    }
                }


            }
            result.inputFileObj.close();

            resultReturn = new ResultObjMtlFileIndexed();
            resultReturn.bufOrderedQuads = bufOrderedQuads;
            resultReturn.bufOrderedTriangles = bufOrderedTriangles;
            resultReturn.nbQuad = nbQuad;
            resultReturn.nbTriangle = nbTriangle;
            resultReturn.nbFaces = nbFaces;
            resultReturn.nbFace = nbFace;
            resultReturn.vertex = vertex;
            resultReturn.normal = normal;
            resultReturn.texture = texture;
            resultReturn.face = faceResult;
            resultReturn.materialGroup = materialGroup;
            resultReturn.fileMTLName = result.fileLocationMtl;
            resultReturn.fileMTLExist = fileMTLExist;

            fileCompile.CompileFile(resultReturn);
        } else //If the file is already read
        {
            resultReturn = fileCompile.ReadFileCompiled();
            nbQuad = resultReturn.nbQuad;
            nbTriangle = resultReturn.nbTriangle;
            materialGroup = resultReturn.materialGroup;
            readMTLFile(resultReturn.fileMTLName);
            wayDrawQuad = GL2.GL_QUADS;
            wayDrawTriangle = GL.GL_TRIANGLES;
        }

        return resultReturn;
    }

    /*
     * -
     * --------------
     * ---- Private Methods -----
     * ---------------------------------------
     * ----------------------------------------------------
     */
    private void readMTLFile(String fileLocationMtl) throws NumberFormatException, IOException {
        BufferedReader inputFileMTL;
        String[] pathArray = fileLocationMtl.split("/");
        String path = "";
        for (int i = 0; i < pathArray.length - 1; i++)
        {
        	path += pathArray[i] + '/';
        }
        
        
        try {
            inputFileMTL = new BufferedReader(new FileReader(fileLocationMtl));
        } catch (FileNotFoundException e) {
            System.out.println("The material file has encountered a problem during first opening.");
            throw e;
        }

        /**
         * Treatment of material file
         */
        logger.debug("2 - Traitement du fichier (matérial) " + fileLocationMtl);
        material = new Hashtable<String, Material>();
        String ligne;
        String indiceMaterialTemp = null;
        while ((ligne = inputFileMTL.readLine()) != null) {
            String[] ligneParse = ligne.split(" ");
            String type = ligneParse[0];
            if (type.equals("newmtl")) {
                /**
                 * New material
                 */
                material.put(ligneParse[1], new Material());
                indiceMaterialTemp = ligneParse[1];
            } else if (type.equals("Ka")) {
                /**
                 * The ambient reflectivity of material (type Ka r g b)
                 */
                material.get(indiceMaterialTemp).setAmbiantReflexivity(parseThreeFloat(ligneParse));
                //TODO type Ka spectral file.rfl factor AND Ka xyz x y z 
            } else if (type.equals("Kd")) {
                /**
                 * The diffuse reflectivity of material (type Kd r g b)
                 */
                material.get(indiceMaterialTemp).setDiffuseReflexivity(parseThreeFloat(ligneParse));
                //TODO type Kd spectral file.rfl factor AND Kd xyz x y z 
            } else if (type.equals("Ks")) {
                /**
                 * The specular reflectivity of material (type Ks r g b)
                 */
                material.get(indiceMaterialTemp).setSpecularReflexivity(parseThreeFloat(ligneParse));
                //TODO type Ks spectral file.rfl factor AND Ks xyz x y z 
            } else if (type.equals("Tf")) {
                /**
                 * The transmission filter of material (type Tf r g b)
                 */
                material.get(indiceMaterialTemp).setTransmissionFilter(parseThreeFloat(ligneParse));
                //TODO type Tf spectral file.rfl factor AND Tf xyz x y z 
            } else if (type.equals("d")) {
                /**
                 * The dissolve (transparency) of material (type Tf r g b)
                 */
                material.get(indiceMaterialTemp).setTransparency(Float.parseFloat(ligneParse[1]));
                //TODO type Tf spectral file.rfl factor AND Tf xyz x y z 
            } else if (type.equals("illum")) {
                /**
                 * The illumination type 0 Color on and Ambient off 1 Color on
                 * and Ambient on 2 Highlight on 3 Reflection on and Ray trace
                 * on 4 Transparency: Glass on and Reflection: Ray trace on 5
                 * Reflection: Fresnel on and Ray trace on 6 Transparency:
                 * Refraction on and Reflection: Fresnel off and Ray trace on 7
                 * Transparency: Refraction on and Reflection: Fresnel on and
                 * Ray trace on 8 Reflection on and Ray trace off 9
                 * Transparency: Glass on and Reflection: Ray trace off 10 Casts
                 * shadows onto invisible surfaces
                 */
                material.get(indiceMaterialTemp).setIlluminationType((int) Float.parseFloat(ligneParse[1]));
            } else if (type.equals("Ns")) {
                /**
                 * The specular exponent of material value 0 to 1000
                 */
                material.get(indiceMaterialTemp).setSpecularExponent((int) Float.parseFloat(ligneParse[1]));
            } else if (type.equals("Ni")) {
                /**
                 * The optical density of material value 0.001 to 10
                 */
                material.get(indiceMaterialTemp).setOpticalDensity(Float.parseFloat(ligneParse[1]));
            } else if (type.equals("map_Kd")) {
                /**
                 * The texture file
                 */
            	material.get(indiceMaterialTemp).setTexture(new TextureObject(path + ligneParse[ligneParse.length - 1], true));
            }
        }
        inputFileMTL.close();
    }

    /**
     * Parse three float contained in string ignoring the first element
     * Typically the form of String is [String, Float in String, Float in
     * String, Float in String]
     *
     * @param threeFloats Table of String to parse
     * @return
     */
    private float[] parseThreeFloat(String[] threeFloats) {
        return new float[]{Float.parseFloat(threeFloats[1]), Float.parseFloat(threeFloats[2]), Float.parseFloat(threeFloats[3])};
    }

    /**
     * Parse Two float contained in string ignoring the first element Typically
     * the form of String is [String, Float in String, Float in String]
     *
     * @param twoFloats Table of String to parse
     * @return
     */
    private float[] parseTwoFloat(String[] twoFloats) {
        return new float[]{Float.parseFloat(twoFloats[1]), Float.parseFloat(twoFloats[2])};
    }

    /**
     * Parse three int contained in string ignoring the first element Typically
     * the form of String is [String, Int in String, Int in String, Int in
     * String]
     *
     * @param threeInt Table of String to parse
     * @return
     */
    private int[] parseThreeInt(String[] threeInt) {
        return new int[]{Integer.parseInt(threeInt[0]), Integer.parseInt(threeInt[1]), Integer.parseInt(threeInt[2])};
    }
    // [0]:offset for triangle, [1]:offset for quad, [2]:length of attribute data
    private static byte[] OFFSET_TEXTURE = {3, 4, 2}, OFFSET_NORMAL = {6, 8, 3};

    /**
     * Move index of attribute for correspond to index position, create new vertex if (s'il y a plus ou moins d'index position par rapport à spécifiés (attribute)) 
     *
     * @param face
     * @param index
     * @param attribut
     * @param tableAttribute
     * @param test
     * @param offset
     * @return Face created if need, null 
     */
    private static void reorganizeIndex(IntegerByReference[][] face, 
    									int index, 
    									FloatBuffer attribut,
    									IntegerByReference[] tableAttribute, 
    									boolean[] test, 
    									byte[] offset,
    									FloatBuffer attributPosition,
    									IntegerByReference[] tableAttributePosition)
    {
    	/*byte[] offsetOther;
    	if (offset == OFFSET_NORMAL)
    		offsetOther = OFFSET_TEXTURE;
    	else
    		offsetOther = OFFSET_NORMAL;*/
    	
        // If we dealing with quad
        if (face[index].length > 11) 
        {
            // For all index concerded by type specified (normal or texture coordinates)
            for (int i = 0; i < 4; i++) {
                int indexVert = face[index][i].getInt(), indexAttribut = face[index][offset[1] + i].getInt();
                // On ne fait rien si l'indice de l'result[1].attrest égale à celui de la position
                if (indexVert != indexAttribut) {
                    if (!test[indexVert]) 
                    {
                    	// Si le vertex n'a pas encore été modifié
                    	attribut.rewind();

                        float[] data = new float[offset[2]];
                        for (byte k = 0; k < offset[2]; k++) 
                        {
                            data[k] = attribut.get(indexAttribut * offset[2] + k);
                            attribut.put(indexAttribut * offset[2] + k, attribut.get(indexVert * offset[2] + k));
                            attribut.put(indexVert * offset[2] + k, data[k]);
                        }

                        int resultSearch = search(tableAttribute, indexVert);
                        tableAttribute[resultSearch].setInt(indexAttribut);
                        face[index][offset[1] + i].setInt(indexVert);
                        test[indexVert] = true;
                    }
                    else
                    {
                    	
                    }
                }
                else
                {
                	test[indexVert] = true;
                }
            }
        } // Else if we dealing with triangle
        else if (face[index].length > 8) 
        {
        	
            // For all index concerned by type specified (normal or texture coordinates)
            for (int i = 0; i < 3; i++) 
            {
                int indexVert = face[index][i].getInt(), 
                	indexAttribut = face[index][offset[0] + i].getInt();
                if (indexVert != indexAttribut) 
                {
                	
                    if (!test[indexVert]) 
                    {
                        float[] data = new float[offset[2]];

                        for (byte k = 0; k < offset[2]; k++) {
                            data[k] = attribut.get(indexAttribut * offset[2] + k);
                            attribut.put(indexAttribut * offset[2] + k, attribut.get(indexVert * offset[2] + k));
                            attribut.put(indexVert * offset[2] + k, data[k]);
                        }

                        int resultSearch = search(tableAttribute, indexVert);
                        tableAttribute[resultSearch].setInt(indexAttribut);
                        face[index][offset[0] + i].setInt(indexVert);
                        test[indexVert] = true;
                    }
                    else
                    {
                    	
                    }
                    
                }
                else
                {
                	test[indexVert] = true;
                }
            }
        }
    }
    private static int search(IntegerByReference[] data, int dataSearch) {
        int i;
        int max = data.length;
        for (i = 0; i < max; i++) {
            if (data[i].equal(dataSearch)) {
                break;
            }
        }
        return i;
    }

    private IntBuffer[][] convertTableTableTableIntegerByRef(IntegerByReference[][][] data) {
        IntBuffer[][] dataResult = new IntBuffer[data.length][];

        for (int i = 0, max = dataResult.length; i < max; i++) {
            dataResult[i] = new IntBuffer[data[i].length];
            for (int j = 0, maxF = data[i].length; j < maxF; j++) {
                dataResult[i][j] = Buffers.newDirectIntBuffer(data[i][j].length);
                dataResult[i][j].put(data[i][j][0].getInt());
                dataResult[i][j].put(data[i][j][1].getInt());
                dataResult[i][j].put(data[i][j][2].getInt());
                dataResult[i][j].put(data[i][j][3].getInt());
                dataResult[i][j].put(data[i][j][4].getInt());
                dataResult[i][j].put(data[i][j][5].getInt());
                dataResult[i][j].put(data[i][j][6].getInt());
                dataResult[i][j].put(data[i][j][7].getInt());
                if (data[i][j].length > 9) {
                    dataResult[i][j].put(data[i][j][8].getInt());
                    dataResult[i][j].put(data[i][j][9].getInt());
                    dataResult[i][j].put(data[i][j][10].getInt());
                    dataResult[i][j].put(data[i][j][11].getInt());
                }
            }
        }


        return dataResult;
    }

    @SuppressWarnings("unused")
    private void createObjFile(String fileName, FloatBuffer[] vertex, FloatBuffer[] normal, FloatBuffer[] texture, IntBuffer face[][]) throws IOException {
        FileWriter file = new FileWriter(new File(fileName));
        file.write("# This file uses centimeters as units for non-parametric coordinates.\n\rmtllib tunel.mtl\ng default\n");
        vertex[0].rewind();
        for (int i = 0, max = vertex[0].capacity(); i < max; i += 3) {
            file.write("v " + formatFloat(vertex[0].get()) + " " + formatFloat(vertex[0].get()) + " " + formatFloat(vertex[0].get()) + "\n");
        }
        texture[0].rewind();
        for (int i = 0, max = texture[0].capacity(); i < max; i += 2) {
            file.write("vt " + formatFloat(texture[0].get()) + " " + formatFloat(texture[0].get()) + "\n");
        }
        normal[0].rewind();
        for (int i = 0, max = normal[0].capacity(); i < max; i += 3) {
            file.write("vn " + formatFloat(normal[0].get()) + " " + formatFloat(normal[0].get()) + " " + formatFloat(normal[0].get()) + "\n");
        }

        file.write("s 1\ng pTorus1\nusemtl layeredShader1SG\n");
        for (int i = 0, max = face[0].length; i < max; i++) {
            if (face[0][i].capacity() > 9) {
                file.write("f " + (1 + face[0][i].get(0)) + "/" + (1 + face[0][i].get(4)) + "/" + (1 + face[0][i].get(8)) + " "
                        + (1 + face[0][i].get(1)) + "/" + (1 + face[0][i].get(5)) + "/" + (1 + face[0][i].get(9)) + " "
                        + (1 + face[0][i].get(2)) + "/" + (1 + face[0][i].get(6)) + "/" + (1 + face[0][i].get(10)) + " "
                        + (1 + face[0][i].get(3)) + "/" + (1 + face[0][i].get(7)) + "/" + (1 + face[0][i].get(11)) + "\n");
            } else {
                file.write("f " + (1 + face[0][i].get(0)) + "/" + (1 + face[0][i].get(3)) + "/" + (1 + face[0][i].get(6)) + " "
                        + (1 + face[0][i].get(1)) + "/" + (1 + face[0][i].get(4)) + "/" + (1 + face[0][i].get(7)) + " "
                        + (1 + face[0][i].get(2)) + "/" + (1 + face[0][i].get(5)) + "/" + (1 + face[0][i].get(8)) + "\n");
            }
        }

        file.close();
    }

    private static String formatFloat(Float f) {
        String result = f.toString();
        int max = 8;
        if (f < 0) {
            max++;
        }
        float x = Math.abs(f);
        if (x > 10) {
            if (x > 100) {
                if (x > 1000) {
                    max += 3;
                } else {
                    max += 2;
                }
            }
            max++;
        }
        for (int i = result.length(); i < max; i++) {
            result += 0;
        }
        return result;
    }
}
