package java_3d_engine.models.object;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.Vector;

import com.jogamp.common.nio.Buffers;

/**
 * 
 * @author Olivier
 *
 */
public class CompiledFile 
{
	private static final String extensionCompileFile = ".objcompile";
	
	//Type of variable
	private static final byte typeFloat		= (byte)0x1; //0b00000001;
	private static final byte typeInt 		= (byte)0x2; //0b00000010;
	//private static final byte typeShort 		= 0b00000011;
	private static final byte typeBoolean 		= (byte)0x4; //0b00000100;
	//private static final byte typeLong 		= 0b00000100;
	//private static final byte typeChar 		= 0b00000101;
	private static final byte typeString 		= (byte)0x6; //0b00000110;
	//private static final byte typeDouble	= 0b00000111;
	
	//Type of data
	private static final byte dataBufOrderedQuads		= (byte)0x8; //0b00001000;
	private static final byte dataBufOrderedTriangles	= (byte)0x10; //0b00010000;
	private static final byte dataVertex			= (byte)0x18; //0b00011000;
	private static final byte dataNormal 			= (byte)0x20; //0b00100000;
	private static final byte dataTexCoord			= (byte)0x28; //0b00101000;
	private static final byte dataNbFace			= (byte)0x30; //0b00110000;
	private static final byte dataNbTriangle		= (byte)0x38; //0b00111000;
	private static final byte dataNbQuad			= (byte)0x40; //0b01000000;
	private static final byte dataFileMTLName		= (byte)0x48; //0b01001000;
	private static final byte dataFace			= (byte)0x50; //0b01010000;
	private static final byte dataNbFaces			= (byte)0x58; //0b01011000;
	private static final byte dataMaterialGroup		= (byte)0x60; //0b01100000;
	private static final byte dataFileMtlExist		= (byte)0x68; //0b01101000;
	//private static final byte dataNum3		= 0b01110000;
	//private static final byte dataNum4		= 0b01111000;
	
	private final String path;
	private final String fileName;
	
	public CompiledFile (String path, String fileName)
	{	
		this.path = path;
		this.fileName = fileName;
	}
	
	//---
	//-----------
	//	Publics Methods
	//--------------------------
	//------------------------------------
	
	public Boolean fileCompiled ()
	{
		return new File(path + fileName + extensionCompileFile).exists();
	}
	
	public ResultObjMtlFileIndexed ReadFileCompiled () throws IOException
	{
		ResultObjMtlFileIndexed dataResult = new ResultObjMtlFileIndexed();
		DataInputStream file = new DataInputStream(new BufferedInputStream(new FileInputStream(path + fileName + extensionCompileFile)));
		
		//mask = 0b 0111 1000
		byte mask = (byte)0x78;//0b1111000;
		
		while (file.available() > 0)
		{
			byte typeOfData = file.readByte();
			
			switch (typeOfData & mask) 
			{
				case dataBufOrderedTriangles:
					dataResult.bufOrderedTriangles = readTableIntBuffer(file); break;
					
				case dataBufOrderedQuads:
					dataResult.bufOrderedQuads = readTableIntBuffer(file); break;
					
				case dataVertex:
					dataResult.vertex = readTableFloatBuffer(file); break;
					
				case dataTexCoord:
					dataResult.texture = readTableFloatBuffer(file); break;
					
				case dataNormal:
					dataResult.normal = readTableFloatBuffer(file); break;
					
				case dataNbFaces:
					dataResult.nbFaces = file.readInt(); break;
					
				case dataNbFace:
					dataResult.nbFace = readVector(file); break;
					
				case dataFace:
					dataResult.face = readTableOfTableIntBuffer(file); break;
					
				case dataNbQuad:
					dataResult.nbQuad = readTableInt(file); break;
					
				case dataNbTriangle:
					dataResult.nbTriangle = readTableInt(file); break;
					
				case dataFileMTLName:
					dataResult.fileMTLName = readString(file); break;
					
				case dataMaterialGroup:
					dataResult.materialGroup = readTableString(file); break;
					
				case dataFileMtlExist:
					dataResult.fileMTLExist = readBoolean(file); break;
					
				default:
					System.err.println("Nothing"); break;
				
			}
		}
		
		file.close();
		return dataResult;
	}
	
	public void CompileFile (ResultObjMtlFileIndexed data) throws IOException
	{
		DataOutputStream file = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(path + fileName + extensionCompileFile)));
		
		byte dataInfo = (byte) (typeInt | dataBufOrderedQuads);
		writeTableBuffer(file, data.bufOrderedQuads, dataInfo);
		
		dataInfo = (byte) (typeInt | dataBufOrderedTriangles);
		writeTableBuffer(file, data.bufOrderedTriangles, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataVertex);
		writeTableBuffer(file, data.vertex, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataTexCoord);
		writeTableBuffer(file, data.texture, dataInfo);
		
		dataInfo = (byte) (typeFloat | dataNormal);
		writeTableBuffer(file, data.normal, dataInfo);
		
		dataInfo = (byte) (typeInt | dataNbFace);
		writeVector(file, data.nbFace, dataInfo);
		
		dataInfo = (byte) (typeInt | dataNbFaces);
		writeInt(file, data.nbFaces, dataInfo);
		
		dataInfo = (byte) (typeInt | dataFace);
		writeTableOfTableIntBuffer(file, data.face, dataInfo);
		
		dataInfo = (byte) (typeInt | dataNbQuad);
		writeTableInt(file, data.nbQuad, dataInfo);
		
		dataInfo = (byte) (typeInt | dataNbTriangle);
		writeTableInt(file, data.nbTriangle, dataInfo);
		
		dataInfo = (byte) (typeString | dataFileMTLName);
		writeString(file, data.fileMTLName, dataInfo);
		
		dataInfo = (byte) (typeString | dataMaterialGroup);
		writeTableString(file, data.materialGroup, dataInfo);
		
		dataInfo = (byte) (typeBoolean | dataFileMtlExist);
		writeBoolean(file, data.fileMTLExist, dataInfo);
		
		file.close();
	}
	
	//---
	//----------
	//	Privates Methods
	//--------------------------
	//------------------------------------
	
		//---------------------
		//	Functions writer
		//---------------------
	private void writeBoolean (DataOutputStream file, boolean data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);

		file.writeBoolean(data);
	}
	
	/**
	 * Write a table of Buffer whatever the type
	 * @param file The file where the data will be written
	 * @param data The table of buffer to write in file
	 * @param infoTypeAndData 
	 * @throws IOException
	 */
	private void writeTableBuffer (DataOutputStream file, Buffer[] data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);

		writeTableBufferUnlessInfo(file, data, infoTypeAndData);
	}
	
	private void writeTableBufferUnlessInfo (DataOutputStream file, Buffer[] data, byte infoTypeAndData) throws IOException
	{
		file.writeInt(data.length);
		
		for (int i = 0, max = data.length; i < max; i++)
		{
			if (data[i] != null)
			{
				data[i].rewind();
				writeBuffer(file, data[i]);
			}
			else
				file.writeInt(0);
		}
	}

	private void writeVector (DataOutputStream file, Vector<Integer> data, byte infoTypeAndData) throws IOException
	{
		int length = data.size();
		
		file.write(infoTypeAndData);
		file.writeInt(length);
		
		for (int i = 0; i < length; i++)
		{
			file.writeInt(data.get(i));
		}
	}
	
	private void writeInt (DataOutputStream file, int data, byte infoTypeAndData) throws IOException
	{
		file.writeByte(infoTypeAndData);
		file.writeInt(data);
	}
	
	private void writeBuffer (DataOutputStream file, Buffer data) throws IOException
	{
		IntBuffer intBufDirect = Buffers.newDirectIntBuffer(0);
		FloatBuffer floatBufDirect = Buffers.newDirectFloatBuffer(0);
		ShortBuffer shortBufDirect = Buffers.newDirectShortBuffer(0);
		IntBuffer intBuf = IntBuffer.allocate(0);
		FloatBuffer floatBuf = FloatBuffer.allocate(0);
		ShortBuffer shortBuf = ShortBuffer.allocate(0);
		
		file.writeInt(data.capacity());

		if (data.getClass().equals(intBuf.getClass()) || data.getClass().equals(intBufDirect.getClass()))
		{
			IntBuffer dataTypeKnown = (IntBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeInt(dataTypeKnown.get(i));
		}
		else if (data.getClass().equals(floatBuf.getClass()) || data.getClass().equals(floatBufDirect.getClass()))
		{
			FloatBuffer dataTypeKnown = (FloatBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeFloat(dataTypeKnown.get(i));
		}
		else if (data.getClass().equals(shortBuf.getClass()) || data.getClass().equals(shortBufDirect.getClass()))
		{
			ShortBuffer dataTypeKnown = (ShortBuffer) data;
			
			for (int i = 0, max = data.capacity(); i < max; i++)
				file.writeShort(dataTypeKnown.get(i));
		}
		else
		{
			System.err.println("Problem when compilation file.");
		}		
	}

	private void writeTableOfTableIntBuffer (DataOutputStream file, Buffer[][] data, byte infoTypeAndData) throws IOException
	{
		int length = data.length;
		
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		
		for (int i = 0; i < length; i++)
		{
			writeTableBufferUnlessInfo(file, data[i], infoTypeAndData);
		}
	}
	
	private void writeTableInt (DataOutputStream file, int[] data, byte infoTypeAndData) throws IOException
	{
		int length = data.length;
		
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		
		for (int i = 0; i < length; i++)
		{
			file.writeInt(data[i]);
		}
	}
	
	private void writeString (DataOutputStream file, String data, byte infoTypeAndData) throws IOException
	{
		file.write(infoTypeAndData);
		writeStringUnlessInfo(file, data, infoTypeAndData);
	}
	
	private void writeStringUnlessInfo (DataOutputStream file, String data, byte infoTypeAndData) throws IOException
	{
		if (data == null)
			file.writeShort(0);
		else
		{
			file.writeShort(data.length());
			file.writeChars(data);
		}
	}
	
	private void writeTableString (DataOutputStream file, String[] data, byte infoTypeAndData) throws IOException
	{
		file.write(infoTypeAndData);
		file.writeInt(data.length);
		for (int i = 0, max = data.length; i < max; i++)
		{
			writeStringUnlessInfo(file, data[i], infoTypeAndData);
		}
	}
	
		//---------------------
		//	Functions reader
		//---------------------	
	
	private boolean readBoolean (DataInputStream file) throws IOException
	{
		return file.readBoolean();
	}
	
	private int[] readTableInt (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		int[] dataResult = new int[max];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = file.readInt();
		}
		
		return dataResult;
	}
	
	private IntBuffer[] readTableIntBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		IntBuffer[] dataResult = new IntBuffer[max];
		
		for (int i = 0; i < max; i++)
		{
			int length = file.readInt();
			dataResult[i] = Buffers.newDirectIntBuffer(length);
			for (int j = 0; j < length; j++)
			{
				dataResult[i].put(file.readInt());
			}
			dataResult[i].rewind();
		}
		return dataResult;
	}
	
	private IntBuffer[][] readTableOfTableIntBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		IntBuffer[][] dataResult = new IntBuffer[max][];
		
		for (int i = 0; i < max; i++)
		{
			dataResult[i] = readTableIntBuffer(file);
		}
		
		return dataResult;
	}
	
	private FloatBuffer[] readTableFloatBuffer (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		FloatBuffer[] dataResult = new FloatBuffer[max];
		
		for (int i = 0; i < max; i++)
		{
			int length = file.readInt();
			dataResult[i] = Buffers.newDirectFloatBuffer(length);
			for (int j = 0; j < length; j++)
			{
				dataResult[i].put(file.readFloat());
			}
			dataResult[i].rewind();
		}
		
		return dataResult;
	}

	private Vector<Integer> readVector (DataInputStream file) throws IOException
	{
		int max = file.readInt();
		Vector<Integer> dataResult = new Vector<Integer>(max);
		
		for (int i = 0; i < max; i++)
		{
			dataResult.add(file.readInt());
		}
		
		return dataResult;
	}
	
	private String readString (DataInputStream file) throws IOException
	{
		short length = file.readShort();
		String dataResult = "";
		for (int i = 0; i < length; i++)
		{
			dataResult += file.readChar();
		}
		return dataResult;
	}
	
	private String[] readTableString (DataInputStream file) throws IOException
	{
		String dataResult[] = new String[file.readInt()];
		for (int i = 0, max = dataResult.length; i < max; i++)
		{
			dataResult[i] = readString(file);
		}
		return dataResult;
	}
}
