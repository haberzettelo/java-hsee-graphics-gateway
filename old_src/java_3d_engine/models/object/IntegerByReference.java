package java_3d_engine.models.object;

/**
 * 
 * @author Olivier
 *
 */
public class IntegerByReference 
{
	private Integer membre;
	public IntegerByReference ()
	{
		membre = 0;
	}
	public IntegerByReference (int x)
	{
		membre = x;
	}
	
	public void tableAttribute(int x)
	{
		membre = x;
	}
	
	public Integer getInt ()
	{
		return membre;
	}
	
	public void setInt(int a)
	{
		membre = a;
	}
	
	public boolean equal (int x)
	{
		return membre == x;
	}
	
	public String toString ()
	{
		return ""+membre;
	}
}
