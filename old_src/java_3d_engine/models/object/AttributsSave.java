package java_3d_engine.models.object;

public class AttributsSave 
{
	/**
	 *  Tableau d'indice de position
	 */
	private int[] indexPos;
	/**
	 *  Tableau d'indice d'attributs
	 */
	private int[] indexAttr;
	/**
	 *  Tableau d'indice de l'autre attribut
	 */
	private int[] indexOAttr;
	/**
	 *  Tableau des nouveaux indices des positions attribuées
	 */
	private int[] indexPosMod;
	/**
	 *  Tableau des nouveaux indices de l'autre attribut
	 */
	private int[] indexOAttrMod;
	/**
	 * Taille completée des tableaux
	 */
	private int lengthFill = 0;
	
	public AttributsSave(int length)
	{
		indexPos = new int[length];
		indexAttr = new int[length];
		indexOAttr = new int[length];
		indexPosMod = new int[length];
		indexOAttrMod = new int[length];
	}
	
	/**
	 * Return the index modify, -1 if don't found
	 * @param indexPosition
	 * @param indexAttribut
	 * @return
	 */
	public int search (int indexPosition, int indexAttribut)
	{
		for(int i = 0;i < lengthFill;i++)
		{
			if (indexPos[i] == indexPosition && indexAttr[i] == indexAttribut)
				return indexPosMod[i];
		}
		return -1;
	}
	
	/**
	 * Return index modify [newIndexPosition, newIndexOtherAttribute], [-1, -1] if don't found
	 * @param indexPosition
	 * @param indexAttribut
	 * @param indexOtherAttribut
	 * @return
	 */
	public int[] search (int indexPosition, int indexAttribut, int indexOtherAttribut)
	{
		for(int i = 0;i < lengthFill;i++)
		{
			if (indexPos[i] == indexPosition && indexAttr[i] == indexAttribut && indexOAttr[i] == indexOtherAttribut)
				return new int[] {indexPosMod[i], indexOAttrMod[i]};
			
		}
		return new int[] {-1,-1};
	}
	
	public void put(int oldIndexPosition, int newIndexPosition, int indexAttribute)
	{
		indexPos[lengthFill] = oldIndexPosition;
		indexPosMod[lengthFill] = newIndexPosition;
		indexAttr[lengthFill] = indexAttribute;
		lengthFill++;
	}
	
	public void put(int oldIndexPosition, int newIndexPosition, int oldIndexOtherAttribute, int newIndexOtherAttribute, int indexAttribute)
	{
		indexPos[lengthFill] = oldIndexPosition;
		indexPosMod[lengthFill] = newIndexPosition;
		indexOAttr[lengthFill] = oldIndexOtherAttribute;
		indexOAttrMod[lengthFill] = newIndexOtherAttribute;
		indexAttr[lengthFill] = indexAttribute;
		lengthFill++;
	}
}
