package java_3d_engine.models.object;

import java.awt.Canvas;

import java_3d_engine.interfaces.GraphicLibrary;

import org.apache.log4j.Logger;

public class ThreadImport extends Canvas implements Runnable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9157716811635640041L;
	ObjectModel _object;
	String _path, _filename;
	
	private final static Logger logger = Logger.getLogger(ThreadImport.class);
	
	
	public ThreadImport (ObjectModel obj, String path, String filename)
	{
		_object = obj;
		_path = path;
		_filename = filename;
	}
	
	public ObjectModel getObject ()
	{
		
		return _object;
	}
	
	@Override
	public void run() 
	{
		try {
			logger.debug("Importation de " + _filename + " en multi-thread.");
			_object.initImport(_path, _filename);
			logger.debug("Fin de l'importation de " + _filename + " en multi-thread.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
