package java_3d_engine.models.object;

import java.io.BufferedReader;
import java.nio.FloatBuffer;
import java.util.Vector;

/**
 * 
 * @author Olivier
 *
 */
class ResultObjMtlFile
{
	public FloatBuffer vertex;
	public FloatBuffer normal;
	public FloatBuffer texture;
	public boolean fileMTLExist;
	public Face[][] face;
	public Vector<Integer> nbVert;
	public Vector<Integer> nbText;
	public Vector<Integer> nbNorm;
	public Vector<Integer> nbFace;
	public int nbVertTot = 0, nbTextTot = 0, nbMaterial = 0, nbNormTot = 0;
	public BufferedReader inputFileObj;
	public String fileLocationMtl;
	
	
	public ResultObjMtlFile() {
		// TODO Auto-generated constructor stub
	}
}
