package java_3d_engine.models.object;

/**
 * 
 * @author Olivier
 *
 */
abstract class Face 
{
	protected int[][] face = new int[3][3];
	protected int[] pointQuad = null;
	protected boolean quad;
	
	public Face (int[] p1, int[] p2, int[] p3)
	{
		face[0] = p1;
		face[1] = p2;
		face[2] = p3;
		quad = false;
	}
	
	public Face (int[][] points)
	{
		face = points;
		quad = false;
	}
	
	public int getP1Vertex ()
	{	return face[0][0];}
	public int getP2Vertex ()
	{	return face[1][0];}
	public int getP3Vertex ()
	{	return face[2][0];}
	public int getP1Texture ()
	{	return face[0][1];}
	public int getP2Texture ()
	{	return face[1][1];}
	public int getP3Texture ()
	{	return face[2][1];}
	public int getP1Normal ()
	{	return face[0][2];}
	public int getP2Normal ()
	{	return face[1][2];}
	public int getP3Normal ()
	{	return face[2][2];}

	public int getP4Vertex ()
	{	return 0;}
	public int getP4Texture ()
	{	return 0;}
	public int getP4Normal ()
	{	return 0;}
	
	public boolean isQuad ()
	{	return quad;}
	
	@Override
	public String toString ()
	{
		if (isQuad())
			return "[[" + face[0][0] + ", " + face[0][1] + ", " + face[0][2] + "], [" + 
					face[1][0] + ", " + face[1][1] + ", " + face[1][2] + "], [" + 
					face[2][0] + ", " + face[2][1] + ", " + face[2][2] + "], [" +
					pointQuad[0] + ", " + pointQuad[1] + ", " + pointQuad[2] + "]]";
		else
			return "[[" + face[0][0] + ", " + face[0][1] + ", " + face[0][2] + "], [" + 
				face[1][0] + ", " + face[1][1] + ", " + face[1][2] + "], [" + 
				face[2][0] + ", " + face[2][1] + ", " + face[2][2] + "]]";
	}

}
