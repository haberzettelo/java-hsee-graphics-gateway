package java_3d_engine.models.object;

/**
 * 
 * @author Olivier
 *
 */
class FaceQuad extends Face
{
	
	public FaceQuad (int[] p1, int[] p2, int[] p3, int[] p4)
	{
		super (p1,p2,p3);
		pointQuad = p4;
		quad = true;
	}
	
	public FaceQuad (int[][] points)
	{
		super (points);
		pointQuad = points[3];
		quad = true;
	}
	
	@Override
	public int getP4Vertex ()
	{	return pointQuad[0];}
	@Override
	public int getP4Texture ()
	{	return pointQuad[1];}
	@Override
	public int getP4Normal ()
	{	return pointQuad[2];}
}
