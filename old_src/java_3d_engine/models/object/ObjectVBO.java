package java_3d_engine.models.object;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;
import java_3d_engine.models.buffers.BufferObject;
import java_3d_engine.models.buffers.IndexBufferObject;
import java_3d_engine.models.buffers.VertexBufferObject;

import javax.media.opengl.GL;
import javax.media.opengl.fixedfunc.GLLightingFunc;

import main.GLException;

import org.apache.log4j.Logger;

import com.jogamp.common.nio.Buffers;
import com.sun.opengl.util.BufferUtil;

/**
 *
 * @author Olivier, Jean-Clement
 *
 */
public class ObjectVBO extends ObjectModel {

    /**
     * Logger qui sert à debugger l'application.
     */
    public final static Logger logger = Logger.getLogger(ObjectVBO.class);
    private int[] positionInMemory;
    private int[] nbFace = null;
    private IndexBufferObject[] IBOs;
    private VertexBufferObject[] VertexPositionNormal;
    private VertexBufferObject[] TextureCoord;
    private int strideVertexNormal = 6 * BufferUtil.SIZEOF_FLOAT;
    private boolean fileMtlExist;
    private ResultObjMtlFileIndexed resultObjMtlFileIndexed = null;
    
    /**
     * This constructor use VBO technology The memory of graphic card will be
     * used (with another method)
     * This constructor read obj file unless stock this in graphic card
     * This method need to call importObject(GraphicLibrary gl) in second time before 
     * use draw method.
     * @param path the path of your obj file (by example : "3DModel/")
     * @param filename the name of your file ("myobject" for myobject.obj)
     * @param gl OpenGL Context, it will not be used, just referenced
     * @return the new instance follow factory pattern
     * @throws Exception
     */
    public void initImport (String path, String filename) throws Exception
    {
    	resultObjMtlFileIndexed = readObjMtlFilesIndexed(path, filename, false);
        imported = true;
    }

    /**
     * This constructor use VBO technology The memory of graphic card will be
     * used
     * It will be import the obj and stock this in graphic card
     * @param path the path of your obj file (by example : "3DModel/")
     * @param filename the name of your file ("myobject" for myobject.obj)
     * @param gl OpenGL Context it will be used
     * @return the new instance follow factory pattern
     * @throws Exception
     */
    public void allImport (String path, String filename, GraphicLibrary gl) throws Exception
    {
    	resultObjMtlFileIndexed = readObjMtlFilesIndexed(path, filename, false);
    	imported = true;
    	importObject(gl);
    }

    /**
     * 
     * @param gl
     * @throws Exception
     */
	public void importObject (GraphicLibrary gl) throws Exception 
	{
        int nbFaces = resultObjMtlFileIndexed.nbFaces;
        FloatBuffer[] vertex = resultObjMtlFileIndexed.vertex;
        FloatBuffer[] normal = resultObjMtlFileIndexed.normal;
        FloatBuffer[] texture = resultObjMtlFileIndexed.texture;
        IntBuffer buf;

        IntBuffer[] bufOrderedQuads = resultObjMtlFileIndexed.bufOrderedQuads;
        IntBuffer[] bufOrderedTriangles = resultObjMtlFileIndexed.bufOrderedTriangles;

        fileMtlExist = resultObjMtlFileIndexed.fileMTLExist;
        
        /**
         * Init all textures
         */
        for (int i = 0; i < materialGroup.length; i++)
        	if (materialGroup[i] != null)
        		if (material.get(materialGroup[i]).getTexture() != null)
        			material.get(materialGroup[i]).getTexture().initTexture(gl);
        
        /**
         * Start of put in memory ordered of vertex, normals and coordTextures
         * for create faces
         */
        logger.debug("3 - Fin de l'importation des fichiers obj et mlt");

        

        /**
         * Put all vertex in memory of graphic card
         */
        positionInMemory = new int[nbFaces + 1];
        positionInMemory[0] = 0;

        /**
         * Count the number of object group by type of face
         */
        int nbOfFaces = 0;
        for (int i = 0; i < nbFaces; i++) {
            if (nbQuad[i] > 0) {
                nbOfFaces++;
            }
            if (nbTriangle[i] > 0) {
                nbOfFaces++;
            }
        }

        buf = IntBuffer.allocate(nbOfFaces * 3);
        gl.getGLx().glGenBuffers(nbOfFaces * 3, buf);
        buf.rewind();

        this.nbFace = new int[nbQuad.length + nbTriangle.length];


        
        IBOs = new IndexBufferObject[nbFace.length];
        VertexPositionNormal = new VertexBufferObject[nbQuad.length + nbTriangle.length];
        TextureCoord = new VertexBufferObject[nbQuad.length + nbTriangle.length];
        /**
         * We going to create an interleaved array for all subobject of this obj
         * file It's an array with all properties as Position, Normal, Textures
         * coordinates, Color ...
         */
        for (int i = 0, j = 0; i < nbFaces; i++) 
        {
            /**
             * We deal with quad if it is one
             */
            if (nbQuad[i] > 0) {
                bufOrderedQuads[i].rewind();
                vertex[i].rewind();
                normal[i].rewind();
                this.nbFace[j] = bufOrderedQuads[i].capacity();

                int maxvn = vertex[i].capacity() * 6 / 3;//8/3

                FloatBuffer vertexNormalInterleaved = Buffers.newDirectFloatBuffer(maxvn);
                FloatBuffer vertexTexCoord = Buffers.newDirectFloatBuffer(maxvn / 3);
                for (int a = 0; a < vertex[i].capacity(); a += 3) {
                    // We start by put the normal in the Interleaved array 
                    if (normal[i].capacity() > a) {
                        vertexNormalInterleaved.put(normal[i].get(a));
                        vertexNormalInterleaved.put(normal[i].get(a + 1));
                        vertexNormalInterleaved.put(normal[i].get(a + 2));
                    } else {
                        vertexNormalInterleaved.put(0);
                        vertexNormalInterleaved.put(0);
                        vertexNormalInterleaved.put(0);
                    }

                    // Next, we put the position in the Interleaved array 
                    if (vertex[i].capacity() > a) {
                        vertexNormalInterleaved.put(vertex[i].get(a));
                        vertexNormalInterleaved.put(vertex[i].get(a + 1));
                        vertexNormalInterleaved.put(vertex[i].get(a + 2));
                    } else {
                        vertexNormalInterleaved.put(0);
                        vertexNormalInterleaved.put(0);
                        vertexNormalInterleaved.put(0);
                    }

                    int aa = a * 2 / 3;
                    // Finally, we put the texture coordinates
                    if (texture[i].capacity() > aa) {
                        
                        vertexTexCoord.put(texture[i].get(aa));
                        vertexTexCoord.put(texture[i].get(aa + 1));
                    } else {
                        vertexTexCoord.put(0);
                        vertexTexCoord.put(0);
                    }

                    // interleavedArray = [N1 N1 N1 P1 P1 P1 T1 T1 ... Na Na Na Pa Pa Pa Ta Ta  0 0 0 0 0 0 0 0 0 ... 0]
                    // Nn : Normal n, Pn : Position n, Tn : Texture coordinates n
                }
                
                vertexNormalInterleaved.rewind();
                vertexTexCoord.rewind();

                // We going to position with pointer for new memory space in graphic card  (bridge to graphic memory)
                VertexPositionNormal[j] = BufferObject.createVBO(gl, buf.get(), BufferUtil.SIZEOF_FLOAT);
                // Put the interleaved Array with the previous pointer
                VertexPositionNormal[j].bufferData(vertexNormalInterleaved);
                GLException.getOpenGLError(gl);

                TextureCoord[j] = BufferObject.createVBO(gl, buf.get(), Buffers.SIZEOF_FLOAT);
                vertexTexCoord.rewind();
                TextureCoord[j].bufferData(vertexTexCoord);
                GLException.getOpenGLError(gl);

                // * 2/3 pour ne prendre en compte que les Vertex et les Normales
                IntBuffer indexInterleaved = Buffers.newDirectIntBuffer(bufOrderedQuads[i].capacity() / 3);
                for (int a = 0, max = bufOrderedQuads[i].capacity() / 3; a < max; a += 4) {
                    //Bon du début à la fin !
                    indexInterleaved.put(bufOrderedQuads[i].get(a));
                    indexInterleaved.put(bufOrderedQuads[i].get(a + 1));
                    indexInterleaved.put(bufOrderedQuads[i].get(a + 2));
                    indexInterleaved.put(bufOrderedQuads[i].get(a + 3));
                }
                indexInterleaved.rewind();
                /*
                logger.debug("Coordonnees de textures :");
                for (int k = 0; k < vertexTexCoord.capacity(); k += 2)
                	System.out.println(vertexTexCoord.get(k) + ", " + vertexTexCoord.get(k + 1));
                
                logger.debug("Index de textures :");
                for (int k = 0; k < indexInterleaved.capacity(); k += 4)
                	System.out.println(indexInterleaved.get(k) + ", " + indexInterleaved.get(k + 1) + ", " + indexInterleaved.get(k + 2) + ", " + indexInterleaved.get(k + 3));
*/
                IBOs[j] = BufferObject.createIBO(gl,
                        IGL.GL_QUADS,
                        this.nbFace[j],
                        buf.get(),
                        BufferUtil.SIZEOF_INT);
                IBOs[j].bufferData(indexInterleaved);
                IBOs[j].bindBuffer();
                GLException.getOpenGLError(gl);
                
                /*gl.getGLx().glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, buf.get());
                 // NumberOfQuad * 4 Vertex per quad * Size of Integer in byte
                 gl.getGLx().glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, indexInterleaved.capacity() * BufferUtil.SIZEOF_INT, indexInterleaved, GL2ES2.GL_STREAM_DRAW);
                 gl.getGLx().glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, 0);*/
                j++;
            }

            /**
             * We deal with triangle if it is one (si c'en est un)
             */
            if (nbTriangle[i] > 0) {
                bufOrderedTriangles[i].rewind();
                vertex[i].rewind();
                normal[i].rewind();
                texture[i].rewind();
                this.nbFace[j] = bufOrderedTriangles[i].capacity();


                int maxvn = vertex[i].capacity() * 6 / 3;//8/3
                FloatBuffer vertexNormalInterleaved = Buffers.newDirectFloatBuffer(maxvn);
                FloatBuffer vertexTexCoord = Buffers.newDirectFloatBuffer(maxvn / 3);
                for (int a = 0; a < vertex[i].capacity(); a += 3) {
                    if (normal[i].capacity() > a) {
                        vertexNormalInterleaved.put(normal[i].get(a));
                        vertexNormalInterleaved.put(normal[i].get(a + 1));
                        vertexNormalInterleaved.put(normal[i].get(a + 2));
                    } else {
                        vertexNormalInterleaved.put(0);
                        vertexNormalInterleaved.put(0);
                        vertexNormalInterleaved.put(0);
                    }
                    if (vertex[i].capacity() > a) {
                        vertexNormalInterleaved.put(vertex[i].get(a));
                        vertexNormalInterleaved.put(vertex[i].get(a + 1));
                        vertexNormalInterleaved.put(vertex[i].get(a + 2));
                    } else {
                        vertexNormalInterleaved.put(0f);
                        vertexNormalInterleaved.put(0f);
                        vertexNormalInterleaved.put(0f);
                    }
                    int aa = a * 2 / 3;
                    if (texture[i].capacity() > aa) {
                        
                        vertexTexCoord.put(texture[i].get(aa));
                        vertexTexCoord.put(texture[i].get(aa + 1));
                    } else {
                        vertexTexCoord.put(0f);
                        vertexTexCoord.put(0f);
                    }
                }
                vertexNormalInterleaved.rewind();
                vertexTexCoord.rewind();

                VertexPositionNormal[j] = BufferObject.createVBO(gl, buf.get(), BufferUtil.SIZEOF_FLOAT);
                VertexPositionNormal[j].bufferData(vertexNormalInterleaved);
                GLException.getOpenGLError(gl);
                
                TextureCoord[j] = BufferObject.createVBO(gl, buf.get(), Buffers.SIZEOF_FLOAT);
                TextureCoord[j].bufferData(vertexTexCoord);
                GLException.getOpenGLError(gl);

                IntBuffer indexInterleaved = Buffers.newDirectIntBuffer(bufOrderedTriangles[i].capacity() / 3);
                for (int a = 0, max = bufOrderedTriangles[i].capacity() / 3; a < max; a += 3) {
                    indexInterleaved.put(bufOrderedTriangles[i].get(a));
                    indexInterleaved.put(bufOrderedTriangles[i].get(a + 1));
                    indexInterleaved.put(bufOrderedTriangles[i].get(a + 2));
                }

                indexInterleaved.rewind();

                IBOs[j] = BufferObject.createIBO(gl,
                        IGL.GL_TRIANGLES,
                        this.nbFace[j],
                        buf.get(),
                        BufferUtil.SIZEOF_INT);
                IBOs[j].bufferData(indexInterleaved);
                GLException.getOpenGLError(gl);
                /*gl.getGLx().glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, buf.get());
                 // NumberOfQuad * 3 Vertex per triangle * Size of Integer in byte
                 gl.getGLx().glBufferData(GL.GL_ELEMENT_ARRAY_BUFFER, indexInterleaved.capacity() * BufferUtil.SIZEOF_INT, indexInterleaved, GL2ES2.GL_STREAM_DRAW);
                 gl.getGLx().glBindBuffer(GL.GL_ELEMENT_ARRAY_BUFFER, 0);*/
                j++;
            }
        }
        //GLException.getOpenGLError(gl);
        buf.rewind();
        
    	resultObjMtlFileIndexed = null;
	}

    public void draw(GraphicLibrary gl, int[] indexShader) 
    {
        //gl.getGLx().glDisable(GL.GL_TEXTURE_2D);
        boolean transparance = false;
        int indexNormal = indexShader[1], indexPosition = indexShader[0], indexTex = indexShader[2];
        //logger.debug("indexVert:" + indexPosition + ", indexNorm:'" + indexNormal + ", indexTex:" + indexTex);
        //TextureCoord[0].bindBuffer();
        //Util.getInstance().displayElementsOfVBO(gl, IGL.GL_ARRAY_BUFFER, 28, 2);
        //IBOs[0].bindBuffer();
        //Util.getInstance().displayElementsOfVBO(gl, IGL.GL_ELEMENT_ARRAY_BUFFER, 36, 3);
        for (int i = 0, j = 0; i < nbQuad.length; i++) {
        	try {
            if (fileMtlExist && material.get(materialGroup[i]) != null) {
                transparance = material.get(materialGroup[i]).isTransparent();
                if (transparance) {
                    gl.getGLx().glEnable(GL.GL_BLEND);
                    gl.getGLx().glBlendFunc(GL.GL_ONE, GL.GL_ONE_MINUS_SRC_ALPHA);
                }
                gl.getGLx().glEnable(GLLightingFunc.GL_COLOR_MATERIAL);
                material.get(materialGroup[i]).enableMaterial(gl, indexTex);
                
            }

            if (nbQuad[i] > 0) {
                // TODO Treatment of textures

                //gl.getGLx().glEnableClientState(GLPointerFunc.GL_VERTEX_ARRAY);
                //gl.getGLx().glEnableClientState(GLPointerFunc.GL_NORMAL_ARRAY);

                //gl.getGLx().glBindBuffer(GL.GL_ARRAY_BUFFER, indexVertexNormal);

                VertexPositionNormal[j].bindBuffer();
                	if (indexNormal != -1)
                	{
		                gl.getGLx().glEnableVertexAttribArray(indexNormal);
		                gl.getGLx().glVertexAttribPointer(indexNormal, 3, GL.GL_FLOAT, false, strideVertexNormal, 0);
                	}
                	if (indexPosition != -1)
                    {
    	                gl.getGLx().glEnableVertexAttribArray(indexPosition);
    	                gl.getGLx().glVertexAttribPointer(indexPosition, 3, GL.GL_FLOAT, false, strideVertexNormal, 3 * BufferUtil.SIZEOF_FLOAT);
                    }
                    else
                    {
                    	logger.fatal("indexPosition = -1");
                    	break;
                    }
                	if (indexTex != -1)
	                {
		                gl.getGLx().glEnableVertexAttribArray(indexTex);
		                TextureCoord[j].bindBuffer();
		                gl.getGLx().glVertexAttribPointer(indexTex, 2, GL.GL_FLOAT, false, 0, 0);
	                }
	                
	                //Util.getInstance().displayElementsOfVBO(gl, IGL.GL_ARRAY_BUFFER, 16, 8);
                
                    GLException.getOpenGLError(gl);

                    IBOs[j].draw();

                    GLException.getOpenGLError(gl);
                
                gl.getGLx().glDisableVertexAttribArray(indexNormal);
                gl.getGLx().glDisableVertexAttribArray(indexPosition);
                j++;
            }

            if (nbTriangle[i] > 0) {
                // TODO Treatment of textures
                VertexPositionNormal[j].bindBuffer();
                if (indexNormal != -1)
                {
	                gl.getGLx().glEnableVertexAttribArray(indexNormal);
	                gl.getGLx().glVertexAttribPointer(indexNormal, 3, GL.GL_FLOAT, false, strideVertexNormal, 0);
                }
                if (indexPosition != -1)
                {
	                gl.getGLx().glEnableVertexAttribArray(indexPosition);
	                gl.getGLx().glVertexAttribPointer(indexPosition, 3, GL.GL_FLOAT, false, strideVertexNormal, 3 * BufferUtil.SIZEOF_FLOAT);
                }
                else
                {
                	logger.fatal("indexPosition = -1");
                	break;
                }
                if (indexTex != -1)
                {
	                gl.getGLx().glEnableVertexAttribArray(indexTex);
	                TextureCoord[j].bindBuffer();
	                gl.getGLx().glVertexAttribPointer(indexTex, 2, GL.GL_FLOAT, false, 0, 0);
                }
                
                GLException.getOpenGLError(gl);
                //Util.getInstance().displayElementsOfVBO(gl, IGL.GL_ARRAY_BUFFER, 16, 8);

                IBOs[j].draw();GLException.getOpenGLError(gl);

                GLException.getOpenGLError(gl);
                
                gl.getGLx().glDisableVertexAttribArray(indexNormal);GLException.getOpenGLError(gl);
                gl.getGLx().glDisableVertexAttribArray(indexPosition);GLException.getOpenGLError(gl);
                j++;
            }

            if (fileMtlExist && material.get(materialGroup[i]) != null) {
                if (transparance) {
                    gl.getGLx().glDisable(GL.GL_BLEND);
                }
                //gl.getGLx().glDisable(GLLightingFunc.GL_COLOR_MATERIAL);
                //material.get(materialGroup[i]).disableMaterial(gl, indexTex);
            }
            } catch (GLException e) {
            	e.printStackTrace();
                logger.error("Erreur lors de l'affichage de l'objet");
                System.exit(0);
            }
        }

        //System.exit(0);
        gl.getGLx().glUseProgram(0);
        gl.getGLx().glBindBuffer(GL.GL_ARRAY_BUFFER, 0);
        //TestErrors.getOpenGLError(gl);

    }

}
