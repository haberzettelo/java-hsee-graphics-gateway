package java_3d_engine.models.buffers;

import java.nio.Buffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;

/**
 * Represent an VertexBufferObject. It is a fast method to render you ObjectModel on screen.
 * VBO use graphic card memory for most speed.
 * @author Olivier
 *
 */
public class VertexBufferObject extends BufferObject
{	
	/**
	 * Create an VertexBufferObject
	 * @param gl OpenGL context
	 * @param index Index of VBO
	 * @param lengthOfVar This value must be BufferUtil.SIZEOF_INT by example
	 */
	VertexBufferObject (GraphicLibrary gl, int index, int lengthOfVar)
	{
		this.gl = gl;
		_type = IGL.GL_ARRAY_BUFFER;
		_address = index;
		_lengthOfVar = lengthOfVar;
	}
	
	/**
	 * Update value of IBO
	 */
	public void bufferData(Buffer data) 
	{
		gl.getGLx().glBindBuffer(_type, _address);
		gl.getGLx().glBufferData(	_type, 
									data.capacity() * _lengthOfVar, 
									data, 
									IGL.GL_STREAM_DRAW);
	}

	/**
	 * Bind this IBO
	 */
	public void bindBuffer() 
	{
		gl.getGLx().glBindBuffer(_type, _address);
	}
	
	/**
	 * Display main information about this IBO
	 */
	public String toString()
	{
		return "Instance of VertexBufferObject : adress=" + _address + ", type=" + _type;
	}
}
