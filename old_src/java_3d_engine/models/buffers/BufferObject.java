package java_3d_engine.models.buffers;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IBufferObject;

public abstract class BufferObject implements IBufferObject
{
	protected GraphicLibrary gl;
	protected int _type;
	protected int _address;
	protected int _lengthOfVar;
	
	
	public static IndexBufferObject createIBO(GraphicLibrary gl, int typeOfFace, int nbFaces, int index, int lengthOfVar)
	{
		return (new IndexBufferObject(gl, typeOfFace, nbFaces, index, lengthOfVar));
	}
	
	public static VertexBufferObject createVBO(GraphicLibrary gl, int index, int lengthOfVar)
	{
		return (new VertexBufferObject(gl, index, lengthOfVar));
	}

	public void unbindBuffer() {
		gl.getGLx().glBindBuffer(_type, 0);
	}
	
	public String toString ()
	{
		return "Instance of BufferObject : adress=" + _address + ", type=" + _type;
	}
}
