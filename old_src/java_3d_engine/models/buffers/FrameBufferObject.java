package java_3d_engine.models.buffers;

import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;
import java_3d_engine.shader.Shader;

import org.apache.log4j.Logger;

public class FrameBufferObject extends BufferObject
{
	private final static Logger log = Logger.getLogger(FrameBufferObject.class);
	Shader shaderToRenderTexture;
	IntBuffer addr = IntBuffer.allocate(1);
	IntBuffer addrTexture = IntBuffer.allocate(1);
	IntBuffer addrRenderBuffer = IntBuffer.allocate(1);
	/**
	 * [0]:offsetX, [1]:offsetY, [2]:width, [3]:height
	 */
	int[] viewportArea;
	
	public FrameBufferObject (GraphicLibrary gl, int width, int height)
	{
		this.gl = gl;
		gl.getGLx().glGenFramebuffers(1, addr);
		_address = addr.get(0);
		gl.getGLx().glGenTextures(1, addrTexture);
		gl.getGLx().glGenRenderbuffers(1, addrRenderBuffer);
		
		gl.getGLx().glBindFramebuffer(IGL.GL_FRAMEBUFFER, _address);
		
		viewportArea = new int[]{0,0,width,height};
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D, addrTexture.get(0));
		gl.getGLx().glTexParameterf(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_MIN_FILTER, IGL.GL_LINEAR);
		//gl.getGLx().glTexImage2D(IGL.GL_TEXTURE_2D, 0, IGL.GL_RGBA8, viewportArea[2], viewportArea[3], 0, IGL.GL_RGBA, IGL.GL_INT, null);
		gl.getGLx().glTexImage2D(IGL.GL_TEXTURE_2D, 0, IGL.GL_RGBA, viewportArea[2], viewportArea[3], 0, IGL.GL_RGB, IGL.GL_UNSIGNED_INT, null);
		
		gl.getGLx().glFramebufferTexture2D(IGL.GL_FRAMEBUFFER, IGL.GL_COLOR_ATTACHMENT0, IGL.GL_TEXTURE_2D, addrTexture.get(0), 0);
		
		// create a render buffer as our depth buffer and attach it
		gl.getGLx().glBindRenderbuffer(IGL.GL_RENDERBUFFER, addrRenderBuffer.get(0));
		gl.getGLx().glRenderbufferStorage(IGL.GL_RENDERBUFFER,
				IGL.GL_DEPTH_COMPONENT24,viewportArea[2], viewportArea[3]);
		gl.getGLx().glFramebufferRenderbuffer(IGL.GL_FRAMEBUFFER,
				IGL.GL_DEPTH_ATTACHMENT,
				IGL.GL_RENDERBUFFER, addrRenderBuffer.get(0));

		// Go back to regular frame buffer rendering
		gl.getGLx().glBindFramebuffer(IGL.GL_FRAMEBUFFER, 0);
		
		
		if (gl.getGLx().glCheckFramebufferStatus(IGL.GL_FRAMEBUFFER) != IGL.GL_FRAMEBUFFER_COMPLETE)
		{
			log.fatal("Erreur lors de la validation du statut d'un FBO.");
			System.exit(2);
		}
		else
			log.debug("FBO validé.");
		
		unbindBuffer();
	}
	public int getAddress ()
	{
		return _address;
	}
	
	public void bindBuffer(int x, int y) 
	{
		//gl.getGLx().glViewport(x, y, viewportArea[2], viewportArea[3]);
		gl.glBindTexture(IGL.GL_TEXTURE_2D, 0);
		gl.getGLx().glBindFramebuffer(IGL.GL_FRAMEBUFFER, _address);
		gl.getGLx().glClearColor (1.0f, 0.0f, 0.0f, 0.5f);
		gl.getGLx().glClear (IGL.GL_COLOR_BUFFER_BIT | IGL.GL_DEPTH_BUFFER_BIT);
	}
	public void bindBuffer() 
	{
		gl.getGLx().glViewport(viewportArea[0], viewportArea[1], viewportArea[2], viewportArea[3]);
		gl.glBindTexture(IGL.GL_TEXTURE_2D, 0);
		gl.getGLx().glBindFramebuffer(IGL.GL_FRAMEBUFFER, _address);
		gl.getGLx().glClearColor (1.0f, 0.0f, 0.0f, 0f);
		gl.getGLx().glClear (IGL.GL_COLOR_BUFFER_BIT | IGL.GL_DEPTH_BUFFER_BIT);
	}
	
	public void unbindBuffer ()
	{
		gl.getGLx().glEnable(IGL.GL_TEXTURE_2D);
		gl.getGLx().glBindFramebuffer(IGL.GL_FRAMEBUFFER, 0);
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D, addrTexture.get(0));
	}
	
	public void undindTexture (int width, int height)
	{
		gl.getGLx().glViewport(0, 0, width, height);
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D, 0);
		gl.getGLx().glBindRenderbuffer(IGL.GL_RENDERBUFFER, 0);
	}
}
