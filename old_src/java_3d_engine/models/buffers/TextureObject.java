package java_3d_engine.models.buffers;

import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.sun.opengl.util.BufferUtil;

public class TextureObject extends BufferObject
{
	private static final Logger logger = Logger.getLogger(TextureObject.class);
	private IntBuffer _texture;
	private String _path;
	private boolean init = false;
	
	private BufferedImage img;
	private	int type;
	private ByteBuffer buffer;
	
	public TextureObject (String Path, boolean mipmap)
	{
		_path = Path;
		generateTexture(mipmap);
	}
	
	public void initTexture (GraphicLibrary gl)
	{
		if (!init)
		{
			this.gl = gl;
			putTextureInGraphicCard();
			init = true;
		}
	}
	
	public void draw()
	{
		enableTexture();
		bindBuffer();
	}
	
	public void stopDraw ()
	{
		disableTexture();
		unbindBuffer();
	}
	
	public void bindBuffer() 
	{
		//logger.debug("Texture " + path + " enable.");
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D, _address);
	}
	
	public void unbindBuffer ()
	{
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D, 0);
	}

	/*// Ne fonctionne pas et je sais pas pourquoi
	public void finalize () throws Throwable
	{
		System.out.println("A texture has been deleted.");
		gl.getGLx().glDeleteTextures(1, texture);
		super.finalize();
	}*/
	
	public void enableTexture ()
	{
		gl.getGLx().glEnable(IGL.GL_TEXTURE_2D);
		
	}
	
	public void disableTexture ()
	{
		gl.getGLx().glDisable(IGL.GL_TEXTURE_2D);
	}
	
	public int getAddress ()
	{
		return _address;
	}
	
	
	
	/*
	 * ------------- Privates Methods ---------------------
	 */
	
	/**
	* Generate a texture and change address for the new adresse of texture generated
	*/
	private void generateTexture(boolean mipmap)
	{
		// Read image (the encryption and decryption are managed by ImageIO class provide by javax)
		img = readImage(_path);
   		type = img.getType();
  		switch (type)
  		{
  			case BufferedImage.TYPE_INT_RGB : type = IGL.GL_RGB; 		break;
  			case BufferedImage.TYPE_INT_BGR : type = IGL.GL_BGR; 		break;
  			case BufferedImage.TYPE_INT_ARGB : type = IGL.GL_ABGR_EXT; 	break;
  			case BufferedImage.TYPE_3BYTE_BGR : type = IGL.GL_BGR; 		break;
  			case BufferedImage.TYPE_4BYTE_ABGR : type = IGL.GL_ABGR_EXT; break;
  			case BufferedImage.TYPE_4BYTE_ABGR_PRE : type = IGL.GL_ABGR_EXT; break;
  			case BufferedImage.TYPE_BYTE_INDEXED : type = IGL.GL_COLOR_INDEX; break;
  			default : type = IGL.GL_RGB;
  		}
        buffer = makeRGBTexture(img);
        logger.debug(buffer + ", w:" + img.getWidth() + ", h:" + img.getHeight() + ", w*h*size:" + img.getWidth() * img.getHeight() * 3);
	}
	
	private void putTextureInGraphicCard()
	{
		/**
		 * New version
		 */
        _texture = genTexture(gl, img.getWidth(), img.getHeight(), buffer, type);
		_address = _texture.get(0);
		buffer = null;
		img = null;
	}
	
	private static IntBuffer genTexture (GraphicLibrary gl, int width, int height, ByteBuffer textureData, int type)
	{
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D,0);
		
		IntBuffer texture = IntBuffer.allocate(1);
		gl.getGLx().glGenTextures(1, texture);
		
		gl.getGLx().glBindTexture(IGL.GL_TEXTURE_2D, texture.get(0)); // Bind the ID texture specified by the 2nd parameter

		// The next commands sets the texture parameters
		gl.getGLx().glTexParameterf(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_WRAP_S, IGL.GL_REPEAT);
		gl.getGLx().glTexParameterf(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_WRAP_T, IGL.GL_REPEAT);
		gl.getGLx().glTexParameterf(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_MAG_FILTER, IGL.GL_LINEAR);
		gl.getGLx().glTexParameterf(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_MIN_FILTER, IGL.GL_LINEAR_MIPMAP_NEAREST);
		
		// infoheader : taille de la texture, l_texture : tableau de donnée
		
		// To mix texture color with based color
		gl.getGLx().glTexEnvf(IGL.GL_TEXTURE_ENV, IGL.GL_TEXTURE_ENV_MODE, IGL.GL_MODULATE);
		
		//gl.getGLx().glTexGeni(IGL.GL_S, IGL.GL_TEXTURE_GEN_MODE, IGL.GL_OBJECT_LINEAR);
	    gl.getGLx().glTexGenfv(IGL.GL_S, IGL.GL_OBJECT_PLANE, new float[] {1,1,1,0}, 0);	
		
		textureData.rewind();
		
		gl.getGLx().glTexImage2D(IGL.GL_TEXTURE_2D, 0, IGL.GL_RGBA, width, height, 0, type, IGL.GL_UNSIGNED_BYTE, textureData);
		//glu.gluBuild2DMipmaps(IGL.GL_TEXTURE_2D, 3, width, height, type, IGL.GL_UNSIGNED_BYTE, textureData);
		
		//glu.gluBuild2DMipmaps(IGL.GL_TEXTURE_2D, 4, width, height, IGL.GL_RGBA, IGL.GL_UNSIGNED_BYTE, texture);
		
		
		gl.getGLx().glGenerateMipmap(IGL.GL_TEXTURE_2D);
		
		gl.getGLx().glTexParameteri(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_BASE_LEVEL, 0);
		gl.getGLx().glTexParameteri(IGL.GL_TEXTURE_2D, IGL.GL_TEXTURE_MAX_LEVEL, 4);
		
		logger.debug("texture was be sent in graphic card");
		
		return texture;
	}

    private static BufferedImage readImage(String filename)
    {
	    try
	    {
	    	logger.debug("Read texture : " + filename);
		    BufferedImage img = ImageIO.read(new File(filename));
		    /*
		    Mirroring the Image
		    */
		    java.awt.geom.AffineTransform tx = java.awt.geom.AffineTransform.getScaleInstance(1, -1);
		    tx.translate(0, -img.getHeight(null));
		    AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		    img = op.filter(img, null);//*/
		    return img;
	    }
	    catch (IOException e)
	    {
	    	throw new RuntimeException(e);
	    }
    }

    private static ByteBuffer makeRGBTexture(BufferedImage img)
    {
	    ByteBuffer dest = null;
	    
	    switch (img.getType())
	    {
		    case BufferedImage.TYPE_3BYTE_BGR:
		    case BufferedImage.TYPE_CUSTOM:
		    {
		    	/*BufferedImage img1 = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		    	for (int x = 0, maxX = img.getWidth(); x < maxX; x++)
		    		for (int y = 0, maxY = img.getHeight(); y < maxY; y++)
		    			img1.setRGB(x, y, img.getRGB(x, y));*/
			    //byte[] data = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
		    	int[] pixels = new int[img.getWidth() * img.getHeight()];
		    	img.getRGB(0, 0, img.getWidth(), img.getHeight(), pixels, 0, img.getWidth());
			    
			   
			    ByteBuffer buffer = BufferUtil.newByteBuffer(img.getWidth() * img.getHeight() * 3); //4 for RGBA, 3 for RGB
		        
		        for(int y = 0; y < img.getHeight(); y++){
		            for(int x = 0; x < img.getWidth(); x++){
		                int pixel = pixels[y * img.getWidth() + x];
		                //logger.debug(pixel);
		                buffer.put((byte) (pixel & 0xFF));               // Blue component
		                buffer.put((byte) ((pixel >> 8) & 0xFF));      // Green component
		                buffer.put((byte) ((pixel >> 16) & 0xFF));     // Red component
		                //buffer.put((byte) ((pixel >> 24) & 0xFF));    // Alpha component. Only for RGBA
		            }
		        }

		        //buffer.flip(); //FOR THE LOVE OF GOD DO NOT FORGET THIS
		        dest = buffer;
			/*
			    dest = ByteBuffer.allocateDirect(pixels.length * BufferUtil.SIZEOF_INT); // If multiplied by 2 shows a dark cube
			    
			    dest.order(ByteOrder.nativeOrder());
			    dest.put(pixels, 0, pixels.length);*/
			    logger.debug("Custom profile color");
			    break;
		    }
		    case BufferedImage.TYPE_INT_RGB:
		    {
			    int[] data = ((DataBufferInt) img.getRaster().getDataBuffer()).getData();
			    dest = ByteBuffer.allocateDirect(data.length * BufferUtil.SIZEOF_INT);
			    dest.order(ByteOrder.nativeOrder());
			    dest.asIntBuffer().put(data, 0, data.length);
			    break;
		    }
		    default:
		    	byte[] data = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
				
			    dest = ByteBuffer.allocateDirect(data.length * BufferUtil.SIZEOF_INT); // If multiplied by 2 shows a dark cube
			    
			    dest.order(ByteOrder.nativeOrder());
			    dest.put(data, 0, data.length);
			    break;
		    	//throw new RuntimeException("Unsupported image type " + img.getType());
	    }
	    return dest;
    }
}

/*
   		int type = img.getType();
  		switch (type)
  		{
  			case BufferedImage.TYPE_INT_RGB : type = GL.GL_RGB; 		break;
  			case BufferedImage.TYPE_INT_BGR : type = GL2GL3.GL_BGR; 		break;
  			case BufferedImage.TYPE_INT_ARGB : type = GL2.GL_ABGR_EXT; 	break;
  			case BufferedImage.TYPE_3BYTE_BGR : type = GL2GL3.GL_BGR; 		break;
  			case BufferedImage.TYPE_4BYTE_ABGR : type = GL2.GL_ABGR_EXT; break;
  			default : type = GL.GL_RGB;
  		}
  		
	    //Generates the texture
	    if(mipmap)
	    {
	        glu.gluBuild2DMipmaps(typeOfTexture, //target : usually GL_TEXTURE_2D
	                3,                              //internal format : RGB so 3 components
	                img.getWidth(),                //image size
	                img.getHeight(),
	                type,                      //fomat : RGB
	                GL.GL_UNSIGNED_BYTE,            //data type : pixels are made of byte
	                buffer);       //picture datas
	    }
	    else
	    {
	        //set the properties of the texture : size, color type...
	        gl.getGLx().glTexImage2D(typeOfTexture,       //target : usually GL_TEXTURE_2D
	                0,                              //level : usually left to zero
	                3,                              //internal format : RGB so 3 components
	                img.getWidth(),                //image size
	                img.getHeight(),
	                0,                              //0 : no border
	                type,                      //format : usually RGB
	                GL.GL_UNSIGNED_BYTE,  //data type : pixels are made of byte
	                buffer.rewind());       //picture datas
	    }
	    */
