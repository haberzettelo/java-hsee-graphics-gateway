package java_3d_engine.models.buffers;

import java.nio.IntBuffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;

public class RenderBufferObject extends BufferObject
{
	/**
	 * 
	 * @param gl
	 * @param width
	 * @param height
	 */
	public RenderBufferObject (GraphicLibrary gl, int width, int height)
	{
		this.gl = gl;
		
		IntBuffer addressBuffer = IntBuffer.allocate(1);
		gl.getGLx().glGenRenderbuffers(1, addressBuffer);
		_address = addressBuffer.get(0);
		
		bindBuffer();
		gl.getGLx().glRenderbufferStorage(IGL.GL_RENDERBUFFER, IGL.GL_DEPTH_COMPONENT32, IGL.GL_TEXTURE_WIDTH, IGL.GL_TEXTURE_HEIGHT);
		unbindBuffer();
	}
	
	public int getAddress()
	{
		return _address;
	}
	
	public void bindBuffer() 
	{
		gl.getGLx().glBindRenderbuffer(IGL.GL_RENDERBUFFER, _address);
	}

	public void unbindBuffer ()
	{
		gl.getGLx().glBindRenderbuffer(IGL.GL_RENDERBUFFER, 0);
	}
}
