package java_3d_engine.models.buffers;

import java.nio.Buffer;

import java_3d_engine.interfaces.GraphicLibrary;
import java_3d_engine.interfaces.IGL;

/**
 * Represent single array of IndexBufferObject
 * @author Olivier
 *
 */
public class IndexBufferObject extends BufferObject
{
	int typeOfFace, nbFaces;
	
	/**
	 * Create an IndexBufferObject
	 * @param gl OpenGL context
	 * @param typeOfFace Often GL_TRIANGLES or GL_TRIANGLES_STRIP (not GL_QUADS if opengl >= 3)
	 * @param nbElements Number of element to be performed
	 * @param index Index of IBO
	 * @param lengthOfVar This value must be BufferUtil.SIZEOF_INT by exemple
	 */
	IndexBufferObject (GraphicLibrary gl, int typeOfFace, int nbFaces, int index, int lengthOfVar)
	{
		this.gl = gl;
		_type = IGL.GL_ELEMENT_ARRAY_BUFFER;
		_address = index;
		_lengthOfVar = lengthOfVar;
		
		this.typeOfFace = typeOfFace;
		this.nbFaces = nbFaces / 3;
	}
	
	/**
	 * Draw the IBO with VBO of you had previously binded
	 */
	public void draw() 
	{
		gl.getGLx().glBindBuffer(_type, _address);
        gl.getGLx().glDrawElements(typeOfFace, nbFaces, IGL.GL_UNSIGNED_INT, 0);
	}

	/**
	 * Update value of IBO
	 */
	public void bufferData(Buffer data) 
	{
		gl.getGLx().glBindBuffer(_type, _address);
		gl.getGLx().glBufferData(	_type, 
									data.capacity() * _lengthOfVar, 
									data, 
									IGL.GL_STREAM_DRAW);
	}

	/**
	 * Bind this IBO
	 */
	public void bindBuffer() 
	{
		gl.getGLx().glBindBuffer(_type, _address);
	}
	
	/**
	 * Display main information about this IBO
	 */
	public String toString()
	{

		return "Instance of IndexBufferObject : adress=" + _address + ", type=" + _type + ", nbElements=" + nbFaces;
	}
	
}
